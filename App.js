/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React , {Component,useEffect}from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Image,
  ActivityIndicator,AppRegistry,
} from 'react-native';

import Without_Login from './src/Navigations/Without_Login.js'
import With_Login from './src/Navigations/With_Login.js';
import With_LoginBusiness from './src/Navigations/With_LoginBusiness.js';
import AsyncStorage from '@react-native-community/async-storage';
import Splash from './src/Screens/Splash/Splash.js'
import { APP_BLACK, APP_WHITE } from './src/Constants/App_Constants.js';
import SplashScreen from 'react-native-splash-screen'
export default class App extends React.Component{

constructor(props){
  super(props)
  global.type=''
  this.state = {
    type :'',
    isLoading: true,
    splash:true,
    userLogin:false,
    businessLogin:false
  }
}


componentDidMount(){
 
   AsyncStorage.getItem("session").then((value) => { 
    if(value!=null){ 
     console.log('user data',value)
     if(value=='User'){
      global.type='User'
      this.setState({userLogin:true,businessLogin:false})
     }else{
      global.type='Business'
      this.setState({businessLogin:true,userLogin:false})
     }
    }
  }).done();
    setTimeout(()=>{
      SplashScreen.hide();
    this.setState({splash:false})
    },1000)
}

forSplash(){
    if(this.state.userLogin==false && this.state.businessLogin==false){
      return(
        <Without_Login/>
      )
    }else if(this.state.userLogin){
      return(
        <With_Login/>
      )
    }else if(this.state.businessLogin){
      return(
         <With_LoginBusiness/>
      )
    }
    
  //}
}

  render(){
    return (
      <View style={{flex:1, backgroundColor:'#212121'}}>
       {this.forSplash()}
       {/* <With_Login/> */}
      </View>
    )
  }

}

// AppRegistry.registerComponent('App', () => App);
//AppRegistry.registerHeadlessTask('RNFirebaseBackgroundMessage', () => bgMessaging);
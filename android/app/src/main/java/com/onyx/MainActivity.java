package com.onyx;
import android.os.Bundle;
import com.facebook.react.ReactActivity;
// import com.google.android.gms.maps.MapFragment;
import org.devio.rn.splashscreen.SplashScreen;
public class MainActivity extends ReactActivity {

  /**
   * Returns the name of the main component registered from JavaScript. This is used to schedule
   * rendering of the component.
   */
  @Override
  protected String getMainComponentName() {
    return "Onyx";
  }
  @Override
    protected void onCreate(Bundle savedInstanceState) {
        SplashScreen.show(this);  // here 
        super.onCreate(savedInstanceState);
    }
  // @Override
  // protected void onCreate(Bundle savedInstanceState) {
  //   super.onCreate(savedInstanceState);
    // setContentView(R.layout.activity_main);
    // FacebookSdk.sdkInitialize(getApplicationContext());
  // } 

}

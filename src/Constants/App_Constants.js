import AsyncStorage from '@react-native-community/async-storage';
// import firebase from 'react-native-firebase';
import React from 'react';

import MapView,{Marker} from 'react-native-maps';


export const FOR_IMAGE_URL="https://apps.appsmaventech.com/onyx/"
///color/// 
//export const APP_WHITE = "#ffffff";
export const APP_WHITE="#f2f3f7"
export const APP_BLACK =  '#161616';
export const APP_FACEBOOK = '#22589A';
export const APP_GREY =  '#A0A0A0';
export const APP_BOTTOMTAB_GREY='#808080'
export const APP_BORDER = '#141414';
///Fonts///
export const FONT_BLACK ="Montserrat-Black";
export const FONT_BOLD ="Montserrat-Bold";
export const FONT_EXTRABOLD ="Montserrat-ExtraBold";
export const FONT_EXTRALIGHT ="Montserrat-ExtraLight";
export const FONT_LIGHT ="Montserrat-Light";
export const FONT_SEMIBOLD ="Montserrat-SemiBold";
export const FONT_THIN ="Montserrat-Thin";
export const FONT_ULTRALIGHT ="Montserrat-UltraLight";
export const FONT_REGULAR ="Montserrat-Regular";
export const FONT_MEDIUM ="Montserrat-Medium";
///icons///
export const LOGIN =require('../Assets/login.png');

export const HOME_ICON_INACTIVE =require('../Assets/inactive_home.png');
export const SEARCH_ICON_INACTIVE =require('../Assets/search.png');
export const PROFILE_ICON_INACTIVE =require('../Assets/account_inactive.png');
export const SETTINGS_ICON_INACTIVE =require('../Assets/setting_inactive.png');

export const SEARCH_ICON_ACTIVE =require('../Assets/search_active.png');
export const PROFILE_ICON_ACTIVE =require('../Assets/account_active.png');
export const SETTINGS_ICON_ACTIVE =require('../Assets/setting_active.png');
export const HOME_ICON_ACTIVE =require('../Assets/home_active.png');
export const SEARCH_FILTER = require("../Assets/filterBlack.png");
export const FILTER_BLACK = require("../Assets/filterBlack2.png");
// export const FAVOURITE_ACTIVE = require("../Assets/favorite.png");
// export const FAVOURITE_INACTIVE =  require("../Assets/favorite_active.png");
export const FAVOURITE_ACTIVE = require("../Assets/heart-active1.png");
export const FAVOURITE_INACTIVE = require("../Assets/heart-inctive.png");


export const BACK_ICON =require('../Assets/back_arrow.png');
export const FORWARD_ICON =require('../Assets/next_arrow.png');

export const NOTIFICATION_ICON =require('../Assets/notification.png');

export const REVIEW_INACTIVE =require('../Assets/review_inactive.png');
export const REVIEW_ACTIVE =require('../Assets/review.png');
 
export const FAVORITE_INACTIVE =require('../Assets/favorite_inactive.png');
export const FAVORITE_ACTIVE =require('../Assets/heart-active1.png');
export const FAVORITE_DETAIL =require('../Assets/detailfavorite.png');

export const SHARE =require('../Assets/share.png');

export const RATTING =require('../Assets/star(small)_active.png');
export const FILTER =require('../Assets/filter.png');
export const RESTAURANT =require('../Assets/restaurant.png');

export const ADD_PROFILE =require('../Assets/add_rating.png');
export const ADD_IMAGE =require('../Assets/add_profile.png');

export const CHECK_BOX_UNCHECK =require('../Assets/uncheck.png');
export const CHECK_BOX_CHECK =require('../Assets/check_box.png');

export const RADIO_BUTTON_OFF =require('../Assets/radio_button_off.png');
export const RADIO_BUTTON_ON =require('../Assets/radio_button_on.png');
export const Map_Marker = require('../Assets/marker3.png')

export const TOGGLE_BUTTON_OFF =require('../Assets/toggle_inactive.png');
export const TOGGLE_BUTTON_ON =require('../Assets/toggle_active.png');

export const USER_PLACEHOLDER =require('../Assets/user.png');

export const ADD_RATTING =require('../Assets/rating.png');
export const USER_ICON =require('../Assets/userIcon.png');
export const BUSINESS_ICON =require('../Assets/business.png');

export const ADD_DOCUMENT =require('../Assets/plus.png');
export const ADD_LOCATION =require('../Assets/add.png');
export const MANAGE_LOCATION_ACTIVE =require('../Assets/manage_location_active.png');
export const FEATURED_BUSSINESS_ACTIVE =require('../Assets/business_active.png');


export const RADIO_OFF =require('../Assets/radio_off.png');
export const RADIO_ON =require('../Assets/radio_on.png');

export const REMOVE_ICON =require('../Assets/remove.png');
export const DOWN_ARROW =require('../Assets/down-arrow.png');

export const SIGNUP_USER =require('../Assets/user_Register.png');
export const SIGNUP_BG =require('../Assets/signup.png');
export const SIGNUP_NEW =require('../Assets/signup2.png');
export const HEADER_ICON =require('../Assets/logo.png'); 

export const EDIT_ICON =require('../Assets/edit.png');
export const LOGIN_BACKGROUND =require('../Assets/login-backgound.png');
//login-backgound

//Strings
export const BUSINESS_NAME = 'Business Name';
export const ADDRESS = 'Address';
export const DISTANCE = 'Distance';
export const AVAILABLE_HOURS = 'Available Hours';
export const DIRECTIONS = 'Directions';
export const WEBSITE_LINK = 'Website Link';
export const Rating_HEADER = 'Rating';
export const ADD = 'Add';


// export const getTokenFromAsync = async () => {
//     try {
//         const value = await AsyncStorage.getItem('fcmToken');
//         if (value != null) {
//             return value;
//         }
//       } catch (error) {
//         return '';
//       }
//   }

//   export const fetchTokenFromFirebase = async () => {
//     let value = '';
//     try{
//       if(Platform.OS==='android'){
//         value = await firebase.messaging().getToken();
//       }else{
//         value = await firebase.messaging().ios.getAPNSToken();
//         //console.log(JSON.stringify(value))
//       }
//       if(value){
//         AsyncStorage.setItem('fcmToken', value);
//       }
//     }catch(error){
//       alert(error)
//       return '';
//     }
// }

// var notification;
// export const makeNotification=(title, body)=>{
//   const channel = new firebase.notifications.Android.Channel(
//     'channelId',
//     'Channel Name',
//     firebase.notifications.Android.Importance.High
//   ).setDescription('A natural description of the channel');
//   firebase.notifications().android.createChannel(channel);
  
//  if(Platform.OS==='android'){
//   notification = new firebase.notifications.Notification()
//   .setNotificationId('notificationId')
//   .setTitle(title)
//   .setBody(body)
//   .android.setSmallIcon('ic_stat_notification')
//   .android.setAutoCancel(true)
//   .android.setChannelId('channelId')
//   .android.setPriority(firebase.notifications.Android.Priority.High);
//   firebase.notifications().displayNotification(notification)
//   console.log(title+body)
//  }else{
//   notification = new firebase.notifications.Notification()
//   .setNotificationId('notificationId')
//     .setTitle(title)
//     .setBody(body);
//   firebase.notifications().displayNotification(notification)
//   console.log(title+body)
//  }
// }

////////Map/////////
export const map=(lat,long)=>{
  if(Platform.OS=='ios'){
    return(
      <MapView
          style={{flex:1}}
          mapType='hybrid'
          provider="google"
          showsMyLocationButton={true}
          showsUserLocation={true}
          initialRegion={{
            latitude: lat,
            longitude: long,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
          }}
          scrollEnabled={false}>
        <Marker coordinate={{latitude: lat,longitude: long}}/>
      </MapView>
    )
  }else{
    return(
      <MapView
          style={{flex:1}}
          mapType='hybrid'
          provider="google"
          showsMyLocationButton={true}
          showsUserLocation={true}
          initialRegion={{
            latitude: lat,
            longitude: long,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
          }}
          scrollEnabled={false}>
        <Marker coordinate={{latitude: lat,longitude: long}}/>
      </MapView>
    )
  }
} 
import { createAppContainer } from "react-navigation";
import { createStackNavigator } from 'react-navigation-stack';
import Bottomtab from '../Navigations/BottomTabnavigation/BottomTab.js'
import FeaturedBusiness from '../Screens/OtherScreens/Featured_Business.js'
import ChangePassword from '../Screens/OtherScreens/Settings/Change_Password.js'
import ContactUs from '../Screens/OtherScreens/Settings/Contact_Us.js'
import Register from '../Screens/Profile_Authentication/Register.js'
import Notifications from '../Screens/OtherScreens/Home/Notifications.js'
import EditAccount from '../Screens/OtherScreens/Account/Edit_Account.js'
import SingleDetail from '../Screens/CommonScreen/Single_Detail.js'
import AddRating from '../Screens/CommonScreen/Ratting.js'
import ForgotPassword from '../Screens/Profile_Authentication/Forgot_Password.js'
import BusinessRegister from '../Screens/Profile_Authentication/Business_Register.js'
import BusinessBottomtab from '../Navigations/BottomTabnavigation/Business_Bottomtab.js'
import AddLocation from '../Screens/OtherScreens/ManageLocation/Add_Location.js'
import EditBusinessPersonal from '../Screens/OtherScreens/BusinessHome/Edit_BusinessPersonal'
import Splash from "../../src/Screens/Splash/Splash.js";
import CategorySelect from "../Screens/Tabs/CategorySelect.js";
import Edit_BusinessProfile from '../Screens/OtherScreens/BusinessHome/Edit_BusinessProfile.js'
import Customer_Concerns from '../Screens/OtherScreens/Settings/Customer_Concerns.js'
import AppLogin from '../Screens/Profile_Authentication/AppLogin.js'
const AppNavigator = createStackNavigator({
BusinessBottomtab: {
    screen: BusinessBottomtab,
    navigationOptions:()=>({
        headerShown:false,    
    }),
},
Bottomtab: {
    screen: Bottomtab,
    navigationOptions:()=>({
        headerShown:false,    
    }),
},
AppLogin:{
    screen: AppLogin,
    navigationOptions:()=>({
        headerShown:false,    
    }),
},
ForgotPassword: {
  screen: ForgotPassword,
  navigationOptions:()=>({
    headerShown:false,    
}),
},
FeaturedBusiness: {
  screen: FeaturedBusiness,
  navigationOptions:()=>({
    headerShown:false,    
}),
},
ChangePassword: {
  screen: ChangePassword,
  navigationOptions:()=>({
    headerShown:false,    
}),
},
ContactUs: {
  screen: ContactUs,
  navigationOptions:()=>({
    headerShown:false,    
}),
},
Notifications: {
  screen: Notifications,
  navigationOptions:()=>({
    headerShown:false,    
}),
},
Register: {
  screen: Register,
  navigationOptions:()=>({
    headerShown:false,    
}),
},
EditAccount: {
  screen: EditAccount,
  navigationOptions:()=>({
    headerShown:false,    
}),
},
SingleDetail: {
  screen: SingleDetail,
  navigationOptions:()=>({
    headerShown:false,    
}),
},
AddRating: {
  screen: AddRating,
  navigationOptions:()=>({
    headerShown:false,    
}),
},
BusinessRegister: {
  screen: BusinessRegister,
  navigationOptions:()=>({
    headerShown:false,    
}),
},
AddLocation: {
  screen: AddLocation,
  navigationOptions:()=>({
    headerShown:false,    
}),
},
EditBusinessPersonal: {
  screen: EditBusinessPersonal,
  navigationOptions:()=>({
    headerShown:false,    
}),
},
Splash: {
  screen: Splash,
  navigationOptions:()=>({
    headerShown:false,    
}),
},
CategorySelect: {
  screen: CategorySelect,
  navigationOptions:()=>({
    headerShown:false,    
}),
},
Edit_BusinessProfile:{
  screen: Edit_BusinessProfile,
  navigationOptions:()=>({
    headerShown:false,    
}),
},
Customer_Concerns:{
  screen: Customer_Concerns,
  navigationOptions:()=>({
    headerShown:false,    
}),
},

},{
    headerMode:'none',
    mode: 'modal',
    defaultNavigationOptions: { gesturesEnabled: false },
}
);
  
  export default createAppContainer(AppNavigator);
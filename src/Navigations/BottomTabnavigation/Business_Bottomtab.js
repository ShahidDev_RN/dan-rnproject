import React , {Component} from 'react';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createAppContainer } from "react-navigation";
import Icon from './Icon.js'
import TabBarComponent from './TabBarComponent.js'
import BusinessHome from '../../Screens/Business_Tabs/BusinessHome.js'
import ManageLocation from '../../Screens/Business_Tabs/ManageLocation.js'
import AddFeaturedBusiness from '../../Screens/Business_Tabs/AddFeaturedBusiness.js'
import BusinessSetings from '../../Screens/Business_Tabs/BusinessSetings.js'
import {SETTINGS_ICON_ACTIVE,APP_GREY, APP_BLACK,HOME_ICON_ACTIVE, MANAGE_LOCATION_ACTIVE,FEATURED_BUSSINESS_ACTIVE} from '../../Constants/App_Constants.js'
const BusinessBottomTab = createBottomTabNavigator({
      Home:{screen:BusinessHome,
        navigationOptions: {
        header:null,
        tabBarIcon: ({ tintColor }) => <Icon name="Home" color={tintColor} icon={HOME_ICON_ACTIVE} />
      }},
      Locations:{screen:ManageLocation,
        navigationOptions: {
        header:null,
        tabBarIcon: ({ tintColor }) => <Icon name="Search" color={tintColor} icon={MANAGE_LOCATION_ACTIVE}/>
      }},
      Business:{screen:AddFeaturedBusiness,
        navigationOptions: {
        header:null,
        tabBarIcon: ({ tintColor }) => <Icon name="Account" color={tintColor} icon={FEATURED_BUSSINESS_ACTIVE}/>
      }},
      Setings:{screen:BusinessSetings,
        navigationOptions: {
        header:null,
        tabBarIcon: ({ tintColor }) => <Icon name="Settings" color={tintColor} icon={SETTINGS_ICON_ACTIVE}/>
      }},
},{
   tabBarComponent: TabBarComponent,
    initialRouteName: '',
    tabBarOptions: {
      activeTintColor: APP_BLACK,
      inactiveTintColor: APP_GREY,
    },
    lazy: true
});

export default createAppContainer(BusinessBottomTab);
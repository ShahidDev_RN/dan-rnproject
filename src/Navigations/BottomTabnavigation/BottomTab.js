import React , {Component} from 'react';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createAppContainer } from "react-navigation";
import Icon from './Icon.js'
import TabBarComponent from './TabBarComponent.js'
import Home from '../../Screens/Tabs/Home.js'
import Search from '../../Screens/Tabs/Search.js'
import Account from '../../Screens/Tabs/Account.js'
import Settings from '../../Screens/Tabs/Settings.js'
import CategorySelect from "../../Screens/Tabs/CategorySelect.js";
import ChangePassword from "../../Screens/OtherScreens/Settings/Change_Password"
import ContactUs from "../../Screens/OtherScreens/Settings/Contact_Us"
import Customer_Concerns from "../../Screens/OtherScreens/Settings/Customer_Concerns"
import EditAccount from "../../Screens/OtherScreens/Account/Edit_Account";
import Notifications from "../../Screens/OtherScreens/Home/Notifications.js";
import SingleDetail from "../../Screens/CommonScreen/Single_Detail";
import FeaturedBusiness from "../../Screens/OtherScreens/Featured_Business.js";
import { createStackNavigator } from 'react-navigation-stack';
import {HOME_ICON_INACTIVE,SEARCH_ICON_ACTIVE,PROFILE_ICON_ACTIVE,SETTINGS_ICON_ACTIVE,APP_GREY, 
  APP_BLACK,HOME_ICON_ACTIVE, APP_WHITE,PROFILE_ICON_INACTIVE,APP_BOTTOMTAB_GREY} from '../../Constants/App_Constants.js'

const CATEGORY_SEARCH= createStackNavigator({
  CategorySelect: { screen: CategorySelect,
    navigationOptions: {
      header:null,
      tabBarIcon: ({ tintColor }) => <Icon name="Profile" color={tintColor} />
    }},
    Search:{screen:Search, 
      navigationOptions:{
      header:null
    }},
   
})

const SETTINGS= createStackNavigator({
  Settings: { screen: Settings,
    navigationOptions: {
      header:null,
      tabBarIcon: ({ tintColor }) => <Icon name="Settings" color={tintColor} />
    }},
    ChangePassword: { screen: ChangePassword,
    navigationOptions: {
      header:null,
      //tabBarIcon: ({ tintColor }) => <Icon name="Profile" color={tintColor} />
    }},
    ContactUs:{screen:ContactUs, 
      navigationOptions:{
      header:null
    }},
    Customer_Concerns:{screen:Customer_Concerns, 
      navigationOptions:{
      header:null
    }},
   
})

const ACCOUNT= createStackNavigator({
  Account:{screen:Account,
    navigationOptions: {
    header:null,
    tabBarIcon: ({ tintColor }) => <Icon name="Account" color={tintColor} icon={PROFILE_ICON_ACTIVE}/>
  }
  
},
  EditAccount:{screen:EditAccount, 
    navigationOptions:{
    header:null
  }},

})

const HOME= createStackNavigator({
  Home:{screen:Home,
    navigationOptions: {
    header:null,
    tabBarIcon: ({ tintColor }) => <Icon name="Account" color={tintColor} icon={PROFILE_ICON_ACTIVE}/>
  }},
  Notifications:{screen:Notifications,
    navigationOptions: {
      header:null,
    }
  },
  SingleDetail:{screen:SingleDetail,
    navigationOptions: {
      header:null,
    }
  },
  FeaturedBusiness:{screen:FeaturedBusiness,
    navigationOptions: {
      header:null,
    }
  }

})


const BottomTab = createBottomTabNavigator({
    Home:{screen:HOME,
        navigationOptions: {
        header:null,
        tabBarIcon: ({ tintColor }) => <Icon name="Home" color={tintColor} icon={HOME_ICON_ACTIVE} />
      }},
      Search:{screen:CATEGORY_SEARCH,
        navigationOptions: {
        header:null,
        tabBarIcon: ({ tintColor }) => <Icon name="Search" color={tintColor} icon={SEARCH_ICON_ACTIVE}/>
      }},
      Account:{screen:ACCOUNT,
        navigationOptions: {
        header:null,
        tabBarIcon: ({ tintColor }) => <Icon name="Account" color={tintColor} icon={PROFILE_ICON_ACTIVE}/>
      }
   
    },
      Settings:{screen:SETTINGS,
        navigationOptions: {
        header:null,
        tabBarIcon: ({ tintColor }) => <Icon name="Settings" color={tintColor} icon={SETTINGS_ICON_ACTIVE}/>
      }},
},{
   tabBarComponent: TabBarComponent,
    initialRouteName: '',
    tabBarOptions: {
      activeTintColor: APP_BLACK,
      inactiveTintColor: APP_GREY,
    },
    lazy: true
});

export default createAppContainer(BottomTab);
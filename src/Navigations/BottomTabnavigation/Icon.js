import React from "react";
import { Text,Image } from "react-native";
import { APP_BLACK } from "../../Constants/App_Constants";

const Icon = ({ name, color, style,icon, ...props }) => {
  //const icon = iconMap[name];
  return(
    <Image 
    style={[{height:15.5,width:15,marginTop:14,tintColor:color},style]}
    source={icon}/>
  ) 
};

export default Icon;
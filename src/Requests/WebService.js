import AsyncStorage from "@react-native-community/async-storage";

export const BASE_URL = "https://apps.appsmaventech.com/onyx/v1/";

// let parseToken = AsyncStorage.getItem('user_token')

//////LOGIN/////////
export async function LOGIN(EMAIL, PASSWORD, DEVICE_TOKEN, TYPE) {

  try {
    const URL = BASE_URL + "userLogin"
    const credentials = {
      "userEmail": EMAIL,
      "userPassword": PASSWORD,
      "deviceToken": [{ "token": DEVICE_TOKEN, "deviceType": TYPE }]
    }
    try {
      let response = await fetch(
        URL,
        {
          'method': 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(credentials)
        }
      );



      let res=await response.json()
      console.log("check reponse???????? userrrrrr",res)

        if(res.status ===0 && res.message==='No account found with this email address'){

          let businessReponse=await  this.BUSINESS_LOGIN(EMAIL,PASSWORD,DEVICE_TOKEN,TYPE)
          console.log("check user", JSON.stringify(businessReponse))
          return businessReponse
        }
        else{
          return res

      }
    } catch (errors) {
      console.log("check login api fun customer??????", errors)
      return errors;

    }
  } catch (error) {
  console.log("check login api fun customer??????", error)

    return error.message;
  }
}

/////BUSINESS_LOGIN////
export async function BUSINESS_LOGIN(EMAIL, PASSWORD, DEVICE_TOKEN, TYPE) {

  console.log("check login api fun business")
  try {
    const URL = BASE_URL + "businesslogin"
    const credentials = {
      "businessuserEmail": EMAIL,
      "businessuserPassword": PASSWORD,
      "deviceToken": [{ "token": DEVICE_TOKEN, "deviceType": TYPE }]
    }
    try {
      let response = await fetch(
        URL,
        {
          'method': 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(credentials)
        }
      );
      let res=await response.json()
      console.log("check res//////////////businessss",res)
      if (res.status == 1) {
        return res
      }
      else{
        return res
      }
    } catch (errors) {
      return errors;

    }
  } catch (error) {
    return error.message;
  }
}

//////REGISTER//////

export async function REGISTER(LONGITUDE, LATITUDE, FIRST_NAME, LAST_NAME, EMAIL, PHONE_NUMBER, PASSWORD, ZIP_CODE, USER_TYPE, FB_ID, DEVICE_TOKEN, DEVICE_TYPE) {
  try {
    const URL = BASE_URL + "userRegister"
    const credentials = {
      "userLongitude": LONGITUDE,
      "userLatitude": LATITUDE,
      "userFirstName": FIRST_NAME,
      "userLastName": LAST_NAME,
      "userEmail": EMAIL,
      "userPhoneNumber": PHONE_NUMBER,
      "userPassword": PASSWORD,
      "userZipcode": ZIP_CODE,
      "usertype": USER_TYPE,
      "userfb_id": FB_ID,
      "deviceToken": [{ "token": DEVICE_TOKEN, "deviceType": DEVICE_TYPE }]
    }
    console.log(JSON.stringify(credentials))
    try {
      let response = await fetch(
        URL,
        {
          'method': 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(credentials)
        }
      );
      console.log(JSON.stringify(response))
      if (response.status == 200) {
        return response.json()
      }
    } catch (errors) {
      return errors;

    }
  } catch (error) {
    return error.message;
  }
}

/////////////BUSINESS_REGISTER//////////////

export async function BUSINESS_REGISTER(LATITUDE, LONGITUDE, EMAIL, FIRST_NAME, LAST_NAME, BUSINESS_HOURS, PHONE_NUMBER, PASSWORD, ZIP_CODE, BUSINESS_NAME,
  BUSINESS_ADDRESS, BUSINESS_LINK, BUSINESS_SUB, BUSINESS_DOC, BUSINESS_IMAGES, BUSINESS_TYPE, DEVICE_TOKEN, DEVICE_TYPE) {
  try {
    //  const URL=BASE_URL+"businessRegister"
    const URL = BASE_URL + "businessRegister"
    const credentials = {
      "businessLatitude": LATITUDE,
      "businessLongitude": LONGITUDE,
      "businessuserEmail": EMAIL,
      "firstname": FIRST_NAME,
      "lastname": LAST_NAME,
      "availablehours": BUSINESS_HOURS,
      "businessuserPhoneNumber": PHONE_NUMBER,
      "businessuserPassword": PASSWORD,
      "businessuserZipcode": ZIP_CODE,
      "businessName": BUSINESS_NAME,
      "businessAddress": BUSINESS_ADDRESS,
      "businesswebsiteLink": BUSINESS_LINK,
      "businesssubscrption": BUSINESS_SUB,
      "businessdocument": BUSINESS_DOC,
      "businessImages": BUSINESS_IMAGES,
      "businessverify": "0",
      "businesstype": BUSINESS_TYPE,
      "businessdeviceToken": [{
        "token": DEVICE_TOKEN,
        "deviceType": DEVICE_TYPE
      }],
      "businessLocations": []
      
    }
    console.log('cred>>>>>>>>>', JSON.stringify(credentials))
    try {
      let response = await fetch(
        URL,
        {
          'method': 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(credentials)
        }
      );
      console.log('response12', JSON.stringify(response.status))
      if (response.status == 200) {
        return response.json()
      }
    } catch (errors) {
      return errors;
    }
  } catch (error) {
    return error.message;
  }
}

///////User reviews on Business //////////////
export async function USER_REVIEWS(businessuserId) {
  try{
    const URL = BASE_URL + "businessReviews"
    const credentials = {
      businessuserId:businessuserId
    }
    try{
      let response = await fetch(
        URL,
        {
          'method':'POST',
          headers:{
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body:JSON.stringify(credentials)
        }
      );
      if (response.status == 200) {
        return response.json()
      }
    } catch (error) {
      return errors;
    }
  } catch (error) {
    return error.message;
  }
}
  


/////VIEW_PROFILE///////
export async function VIEW_PROFILE(TOKEN) {
  try {
    const URL = BASE_URL + "viewOwnProfile"
    const credentials = {
      "token": TOKEN
    }
    try {
      let response = await fetch(
        URL,
        {
          'method': 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(credentials)
        }
      );
      if (response.status == 200) {
        return response.json()
      }
    } catch (errors) {
      return errors;

    }
  } catch (error) {
    return error.message;
  }
}

/////UPDATE_PROFILE////
export async function UPDATE_PROFILE(TOKEN, FIRST_NAME, LAST_NAME, ZIP_CODE, PHONE_NUMBER, USER_DESCRIPTION) {
  try {
    const URL = BASE_URL + "updateProfile"
    const credentials = {
      "userFirstName": FIRST_NAME,
      "userLastName": LAST_NAME,
      "userZipcode": ZIP_CODE,
      "userPhoneNumber": PHONE_NUMBER,
      "userdescription": USER_DESCRIPTION
    }
    try {
      let response = await fetch(
        URL,
        {
          'method': 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'token': TOKEN
          },
          body: JSON.stringify(credentials)
        }
      );
      if (response.status == 200) {
        return response.json()
      }
    } catch (errors) {
      return errors;

    }
  } catch (error) {
    return error.message;
  }
}




//////contactus/////////
export async function CONTACT_US(TOKEN, USER_ID, USER_NAME, USER_EMAIL, USER_COMMENT) {
  try {
    const URL = BASE_URL + "contactus"
    const credentials = {
      "userId": USER_ID,
      "userName": USER_NAME,
      "userEmail": USER_EMAIL,
      "userComment": USER_COMMENT
    }
    try {
      let response = await fetch(
        URL,
        {
          'method': 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'token': TOKEN
          },
          body: JSON.stringify(credentials)
        }
      );
      if (response.status == 200) {
        return response.json()
      }
    } catch (errors) {
      return errors;

    }
  } catch (error) {
    return error.message;
  }
}

//////LOGOUT////////
export async function LOGOUT(DEVICE_TOKEN, TOKEN) {

  try {
    const URL = BASE_URL + "logout"
    const credentials = {
      "deviceToken": DEVICE_TOKEN,
    }
    try {
      let response = await fetch(
        URL,
        {
          'method': 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'token': TOKEN
          },
          body: JSON.stringify(credentials)
        }
      );
      if (response.status == 200) {
        return response.json()
      }
    } catch (errors) {
      return errors;

    }
  } catch (error) {
    return error.message;
  }
}

///////FILE UPLOAD//////
export async function FILE_UPLOAD(source) {
  try {
    let response = await fetch(
      BASE_URL + 'fileupload',
      {
        'method': 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'multipart/form-data',
        },
        body: source
      }
    );
    if (response.status == 200) {
      response.json().then(data => {
        switch (data.status) {
          case 200:
            return data;
            break;
        }
      });
    } else {
      return response;
    }
  } catch (error) {
    return error;
  }
}

///forgot password user///
export async function FORGOT_PASSWORD(email) {
  try {
    const URL = BASE_URL + "userForgotPassword"
    const credentials = {
      "userEmail": email,
    }
    try {
      let response = await fetch(
        URL,
        {
          'method': 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(credentials)
        }
      );
      if (response.status == 200) {
        return response.json()
      }
    } catch (errors) {
      return errors;

    }
  } catch (error) {
    return error.message;
  }
}

///forgot password business///
export async function FORGOT_PASSWORD_BUSINESS(email) {
  try {
    const URL = BASE_URL + "businessForgotPassword"
    const credentials = {
      "businessuserEmail": email,
    }
    try {
      let response = await fetch(
        URL,
        {
          'method': 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(credentials)
        }
      );
      if (response.status == 200) {
        return response.json()
      }
    } catch (errors) {
      return errors;

    }
  } catch (error) {
    return error.message;
  }
}

////change password user////
export async function CHANGE_PASSWORD(userid, oldPassword, newPassword) {
  try {
    const URL = BASE_URL + "userChangePassword"
    const credentials = {
      "userId": userid,
      "oldPassword": oldPassword,
      "newPassword": newPassword
    }
    console.log(JSON.stringify(credentials))
    try {
      let response = await fetch(
        URL,
        {
          'method': 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(credentials)
        }
      );
      if (response.status == 200) {
        return response.json()
      }
    } catch (errors) {
      return errors;

    }
  } catch (error) {
    return error.message;
  }
}


////change password business////
export async function CHANGE_PASSWORD_BUSINESS(userid, oldPassword, newPassword) {
  try {
    const URL = BASE_URL + "businessChangePassword"
    const credentials = {
      "businessuserId": userid,
      "oldPassword": oldPassword,
      "newPassword": newPassword
    }
    console.log('cred', JSON.stringify(credentials))
    try {
      let response = await fetch(
        URL,
        {
          'method': 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(credentials)
        }
      );
      if (response.status == 200) {
        return response.json()
      }
    } catch (errors) {
      return errors;

    }
  } catch (error) {
    return error.message;
  }
}

////getBusiness//////
export async function GET_BUSINESS(userid, latitude, longitude) {
  try {
    const URL = BASE_URL + "getBusinesses"
    const credentials = {
      "userId": userid,
      "latitude": latitude,
      "longitude": longitude
    }
    console.log(JSON.stringify(credentials))
    try {
      let response = await fetch(
        URL,
        {
          'method': 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(credentials)
        }
      );
      if (response.status == 200) {
        return response.json()
      }
    } catch (errors) {
      return errors;

    }
  } catch (error) {
    return error.message;
  }
}

////search nearby bussiness//////
export async function NEARBY_SEARCH(userid, latitude, longitude, businesstype, searchText) {
  try {
    const URL = BASE_URL + "getNearByBusinesses"
    const credentials = {
      "userId": userid,
      "latitude": latitude,
      "longitude": longitude,
      "businesstype": businesstype,
      "searchText": searchText
    }
    console.log('cred======>', JSON.stringify(credentials))
    try {
      let response = await fetch(
        URL,
        {
          'method': 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(credentials)
        }
      );
      if (response.status == 200) {
        return response.json()
      }
    } catch (errors) {
      return errors;

    }
  } catch (error) {
    return error.message;
  }
}

////single business////
export async function GET_SINGLE_BUSINESS(userid, businessId) {
  try {
    const URL = BASE_URL + "getSingleBusiness"
    const credentials = {
      // "businessuserId":businessuserId,
      "businessuserId": businessId,
      "userId": userid,

    }
    //  console.log('cred-------->',JSON.stringify(credentials))
    console.log('cred-+++------->', credentials)
    try {
      let response = await fetch(
        URL,
        {
          'method': 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(credentials)
        }
      );
      if (response.status == 200) {
        console.log('kkkkkk')
        return response.json()
      }
    } catch (errors) {
      return errors;

    }
  } catch (error) {
    return error.message;
  }
}

////////////Add Reviews//////////////////
export async function Add_Reviews(businessuserId, businessRatings, businessReviews, token) {
  try {
    const URL = BASE_URL + "addReviews"
    const credentials = {

      "businessuserId": businessuserId,
      "businessRatings": businessRatings,
      "businessReviews": businessReviews
    }
    let newToken = await AsyncStorage.getItem("user_token")
 //   console.log("cherck token??????????????????????????????", newToken)
    console.log('AddReviewss--------->credentials' , JSON.stringify(credentials))
    try {
      let response = await fetch(
        URL,
        {
          'method': 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'token': newToken
            // 'token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6InNtaXRoQGdtYWlsLmNvbSIsIl9pZCI6IjVmZjcxMWNmZjA5OGE1MDczZjEwM2NlMyIsImlhdCI6MTYzMDY0OTQ4NX0.HngekgcpTRg-jDKIMTbqI_srkybuzydyrLvyKXcmklo',
            //'token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6Im5paGFyaWthQGdtYWlsLmNvbSIsIl9pZCI6IjYxMjVjY2NiYzc2ZWRmMWZkY2YyMmM1ZCIsImlhdCI6MTYzMTI2OTk0OX0.ntSeqdRNW8qFQWUtmFugDyrQVIdSlBHYf7slEU3EbjY',
            //'token':tokenss
          },
          body: JSON.stringify(credentials)
        }
      );
      console.log("responseeeeeee+200", JSON.stringify(response))
      if (response.status == 200) {
        console.log("000000007")
        return response.json()
      }
    } catch (errors) {
      return errors;
    }
  } catch (errors) {
    return error.message
  }
}



////////Get Business Reviews///////
export async function GET_REVIEWS(businessuserId) {
  try {
    const URL = BASE_URL + "getReviews"
    const credentials = {
      "businessuserId": businessuserId
    }
    console.log("GetWishlisttttttttt", JSON.stringify(credentials))
    try {
      let response = await fetch(
        URL,
        {
          'method': 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'token': token
          },
          body: JSON.stringify(credentials)
        }
      );
      if (response.status == 200) {
        console.log("getWishlisttStatussss")
        return response.json()
      }
    } catch (errors) {
      return errors;
    }
  } catch (errors) {
    return error.message;
  }
}

////////////Get businessReviews by USer//////////////
export async function GET_BUSINESS_REVIEWS(token){
  try {
    const URL = BASE_URL + "businessReviewsByUser"

  try{
    const response = await fetch(
      URL,
      {
       ' method': 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'token': token
        },
        //body: JSON.stringify(credentials)
      }
    );
    console.log('ReviewByUaser',response)
    if(response.status == 200) {
      console.log("Get_BUsiness_Reviews")
      return response.json()
    }
  } catch (errors) {
    return errors;
  }
} catch (errors) {
  return error.message
}
}

///Add to Wishlist//////
export async function ADD_FAVOURITE(businessuserId) {
  try {
    const URL = BASE_URL + "addInWishlist"
    const credentials = {
      "businessuserId": businessuserId
    }
    console.log('AddToFavourite>>>>>>', JSON.stringify(credentials))
    let newToken = await AsyncStorage.getItem("user_token")
    try {
      const response = await fetch(
        URL,
        {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'token': newToken
          },
          body: JSON.stringify(credentials)
        }
      );
      console.log('statusssssssss>>>>', response)
      if (response.status == 200) {

        return response.json()
      }
    } catch (errors) {
      return errors;
    }
  } catch (errors) {
    return error.message;
  }
}


////Get_Wishlist/////
export async function GET_WISHLIST() {
  let newToken = await AsyncStorage.getItem("user_token")
  try {
    const URL = BASE_URL + "getWishlist"
    try {
      let response = await fetch(
        URL,
        {
          'method': 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'token': newToken
          },
        }
      );
      if (response.status == 200) {
        console.log("getStatusssss")
        return response.json()
      }
    } catch (errors) {
      return errors;
    }
  } catch (errors) {
    return error.message;
  }
}
////////Remove Businee from Whishlist///
export async function DELETE_WISHLIST(businessuserId) {

  try {
    const URL = BASE_URL + "deleteWishlist"
    const credentials = {
      "businessuserId": businessuserId
    }
    console.log('deletet>>????????????', JSON.stringify(credentials))
    let newToken = await AsyncStorage.getItem("user_token")
    console.log(newToken, 'newtokeen--.')
    try {
      let response = await fetch(
        URL,
        {
          'method': 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'token': newToken
          },
          body: JSON.stringify(credentials)
        }
      );
      if (response.status == 200) {
        console.log("getStatusssss")
        return response.json()
      }
    } catch (errors) {
      return errors;
    }
  } catch (errors) {
    return error.message;
  }
}

///business location///
export async function GET_BUSINESS_LOCATION(businessuserId) {
  try {
    const URL = BASE_URL + "getBusinessLocation"
    const credentials = {
      "businessuserId": businessuserId
    }
    try {
      let response = await fetch(
        URL,
        {
          'method': 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'token': ''
          },
          body: JSON.stringify(credentials)
        }
      );
      if (response.status == 200) {
        return response.json()
      }
    } catch (errors) {
      return errors;

    }
  } catch (error) {
    return error.message;
  }
}

///add location///
export async function ADD_LOCATION(businessuserId, businessLatitude, businessLongitude, businessLocationZipcode, businessLocationIsFeatured, businessLocationAddress, businessLocationDocument) {
  try {
    const URL = BASE_URL + "addBusinessLocation"
    const credentials = {
      "businessuserId": businessuserId,
      "businessLatitude": businessLatitude,
      "businessLongitude": businessLongitude,
      "businessLocationZipcode": businessLocationZipcode,
      "businessLocationIsFeatured": businessLocationIsFeatured,
      "businessLocationAddress": businessLocationAddress,
      "businessLocationDocument": businessLocationDocument
    }
    try {
      let response = await fetch(
        URL,
        {
          'method': 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(credentials)
        }
      );
      if (response.status == 200) {
        return response.json()
      }
    } catch (errors) {
      return errors;

    }
  } catch (error) {
    return error.message;
  }
}

//feature Business
export async function ADD_TO_FEATURE(businessLocationId, status) {
  try {
    const URL = BASE_URL + "updateBusinessLocationFeature"
    const credentials = {
      "businessLocationId": businessLocationId,
      "status": status
    }
    try {
      let response = await fetch(
        URL,
        {
          'method': 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(credentials)
        }
      );
      if (response.status == 200) {
        return response.json()
      }
    } catch (errors) {
      return errors;

    }
  } catch (error) {
    return error.message;
  }
}

//categories
export async function GET_CATEGORIES() {
  try {
    const URL = BASE_URL + "getAllCategories"
    try {
      let response = await fetch(
        URL,
        {
          'method': 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          //body: JSON.stringify(credentials)
        }
      );
      if (response.status == 200) {
        return response.json()
      }
    } catch (errors) {
      return errors;

    }
  } catch (error) {
    return error.message;
  }
}

//update Business
export async function UPDATE_BUSINESS(businessuserId, businessName, businessAddress, businessuserFirstName, businessuserLastName, businessuserPhoneNumber, businessImages, businessuserZipcode,
  availablehours,businesswebsiteLink,businessLatitude,businessLongitude, ) {
  try {
    const URL = BASE_URL + "updateBusinessProfile"
    const credentials = {

      "businessuserId": businessuserId,
      "businessName": businessName,
      "businessAddress": businessAddress,
      "businessuserFirstName":businessuserFirstName,
      "businessuserLastName":businessuserLastName,
      "businessuserPhoneNumber":businessuserPhoneNumber,
      "businessImages":businessImages,
      "businessuserZipcode":businessuserZipcode,
      "availablehours":availablehours,
      "businesswebsiteLink":businesswebsiteLink,
      "businessLatitude":businessLatitude,
     "businessLongitude":businessLongitude
      // "businessuserId": businessuserId,
      // "businessName": businessName,
      // "businessAddress": businessAddress,
      // "businessuserFirstName": businessuserFirstName,
      // "businessuserLastName": businessuserLastName,
      // "businessuserPhoneNumber": businessuserPhoneNumber,
      // "businessImages": businessImages,
      // "businessuserZipcode": businessuserZipcode
    }
    console.log('updarteBusinessCred:', JSON.stringify(credentials))
    try {
      let response = await fetch(
        URL,
        {
          'method': 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(credentials)
        }
      );
      if (response.status == 200) {
        return response.json()
      }
    } catch (errors) {
      return errors;

    }
  } catch (error) {
    return error.message;
  }
}
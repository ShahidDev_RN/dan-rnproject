import React, { useEffect, useState } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Image,
  StatusBar,
  Dimensions,
  ImageBackground,
  TouchableOpacity
} from 'react-native';
import MapView, { Marker, ProviderProp } from 'react-native-maps';
import AsyncStorage from '@react-native-community/async-storage';
import { Rating, AirbnbRating } from 'react-native-ratings';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {
  APP_BLACK,
  APP_GREY,
  APP_WHITE,
  BUSINESS_NAME,
  ADDRESS,
  DISTANCE,
  DIRECTIONS,
  AVAILABLE_HOURS,
  WEBSITE_LINK,
  Rating_HEADER,
  RESTAURANT,
  FONT_REGULAR,
  FONT_MEDIUM,
  FONT_SEMIBOLD,
  FONT_BOLD,
  SHARE,
  BACK_ICON,
  FAVORITE_INACTIVE,
  APP_BORDER,
  FAVORITE_DETAIL,
  ADD_PROFILE,
  EDIT_ICON,
  map
} from '../../Constants/App_Constants.js';


let map_refs = ''
let { width, height } = Dimensions.get('window')
const maps = (lat, long) => {
  if (Platform.OS == 'ios') {
    return (
      <MapView
        style={Styles.Maps}
        mapType='standard'
        ref={(ref) => map_refs = ref}
        //provider={'google'}
       // showsMyLocationButton={true}
        //showsUserLocation={true}
       // initialRegion={this.state.region}
        //region={this.state.region}
        // scrollEnabled={true}>
        // {!isNaN(this.state.latitude) && <Marker
        //     coordinate={{
        //         latitude: this.state.latitude,
        //         longitude: this.state.longitude,
        //     }}>
        //     <Image resizeMode='contain' source={Map_Marker} style={{ height: 50, width: 50 }} />
        // </Marker>}
         initialRegion={{
          latitude: lat,
          //latitude: 30.7089,
          //longitude: 76.7018,
          longitude: long,
          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421,
        }}
        scrollEnabled={true}>
        <Marker coordinate={{ latitude: lat, longitude: long }} /> 
      </MapView>
    )
  } else {
    return (
      <MapView
        style={Styles.Maps}
        mapType='standard'
        provider="google"
        showsMyLocationButton={true}
        showsUserLocation={true}
        initialRegion={{
          // latitude: lat,
          // latitude: lat,
          // longitude: long,
          // longitude: ,
          latitude: lat,
          longitude:long,
          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421,
        }}
        scrollEnabled={true}>
        {/* <Marker coordinate={{ latitude: lat, longitude: long }} /> */}
      </MapView>
    )
  }
}


const BusinessHome = ({ navigation }) => {
  const [data, setData] = useState('')
  const [image, setImage] = useState('');
  const [coordinates, setCord] = useState('');
  // useEffect(() => {
  //   AsyncStorage.getItem("user_info").then(async(value) => {
  //     console.log("checkkkk value of user data",value)
  //    await setData(JSON.parse(value))
  //     if (data.businessImages != undefined) {
  //     await  setImage(data.businessImages[0].image)
  //     }
  //     //setCord(JSON.parse(value).businessLocation.coordinates)
  //   }).done();
  // },[]);

  // useEffect(async() => {
   
  //   const interval = setInterval(() => {
    
  //   }, );

  //   const unsubscribe = navigation.addListener('focus', () => {
  //     console.log('check login');
  //     AsyncStorage.getItem("user_info").then(async(value) => {
  //       console.log("checkkkk value of user data",value)
  //      await setData(JSON.parse(value))
  //       if (data.businessImages != undefined) {
  //       await  setImage(data.businessImages[0].image)
  //       }
  //       //setCord(JSON.parse(value).businessLocation.coordinates)
  //     })
     
  //   })

  //   return () => {
  //     clearTimeout(interval);
  //     unsubscribe;
  //   };
  // },[navigation]);


  useEffect(() => {
   
    const interval = setInterval(() => {
    getData();

    }, 1000);

    const unsubscribe = navigation.addListener('focus', () => {
     // console.log('check login');

      getData()
    });

    return () => {
      clearTimeout(interval);
      unsubscribe;
    };
  }, [navigation]);
const getData=async()=>{
  await AsyncStorage.getItem("user_info", '').then(async(value) => {
    
    let businessData=JSON.parse(value)
    if(businessData!== null && businessData !== '' && businessData!==undefined){
   console.log('user_info_data_businessLocation', businessData.businessLocation)
   await setData(businessData)
    if (businessData.businessImages != undefined) {
    await  setImage(businessData.businessImages[0].image)
    await setCord(businessData.businessLocation.coordinates)
    map_refs.animateToRegion({
      latitude:businessData.businessLocation.coordinates[0],
      longitude: businessData.businessLocation.coordinates[1],
      latitudeDelta: 0.0922,
      longitudeDelta: 0.0421
  })
    }
    if(businessData.businessLocation.coordinates.length > 0){
     
    // console.log('latt', coordinates[0])
    }
  }
    //setCord(JSON.parse(value).businessLocation.coordinates)
  })
}
  return (
    <SafeAreaView style={Styles.SafeArea}>
      <StatusBar barStyle={'dark-content'}></StatusBar>
      <ScrollView>
        <ImageBackground
          source={{ uri: image }}
          style={Styles.Image}>
          <View style={Styles.IconContainer}>
            <View style={{ backgroundColor: 'transparent', width: "65%" }}>
              {/* <TouchableOpacity style={{ width: 40, height: 30, }} onPress={() => navigation.navigate('AppLogin')}>
                <Image source={BACK_ICON} style={[Styles.Icons], { alignSelf: 'center', tintColor: APP_BLACK }} />
              </TouchableOpacity> */}
            </View>
            <View style={{ flexDirection: 'row', width: '35%' }}>
              <TouchableOpacity style={{ width: 40, height: 30, backgroundColor: 'transparent' }}>
                <Image source={FAVORITE_DETAIL} style={[Styles.Icons], { alignSelf: 'center', tintColor: APP_BLACK }} />
              </TouchableOpacity>
              <TouchableOpacity style={{ width: 40, height: 30, backgroundColor: 'transparent' }}>
                <Image source={SHARE} style={[Styles.Icons], { alignSelf: 'center', tintColor: APP_BLACK }} />
              </TouchableOpacity>
              <TouchableOpacity onPress={() => navigation.navigate('Edit_BusinessProfile')} style={{ width: 40, height: 30, backgroundColor: 'transparent' }}>
                <Image source={EDIT_ICON} style={[Styles.Icons], { alignSelf: 'center', tintColor: APP_BLACK }} />
              </TouchableOpacity>
            </View>
          </View>

        </ImageBackground>
        <View style={Styles.Border} />
        <View style={Styles.TextContainer}>
          <Text style={Styles.TextHeader}>{BUSINESS_NAME}</Text>
          <Text style={Styles.TextNormal}>{data.businessName}</Text>
        </View>
        <View style={Styles.Border} />
        <View style={Styles.TextContainer}>
          <Text style={Styles.TextHeader}>{ADDRESS}</Text>
          <Text style={Styles.TextNormal}>{data.businessAddress}</Text>
        </View>
        <View style={Styles.Border} />
        <View style={Styles.TextContainer}>
          <Text style={Styles.TextHeader}>Email Address</Text>
          <Text style={Styles.TextNormal}>{data.businessuserEmail}</Text>
        </View>
        <View style={Styles.Border} />
        <View style={Styles.TextContainer}>
          <Text style={Styles.TextHeader}>Phone Number</Text>
          <Text style={Styles.TextNormal}>{data.businessuserPhoneNumber}</Text>
        </View>
        
        {/* <View style={Styles.Border} />
        <View style={Styles.TextContainer}>
          <Text style={Styles.TextHeader}>{DISTANCE}</Text>
          <Text style={Styles.TextNormal}>2KM Far</Text>
        </View> */}

        <View style={Styles.Border} />
        <View style={Styles.TextContainer}>
          <Text style={Styles.TextHeader}>{AVAILABLE_HOURS}</Text>
          <Text style={Styles.TextNormal}>{data.availablehours}</Text>
        </View>
        <View style={Styles.Border} />
        <View style={Styles.TextContainer}>
          <Text style={Styles.TextHeader}>{DIRECTIONS}</Text>
          <View style={Styles.MapsContainer}>
            {maps(coordinates[0], coordinates[1])}
          </View>
        </View>
        {/* <View style={Styles.Border} />
        <View style={Styles.TextContainer}>
          <Text style={Styles.TextHeader}>{WEBSITE_LINK}</Text>
          <Text style={[Styles.TextNormal, { color: 'dodgerblue' }]}>{data.businesswebsiteLink}</Text>
        </View>
        <View style={Styles.Border} />
        <View style={[Styles.TextContainer, { flexDirection: 'row', justifyContent: 'space-between' }]}>
          <Text style={Styles.TextHeader}>{Rating_HEADER}</Text>
          <TouchableOpacity>
            <Image source={ADD_PROFILE} style={Styles.AddIcon} />
          </TouchableOpacity>
        </View> */}
        <View style={Styles.RatingContainer}>
          <Image
            source={RESTAURANT}
            style={{
              width: 50,
              height: 50,
              borderRadius: 10,
            }}
          />
          <View style={Styles.ReviewContainer}>
            <View
              style={{
                flexDirection: 'row',
                alignContent: 'center',
              }}>
              <Text style={Styles.ReviewName}>George</Text>
              <Rating
                style={Styles.RatingBar}
                type="custom"
                imageSize={20}
                readonly
                tintColor={APP_WHITE}
                ratingBackgroundColor={APP_GREY}
              />
            </View>
            <Text style={Styles.ReviewText}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam
              viverra purus sit amet ex efficitur suscipit. Nulla ultricies
              mauris quam, quis ultrices mi sodales vel.
            </Text>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const Styles = StyleSheet.create({
  SafeArea: {
    flex: 1,
    backgroundColor: APP_WHITE,
  },
  Image: {
    width: Dimensions.get('window').width,
    height: 220,
    resizeMode: 'cover',
  },
  IconContainer: {
    paddingHorizontal: 0,
    marginTop: 20,
    flexDirection: 'row',
    width: '100%',
    backgroundColor: 'transparent',
    //justifyContent:'space-around'
  },
  Icons: {
    width: 18,
    height: 18,
    marginTop: 0,
    resizeMode: 'contain',
    tintColor: APP_BLACK,
  },
  AddIcon: {
    width: 20,
    height: 20,
    marginTop: 0,
    right: 10,
    resizeMode: 'contain',
    tintColor: APP_WHITE,
  },
  Border: {
    backgroundColor: '#e3e5e8',
    width: Dimensions.get('window').width,
    height: 2,
  },
  TextContainer: {
    paddingHorizontal: 10,
    paddingVertical: 20,
  },
  TextHeader: {
    //fontWeight: 'normal',
    color: APP_BLACK,
    fontSize: 14,
    fontFamily: FONT_MEDIUM
  },
  TextNormal: {
    color: APP_GREY,
    fontFamily: FONT_MEDIUM,
    marginTop: 5,
    fontSize: 14,
  },
  MapsContainer: {
    width: '100%',
    height: 160,
    alignSelf: 'center',
    overflow: 'hidden',
    borderRadius: 10,
    marginTop: 10,
  },
  Maps: { flex: 1 },
  Icon: {
    alignSelf: 'center',
    position: 'absolute',
    right: 20,
    color: APP_WHITE,
    fontSize: 25,
  },
  RatingContainer: {
    flex: 1,
    flexDirection: 'row',
    paddingVertical: 15,
    paddingStart: 20,
  },
  ReviewContainer: {
    flex: 1,
    flexDirection: 'column',
    marginStart: 20,
    marginEnd: 20,
  },
  ReviewName: {
    color: APP_BLACK,
    fontSize: 16,
    fontFamily: FONT_SEMIBOLD
  },
  ReviewText: {
    color: APP_GREY,
    marginTop: 5,
    fontSize: 11,
    fontFamily: FONT_REGULAR
  },
  RatingBar: {
    flex: 1,
    position: 'absolute',
    right: 0,
    alignSelf: 'center',
  },
});
export default BusinessHome;
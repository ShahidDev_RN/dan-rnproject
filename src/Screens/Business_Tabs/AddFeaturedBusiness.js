import React, {Component} from 'react';
import {View,
    Dimensions ,
    Text,
    StyleSheet,
    TouchableOpacity,
    Image,
    TextInput,
    BackHandler,
    Alert,
    SafeAreaView,
    Platform,
    ScrollView,FlatList,
    KeyboardAvoidingView,StatusBar,
    ImageBackground} from 'react-native';
    import AsyncStorage from '@react-native-community/async-storage';
    import * as API from '../../Requests/WebService.js'
    import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
import {FONT_BLACK,FONT_BOLD,FONT_EXTRABOLD,FONT_EXTRALIGHT,FONT_LIGHT,FONT_SEMIBOLD,RADIO_OFF,RADIO_ON,DOWN_ARROW,HEADER_ICON,
    FONT_THIN,APP_WHITE,APP_BLACK,APP_FACEBOOK,BACK_ICON, FONT_REGULAR,FONT_MEDIUM,NOTIFICATION_ICON, APP_GREY, RADIO_BUTTON_ON, RADIO_BUTTON_OFF} from '../../Constants/App_Constants.js'
    let { width, height } = Dimensions.get('window')
    const ASPECT_RATIO = width / height
class AddFeaturedBusiness extends Component {
    constructor(props){
        super(props);
        this.state={
            username:'',
            password:'',
            subscriptionType:0,
            data:'',
            showLocations:false,
            subscriptionIconFree:RADIO_ON,
            subscriptionIconPaid:RADIO_OFF,
            showBusiness:false,
            padding:55,
            locationName:'Name of the Location',
            locationId:'',
            serverError:false,
            serverErrorText:'',
            serverErrorTextColor:'#dd4b39',
            isloading:false
        }  
        this.props.navigation.addListener(
            'didFocus',payload => {
             this.setState({showBusiness:false,locationName:'Name of the Location',locationId:'',padding:55},()=>{
                AsyncStorage.getItem("user_info").then((value) => { 
                    var data= JSON.parse(value)
                   API.GET_BUSINESS_LOCATION(data._id).then((response)=>{
                    this.setState({data:response.data.businessLocation},()=>{
                    })
                   })
                }).done();
             })
            }
          );
      } 

      showBusiness(){
        if(this.state.showBusiness){
            if(this.state.data.length>0){
            return(
                <View style={styles.availableCategories}>
                    <FlatList
                    data={this.state.data} 
                    showsVerticalScrollIndicator={false}
                    contentContainerStyle={{backgroundColor:'transparent',width:'90%',alignSelf:'center',marginTop:10,marginBottom:10}}
                    renderItem={({ item: data, index }) => { 
                    return(
                        <TouchableOpacity onPress={()=>this.setState({showBusiness:false,padding:55,locationName:data.businessLocationAddress,locationId:data._id},()=>{
                            console.log(JSON.stringify(this.state.locationId))
                        })} style={{alignSelf:'center',height:23,backgroundColor:'transparent',width:'100%',marginTop:2,}}>
                                <Text style={{fontSize:14,fontFamily:FONT_MEDIUM,color:APP_BLACK}}>{data.businessLocationAddress.toUpperCase()}</Text>
                        </TouchableOpacity>
                    )
                    }}
                    keyExtractor={(item, index) => index.toString()}/>
                </View>
            )}else{
            return(
                <View style={styles.availableCategories}>
                    <Text style={{fontSize:14,fontFamily:FONT_SEMIBOLD,color:APP_GREY,alignSelf:'center',marginTop:20}}>No Featured Business Found</Text>
                </View>
            )
        }}
    }

    selectSubRadioFree(){
        this.setState({subscriptionType:0,subscriptionIconFree:RADIO_ON,subscriptionIconPaid:RADIO_OFF})
    }
    selectSubRadioPaid(){
        this.setState({subscriptionType:1,subscriptionIconPaid:RADIO_ON,subscriptionIconFree:RADIO_OFF})
    }

      header(){
        if(Platform.OS=='ios'){
          return(
            <View style={{height:75,width:'100%',backgroundColor:'white',justifyContent:'flex-end',borderBottomColor:'#e3e5e8',borderBottomWidth:3}}>
                <View style={{height:'80%',width:'95%',backgroundColor:'transparent',alignSelf:'center'}}>
                    <View style={{flexDirection:'row',justifyContent:'space-between',height:'100%',width:'55%',backgroundColor:'transparent',alignSelf:'flex-start'}}>
                        <TouchableOpacity onPress={()=>this.props.navigation.goBack()}>
                            <Image source={HEADER_ICON} style={{marginTop:13, height:32,width:32}}></Image>
                        </TouchableOpacity>
                        <Text style={{color:APP_BLACK,fontSize:19,marginTop:15,fontFamily:FONT_MEDIUM,width:"100%",marginLeft:height<896?10:20}}>Featured Business</Text>
                    </View>
                </View>
            </View>
          )
        }else{
          return(
            <View style={{height:75,width:'100%',backgroundColor:'white',justifyContent:'flex-end',borderBottomColor:'#e3e5e8',borderBottomWidth:3}}>
            <View style={{height:'80%',width:'95%',backgroundColor:'transparent',alignSelf:'center'}}>
                <View style={{flexDirection:'row',justifyContent:'space-between',height:'100%',width:'55%',backgroundColor:'transparent',alignSelf:'flex-start'}}>
                    <TouchableOpacity onPress={()=>this.props.navigation.goBack()}>
                        <Image source={HEADER_ICON} style={{marginTop:13, height:32,width:32}}></Image>
                    </TouchableOpacity>
                    <Text style={{color:APP_BLACK,fontSize:19,marginTop:15,fontFamily:FONT_MEDIUM,width:190,marginLeft:height<896?10:20}}>Featured Business</Text>
                </View>
            </View>
        </View>
          )
        }
      } 


      showDropdown(){
        this.setState({showBusiness:!this.state.showBusiness,serverError:false})
        if(this.state.padding==55){
            this.setState({padding:20})
        }else{
            this.setState({padding:55})
        }
      }

      componentDidMount(){
        AsyncStorage.getItem("user_info").then((value) => { 
            var data= JSON.parse(value)
           API.GET_BUSINESS_LOCATION(data._id).then((response)=>{
            this.setState({data:response.data.businessLocation},()=>{
            })
           })
        }).done();
      }

      featureBusiness(){
          if(this.state.locationId==''){
            this.setState({serverError:true,serverErrorText:'No location selected'})
          }else{
            this.setState({isloading:true})
            API.ADD_TO_FEATURE(this.state.locationId,this.state.subscriptionType).then((response)=>{
                if(response.status==1){
                    this.setState({serverError:true,serverErrorText:'Location Featured Successfully',serverErrorTextColor:'green',isloading:false})
                    setTimeout(() => {this.setState({serverError: false})}, 2000)
                }else{
                    this.setState({serverError:true,serverErrorText:response.message,isloading:false})
                }
            })
          }
      }

    render(){ 
        return(
            <SafeAreaView forceInset={{ top: 'always' }} style={{flex:1,backgroundColor:APP_WHITE}}>
                {this.header()}
                <StatusBar barStyle={'dark-content'}></StatusBar>
                <OrientationLoadingOverlay
                visible={this.state.isloading}
                color="white"
                indicatorSize="large"
                />
               <View style={styles.OptionsView}>
                   <View style={styles.view}>
                        {/* <View style={styles.feildBorder}>
                            <TouchableOpacity onPress={()=>this.showDropdown()} style={{height:'100%',flexDirection:'row',justifyContent:'space-between'}}>
                                <Text style={{color:APP_BLACK, fontFamily:FONT_MEDIUM,paddingTop:14,paddingLeft:20}}>{this.state.locationName}</Text>
                                <Image source={DOWN_ARROW} style={{height:13,width:18,marginRight:20,marginTop:17,tintColor:APP_BLACK}}></Image>
                            </TouchableOpacity>
                        </View> */}
                        {this.showBusiness()}
                <View style={{bottom:65}}>
                        <View style={[{paddingVertical:this.state.padding},styles.TextContainer]}>
                            <Text style={styles.TextHeader}>Select Subscription Plan</Text>
                            <View style={styles.ImageContainer}>
                                    <TouchableOpacity onPress={()=>this.selectSubRadioFree()} style={{width:'50%',height:'100%',backgroundColor:'transparent',flexDirection:'row'}}>
                                        <Image style={{height:25,width:25,alignSelf:'center',tintColor:APP_BLACK}} source={this.state.subscriptionIconFree}></Image>
                                        <Text style={{fontFamily:FONT_MEDIUM, fontSize:16,color:APP_BLACK,marginLeft:10,marginTop:0,alignSelf:'center'}}>Free  <Text onPress={()=>alert('kk')} style={{ color: APP_GREY,fontFamily:FONT_MEDIUM,fontSize: 14,color:'dodgerblue'}}>Detail</Text></Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={()=>this.selectSubRadioPaid()} style={{width:'50%',height:'100%',backgroundColor:'transparent',flexDirection:'row'}}>
                                        <Image style={{height:25,width:25,alignSelf:'center',tintColor:APP_BLACK}} source={this.state.subscriptionIconPaid}></Image>
                                        <Text style={{fontFamily:FONT_MEDIUM, fontSize:16,color:APP_BLACK,marginLeft:10,marginTop:0,alignSelf:'center'}}>Paid($20)  <Text onPress={()=>alert('kk')} style={{ color: APP_GREY,fontFamily:FONT_MEDIUM,fontSize: 14,color:'dodgerblue'}}>Detail</Text></Text>
                                    </TouchableOpacity>
                            </View>
                        </View>

                      {this.state.serverError?<View style={{width:'100%', justifyContent:'center', alignItems:'center',height:20,backgroundColor:'transparent',marginBottom:30}}>
                        <Text style={{alignSelf:'center',color:this.state.serverErrorTextColor,fontFamily:FONT_SEMIBOLD,fontSize:16}}>{this.state.serverErrorText}</Text>
                        </View>:null}

                        <View style={styles.button}>
                            <TouchableOpacity onPress={()=>this.featureBusiness()} style={{height:'100%'}}>
                                <Text style={styles.buttonText}>Submit</Text>
                            </TouchableOpacity>
                        </View>
</View>
                    </View>
               </View>
            </SafeAreaView>
        )
    }
};

const styles = StyleSheet.create({
    OptionsView:{
        // height:400,
        width:width,
        backgroundColor:APP_WHITE,
    },
   view:{
    width:"90%",
    height:"80%",
    alignSelf:'center',
    backgroundColor:'transparent',
    marginTop:30
   },
    feildBorder:{
        width:width-40,
        height:50,
        borderRadius:30,
        borderWidth:2,
        borderColor:APP_GREY,
        backgroundColor:'#e3e5e8',
        alignSelf:'center'
    },
    feildBorder1:{
        width:width-40,
        height:50,
        borderRadius:30,
        borderWidth:1,
        marginTop:15,
        borderColor:APP_WHITE,
        backgroundColor:APP_BLACK,
        alignSelf:'center'
    },
    textfeild:{
        width:width-80,
        height:47,
        fontSize:13,
        fontFamily:FONT_MEDIUM,
        borderRadius:30,
        alignSelf:'center',
        color:APP_WHITE,
        
        },
    button:{
        width:width-45,
        height:60,
        borderRadius:30,
        //borderWidth:1,
        marginTop:10,
        //borderColor:APP_BLACK,
        backgroundColor:'#e3e5e8',
        alignSelf:'center',
        shadowOffset: {
            width: 0,
            height: 1
          },
          shadowRadius:1,
          shadowOpacity: 0.5,
          elevation:Platform.OS=='android'?5:0
    },
    buttonText:{
        fontSize:18,
        fontFamily:FONT_SEMIBOLD,
        color:APP_BLACK,
        alignSelf:'center',
        marginTop:15
    },
    TextContainer: {
        paddingHorizontal: 0,
        // paddingVertical: 55,
        backgroundColor:'transparent'
      },
      TextHeader: {
        color: APP_BLACK,
        fontSize: 20,
        fontFamily:FONT_MEDIUM,
        
      },
      ImageContainer: {
        width:'100%',
        height: 70,
        backgroundColor:'transparent',
        alignSelf:'center',
        overflow: 'hidden',
        flexDirection:'row',
        justifyContent:'flex-start',
        marginTop: 10,
      },
      availableCategories:{ 
        width:'100%',
        alignSelf:'center',
        minHeight:60,
        marginTop:10,
        backgroundColor:'#e3e5e8',
        borderRadius:10,
        // borderWidth:1,
        // borderColor:APP_GREY,
        elevation:5
      },

});

export default AddFeaturedBusiness;

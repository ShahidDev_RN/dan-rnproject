import React, { Component } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  View,
  Text,
  Image,
  StatusBar,
  Dimensions,
  ActivityIndicator,
  ImageBackground,
  FlatList
} from 'react-native';
import MapView, { Marker, ProviderProp } from 'react-native-maps';
import AsyncStorage from '@react-native-community/async-storage';
import { Rating, AirbnbRating } from 'react-native-ratings';
import  Share  from 'react-native-share';
import Icon from 'react-native-vector-icons/MaterialIcons';
import * as API from '../../Requests/WebService.js';
import {
  APP_BLACK,
  APP_GREY,
  APP_WHITE,
  BUSINESS_NAME,
  ADDRESS,
  DISTANCE,
  DIRECTIONS,
  AVAILABLE_HOURS,
  WEBSITE_LINK,
  Rating_HEADER,
  RESTAURANT,
  FONT_REGULAR,
  FONT_MEDIUM,
  FONT_SEMIBOLD,
  FONT_BOLD,
  SHARE,
  BACK_ICON,
  FAVORITE_INACTIVE,
  APP_BORDER,
  FAVORITE_DETAIL,
  ADD_PROFILE,
  EDIT_ICON,
  Map_Marker,
  FAVOURITE_ACTIVE,
  FAVOURITE_INACTIVE,
  map
} from '../../Constants/App_Constants.js';
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
import { times } from 'lodash';
import { TextInput } from 'react-native-gesture-handler';
const { width, height } = Dimensions.get('window')
const ASPECT_RATIO = width / height
const LATITUDE_DELTA = 0.005
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO

function showLoader() {
  return (
    <View style={{
      width: '100%', height: '100%',
      position: 'absolute', justifyContent: 'center',
      alignSelf: 'center', backgroundColor: 'transparent'
    }}>
      <View style={{
        alignSelf: 'center', borderRadius: 5, padding: 10,
        backgroundColor: 'transparent', opacity: 1, height: 100, width: 100,
        justifyContent: 'center', alignItems: 'center'
      }}>
        <ActivityIndicator
          style={{ height: 100, width: 100 }}
          size={Platform.OS === 'android' ? 100 : 'large'} color={'white'}
        />
      </View>
    </View>
  )
}

class BusinessHome extends Component {
  constructor(props) {
    super(props)
    this.state = {
      data: '',
      image: '',
      coordinates: '',
      isloading: true,
      businessReviews:'',
      businessuserFirstName:'',
      businessuserLastName:'',
      businessuserId:'',
      data2:'',
      isFavourite:false,
      // liked:true,
      liked:false
    }
    this.props.navigation.addListener(
      'didFocus',
      payload => {
        this.getData()
        //logEvent('MarketPlaceScreen')
        // console.log('payload', 'mjfygmgjmftggj')
      })
  }

  like_touch(value){
    this.setState({
      liked: value
    })
  
  }

  shareBusinessLink() {
    let options={
      title: "Business Screen",
      message:"Message",
      subject:"Subject",
      url:"URl"
    };
    
    Share.open(options)
      .then((res) => {
        console.log(res);
      })
      .catch((err)=> {
        err && console.log(err);
      })
    };

  async componentDidMount() {
    await this.getData(),
    this.User_Rating()
  }

 
  async getData() {
    await AsyncStorage.getItem('user_info', '').then(async (value) => {
       console.log('welcome', JSON.parse(value))

     // console.log('welcome---->', JSON.stringify(value))
      if (value === null || value === undefined || value === '') {
        // await this.props.changeLoginStatus(false)
        //SplashScreen.hide()

      } else {
        var businessData = JSON.parse(value)
        console.log(businessData,'businessDATA')
        // console.log(businessData.businessImages[0].image,'businessData==>')
        this.setState({
          data: businessData,
          image:businessData.businessImages.reverse()[0].image,
          coordinates: businessData.businessLocation.coordinates,
          businessuserId: businessData._id
        }, ()=>{
          // console.log('stateData', JSON.stringify(this.state.data))
         
          this.setState({ isloading: false })
          this.map_refs.animateToRegion({
            latitude: businessData.businessLocation.coordinates[0],
            longitude: businessData.businessLocation.coordinates[1],
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,  
            latitude: businessData.businessLocation.coordinates[0],
            
          })
        })
      }  
    });
  }

  User_Rating(){
    API.USER_REVIEWS(this.state.businessuserId).then((response)=> {
        console.log("User Rating to Business--->>>>",JSON.stringify(response))
        if(response.status==1){
          if(response.data.businessReviews.length > 0){

           }
            this.setState({
               data2:response.data.businessReviews,
            })
        }else{
          console.log("Errorrr Occurs")
        }
    })
  }
  
  maps(lat, long) {
    //let lat = this.state.coordinates[0]
    // let long = this.state.coordinates[1]
    console.log(lat, 'lat}{}')
    if (Platform.OS == 'ios') {
      return (
        <MapView
          style={Styles.Maps}
          mapType='standard'
          ref={(ref) => this.map_refs = ref}
          initialRegion={{
            //latitude: 30.7089,
            //longitude: 76.7018,
            latitude: lat,
            longitude: long,
            latitudeDelta:LATITUDE_DELTA,
            longitudeDelta:LONGITUDE_DELTA,
          }}
          scrollEnabled={true}>
          <Marker coordinate={{ latitude: lat, longitude: long }}>
            <Image resizeMode='contain' source={Map_Marker} style={{ height: 50, width: 50 }} />
          </Marker>
        </MapView>
      )
    } 
    else {
      return (
        <MapView
          style={Styles.Maps}
          mapType='standard'
          provider="google"
          showsMyLocationButton={true}
          showsUserLocation={true}
          ref={(ref) => this.map_refs = ref}
          initialRegion={{
            // latitude: 40.7127753,
            // longitude: -74.0059728,
            // latitude: 28.644800,
            // longitude: 77.216721,
            latitude: lat,
            longitude: long,
            latitudeDelta:LATITUDE_DELTA,
            longitudeDelta:LONGITUDE_DELTA,
          }}
          scrollEnabled={true}>
          <Marker coordinate={{ latitude: lat,longitude: long, }}>
            <Image resizeMode='contain' source={Map_Marker} style={{ height: 50, width: 50 }} />
          </Marker>
        </MapView>
       )
    }
  }

  render() {
    return (
      <SafeAreaView style={Styles.SafeArea}>
        <StatusBar barStyle={'dark-content'}/>
        {/* <OrientationLoadingOverlay
                    visible={this.state.isloading}
                    color="white"
                    indicatorSize="large"
                /> */}
        <ScrollView style={{flex:1}}>
          <ImageBackground
            source={{ uri: this.state.data !== '' ? this.state.image : '' }}
            style={Styles.Image}>
            <View style={Styles.IconContainer}>
              <View style={{ backgroundColor: 'transparent', width: "65%" }}>
                {/* <TouchableOpacity style={{ width: 40, height: 30, }} onPress={() => navigation.navigate('AppLogin')}>
                            <Image source={BACK_ICON} style={[Styles.Icons], { alignSelf: 'center', tintColor: APP_BLACK }} />
                          </TouchableOpacity> */}
              </View>
              <View style={{ flexDirection: 'row', width: '25%',marginLeft:20}}>
                <TouchableOpacity onPress={() => this.like_touch(!this.state.liked)}>
                <Image source={this.state.liked ? FAVOURITE_INACTIVE:FAVOURITE_ACTIVE} style={{ alignSelf: 'center', height:23,width:26 }}/>
                </TouchableOpacity>
                {/* <TouchableOpacity style={{ width: 40, height: 30,backgroundColor: 'red' }}>
                  <Image source={this.state.isFavourite ? FAVOURITE_ACTIVE:FAVOURITE_INACTIVE} style={[Styles.Icons], { alignSelf: 'center', tintColor: APP_BLACK }} />
                </TouchableOpacity> */}
                <TouchableOpacity onPress={()=>this.shareBusinessLink()} style={{ width: 40, height: 30, backgroundColor: 'transparent',}}>
                  <Image source={SHARE} style={[Styles.Icons], { alignSelf: 'center', tintColor: APP_BLACK }} />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Edit_BusinessProfile')} style={{ width: 37, height: 27, backgroundColor: 'transparent' }}>
                  <Image source={EDIT_ICON} style={[Styles.Icons], { alignSelf: 'center', tintColor: APP_BLACK }} />
                </TouchableOpacity>
              </View>
            </View>

          </ImageBackground>
          <View style={Styles.Border} />
          <View style={Styles.TextContainer}>
            <Text style={Styles.TextHeader}>{BUSINESS_NAME}</Text>
            <Text style={Styles.TextNormal}>{this.state.data !== '' ?  this.state.data.businessName:'' }</Text>
          </View>
          <View style={Styles.Border} />
          <View style={Styles.TextContainer}>
            <Text style={Styles.TextHeader}>{ADDRESS}</Text>
            <Text style={Styles.TextNormal}>{this.state.data !== '' ? this.state.data.businessAddress : ''}</Text>
          </View>
          <View style={Styles.Border} />
          <View style={Styles.TextContainer}>
            <Text style={Styles.TextHeader}>Email Address</Text>
            <Text style={Styles.TextNormal}>{this.state.data !== '' ? this.state.data.businessuserEmail : ''}</Text>
          </View>
          <View style={Styles.Border} />
          <View style={Styles.TextContainer}>
            <Text style={Styles.TextHeader}>Phone Number</Text>
            <Text style={Styles.TextNormal}>{this.state.data !== '' ? this.state.data.businessuserPhoneNumber : ''}</Text>
          </View>

          {/* <View style={Styles.Border} />
                    <View style={Styles.TextContainer}>
                      <Text style={Styles.TextHeader}>{DISTANCE}</Text>
                      <Text style={Styles.TextNormal}>2KM Far</Text>
                    </View> */}

          <View style={Styles.Border} />
          <View style={Styles.TextContainer}>
            <Text style={Styles.TextHeader}>{AVAILABLE_HOURS}</Text>
            <Text style={Styles.TextNormal}>24 Hours</Text>
          </View>
          <View style={Styles.Border} />
          <View style={Styles.TextContainer}>
            <Text style={Styles.TextHeader}>{DIRECTIONS}</Text>
            <View style={Styles.MapsContainer}>
              {this.maps(this.state.coordinates[0], this.state.coordinates[1])}
            </View>
          </View>
          <View style={Styles.Border} />
          <View style={Styles.TextContainer}>
            <Text style={Styles.TextHeader}>{WEBSITE_LINK}</Text>
            <Text style={[Styles.TextNormal, { color: 'dodgerblue' }]}>{this.state.data.businesswebsiteLink}</Text>
          </View>
          <View style={Styles.Border} />
          
          <View style={[Styles.TextContainer, { flexDirection: 'row', justifyContent: 'space-between' }]}>
                    <Text style={Styles.TextHeader}>{Rating_HEADER}</Text>
                    {/* <TouchableOpacity>
                      <Image source={ADD_ICON} style={Styles.AddIcon} />
                    </TouchableOpacity> */}
                  </View>
         
          <FlatList
            data={this.state.data2}
            scrollEnabled={true}
            renderItem={({ item, index }) => {
              return (
                <View>
                  <View style={Styles.RatingContainer}>
                    <Image
                      source={{uri:'https://apps.appsmaventech.com/onyx/'+item.userId.userImage}}
                      style={{
                        width: 50,
                        height: 50,
                        borderRadius: 10,
                      }}
                    />
                    <View style={Styles.ReviewContainer}>
                      <View
                        style={{
                          flexDirection: 'row',
                          alignContent: 'center',
                        }}>
                        <Text style={Styles.ReviewName}>{item.userId.userFirstName} {item.userId.userLastName}</Text>
                        <Rating
                          style={Styles.RatingBar}
                          startingValue={item.rating}
                          type="custom"
                          imageSize={20}
                          readonly
                          tintColor={APP_WHITE}
                          ratingBackgroundColor={APP_GREY}
                        />
                      </View>
                      <Text style={Styles.ReviewText}>{item.reviews}
                      </Text>
                    </View>
                  </View>
                </View>
              )
            }}
            keyExtractor={(item, index) => index.toString()}
          ></FlatList>
       
        </ScrollView>
        {this.state.isloading ? <View style={{
          width: '100%', height: '100%',
          position: 'absolute', justifyContent: 'center',
          alignSelf: 'center', backgroundColor: 'transparent'
        }}>
          <View style={{
            alignSelf: 'center', borderRadius: 5, padding: 10,
            backgroundColor: 'transparent', opacity: 1, height: 100, width: 100,
            justifyContent: 'center', alignItems: 'center'
          }}>
            <ActivityIndicator
              style={{ height: 100, width: 100 }}
              size={Platform.OS === 'android' ? 100 : 'large'} color={'white'}
            />
          </View>
        </View> : null}
      </SafeAreaView>
    );
  }


}
const Styles = StyleSheet.create({
  SafeArea: {
    flex: 1,
    backgroundColor: APP_WHITE,
  },
  Image: {
    width: Dimensions.get('window').width,
    height:250,
    resizeMode: 'cover',
  },
  IconContainer: {
    paddingHorizontal: 0,
    marginTop: 20,
    flexDirection: 'row',
    width: '100%',
    backgroundColor: 'transparent',
    //justifyContent:'space-around'
  },
  Icons: {
    width: 30,
    height: 50,
    marginTop: 0,
    resizeMode: 'contain',
    tintColor: APP_BLACK,
  },
  AddIcon: {
    width: 20,
    height: 20,
    marginTop: 0,
    right: 10,
    resizeMode: 'contain',
    tintColor: APP_WHITE,
  },
  Border: {
    backgroundColor: '#e3e5e8',
    width: Dimensions.get('window').width,
    height: 2,
  },
  TextContainer: {
    paddingHorizontal: 10,
    paddingVertical: 20,
  },
  TextHeader: {
    //fontWeight: 'normal',
    color: APP_BLACK,
    fontSize: 14,
    fontFamily: FONT_MEDIUM
  },
  TextNormal: {
    color: APP_GREY,
    fontFamily: FONT_MEDIUM,
    marginTop: 5,
    fontSize: 14,
  },
  MapsContainer: {
    width: '100%',
    height: 160,
    alignSelf: 'center',
    overflow: 'hidden',
    borderRadius: 10,
    marginTop: 10,
  },
  Maps: { flex: 1 },
  Icon: {
    alignSelf: 'center',
    position: 'absolute',
    right: 20,
    color: APP_WHITE,
    fontSize: 25,
  },
  RatingContainer: {
    flex: 1,
    flexDirection: 'row',
    paddingVertical: 15,
    paddingStart: 20,
  },
  ReviewContainer: {
    flex: 1,
    flexDirection: 'column',
    marginStart: 20,
    marginEnd: 20,
  },
  ReviewName: {
    color: APP_BLACK,
    fontSize: 16,
    fontFamily: FONT_SEMIBOLD
  },
  ReviewText: {
    color: APP_GREY,
    marginTop: 5,
    fontSize: 11,
    fontFamily: FONT_REGULAR
  },
  RatingBar: {
    flex: 1,
    position: 'absolute',
    right: 0,
    alignSelf: 'center',
  },
});

export default BusinessHome
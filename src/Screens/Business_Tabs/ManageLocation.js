import React, {Component} from 'react';
import {View,
    Dimensions ,
    Text,
    StyleSheet,
    TouchableOpacity,
    Image,
    TextInput,
    BackHandler,
    Alert,
    SafeAreaView,
    Platform,
    ScrollView,
    KeyboardAvoidingView,
    ImageBackground,FlatList} from 'react-native';
    import AsyncStorage from '@react-native-community/async-storage';
    import * as API from '../../Requests/WebService.js'
    import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
    import {FONT_BLACK,FONT_BOLD,FONT_EXTRABOLD,FONT_EXTRALIGHT,FONT_LIGHT,FONT_SEMIBOLD,SEARCH_ICON_INACTIVE,FILTER,RESTAURANT,ADD_PROFILE,HEADER_ICON,
        FONT_THIN,APP_WHITE,APP_BLACK,APP_FACEBOOK,BACK_ICON, FONT_REGULAR,FONT_MEDIUM,NOTIFICATION_ICON, APP_GREY, RATTING, ADD_IMAGE, ADD_DOCUMENT, ADD_LOCATION,} from '../../Constants/App_Constants.js'
    let { width, height } = Dimensions.get('window')
    const ASPECT_RATIO = width / height
class ManageLocation extends Component {
    constructor(props){
        super(props);
        this.state={
            businessName:'',
            image:'',
            data:'',
            fontsize:13,
            isloading:false
        }  
        this.props.navigation.addListener(
            'didFocus',payload => {
                AsyncStorage.getItem("user_info").then((value) => { 
                    var data= JSON.parse(value)
                   this.setState({businessName:data.businessName,image:data.businessImages[0].image})
                   API.GET_BUSINESS_LOCATION(data._id).then((response)=>{
                    this.setState({data:response.data.businessLocation})
                   })
                }).done();
            }
          );
      } 

      header(){ 
        if(Platform.OS=='ios'){
          return(
            <View style={{height:75,width:'100%',backgroundColor:'white',justifyContent:'flex-end',borderBottomColor:'#e3e5e8',borderBottomWidth:3}}>
                <View style={{height:'80%',width:'95%',backgroundColor:'transparent',alignSelf:'center'}}>
                    <View style={{flexDirection:'row',justifyContent:'space-between',height:'100%',width:'100%',backgroundColor:'transparent',alignSelf:'flex-start'}}>
                        <View style={{flexDirection:'row',width:200,backgroundColor:'transparent',justifyContent:'flex-start'}}>
                            <TouchableOpacity onPress={()=>this.props.navigation.goBack()} style={{height:'100%',backgroundColor:'transparent',width:40,alignItems:'center'}}>
                                <Image source={HEADER_ICON} style={{marginTop:10, height:32,width:32}}></Image>
                            </TouchableOpacity>
                            <Text style={{color:APP_BLACK,fontSize:20,marginTop:14,fontFamily:FONT_MEDIUM,backgroundColor:'transparent',marginLeft:height<896?10:15,width:'100%'}}>Manage Locations</Text>
                        </View>
                    </View>
                </View>
            </View>
          )
        }else{
          return(
            <View style={{height:65,width:'100%',backgroundColor:'white',justifyContent:'flex-end',borderBottomColor:'#e3e5e8',borderBottomWidth:3}}>
                <View style={{height:'80%',width:'95%',backgroundColor:'transparent',alignSelf:'center'}}>
                    <View style={{flexDirection:'row',justifyContent:'space-between',height:'100%',width:'100%',backgroundColor:'transparent',alignSelf:'flex-start'}}>
                        <View style={{flexDirection:'row',width:200,backgroundColor:'transparent',justifyContent:'flex-start'}}>
                            <TouchableOpacity onPress={()=>this.props.navigation.goBack()} style={{height:'100%',backgroundColor:'transparent',width:40,alignItems:'center'}}>
                                <Image source={HEADER_ICON} style={{marginTop:10, height:32,width:32}}></Image>
                            </TouchableOpacity>
                            <Text style={{color:APP_BLACK,fontSize:18,marginTop:14,fontFamily:FONT_MEDIUM,backgroundColor:'transparent',marginLeft:height<896?10:15,width:'100%'}}>Manage Locations</Text>
                        </View>
                    </View>
                </View>
            </View>
          )
        }
      }

      componentDidMount(){
        AsyncStorage.getItem("user_info").then((value) => { 
            var data= JSON.parse(value)
           this.setState({businessName:data.businessName,image:data.businessImages[0].image,isloading:true})
           API.GET_BUSINESS_LOCATION(data._id).then((response)=>{
            this.setState({data:response.data.businessLocation,isloading:false})
           })
        }).done(); 
      }

    render(){
        return(
            <SafeAreaView forceInset={{ top: 'always' }} style={{flex:1,backgroundColor:APP_WHITE}}>
                {this.header()}
                <OrientationLoadingOverlay
                visible={this.state.isloading}
                color="white"
                indicatorSize="large"
                />
                <View style={styles.nearbyTextView}>      
                    <View style={styles.listView}>
                    <FlatList
                        data={this.state.data}
                        showsHorizontalScrollIndicator={false}
                        numColumns={2}
                        contentContainerStyle={{width:'95%',backgroundColor:APP_WHITE,alignSelf:'center',minHeight:600,alignItems:'flex-start'}}
                        renderItem={({ item: data, index }) => {
                            var margin=0
                            var marginTop=0
                          var font=13
                            if(index%2==0||index==0){
                                margin=10
                            }else{
                                margin=height<896?'5%':'5%'
                            }
                            if(index==0||index==1){
                                marginTop=20
                            }else{
                                marginTop=10
                            }
                            if(data.businessLocationAddress.length>17){
                                font=11
                            }
                            
                            return(    
                                <TouchableOpacity style={[{marginLeft:margin,marginTop:marginTop},styles.FeaturedView]} activeOpacity={1}>
                                    <Image style={{height:'75%',width:'100%',borderRadius:12,borderColor:APP_GREY,borderWidth:1}} source={{uri:this.state.image}}>
                                    </Image>
                                    <View style={styles.downView}>
                                        <Text style={styles.textName}>{this.state.businessName}</Text> 
                                        <Text style={[{fontSize:font},styles.textDiscription]}>{data.businessLocationAddress}</Text>
                                </View>
                                </TouchableOpacity>
                            )
                        }}keyExtractor={item => item.id}/>

                        <TouchableOpacity onPress={()=>this.props.navigation.navigate('AddLocation')}
                            style={{height:75,width:75,backgroundColor:'transparent',justifyContent:'flex-start',borderRadius:50,position:'absolute',right:20,top:height<896?height-290:height-350}}>
                            <Image source={ADD_LOCATION} style={{alignSelf:'flex-start',backgroundColor:'transparent',borderRadius:50,tintColor:APP_BLACK}}></Image>
                        </TouchableOpacity>
                </View> 
                </View>
            </SafeAreaView>
        )
    }
};

const styles = StyleSheet.create({
  
  feildBorder:{
      width:290,
      height:35,
      borderRadius:30,
      borderColor:APP_BLACK,
      backgroundColor:APP_WHITE,
      marginTop:9,
      justifyContent:"space-between",
      flexDirection:'row'
  },
 
textfeild:{
    width:'88%',
    height:34,
    fontSize:11,
    fontFamily:FONT_MEDIUM,
    borderRadius:30,
    alignSelf:'center',
    color:APP_BLACK,
    backgroundColor:APP_WHITE
    },

nearbyTextView:{
    height:'auto',
    width:width,
    backgroundColor:APP_WHITE
},
nearbyTextInnerView:{
    height:50,
    width:'90%',
    backgroundColor:APP_WHITE,
    alignSelf:'center'
},
nearbyText:{
    fontFamily:FONT_MEDIUM,
    fontSize:14,
    color:APP_BLACK,
    alignSelf:'flex-start',
    marginTop:18
},
listView:{
    height:'auto',
    width:'100%',
    alignSelf:'center',
    marginBottom:70,
    backgroundColor:APP_WHITE
},
singleListview:{
    height:220,
    width:'100%',
    backgroundColor:APP_WHITE,
    marginBottom:15
},
NearbyView:{
    height:160,
    width:'100%',
    borderRadius:15,
    alignSelf:'flex-start',
    backgroundColor:'transparent'
},
downView:{
    height:60,
    backgroundColor:"transparent"
},
textName:{
    fontFamily:FONT_MEDIUM,
    fontSize:14,
    marginTop:4,
    marginLeft:1,
    color:APP_BLACK
},
textDiscription:{
    fontFamily:FONT_MEDIUM,
    marginTop:3,
    color:APP_GREY,
    marginLeft:1
},
imageStar:{
    height:9,
    width:9,
    marginTop:5,
    marginLeft:3
},
rattingView:{
    width:"100%",
    height:'auto',
    backgroundColor:APP_BLACK,
    flexDirection:'row',
    justifyContent:'space-between'
},
rattingNumber:{
    color:APP_WHITE,
    fontSize:10,
    fontFamily:FONT_MEDIUM,
    marginTop:4
},
FeaturedView:{
    height:height<896?230:230,
    width:height<896?155:170,
    alignSelf:'flex-start',
    backgroundColor:'transparent'
},
addImage:{
    height:50,
    width:50,
    position:'absolute',
}

});

export default ManageLocation;

import React, { Component } from 'react';
import {
    View,
    Dimensions,
    Text,
    StyleSheet,
    TouchableOpacity,
    Image,
    FlatList,
    TextInput,
    BackHandler,
    Alert,
    SafeAreaView,
    Platform,
    ScrollView,
    KeyboardAvoidingView, StatusBar,
    ImageBackground,
    PermissionsAndroid
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
import * as API from '../../Requests/WebService.js'
import Star from 'react-native-star-view';
import { Rating } from 'react-native-ratings';
// import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import {
    FONT_BLACK, FONT_BOLD, FONT_EXTRABOLD, FONT_EXTRALIGHT, FONT_LIGHT, FONT_SEMIBOLD,
    FONT_THIN, APP_WHITE, APP_BLACK, APP_FACEBOOK, BACK_ICON, USER_PLACEHOLDER, RESTAURANT, HEADER_ICON,
    FONT_REGULAR, FONT_MEDIUM, NOTIFICATION_ICON, APP_GREY, FAVORITE_ACTIVE, FAVORITE_INACTIVE, REVIEW_ACTIVE, REVIEW_INACTIVE, RATTING, APP_BORDER, ADD_IMAGE
} from '../../Constants/App_Constants.js'
import ImagePicker from 'react-native-image-picker';
import ImageResizer from 'react-native-image-resizer';
let { width, height } = Dimensions.get('window')
const ASPECT_RATIO = width / height

const createFormData = (image) => {
    var data = new FormData();
    data.append('image', {
        uri: Platform.OS === "android" ? image.uri : image.uri.replace("file://", ""),
        name: `ony${Date.now()}.jpg`,
        type: 'image/*'
    })
    return data
}
class Account extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            email: '',
            description: 'No description yet',
            isloading: true,
            tabSelector: true,
            bottomColorReview: APP_BORDER,
            bottomColorFavorite: APP_WHITE,
            tabIconReview: REVIEW_ACTIVE,
            tabIconFavorite: FAVORITE_INACTIVE,
            data: [1, 2],
            // data2:[1],
            data2: '',
            emailFontSize: 13,
            descriptionScroll: false,
            selectedImg: '',
            businessId: '',
            userProfileImg: '',
            businessReviews: '',
            businessName: '',
            businessImages: '',
            rating: '',
            data3:''
        }
        console.log(this.state.businessId, 'okokok')

        this.props.navigation.addListener(
            'didFocus', payload => {
                AsyncStorage.getItem("user_token").then((value) => {
                    this.onFetchViewProfile(value)
                    this.Favourite_Data()
                    this. RATED_BUSINESS(value)
                }).done();
            }
        );
    }



    pickImage() {
        if (Platform.OS === 'android') {
            PermissionsAndroid.requestMultiple(
                [PermissionsAndroid.PERMISSIONS.CAMERA,
                PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
                PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE]
            ).then((result) => {
                if (result['android.permission.CAMERA']
                    && result['android.permission.READ_EXTERNAL_STORAGE']
                    && result['android.permission.WRITE_EXTERNAL_STORAGE'] === 'granted') {
                    ImagePicker.showImagePicker(async (response) => {
                        if (response.didCancel) {
                            console.log('User cancelled image picker');
                        } else if (response.error) {
                            console.log('ImagePicker Error: ', response.error);
                        } else {
                            //  console.log("check response of image picker ",response)
                            this.setState({
                                selectedImg: response.uri
                            })
                            console.log("check imag upload ???????????????")
                            this._compressImage(response)

                            // let res=await API.FILE_UPLOAD_USER(response.uri)
                            // console.log("check image upload api code ",res)
                        }
                    })
                } else if (result['android.permission.CAMERA']
                    || result['android.permission.READ_EXTERNAL_STORAGE']
                    || result['android.permission.WRITE_EXTERNAL_STORAGE'] === 'never_ask_again') {
                    alert("Required permissions denied");
                }
            });
        } else {
            ImagePicker.showImagePicker(async (response) => {
                if (response.didCancel) {
                    console.log('User cancelled image picker');
                } else if (response.error) {
                    console.log('ImagePicker Error: ', response.error);
                } else {
                    const source = response;
                    //  console.log("check response of image picker ",response)
                    this.setState({
                        selectedImg: response.uri
                    })
                    console.log("check imag upload ???????????????")
                    this._compressImage(response)

                }
            })
        }
    }



    async _compressImage(source) {
        console.log("check in comparess")
        var v = this;
        let response = await ImageResizer.createResizedImage(source.uri, 400, 400, 'JPEG', 80);
        if (response != null) {
            v.imageUpload(response)
        }
    }


    async imageUpload(source) {
        console.log("check in comparess image upload", source)
        let newToken = await AsyncStorage.getItem("user_token")

        try {
            const response = await fetch(
                'https://apps.appsmaventech.com/onyx/v1/uploadImage',
                {
                    'method': 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data',
                        'token': newToken

                    },
                    body: createFormData(source)
                }
            );
            // let res=await response.json()
            console.log("check response of image upload ///////////////////////////", response)

            if (response.status == 200) {
                return Alert.alert('Profile pic uploaded successfully')
            } else {
                console.log('error', response)
            }
        } catch (error) {
            console.error(error);
        }
    }


    header() {
        if (Platform.OS == 'ios') {
            return (
                <View style={{ height: 65, width: '100%', backgroundColor: 'white', justifyContent: 'flex-end', borderBottomColor: '#e3e5e8', borderBottomWidth: 3 }}>
                    <View style={{ height: '80%', width: '95%', backgroundColor: 'transparent', alignSelf: 'center' }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', height: '100%', width: '50%', backgroundColor: 'transparent', alignSelf: 'flex-start' }}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('Home')}>
                                <Image source={HEADER_ICON} style={{ marginTop: 6, height: 32, width: 32 }}></Image>
                            </TouchableOpacity>
                            <Text style={{ color: APP_BLACK, fontSize: 19, marginTop: 10, fontFamily: FONT_SEMIBOLD, width: 150, marginLeft: height < 896 ? 20 : 0 }}>Account</Text>
                        </View>
                    </View>
                </View>
            )
        } else {
            return (
                <View style={{ height: 65, width: '100%', backgroundColor: 'white', justifyContent: 'flex-end', borderBottomColor: '#e3e5e8', borderBottomWidth: 3 }}>
                    <View style={{ height: '80%', width: '95%', backgroundColor: 'transparent', alignSelf: 'center' }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'flex-start', height: '100%', width: '100%', backgroundColor: 'transparent', alignSelf: 'flex-start' }}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('Home')}>
                                <Image source={HEADER_ICON} style={{ marginTop: 6, height: 32, width: 32 }}></Image>
                            </TouchableOpacity>
                            <Text style={{ color: APP_BLACK, fontSize: 19, marginTop: 10, fontFamily: FONT_SEMIBOLD, width: 150, marginLeft: 12 }}>Account</Text>
                        </View>
                    </View>
                </View>
            )
        }
    }

    componentDidMount() {
        AsyncStorage.getItem("user_token").then((value) => {
            this.onFetchViewProfile(value)
            this.Favourite_Data()
           // this.Favourite_Data()
            this.RATED_BUSINESS(value)
        }).done();
       
    }

    Favourite_Data() {
        API.GET_WISHLIST().then((response) => {
            // console.log('businessUserId', JSON.stringify(response.data.getBusiness[0]._id))
            //this.setState({ singleData: response.data.getBusiness[0], isloading: false })

            console.log("check reponsep of get wish list////", JSON.stringify(response.data[0]), '?????????')
            if (response.status == 1) {
                this.setState({
                    data2: response.data,
                    businessId: response.data[0]._id,
                    
                }, () => console.log('businessID', this.state.businessId))

            } else {
                console.log('nothing')
            }
        })
    }


    RATED_BUSINESS(token) {
        API.GET_BUSINESS_REVIEWS(token).then((response) => {
            console.log("Checkkk Get Business LISt>>>>>>>>>>", JSON.stringify(response))
            if (response.status == 1) {
                this.setState({
                    data3: response.data.businessReviews,
                    businessName: response.data.businessReviews[0].businessName,
                    businessImages: response.data.businessReviews[0].businessImages[0].image,
                    businessReviews: response.data.businessReviews.reviews,
                    rating: response.data.businessReviews[0].businessReviews.rating,
                },()=>{console.log(this.state.businessImages,'businessiMages-->')})
            }else {
                console.log("Errorrrrr businesss reviewssss")
            }
        })
    }

    onFetchViewProfile(token) {
        API.VIEW_PROFILE(token).then((response) => {
            if (response.status == 1) {
                AsyncStorage.setItem("user_information", JSON.stringify(response), () => { });
                this.setState({
                    email: response.data.user.userEmail, username: response.data.user.userFirstName + ' ' + response.data.user.userLastName,
                    description: response.data.user.userdescription,
                    userProfileImg: 'https://apps.appsmaventech.com/onyx/' + response.data.user.userImage,
                    isloading: false
                }, () => {
                    if (response.data.user.userdescription != undefined) {
                        if (this.state.description.length > 160) {
                            this.setState({ descriptionScroll: true })
                        } else {
                            this.setState({ descriptionScroll: false })
                        }
                    }
                })
                if (response.data.user.userEmail.length > 20) {
                    this.setState({ emailFontSize: 11 })
                }
            } else if (response.status == 0) {
                this.setState({ serverError: true, serverErrorText: response.message, isloading: false })
            }
        })
    }


    tabsView() {
        //true==review, false==favorite
        if (this.state.tabSelector) {
            return (
               
                <View style={{ flex:1, width: width,backgroundColor:"#e3e5e8"
                //  backgroundColor: APP_WHITE 
                 }}>

                    <FlatList
                        data={this.state.data3}
                        showsVerticalScrollIndicator={false}
                      
                        contentContainerStyle={{  width: width, backgroundColor: "#e3e5e8" }}
                      //  style={{flex:1}}
                        renderItem={({ item, index }) => {
                            console.log('rating', JSON.stringify(item.businessReviews.rating))
                            return (
                                <View style={{ minHeight: 50, width: '90%', backgroundColor: 'transparent', alignSelf: 'center', flexDirection: 'row', marginTop: 20 }}>
                                    <View style={{ width: "18%", backgroundColor: 'transparent' }}>
                                        <View style={{ width: 45, height: 45, backgroundColor: 'black', alignSelf: 'flex-start', marginTop: 2, borderRadius: 10 }}> 
                                            <Image source={{ uri: item.businessImages[0].image }} style={{ height: '100%', width: '100%', borderRadius: 10 }}></Image>   
                                        </View>
                                    </View>
                                    <View style={{ width: "82%", backgroundColor: 'transparent', marginBottom: 20 }}>
                                        <View style={{ height: 25, width: '97%', flexDirection: 'row', justifyContent: 'space-between', backgroundColor: 'transparent' }}>
                                            <Text style={{ fontSize: 13, fontFamily: FONT_SEMIBOLD, color: APP_BLACK }}>{item.businessName}</Text>
                                            <View >
                                                <Rating
                                                    ratingCount={5}
                                                    type={'star'}
                                                    startingValue={item.businessReviews.rating}
                                                    fractions={1}
                                                    readonly
                                                    jumpValue={0.5}
                                                    imageSize={20}
                                                    tintColor="#e3e5e8"
                                                />
                                         </View>  
                                        </View>
                                        <View style={{ height: 'auto', width: '100%', backgroundColor: 'transparent' }}>
                                            <Text style={{ fontSize: 10, fontFamily: FONT_MEDIUM, color: APP_BLACK }}>{item.businessReviews.reviews}</Text>
                                        </View>
                                    </View>
                                </View>
                            )
                        }
                        }
                        keyExtractor={(item, index) => index.toString()} />
                </View>
            )
        } else {
            let h = parseFloat(height - 270)
            // alert(h)

            return (
                <View style={{ flex:1, width: width, backgroundColor:"#e3e5e8"
               // backgroundColor: APP_WHITE
                 }}>
                    {/* height: Platform.OS==='android'?350:height - 270 */}
                    <FlatList
                        data={this.state.data2}
                        showsVerticalScrollIndicator={false}
                        contentContainerStyle={{ width: width, backgroundColor: '#e3e5e8', }}
                        renderItem={({ item, index }) => {
                            let hour = item.availablehours[index]
                             console.log('accountScreenHour', JSON.stringify(item))
                            return (
                                <View>
                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('SingleDetail', { businessId: item._id })}>
                                        <View style={{ flex: 1, minHeight: 60, width: '90%', backgroundColor: 'transparent', alignSelf: 'center', flexDirection: 'row', marginTop: 20 }}>
                                            <View style={{ width: "18%", backgroundColor: 'transparent' }}>
                                                <View style={{ width: 45, height: 45, backgroundColor: 'black', alignSelf: 'flex-start', marginTop: 2, borderRadius: 10 }}>
                                                    {item.businessImages.map((value) =>
                                                        <Image source={{ uri: value.id == 1 ? null : value.image }} style={{ height: '100%', width: '100%', borderRadius: 10 }} />
                                                    )}
                                                </View>
                                            </View>

                                            <View style={{ width: "82%", backgroundColor: 'transparent', marginBottom: 20 }}>
                                                <View style={{ height: 25, width: '97%', flexDirection: 'row', justifyContent: 'space-between', backgroundColor: 'transparent' }}>
                                                    <View style={{ flexDirection: "column", marginTop: 1 }}>
                                                        <Text style={{ fontSize: 13, fontFamily: FONT_SEMIBOLD, color: APP_BLACK }}>{item.businessName}</Text>
                                                        <Text style={{ fontSize: 13, fontFamily: FONT_SEMIBOLD, color: APP_BLACK }}>{item.businessAddress}</Text>
                                                    </View>    
                                                <View>
                                                   {item.businessReviews.length>0 && <Rating
                                                    ratingCount={5}
                                                    type={'star'}
                                                    startingValue={item.businessReviews[0].rating}
                                                    fractions={1}
                                                    readonly
                                                    jumpValue={0.5}
                                                    imageSize={20}
                                                    tintColor="#e3e5e8"/>}   
                                                    </View>
                                                </View>  
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            )
                        }
                        }
                        keyExtractor={(item, index) => index.toString()} />
                </View>
            )
        }
    }

    tabBarReviewControl() {
        this.setState({
            bottomColorReview: APP_BORDER, bottomColorFavorite: APP_WHITE,
            tabIconReview: REVIEW_ACTIVE, tabIconFavorite: FAVORITE_INACTIVE, tabSelector: true
        })
    }

    tabBarFavoriteControl() {
        this.setState({
            bottomColorReview: APP_WHITE, bottomColorFavorite: APP_BORDER,
            tabIconFavorite: FAVORITE_INACTIVE, tabIconReview: REVIEW_INACTIVE, tabSelector: false
        })
    }


    render() {
        return (
            <SafeAreaView forceInset={{ top: 'always' }} style={{ flex: 1, backgroundColor: APP_WHITE }}>
                <StatusBar barStyle={'dark-content'}></StatusBar>
                {this.header()}
                <OrientationLoadingOverlay
                    visible={this.state.isloading}
                    color="white"
                    indicatorSize="large"
                />
                <View style={styles.UpperView}>
                    <View style={styles.InnerView}>
                        <View style={styles.ProfileImage}>
                            <TouchableOpacity onPress={() =>
                                this.pickImage()
                            }>
                                <View style={styles.Image}>
                                    <Image source={this.state.selectedImg == '' ? { uri: this.state.userProfileImg } : { uri: this.state.selectedImg }} style={{ height: '100%', width: '100%', borderRadius: 50 }}></Image>
                                    {/* <Image source={this.state.selectedImg==''? USER_PLACEHOLDER:{uri:this.state.selectedImg}} style={{ height: '100%', width: '100%', borderRadius: 50 }}></Image> */}
                                    <TouchableOpacity style={{ height: 20, width: 20, alignSelf: 'center', position: 'absolute', backgroundColor: 'white', borderRadius: 10, top: 60, left: 60 }}>
                                        <Image source={ADD_IMAGE} style={{ height: 20, width: 20, borderRadius: 10, borderWidth: 0.5, borderColor: APP_WHITE }}></Image>
                                    </TouchableOpacity>
                                </View>
                            </TouchableOpacity>
                            <Text style={styles.nameText}>{this.state.username}</Text>
                        </View>
                        <View style={styles.Detail}>
                            <Text style={{
                                fontFamily: FONT_SEMIBOLD,
                                alignSelf: 'flex-start',
                                color: APP_BLACK,
                                marginTop: 20,
                                marginLeft: 10, fontSize: this.state.emailFontSize
                            }}>{this.state.email}</Text>
                            <ScrollView showsVerticalScrollIndicator={false} scrollEnabled={this.state.descriptionScroll}>
                                <Text style={styles.detailText}>{this.state.description}</Text>
                            </ScrollView>

                        </View>
                    </View>
                    <View style={styles.button}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('EditAccount')} style={{ height: '100%' }}>
                            <Text style={styles.buttonText}>Edit Account</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={styles.tabButtons}>
                        <TouchableOpacity onPress={() => this.tabBarReviewControl()}>
                            <View style={styles.tabs}>
                                <Image style={styles.tabImageReview} source={this.state.tabIconReview}></Image>
                                <View style={[{ backgroundColor: this.state.bottomColorReview }, styles.tabBottom1]}></View>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.tabBarFavoriteControl()}>
                            <View style={styles.tabs2}>
                                <Image style={styles.tabImageFavorite} source={this.state.tabIconFavorite}></Image>
                                <View style={[{ backgroundColor: this.state.bottomColorFavorite }, styles.tabBottom2]}></View>
                            </View>
                        </TouchableOpacity>
                    </View>
                    {this.tabsView()}
                </View>

            </SafeAreaView>
        )
    }
};

const styles = StyleSheet.create({
    UpperView: {
        //height: 270,
        flexDirection: 'column',
        width: width,
        backgroundColor: APP_WHITE,
        flex:1
    },
    InnerView: {
        height: 'auto',
        width: '90%',
        marginTop: 20,
        flexDirection: 'row',
        alignSelf: 'center',
        backgroundColor: APP_WHITE
    },
    ProfileImage: {
        height: 120,
        width: 100,
        backgroundColor: APP_WHITE
    },
    Detail: {
        height: 120,
        width: 217,
        backgroundColor: APP_WHITE
    },
    Image: {
        height: 80,
        width: 80,
        alignSelf: 'flex-start',
        marginTop: 10,
        borderRadius: 50,
        borderWidth: 0.5,
        borderColor: '#e3e5e8',
        backgroundColor: APP_GREY
    },
    nameText: {
        fontFamily: FONT_SEMIBOLD,
        color: APP_BLACK,
        alignSelf: 'flex-start',
        marginTop: 10,
        marginLeft: 10,
        fontSize: 12,
        width: 'auto'
    },
    detailText: {
        fontFamily: FONT_MEDIUM,
        alignSelf: 'flex-start',
        color: APP_BLACK,
        marginTop: 6,
        marginLeft: 10,
        marginBottom: 10,
        fontSize: 12
    },
    emailText: {
        fontFamily: FONT_SEMIBOLD,
        alignSelf: 'flex-start',
        color: APP_WHITE,
        marginTop: 20,
        marginLeft: 10,
        //fontSize:13
    },
    button: {
        width: width - 45,
        height: 45,
        borderRadius: 30,
        //borderWidth:0.5,
        marginTop: 20,
        borderColor: 'white',
        backgroundColor: '#e3e5e8',
        alignSelf: 'center',
        shadowOffset: {
            width: 0,
            height: 1
        },
        shadowRadius: 1,
        shadowOpacity: 0.5,
        elevation: Platform.OS == 'android' ? 5 : 0
    },
    buttonText: {
        fontSize: 13,
        fontFamily: FONT_SEMIBOLD,
        color: APP_BLACK,
        alignSelf: 'center',
        marginTop: 13
    },
    tabButtons: {
        width: width,
        height: 50,
        marginTop: 15,
        backgroundColor: APP_WHITE,
        alignSelf: 'center',
        flexDirection: 'row'
    },
    tabs: {
        width: width / 2,
        height: '100%',
        backgroundColor: APP_WHITE
    },
    tabs2: {
        width: width / 2,
        height: '100%',
        backgroundColor: APP_WHITE
    },
    tabImageReview: {
        alignSelf: 'center',
        marginTop: 15,
        tintColor: APP_BLACK
    },
    tabImageFavorite: {
        alignSelf: 'center',
        marginTop: 17,
        tintColor: APP_BLACK
    },
    tabBottom1: {
        width: '100%',
        height: 1,
        marginTop: 9
    },
    tabBottom2: {
        width: '100%',
        height: 1,
        marginTop: 14
    }
});

export default Account;



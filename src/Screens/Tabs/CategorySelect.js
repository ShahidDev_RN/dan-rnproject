import React, { Component } from 'react';
import {
    View,
    Dimensions,
    Text,
    StyleSheet,
    TouchableOpacity,
    Image,
    TextInput,
    BackHandler,
    Alert,
    SafeAreaView,
    Platform,
    ScrollView,
    KeyboardAvoidingView,
    ImageBackground, FlatList, StatusBar, Keyboard
} from 'react-native';
import * as API from '../../Requests/WebService.js'
import AsyncStorage from '@react-native-community/async-storage';
import {
    FONT_BLACK, FONT_BOLD, FONT_EXTRABOLD, FONT_EXTRALIGHT, FONT_LIGHT, FONT_SEMIBOLD, SEARCH_ICON_INACTIVE, FILTER, RESTAURANT, RADIO_BUTTON_OFF, RADIO_BUTTON_ON,
    FONT_THIN, APP_WHITE, APP_BLACK, APP_FACEBOOK, BACK_ICON, FONT_REGULAR, FONT_MEDIUM, NOTIFICATION_ICON, APP_GREY, RATTING, HEADER_ICON, FOR_IMAGE_URL, APP_BORDER
} from '../../Constants/App_Constants.js'
let { width, height } = Dimensions.get('window')
const ASPECT_RATIO = width / height

import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
class CategorySelect extends Component {
    constructor(props) {
        super(props);
        this.state = {
            longitude: '76.7018',
            latitude: '30.7089',
            category: '',
            searchtext: '',
            nearbyBusiness: '',
            showlist: false,
            data: [1, 2],
            restaurantSelected: false,
            restaurantImage: RADIO_BUTTON_OFF,
            barsSelected: false,
            barImage: RADIO_BUTTON_OFF,
            fineDinning: false,
            fineDinningImage: RADIO_BUTTON_OFF,
            isloading: false,
            bar_Id: '',
            restaurant_Id: ''
        }
        this.props.navigation.addListener(
            'didFocus', payload => {
                // this.nearbyListApi()
            }
        );

    }

    header() {
        if (Platform.OS == 'ios') {
            return (
                <View style={{ height: 65, width: '100%', backgroundColor: 'white', justifyContent: 'flex-end', borderBottomColor: '#e3e5e8', borderBottomWidth: 3 }}>
                    <View style={{ height: '80%', width: '95%', backgroundColor: 'transparent', alignSelf: 'center' }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', height: '100%', width: '50%', backgroundColor: 'transparent', alignSelf: 'flex-start' }}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('Home')}>
                                <Image source={HEADER_ICON} style={{ marginTop: 6, height: 32, width: 32 }}></Image>
                            </TouchableOpacity>
                            <Text style={{ color: APP_BLACK, fontSize: 18, marginTop: 10, fontFamily: FONT_SEMIBOLD, width: 150, marginLeft: height < 896 ? 20 : 0 }}>Select Category</Text>
                        </View>
                    </View>
                </View>
            )
        } else {
            return (
                <View style={{ height: 65, width: '100%', backgroundColor: 'white', justifyContent: 'flex-end', borderBottomColor: '#e3e5e8', borderBottomWidth: 3 }}>
                    <View style={{ height: '80%', width: '95%', backgroundColor: 'transparent', alignSelf: 'center' }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', height: '100%', width: '50%', backgroundColor: 'transparent', alignSelf: 'flex-start' }}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('Home')}>
                                <Image source={HEADER_ICON} style={{ marginTop: 6, height: 32, width: 32 }}></Image>
                            </TouchableOpacity>
                            <Text style={{ color: APP_BLACK, fontSize: 18, marginTop: 10, fontFamily: FONT_SEMIBOLD, width: 175, marginLeft: 20 }}>Select Category</Text>
                        </View>
                    </View>
                </View>
            )
        }
    }

    restaurantSelect() {
        this.setState({ restaurantSelected: true, barsSelected: false, fineDinning: false, restaurantImage: RADIO_BUTTON_ON, barImage: RADIO_BUTTON_OFF, fineDinningImage: RADIO_BUTTON_OFF, category: this.state.restaurant_Id })
    }

    barsSelected() {
        this.setState({ restaurantSelected: false, barsSelected: true, fineDinning: false, restaurantImage: RADIO_BUTTON_OFF, barImage: RADIO_BUTTON_ON, fineDinningImage: RADIO_BUTTON_OFF, category: this.state.bar_Id })
    }

    fineDinning() {
        this.setState({ restaurantSelected: false, barsSelected: false, fineDinning: true, restaurantImage: RADIO_BUTTON_OFF, barImage: RADIO_BUTTON_OFF, fineDinningImage: RADIO_BUTTON_ON })
    }

    clear() {
        this.setState({ restaurantSelected: false, barsSelected: false, fineDinning: false, restaurantImage: RADIO_BUTTON_OFF, barImage: RADIO_BUTTON_OFF, fineDinningImage: RADIO_BUTTON_OFF, category: '' })
    }

    getCategories() {
        API.GET_CATEGORIES().then((response) => {
            if (response.status == 200) {
                console.log('data :', JSON.stringify(response.data))
                this.setState({ nearbyBusiness: response.data })
            }
        })
    }

    componentDidMount() {
        this.getCategories()
    }

    nearByList() {
        // if(this.state.showlist){
        return (
            <FlatList
                data={this.state.nearbyBusiness}
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{ backgroundColor: APP_WHITE, width: '98%', alignSelf: 'center', paddingBottom: 10 }}
                renderItem={({ item: data, index }) => {
                    return (
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Search', { id: data._id })} style={styles.singleListview}>
                            <View style={styles.downView}>
                                <Text style={styles.textName}>{data.categoryName}</Text>
                                {/* <Text style={styles.textDiscription}>{}</Text>
                        <View style={styles.rattingView}>
                            <Image style={styles.imageStar} source={RATTING}></Image>
                            <Text style={styles.rattingNumber}>5.2</Text>
                        </View> */}
                            </View>
                            <View style={styles.NearbyView}>
                                <Image style={{ height: '100%', width: '95%', borderRadius: 15, borderColor: APP_GREY, borderWidth: 1, backgroundColor: '#e3e5e8', alignSelf: 'flex-start' }} source={{ uri: FOR_IMAGE_URL + data.categoryImage }}></Image>
                            </View>
                        </TouchableOpacity>)
                }} keyExtractor={item => item.id} />
        )
    }

    render() {
        return (
            <SafeAreaView forceInset={{ top: 'always' }} style={{ flex: 1, backgroundColor: APP_WHITE }}>
                {this.header()}
                <OrientationLoadingOverlay
                    visible={this.state.isloading}
                    color="white"
                    indicatorSize="large"
                />
                 {this.nearByList()}
            </SafeAreaView>

        )
    }
};

const styles = StyleSheet.create({

    feildBorder: {
        width: 290,
        height: 40,
        borderRadius: 30,
        borderColor: APP_BLACK,
        backgroundColor: APP_WHITE,
        //alignSelf:'center',
        marginTop: 4,
        justifyContent: "space-between",
        flexDirection: 'row'
    },

    textfeild: {
        width: '88%',
        height: 36,
        fontSize: 13,
        fontFamily: FONT_MEDIUM,
        borderRadius: 30,
        alignSelf: 'center',
        color: APP_BLACK,
        //fontWeight:'400',
        backgroundColor: APP_WHITE
    },

    nearbyTextView: {
        height: height,
        width: width,
        backgroundColor: APP_WHITE
    },
    nearbyTextInnerView: {
        height: 30,
        width: '90%',
        backgroundColor: APP_WHITE,
        alignSelf: 'center'
    },
    nearbyText: {
        fontFamily: FONT_SEMIBOLD,
        fontSize: 16,
        color: APP_BLACK,
        alignSelf: 'flex-start',
        marginTop: 18
    },
    listView: {
        height: height < 896 ? '75%' : '100%',
        width: '100%',
        alignSelf: 'center',
        marginBottom: 10,
        backgroundColor: APP_WHITE
    },
    singleListview: {
        height: 130,
        width: '100%',
        backgroundColor: APP_WHITE,
        borderBottomWidth: 2.5,
        flexDirection: 'row',
        borderColor: '#e3e5e8',
        marginBottom: 0
    },
    NearbyView: {
        height: 110,
        width: '50%',
        borderRadius: 15,
        alignSelf: 'center',
        backgroundColor: 'transparent'
    },
    downView: {
        height: 60,
        width: '50%',
        alignSelf: 'center',
        backgroundColor: 'transparent'
    },
    textName: {
        fontFamily: FONT_SEMIBOLD,
        color: APP_BLACK,
        fontSize: 15,
        marginTop: 16,
        marginLeft: 13
    },
    textDiscription: {
        fontFamily: FONT_REGULAR,
        fontSize: 11,
        marginTop: 5,
        color: APP_GREY,
        marginLeft: 3
    },
    imageStar: {
        height: 8,
        width: 8,
        marginTop: 5,
        marginLeft: 3
    },
    rattingView: {
        width: "100%",
        height: 'auto',
        backgroundColor: APP_BLACK,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    rattingNumber: {
        color: APP_WHITE,
        fontSize: 14,
        fontFamily: FONT_SEMIBOLD
    },
    filter: {
        height: 300,
        width: width,
        backgroundColor: APP_WHITE,
        borderTopRightRadius: 50,
        borderTopLeftRadius: 50
    },
    filterText: {
        width: '80%',
        height: 40,
        marginTop: 30,
        alignSelf: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: APP_WHITE
    },
    filterButtons: {
        width: '80%',
        height: 100,
        marginTop: 20,
        alignSelf: 'center',
        backgroundColor: APP_WHITE
    },
    radioText: {
        marginLeft: 10, fontFamily: FONT_MEDIUM, color: APP_GREY, fontSize: 12
    },
    button: {
        width: '85%',
        height: 50,
        borderRadius: 30,
        borderWidth: 1,
        borderColor: APP_BLACK,
        backgroundColor: APP_BLACK,
        alignSelf: 'center'
    },
    buttonText: {
        fontSize: 15,
        fontFamily: FONT_SEMIBOLD,
        color: APP_WHITE,
        alignSelf: 'center',
        marginTop: 13
    },

});

export default CategorySelect;

import React, { Component } from 'react';
import {
    View,
    Dimensions,
    Text,
    StyleSheet,
    TouchableOpacity,
    Image,
    TextInput,
    BackHandler,
    Alert,
    SafeAreaView,
    Platform,
    ScrollView,
    FlatList,
    KeyboardAvoidingView, StatusBar,
    ImageBackground,
    Keyboard,
    TouchableWithoutFeedback
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import * as API from '../../Requests/WebService.js'
//import firebase from 'react-native-firebase';
import {
    FONT_BLACK, FONT_BOLD, FONT_EXTRABOLD, FONT_EXTRALIGHT, FONT_LIGHT, FONT_SEMIBOLD, RESTAURANT, FORWARD_ICON, HEADER_ICON,
    FONT_THIN, APP_WHITE, APP_BLACK, APP_FACEBOOK, BACK_ICON, FONT_REGULAR, FONT_MEDIUM, NOTIFICATION_ICON, APP_GREY, RATTING, APP_BORDER
} from '../../Constants/App_Constants.js'
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
let { width, height } = Dimensions.get('window')
const ASPECT_RATIO = width / height
class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            featuredBusiness: '',
            nearbyBusiness: '',
            longitude: '76.7018',
            latitude: '30.7089',
            isloading: false
        }
        this.props.navigation.addListener(
            'didFocus', payload => {
            
                console.log("check my data")


                AsyncStorage.getItem("user_info").then((value) => {
                    var data = JSON.parse(value)
                    //this.setState({isloading:true})
                    console.log(data._id)
                    API.GET_BUSINESS(data._id, this.state.latitude, this.state.longitude).then((response) => {
                        console.log(JSON.stringify(response))
                        if (response.status == 1) {
                            AsyncStorage.setItem("featuredData", JSON.stringify(response.data.getFeaturedBusiness))
                            this.setState({ featuredBusiness: response.data.getFeaturedBusiness, nearbyBusiness: response.data.getNearbyBusiness }, () => {
                            })
                            this.setState({ isloading: false })
        
                        } else {
                            this.setState({ isloading: false }, () => {
                                alert(response.message)
                            })
                        }
                    })
                }).done();
        
            }
        );
    }

    header() {
        if (Platform.OS == 'ios') {
            return (
                <View style={{ height: 65, width: '100%', backgroundColor: "white", justifyContent: 'flex-end', borderBottomColor: '#e3e5e8', borderBottomWidth: 3 }}>
                    <View style={{ height: '80%', width: '95%', backgroundColor: 'transparent', alignSelf: 'center' }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', height: '100%', width: '100%', backgroundColor: 'transparent', alignSelf: 'flex-start' }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', height: '100%', width: '50%', backgroundColor: 'transparent', alignSelf: 'flex-start' }}>
                                <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('Home')} style={{ height: '100%', width: 30 }}>
                                    <Image source={HEADER_ICON} style={{ marginTop: 0, height: 32, width: 32, alignSelf: 'center' }}></Image>
                                </TouchableWithoutFeedback>
                                <Text style={{ color: APP_BLACK, fontSize: 19, marginTop: 12, fontFamily: FONT_SEMIBOLD, width: 150, marginLeft: 15 }}>Home</Text>
                            </View>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('Notifications')} style={{ height: '100%', width: 30 }}>
                                <Image source={NOTIFICATION_ICON} style={{ marginTop: 13, height: 17, width: 15, alignSelf: 'center', tintColor: APP_BLACK }}></Image>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            )
        } else {
            return (
                <View style={{ height: 65, width: '100%', backgroundColor: APP_WHITE, justifyContent: 'flex-end', borderBottomColor: '#e3e5e8', borderBottomWidth: 3 }}>
                    <View style={{ height: '80%', width: '90%', backgroundColor: 'transparent', alignSelf: 'center' }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', height: '100%', width: '100%', backgroundColor: 'transparent', alignSelf: 'flex-start' }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', height: '100%', width: '50%', backgroundColor: 'transparent', alignSelf: 'flex-start' }}>
                                <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('Home')} style={{ height: '100%', width: 30 }}>
                                    <Image source={HEADER_ICON} style={{ marginTop: 2, height: 30, width: 30, alignSelf: 'center' }}></Image>
                                </TouchableWithoutFeedback>
                                <Text style={{ color: APP_BLACK, fontSize: 19, marginTop: 12, fontFamily: FONT_SEMIBOLD, width: 150, marginLeft: 15 }}>Home</Text>
                            </View>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('Notifications')} style={{ height: '100%', width: 30 }}>
                                <Image source={NOTIFICATION_ICON} style={{ marginTop: 13, height: 17, width: 15, alignSelf: 'center', tintColor: APP_BLACK }}></Image>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            )
        }
    }

    //   componentWillUnmount(){
    //     this.messageListener();
    //     this.notificationOpenedListener();
    //     this.notificationListener();
    //   }
    //   async getTokenFromAsync(){
    //     try {
    //         const value = await AsyncStorage.getItem('fcmToken');
    //         if (value !== null) {
    //           this.setState({fcmToken:value},()=>{
    //             console.log(this.state.fcmToken)
    //           })
    //         }
    //       } catch (error) {
    //         this.setState({fcmToken:''}) 
    //       }
    //   }

    componentDidMount() {
        Keyboard.dismiss()
        AsyncStorage.getItem("user_info").then((value) => {
            var data = JSON.parse(value)
            //this.setState({isloading:true})
            console.log(data._id)
            API.GET_BUSINESS(data._id, this.state.latitude, this.state.longitude).then((response) => {
                console.log(JSON.stringify(response))
                if (response.status == 1) {
                    AsyncStorage.setItem("featuredData", JSON.stringify(response.data.getFeaturedBusiness))
                    this.setState({ featuredBusiness: response.data.getFeaturedBusiness, nearbyBusiness: response.data.getNearbyBusiness }, () => {
                    })
                    this.setState({ isloading: false })

                } else {
                    this.setState({ isloading: false }, () => {
                        alert(response.message)
                    })
                }
            })
        }).done();

        // this.getTokenFromAsync();
        // this.notificationListener = firebase.notifications().onNotification((notification) => {
        //     const { title, body } = notification;
        //     makeNotification(title,body);
        // });
        // this.messageListener = firebase.messaging().onMessage((message) => {
        //     if(Platform.OS === 'android'){
        //     const channel = new firebase.notifications.Android.Channel(
        //     'channelId',
        //     'Channel Name',
        //     firebase.notifications.Android.Importance.High
        //     ).setDescription('A natural description of the channel');
        //     firebase.notifications().android.createChannel(channel);
        //     const localNotif = new firebase.notifications.Notification()
        //                 .setTitle(message._data.title)
        //                 .setBody(message._data.displayMsg)
        //                 .setSound('default')
        //                 .android.setChannelId('channelId')
        //     firebase.notifications().displayNotification(localNotif).catch(err => console.error(err))}

        //     if(AppState.currentState!='active'){ 
        //     makeNotification(message._data.title!=undefined?message._data.title:'Onyx', 
        //                 message._data.displayMsg!=undefined?message._data.displayMsg:'Onyx Notification');
        // }
        //     this.setState({isloading:true},()=>{
        //     this.onGetNotif(global.userId)
        //     })
        //     });
        // this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {});
    }


    render() {
        return (
            <SafeAreaView forceInset={{ top: 'always' }} style={{ flex: 1, backgroundColor: APP_WHITE }}>
                {this.header()}
                <StatusBar barStyle={'dark-content'}></StatusBar>
                <OrientationLoadingOverlay
                    visible={this.state.isloading}
                    color="white"
                    indicatorSize="large"
                />
                <View style={styles.ListView}>
                    <View style={styles.UpperView}>
                        <View style={styles.UpperListText}>
                            <Text style={styles.featuredText}>Featured Business </Text>
                            <TouchableOpacity style={styles.moreTextView} onPress={() => this.props.navigation.navigate('FeaturedBusiness')}>
                                <Text style={styles.featuredText}>More</Text>
                                <Image style={{ marginTop: 25, marginLeft: 5, height: 10, width: 12, tintColor: APP_BLACK }} source={FORWARD_ICON}></Image>
                            </TouchableOpacity>
                        </View>
                        <FlatList
                            data={this.state.featuredBusiness}
                            showsHorizontalScrollIndicator={false}
                            horizontal={true}
                            renderItem={({ item: data, index }) => {
                                return (
                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('SingleDetail', { businessId: data._id })} style={styles.FeaturedView}>
                                        <Image style={{ height: '69%', width: '100%', borderRadius: 15, borderColor: APP_GREY, borderWidth: 1, backgroundColor: '#e3e5e8' }} source={{ uri: data.businessImages[0].image }}>

                                        </Image>
                                        <View style={styles.downView}>
                                            <Text style={styles.textName}>{data.firstname} {data.lastname}</Text>
                                            <Text style={[{ fontSize: data.businessAddress.length >= 17 ? 11 : 11 }, styles.textDiscription]}>{data.businessAddress}</Text>
                                            <View style={styles.rattingView}>

                                          
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                )
                            }} keyExtractor={item => item._id} />

                    </View>
                    <View style={styles.Middle}></View>
                    <View style={styles.BottomView}>
                        <View style={styles.BottomListText}>
                            <Text style={styles.moreText}>Nearby</Text>
                        </View>

                        <FlatList
                            data={this.state.nearbyBusiness}
                            showsHorizontalScrollIndicator={false}
                            contentContainerStyle={{ backgroundColor: APP_WHITE }}
                            horizontal={true}
                            renderItem={({ item: data, index }) => {
                                var image = ''
                                if (data.businessImages[0] != undefined) {
                                    if (data.businessImages[0].image == undefined) {
                                        image = { uri: data.businessImages[0] }
                                    } else {
                                        image = { uri: data.businessImages[0].image }
                                    }
                                } else {
                                    image = RESTAURANT
                                }
                                return (
                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('SingleDetail', { businessId: data._id })} style={styles.NearbyView}>
                                        <Image style={{ height: '75%', width: '100%', borderRadius: 15, borderColor: APP_GREY, borderWidth: 1, backgroundColor: '#e3e5e8' }} source={image}>

                                        </Image>
                                        <View style={styles.downViewNearBy}>
                                            <Text style={styles.textNameNearBy}>{data.businessName}</Text>
                                            <Text style={[{ fontSize: data.businessAddress.length >= 17 ? 10 : 11 }, styles.textDiscriptionNearBy]}>{data.businessAddress}</Text>
                                            <View style={styles.rattingViewNearBy}>


                                             {/* {data.businessReviews.map((value,index)=>(
                                              

                                             ))} */}
                                                  <FlatList
                            data={data.businessReviews}
                            inverted
                            renderItem={({ item ,index }) => {

                                // index==data.businessReviews.length -1?
                                // :null

                                if(index==data.businessReviews.length -1){
                                   if(item.rating==1){
                                 return   <Image style={styles.imageStarNearBy} source={RATTING}></Image>

                                   }

                                   else if(item.rating==2){
                                       return <View style={{flexDirection:"row"}}>
                                            <Image style={styles.imageStarNearBy} source={RATTING}></Image>
                                            <Image style={styles.imageStarNearBy} source={RATTING}></Image>
                                           </View>
                                   }
                                   else if(item.rating==3){
                                    return <View style={{flexDirection:"row"}}>
                                         <Image style={styles.imageStarNearBy} source={RATTING}></Image>
                                         <Image style={styles.imageStarNearBy} source={RATTING}></Image>
                                         <Image style={styles.imageStarNearBy} source={RATTING}></Image>

                                        </View>
                                }
                                else if(item.rating==4){
                                    return <View style={{flexDirection:"row"}}>
                                         <Image style={styles.imageStarNearBy} source={RATTING}></Image>
                                         <Image style={styles.imageStarNearBy} source={RATTING}></Image>
                                         <Image style={styles.imageStarNearBy} source={RATTING}></Image>
                                         <Image style={styles.imageStarNearBy} source={RATTING}></Image>

                                        </View>
                                }
                                else if(item.rating==5){
                                    return <View style={{flexDirection:"row"}}>
                                         <Image style={styles.imageStarNearBy} source={RATTING}></Image>
                                         <Image style={styles.imageStarNearBy} source={RATTING}></Image>
                                         <Image style={styles.imageStarNearBy} source={RATTING}></Image>
                                         <Image style={styles.imageStarNearBy} source={RATTING}></Image>
                                         <Image style={styles.imageStarNearBy} source={RATTING}></Image>

                                        </View>
                                }
                                }
                            }}/>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                )
                            }} keyExtractor={item => item._id} />
                    </View>
                </View>
            </SafeAreaView>
        )
    }
};


const styles = StyleSheet.create({
    ListView: {
        backgroundColor: APP_WHITE,
        height: height,

    },
    UpperView: {
        // height:height<896?height/1.9:height/3,
        height: height / 2.7,
        width: width,
        backgroundColor: APP_WHITE
    },
    UpperListText: {
        height: 55,
        width: width - 30,
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: APP_WHITE,
        alignSelf: 'center'
    },
    featuredText: {
        color: APP_BLACK,
        fontFamily: FONT_SEMIBOLD,
        fontSize: 16,
        marginTop: 20
    },
    moreText: {
        color: APP_BLACK,
        fontFamily: FONT_SEMIBOLD,
        fontSize: 16,
        marginTop: 20
    },
    moreTextView: {
        flexDirection: 'row',
    },
    Middle: {
        height: 2,
        width: width,
        marginTop: 5,
        //backgroundColor:'#2d2e2d'
        backgroundColor: '#e3e5e8'
    },
    BottomView: {
        height: height / 2,
        width: width,
        backgroundColor: APP_WHITE
    },
    BottomListText: {
        height: 45,
        width: width - 30,
        marginTop: height < 896 ? 0 : 0,
        backgroundColor: APP_WHITE,
        alignSelf: 'center'
    },
    NearbyView: {
        height: height < 896 ? height / 3.4 : height / 3,
        width: height < 896 ? 270 : 340,
        marginLeft: 20,
        marginRight: 10,
        marginTop: 10,
        borderRadius: 15,
        alignSelf: 'flex-start',
        backgroundColor: APP_WHITE
    },
    FeaturedView: {
        height: height < 896 ? height / 3.4 : height / 3,
        width: height < 896 ? 270 : 340,
        marginTop: 2,
        borderRadius: 15,
        marginLeft: 20,
        alignSelf: 'flex-start',
        backgroundColor: APP_WHITE,

    },
    downView: {
        height: 50,
        backgroundColor: "transparent"
    },
    textName: {
        fontFamily: FONT_BOLD,
        fontSize: 13,
        color: APP_BLACK,
        marginTop: 4,
        marginLeft: 4
    },
    textDiscription: {
        fontFamily: FONT_MEDIUM,
        //fontSize:11,
        marginTop: 3,
        color: APP_GREY,
        marginLeft: 4
    },
    imageStar: {
        height: 9,
        width: 9,
        marginTop: 5,
        marginLeft: 4
    },
    rattingView: {
        width: "100%",
        height: 'auto',
        backgroundColor: APP_WHITE,
        marginTop: 1,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    rattingNumber: {
        color: APP_BLACK,
        fontSize: 12,
        fontFamily: FONT_SEMIBOLD,
        marginTop: 4
    },
    downViewNearBy: {
        height: 60,
        backgroundColor: APP_WHITE
    },
    textNameNearBy: {
        fontFamily: FONT_SEMIBOLD,
        color: APP_BLACK,
        fontSize: 14,
        marginTop: 5,
        marginLeft: 3
    },
    textDiscriptionNearBy: {
        fontFamily: FONT_REGULAR,
        //fontSize:11,
        marginTop: 3,
        color: APP_GREY,
        marginLeft: 3
    },
    imageStarNearBy: {
        height: 8,
        width: 8,
        marginTop: 5,
        marginLeft: 3
    },
    rattingViewNearBy: {
        width: "100%",
        height: 'auto',
        backgroundColor: APP_WHITE,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    rattingNumberNearBy: {
        color: APP_BLACK,
        fontSize: 13,
        fontFamily: FONT_SEMIBOLD
    }

});

export default Home;


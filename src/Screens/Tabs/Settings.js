import React, { Component } from 'react';
import {
    View,
    Dimensions,
    Text,
    StyleSheet,
    TouchableOpacity,
    Image,
    TextInput,
    BackHandler,
    Alert,
    SafeAreaView,
    Platform,
    ScrollView,
    KeyboardAvoidingView, StatusBar,
    ImageBackground
} from 'react-native';
import * as API from '../../Requests/WebService.js';
import AsyncStorage from '@react-native-community/async-storage';
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
import { StackActions, NavigationActions } from 'react-navigation';
import {
    FONT_BLACK, FONT_BOLD, FONT_EXTRABOLD, FONT_EXTRALIGHT, FONT_LIGHT, FONT_SEMIBOLD, APP_BORDER,
    FONT_THIN, APP_WHITE, APP_BLACK, APP_FACEBOOK, BACK_ICON, FONT_REGULAR, FONT_MEDIUM, NOTIFICATION_ICON, APP_GREY, HEADER_ICON, RADIO_ON, RADIO_OFF
} from '../../Constants/App_Constants.js'
let { width, height } = Dimensions.get('window')
const ASPECT_RATIO = width / height

const resetAction = StackActions.reset({
    index: 0,
    actions: [NavigationActions.navigate({ routeName: 'AppLogin' })],
  });
class Settings extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            serverError: false,
            serverErrorText: '',
            api_token: '',
            isloading: false,
            device_token: 'dMHNX87sSuqqEqhtXeT4K:APA91bFxsGOzSiTtGbasYHeeKIt6FvgKKrjd0j1CDOBXtSL6ryKAIKdJ1HtDQJYUMOHbVL5KLBTvbild5XtOTG1yVSnFSXGZZI1uHsvla2eRivH5gtwwwaEsV33654g_VIi0PVPn4TKl'
        }
    }

    header() {
        if (Platform.OS == 'ios') {
            return (
                <View style={{ height: 65, width: '100%', backgroundColor: 'white', justifyContent: 'flex-end', borderBottomColor: '#e3e5e8', borderBottomWidth: 3 }}>
                    <View style={{ height: '80%', width: '95%', backgroundColor: 'transparent', alignSelf: 'center' }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', height: '100%', width: '50%', backgroundColor: 'transparent', alignSelf: 'flex-start' }}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('Home')}>
                                <Image source={HEADER_ICON} style={{ marginTop: 6, height: 32, width: 32 }}></Image>
                            </TouchableOpacity>
                            <Text style={{ color: APP_BLACK, fontSize: 18, marginTop: 10, fontFamily: FONT_SEMIBOLD, width: 150, marginLeft: height < 896 ? 20 : 0 }}>Settings</Text>
                        </View>
                    </View>
                </View>
            )
        } else {
            return (
                <View style={{ height: 65, width: '100%', backgroundColor: 'white', justifyContent: 'flex-end', borderBottomColor: '#e3e5e8', borderBottomWidth: 3 }}>
                    <View style={{ height: '80%', width: '95%', backgroundColor: 'transparent', alignSelf: 'center' }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', height: '100%', width: '50%', backgroundColor: 'transparent', alignSelf: 'flex-start' }}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('Home')}>
                                <Image source={HEADER_ICON} style={{ marginTop: 6, height: 32, width: 32 }}></Image>
                            </TouchableOpacity>
                            <Text style={{ color: APP_BLACK, fontSize: 18, marginTop: 10, fontFamily: FONT_SEMIBOLD, width: 175, marginLeft: 20 }}>Settings</Text>
                        </View>
                    </View>
                </View>
            )
        }
    }

    componentDidMount() {
        AsyncStorage.getItem("user_token").then((value) => {
            this.setState({ api_token: value })
        }).done();
    }

    logout = (devicetoken, token) => {
        this.setState({ isloading: true })
        API.LOGOUT(devicetoken, token).then((response) => {
            if (response.status == 1) {
                this.setState({ isloading: false }, () => {
                    // this.props.navigation.navigate('Home')
                    this.props.navigation.navigate('AppLogin')
                    AsyncStorage.removeItem('user_info').then(() => {
                    })
                    AsyncStorage.removeItem('user_token').then(() => {
                    })
                    AsyncStorage.removeItem('session').then(() => {
                    })
                    this.props.navigation.dispatch(resetAction);
                })
            } else if (response.status == 0) {
                this.setState({ serverError: true, serverErrorText: response.message })
            }
        })
    }



    render() {
        return (
            <SafeAreaView forceInset={{ top: 'always' }} style={{ flex: 1, backgroundColor: APP_WHITE }}>
                {this.header()}
                {/* <StatusBar barStyle={'light-content'}></StatusBar> */}
                <OrientationLoadingOverlay
                    visible={this.state.isloading}
                    color="white"
                    indicatorSize="large"
                />
                <ScrollView contentContainerStyle={{ flex: 1 }}>

                    <View style={{ justifyContent: "space-between", flex: 1 }}>
                        <View>
                            <View style={styles.Options}>
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('ChangePassword')}
                                    style={{ height: '100%' }}>
                                    <Text style={styles.settingText}>Change Password</Text>
                                </TouchableOpacity>
                            </View>

                            <View style={styles.Options}>
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('ContactUs')}
                                    style={{ height: '100%' }}>
                                    <Text style={styles.settingText}>Contact Us</Text>
                                </TouchableOpacity>
                            </View>
                        </View>

                     
                        
                        <View>
                            <View style={styles.Logout}>
                                <TouchableOpacity onPress={() => {
                                    //this.logout(this.state.device_token, this.state.api_token)
                                    Alert.alert(
                                        "",
                                        "Are You sure you want to logout?",
                                        [
                                          {
                                            text: "Cancel",
                                            onPress: () => console.log("Cancel Pressed"),
                                            style: "cancel"
                                          },
                                          { text: "OK", onPress: () => this.logout(this.state.device_token, this.state.api_token) }
                                        ]
                                      );
                                    }} style={{ height: '100%', width: '100%' }}>
                                    <Text style={{ color: APP_BLACK, fontFamily: FONT_MEDIUM, fontSize: 15, alignSelf: 'flex-start', paddingLeft: 24, marginBottom: 40 }}>Logout</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </SafeAreaView>
        )
    }
};

{/* <View style={styles.Options}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Customer_Concerns')} style={{ height: '100%' }}>
                            <Text style={styles.settingText}>Customer Service Concerns</Text>
                        </TouchableOpacity>
                    </View> */}
{/* <View style={styles.Options}>
                        <TouchableOpacity style={{height:'100%'}}>
                        <Text style={styles.settingText}>Request assistance</Text>
                        </TouchableOpacity>
                    </View> */}
{/* <View style={styles.Middle}>

                    </View> */}


const styles = StyleSheet.create({
    OptionsView: {
        minHeight: 300,
        width: width,
        backgroundColor: APP_WHITE,
    },
    Options: {
        height: 60,
        width: width,
        backgroundColor: 'transparent',
        borderBottomWidth: 1.7,
        borderColor: '#e3e5e8'
    },
    settingText: {
        color: APP_BLACK,
        fontFamily: FONT_MEDIUM,
        fontSize: 15,
        alignSelf: 'flex-start',
        paddingLeft: 24,
        paddingTop: 24
    },
    Middle: {
        height: height < 896 ? height - 350 : height - 400,
        width: width,
        backgroundColor: 'transparent'
    },
    Logout: {
        height: 60,
        width: width,
        backgroundColor: 'transparent',
        justifyContent: 'flex-end',
        alignSelf: 'flex-end',
    }

});

export default Settings;

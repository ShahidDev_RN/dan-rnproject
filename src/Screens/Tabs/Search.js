import React, { Component } from 'react';
import {
    View,
    Dimensions,
    Text,
    StyleSheet,
    TouchableOpacity,
    Image,
    TextInput,
    BackHandler,
    Alert,
    SafeAreaView,
    Platform,
    ScrollView,
    KeyboardAvoidingView,
    ImageBackground, FlatList, StatusBar, Keyboard, Modal
} from 'react-native';
import * as API from '../../Requests/WebService.js'
import AsyncStorage from '@react-native-community/async-storage';
import {
    FONT_BLACK, FONT_BOLD, FONT_EXTRABOLD, FONT_EXTRALIGHT, FONT_LIGHT, FONT_SEMIBOLD, SEARCH_ICON_INACTIVE, FILTER, RESTAURANT, RADIO_BUTTON_OFF, RADIO_BUTTON_ON,
    FONT_THIN, APP_WHITE, APP_BLACK, APP_FACEBOOK, BACK_ICON, FONT_REGULAR, FONT_MEDIUM, NOTIFICATION_ICON, APP_GREY, RATTING, SEARCH_FILTER, FILTER_BLACK,
} from '../../Constants/App_Constants.js'
let { width, height } = Dimensions.get('window')
const ASPECT_RATIO = width / height
import RBSheet from "react-native-raw-bottom-sheet";
import _ from "lodash";
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
class Search extends Component {
    constructor(props) {
        super(props);
        this.state = {
            longitude: '76.7018',
            latitude: '30.7089',
            category: '',
            searchtext: '',
            nearbyBusiness: '',
            showlist: false,
            data: [1, 2],
            restaurantSelected: false,
            restaurantImage: RADIO_BUTTON_OFF,
            barsSelected: false,
            barImage: RADIO_BUTTON_OFF,
            fineDinning: false,
            fineDinningImage: RADIO_BUTTON_OFF,
            isloading: false,
            bar_Id: '',
            restaurant_Id: '',
            // modalVisible: false,
            // button:false,
        }
        this.props.navigation.addListener(
            'didFocus', payload => {
                // this.setState({category:this.props.navigation.getParam('id','')},()=>{
                //     this.nearbyListApi()
                //     //this.getCategories()
                // })
            }
        );
        this.nearbyListApiDebounced = _.debounce(this.nearbyListApi, 800)
    }

    // setModalVisible(visible) {
    //     this.setState({ modalVisible: visible });
    // }

    // touchButton() {
    //     this.setState(state => ({liked: !this.state.button}));
    // } 
    //   }


    // FilterVisible() {
    //     return(
    //         <View>
    //             <Modal
    //                 animationType={"fade"}
    //                 transparent={false}
    //                 visible={this.state.modalVisible}
    //                 onRequestClose = {() =>{console.log("Modal has been cancelled ");}}>

    //                 <View style={{backgroundColor:"red"}}>
    //                     <View style={{backgroundColor:"yellow"}}>
    //                          <Text style={{color:"black"}}>Select Categories</Text> 
    //                          <Text style={{color:"black"}}>Clear</Text>  
    //                     </View>
    //                 </View>
    //             </Modal>
    //         </View>
    //     )
    // }

    header() {
        if (Platform.OS == 'ios') {
            return (
                <View style={{ height: 70, width: '100%', backgroundColor: 'white', justifyContent: 'flex-end', borderBottomColor: '#e3e5e8', borderBottomWidth: 3 }}>
                    <View style={{ height: '80%', width: '90%', backgroundColor: 'transparent', alignSelf: 'center' }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'flex-start', height: '100%', width: '100%', backgroundColor: 'transparent', alignSelf: 'flex-start' }}>
                            <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={{ height: '100%' }}>
                                <Image source={BACK_ICON} style={{ marginTop: 17, height: 17, width: 25, tintColor: APP_BLACK }}></Image>
                            </TouchableOpacity>
                            <View style={styles.feildBorder}>
                                <Image source={SEARCH_ICON_INACTIVE} style={{ marginTop: 12, marginLeft: 10, height: 15, width: 15, tintColor: APP_BLACK }}></Image>

                                <TextInput ref={input => { this.textInput = input }}
                                    style={styles.textfeild}
                                    placeholder="Search"
                                    keyboardType="default"
                                    placeholderTextColor={APP_GREY}
                                    onChangeText={this.handlesearchText} />
                            </View>
                            
                            {/* <TouchableOpacity onPress={()=>this.RBSheet.open()} style={{height:'100%'}}>
                        <Image source={FILTER} style={{marginTop:17, height:17,width:15}}></Image>
                        </TouchableOpacity> */}
                        </View>

                    </View>
                </View>
            )
        } else {
            return (
                <View style={{ height: 70, width: '100%', backgroundColor: 'white', justifyContent: 'flex-end', borderBottomColor: '#e3e5e8', borderBottomWidth: 3 }}>
                    <View style={{ height: '80%', width: '90%', backgroundColor: 'transparent', alignSelf: 'center' }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'flex-start', height: '100%', width: '100%', backgroundColor: 'transparent', alignSelf: 'flex-start' }}>
                            <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={{ height: '100%' }}>
                                <Image source={BACK_ICON} style={{ marginTop: 17, height: 17, width: 25, tintColor: APP_BLACK }}></Image>
                            </TouchableOpacity>
                            <View style={styles.feildBorder}>
                                <Image source={SEARCH_ICON_INACTIVE} style={{ marginTop: 12, marginLeft: 10, height: 15, width: 15 }}></Image>

                                <TextInput ref={input => { this.textInput = input }}
                                    style={styles.textfeild}
                                    placeholder="Search"
                                    keyboardType="default"
                                    placeholderTextColor={APP_GREY}
                                    onChangeText={this.handlesearchText} />
                            </View>
                            {/* <TouchableOpacity onPress={() => this.setModalVisible(true)}>
                                <View style={{ padding: 10, marginTop: 4 }}>
                                    <Image source={FILTER_BLACK} style={{ height: 23, width: 23 }}></Image>
                                </View>
                            </TouchableOpacity> */}
                            {/* <TouchableOpacity onPress={()=>this.RBSheet.open()} style={{height:'100%'}}>
                    <Image source={FILTER} style={{marginTop:17, height:17,width:15}}></Image>
                    </TouchableOpacity> */}
                        </View>
                    </View>
                </View>
            )
        }
    }

    restaurantSelect() {
        this.setState({ restaurantSelected: true, barsSelected: false, fineDinning: false, restaurantImage: RADIO_BUTTON_ON, barImage: RADIO_BUTTON_OFF, fineDinningImage: RADIO_BUTTON_OFF, category: this.state.restaurant_Id })
    }

    barsSelected() {
        this.setState({ restaurantSelected: false, barsSelected: true, fineDinning: false, restaurantImage: RADIO_BUTTON_OFF, barImage: RADIO_BUTTON_ON, fineDinningImage: RADIO_BUTTON_OFF, category: this.state.bar_Id })
    }

    fineDinning() {
        this.setState({ restaurantSelected: false, barsSelected: false, fineDinning: true, restaurantImage: RADIO_BUTTON_OFF, barImage: RADIO_BUTTON_OFF, fineDinningImage: RADIO_BUTTON_ON })
    }

    clear() {
        this.setState({ restaurantSelected: false, barsSelected: false, fineDinning: false, restaurantImage: RADIO_BUTTON_OFF, barImage: RADIO_BUTTON_OFF, fineDinningImage: RADIO_BUTTON_OFF, category: '' })
    }

    componentDidMount() {
        this.setState({ category: this.props.navigation.getParam('id', '') }, () => {
            this.nearbyListApi()
            //this.getCategories()
            //this.setState({isloading:false})
        })
    }


    getCategories() {
        API.GET_CATEGORIES().then((response) => {
            if (response.status == 200) {
                this.setState({ bar_Id: response.data[0]._id, restaurant_Id: response.data[1]._id })
            }
        })
    }

    categoryApply() {
        this.nearbyListApi()
        this.RBSheet.close()
    }

    nearbyListApi() {
        AsyncStorage.getItem("user_info").then((value) => {
            var data = JSON.parse(value)
            this.setState({ isloading: true })
            Keyboard.dismiss()
            API.NEARBY_SEARCH(data._id, this.state.latitude, this.state.longitude, this.state.category, this.state.searchtext).then((response) => {
                this.setState({ nearbyBusiness: response.data.getNearbyBusiness }, () => {
                    if (response.status == 1) {
                        if (this.state.nearbyBusiness.length > 0) {
                            this.setState({ showlist: true, isloading: false })
                        } else {
                            this.setState({ showlist: false, isloading: false })
                        }
                    } else {
                        alert(response.message)
                        this.setState({ isloading: false })
                    }
                })
            })
        }).done();
    }

    handlesearchText = (text) => {
        this.setState({ searchtext: text }, () => {
            if (text.length > 0) {
                this.nearbyListApiDebounced()
            } else {
                this.nearbyListApiDebounced()
            }
        })
    }


    nearByList() {
        if (this.state.showlist) {
            return (
                <FlatList
                    data={this.state.nearbyBusiness}
                    showsVerticalScrollIndicator={false}
                    contentContainerStyle={{ backgroundColor: APP_WHITE, width: '98%', alignSelf: 'center', marginBottom: 10 }}
                    renderItem={({ item: data, index }) => {
                        var image = ''
                        if (data.businessImages[0] != undefined) {
                            if (data.businessImages[0].image == undefined) {
                                image = { uri: data.businessImages[0] }
                            } else {
                                image = { uri: data.businessImages[0].image }
                            }
                        } else {
                            image = RESTAURANT
                        }
                        return (
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('SingleDetail', { userid: data._id })} style={styles.singleListview}>
                                <View style={styles.NearbyView}>
                                    <Image style={{ height: '100%', width: '100%', borderRadius: 15, borderColor: APP_GREY, borderWidth: 1, backgroundColor: 'grey' }} source={image}></Image>
                                </View>

                                <View style={styles.downView}>
                                    <Text style={styles.textName}>{data.businessName}</Text>
                                    <Text style={styles.textDiscription}>{data.businessAddress}</Text>
                                    <View style={styles.rattingView}>
                                        <Image style={styles.imageStar} source={RATTING}></Image>
                                        <Text style={styles.rattingNumber}>5.2</Text>
                                    </View>
                                </View>
                            </TouchableOpacity>)
                    }} keyExtractor={item => item.id} />
            )
        } else {
            return (
                <View style={{ backgroundColor: APP_WHITE, width: '90%', height: 300, justifyContent: 'flex-start', alignSelf: 'center' }}>
                    <Text style={{ alignSelf: 'center', fontFamily: FONT_BOLD, fontSize: 17, color: APP_GREY, marginTop: 20 }}>No Nearby Business Added</Text>
                </View>
            )
        }
    }

    render() {
        return (
            <SafeAreaView forceInset={{ top: 'always' }} style={{ flex: 1, backgroundColor: APP_WHITE }}>
                {this.header()}
                <OrientationLoadingOverlay
                    visible={this.state.isloading}
                    color="white"
                    indicatorSize="large"
                />
                <View style={styles.nearbyTextView}>
                    <View style={styles.nearbyTextInnerView}>
                        {this.state.showlist ? <Text style={styles.nearbyText}>Nearby</Text> : <Text style={styles.nearbyText}></Text>}
                    </View>
                    <View style={styles.listView}>
                        {this.nearByList()}
                    </View>

                    {/* <View>
                        <Modal
                            animationType={"slide"}
                            transparent={true}
                            visible={this.state.modalVisible}
                            style={{ justifyContent: "flex-end", alignSelf: "flex-end" }}
                            onRequestClose={() => { console.log("Modal has been cancelled "); }}>

                            <View style={{ flex: 1, justifyContent: "flex-end", alignItems: "center", height: 100, width: 390, backgroundColor: "transparent" }}>

                                <View style={{
                                    backgroundColor: "white", height: 350, width: 400,
                                    borderWidth: 1, borderTopLeftRadius: 60, borderTopRightRadius: 60, padding: 20
                                }}>

                                    <View style={{ flexDirection: "row", justifyContent: "space-between",padding:15}}>
                                        <Text style={{ color: "black", marginLeft: 10, fontSize: 18 }}>Select Categories</Text>
                                        <TouchableOpacity onPress={() => { this.setModalVisible(!this.state.modalVisible) }}>
                                            <Text style={{ color: "black",  fontSize: 20, marginRight: 10 }}>Clear</Text>
                                        </TouchableOpacity>
                                    </View>

                                    <View style={{ padding:10}}>
                                        <View style={{ flexDirection: "row",marginLeft:15}}>
                                            <TouchableOpacity onPress={() => this.touchButton()}>
                                            <Image
                                                style={{ height: 18, width: 18, marginTop: 4 }}
                                                source={this.state.button? require("../../Assets/radio_button_off3.png"):
                                                            require("../../Assets/radio_button_on3.png")}></Image>
                                            </TouchableOpacity>
                                            <Text style={{ color: "grey", fontSize: 18, marginLeft: 8 }}>Restaurants</Text>
                                        </View>



                                        <View style={{ flexDirection: "row", marginTop: 20,marginLeft:15}}>
                                            <Image
                                                style={{ height: 18, width: 18, marginTop:4 }}
                                                source={require("../../Assets/radio_button_off3.png")}></Image>
                                            <Text style={{ color: "grey", fontSize: 18, marginLeft: 8 }}>Bars</Text>
                                        </View>


                                        <View style={{ flexDirection: "row", marginTop: 20, marginLeft:15 }}>
                                            <Image
                                                style={{ height: 18, width: 18, marginTop: 4 }}
                                                source={require("../../Assets/radio_button_off3.png")}></Image>
                                            <Text style={{ color: "grey", fontSize: 18, marginLeft: 8 }}>Fine Dining</Text>
                                        </View>

                                    </View>

                                    <TouchableOpacity>
                                    <View style={{width: width - 50,height: 58,borderRadius: 30,borderWidth: 1,
                                    marginTop: 20,borderColor: APP_BLACK,backgroundColor: APP_BLACK,alignSelf: 'center',
                                    // marginBottom: 40
                                    }}>
                                        <Text style={{fontSize: 18,fontFamily: '700',color: APP_WHITE,
                                            alignSelf: 'center',marginTop: 14}}>Apply</Text>
                                    </View>
                                    </TouchableOpacity>
                                </View>

                            </View>
                        </Modal>
                    </View> */}
                    {/* <RBSheet
                        ref={ref => {
                            this.RBSheet = ref;
                        }}
                        height={300}
                        openDuration={250}
                        customStyles={{
                            container: {
                                justifyContent: "center",
                                alignItems: "center",
                                backgroundColor: 'transparent'
                            }
                        }}>
                        <View style={styles.filter}>
                            <View style={styles.filterText}>
                                <Text style={{ fontSize: 16, color: APP_BLACK, fontFamily: FONT_SEMIBOLD, marginTop: 10 }}>Select Category</Text>
                                <TouchableOpacity onPress={() => this.clear()}>
                                    <Text style={{ fontSize: 16, color: APP_BLACK, fontFamily: FONT_SEMIBOLD, marginTop: 10 }}>Clear</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={styles.filterButtons}>
                                <TouchableOpacity onPress={() => this.restaurantSelect()}>
                                    <View style={{ width: '100%', justifyContent: 'flex-start', flexDirection: 'row', height: 40 }}>
                                        <Image source={this.state.restaurantImage}></Image>
                                        <Text style={styles.radioText}>Restaurant</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.barsSelected()}>
                                    <View style={{ width: '100%', justifyContent: 'flex-start', flexDirection: 'row', height: 40 }}>
                                        <Image source={this.state.barImage}></Image>
                                        <Text style={styles.radioText}>Bars</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                            <View style={styles.button}>
                                <TouchableOpacity onPress={() => this.categoryApply()} style={{ height: '100%' }}>
                                    <Text style={styles.buttonText}>APPLY</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </RBSheet> */}
                </View>
            </SafeAreaView>

        )
    }
};

const styles = StyleSheet.create({

    feildBorder: {
        width: 260,
        height: 40,
        borderRadius: 30,
        borderColor: APP_BLACK,
        backgroundColor: '#e3e5e8',
        //alignSelf:'center',
        marginLeft: 15,
        marginTop: 4,
        justifyContent: "space-between",
        flexDirection: 'row'
    },

    textfeild: {
        width: '98%',
        height: 41,
        fontSize: 13,
        fontFamily: FONT_MEDIUM,
        borderRadius: 30,
        alignSelf: 'center',
        color: APP_BLACK,
        //fontWeight:'400',
        backgroundColor: '#e3e5e8',
        marginLeft:5
    },

    nearbyTextView: {
        height: height,
        width: width,
        backgroundColor: APP_WHITE
    },
    nearbyTextInnerView: {
        height: 50,
        width: '90%',
        backgroundColor: APP_WHITE,
        alignSelf: 'center'
    },
    nearbyText: {
        fontFamily: FONT_SEMIBOLD,
        fontSize: 16,
        color: APP_BLACK,
        alignSelf: 'flex-start',
        marginTop: 18
    },
    listView: {
        height: '70%',
        width: '90%',
        alignSelf: 'center',
        backgroundColor: APP_WHITE
    },
    singleListview: {
        height: 230,
        width: '100%',
        backgroundColor: APP_WHITE,
        marginBottom: 15
    },
    NearbyView: {
        height: 170,
        width: '100%',
        borderRadius: 15,
        alignSelf: 'flex-start',
        backgroundColor: 'transparent'
    },
    downView: {
        height: 60,
        backgroundColor: APP_WHITE
    },
    textName: {
        fontFamily: FONT_SEMIBOLD,
        color: APP_BLACK,
        fontSize: 13,
        marginTop: 8,
        marginLeft: 3
    },
    textDiscription: {
        fontFamily: FONT_REGULAR,
        fontSize: 11,
        marginTop: 5,
        color: APP_GREY,
        marginLeft: 3
    },
    imageStar: {
        height: 8,
        width: 8,
        marginTop: 5,
        marginLeft: 3
    },
    rattingView: {
        width: "100%",
        height: 'auto',
        backgroundColor: APP_WHITE,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    rattingNumber: {
        color: APP_WHITE,
        fontSize: 14,
        fontFamily: FONT_SEMIBOLD
    },
    filter: {
        height: 300,
        width: width,
        backgroundColor: APP_WHITE,
        borderTopRightRadius: 50,
        borderTopLeftRadius: 50
    },
    filterText: {
        width: '80%',
        height: 40,
        marginTop: 30,
        alignSelf: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: APP_WHITE
    },
    filterButtons: {
        width: '80%',
        height: 100,
        marginTop: 20,
        alignSelf: 'center',
        backgroundColor: APP_WHITE
    },
    radioText: {
        marginLeft: 10, fontFamily: FONT_MEDIUM, color: APP_GREY, fontSize: 12
    },
    button: {
        width: '85%',
        height: 50,
        borderRadius: 30,
        borderWidth: 1,
        borderColor: APP_BLACK,
        backgroundColor: APP_BLACK,
        alignSelf: 'center'
    },
    buttonText: {
        fontSize: 15,
        fontFamily: FONT_SEMIBOLD,
        color: APP_WHITE,
        alignSelf: 'center',
        marginTop: 13
    },

});

export default Search;

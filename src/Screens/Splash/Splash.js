import React, {Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,Image,
  Dimensions
} from 'react-native';
import Video from 'react-native-video'
import {
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
let { width, height } = Dimensions.get('window')
    const ASPECT_RATIO = width / height
class Splash extends Component {
  constructor(props){
    super(props)
  }
    render(){
   return(
    <SafeAreaView style={{flex:1,backgroundColor:'white',justifyContent:'center'}}>
      <Image style={{height:'100%',width:height<896?'85%':'80%',alignSelf:'center'}} source={require('../../Assets/Splash.gif')}>
      </Image>
      {/* <Image style={{height:'100%',width:height<896?'85%':'80%',alignSelf:'center'}} source={require('../../Assets/OnyxTransparentBg.gif')}>
      </Image> */}
        {/* <Video
        style={{position: 'absolute',top: 0,left: 0,bottom: 0,right: 0,backgroundColor:'white'}}
        fullscreen={true}
        ignoreSilentSwitch={"ignore"}
        //onEnd={()=>this.welcome()}
        source={require('../../Assets/OnyxBlackLogo.mp4')}
        controls={false}/> */}
    </SafeAreaView>
   ) 
} 
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default Splash;


// import React, {Component} from 'react';
// import {
//   SafeAreaView,
//   StyleSheet,
//   ScrollView,
//   View,
//   Text,
//   StatusBar,Image,
//   Dimensions
// } from 'react-native';
// import Video from 'react-native-video'
// import {
//   LearnMoreLinks,
//   Colors,
//   DebugInstructions,
//   ReloadInstructions,
// } from 'react-native/Libraries/NewAppScreen';
// let { width, height } = Dimensions.get('window')
//     const ASPECT_RATIO = width / height
// class Splash extends Component {
//   constructor(props){
//     super(props)
//   }
//     render(){
//    return(
//     <SafeAreaView style={{flex:1,backgroundColor:'white',justifyContent:'center'}}>
//       <Image style={{height:'100%',width:height<896?'85%':'80%',alignSelf:'center'}} 
//       source={require('../../Assets/OnyxTransparentBg.gif')}>
//       </Image>
//         {/* <Video
//         style={{position: 'absolute',top: 0,left: 0,bottom: 0,right: 0,backgroundColor:'white'}}
//         fullscreen={true}
//         ignoreSilentSwitch={"ignore"}
//         //onEnd={()=>this.welcome()}
//         source={require('../../Assets/OnyxBlackLogo.mp4')}
//         controls={false}/> */}
//     </SafeAreaView>
//    ) 
// } 
// };

// const styles = StyleSheet.create({
//   scrollView: {
//     backgroundColor: Colors.lighter,
//   },
//   engine: {
//     position: 'absolute',
//     right: 0,
//   },
//   body: {
//     backgroundColor: Colors.white,
//   },
//   sectionContainer: {
//     marginTop: 32,
//     paddingHorizontal: 24,
//   },
//   sectionTitle: {
//     fontSize: 24,
//     fontWeight: '600',
//     color: Colors.black,
//   },
//   sectionDescription: {
//     marginTop: 8,
//     fontSize: 18,
//     fontWeight: '400',
//     color: Colors.dark,
//   },
//   highlight: {
//     fontWeight: '700',
//   },
//   footer: {
//     color: Colors.dark,
//     fontSize: 12,
//     fontWeight: '600',
//     padding: 4,
//     paddingRight: 12,
//     textAlign: 'right',
//   },
// });

// export default Splash;

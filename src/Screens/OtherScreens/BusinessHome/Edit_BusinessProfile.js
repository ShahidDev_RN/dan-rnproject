import React, { Component, PureComponent, useCallback } from 'react'
import {
    View,
    Modal,
    Text,
    Image,
    FlatList,
    Dimensions,
    StatusBar,
    KeyboardAvoidingView,
    ScrollView,
    TextInput,
    StyleSheet, ImageBackground, PermissionsAndroid,
    TouchableOpacity,
    Platform,
    Keyboard
} from 'react-native'
import { APP_BLACK, APP_BORDER, APP_GREY, APP_WHITE, FONT_MEDIUM, FONT_REGULAR, Map_Marker, FONT_SEMIBOLD, RESTAURANT, FAVORITE_DETAIL, SHARE, EDIT_ICON, BACK_ICON, ADD_DOCUMENT, REMOVE_ICON, FOR_IMAGE_URL, map } from '../../../Constants/App_Constants'
import MapView, { Marker, ProviderProp } from 'react-native-maps';
import AsyncStorage from '@react-native-community/async-storage';
import * as API from '../../../Requests/WebService.js'
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import ImageResizer from 'react-native-image-resizer';
let { width, height } = Dimensions.get('window')
const ASPECT_RATIO = width / height
import ImagePicker from 'react-native-image-picker';
var images = []
var id = 0
let map_refs = ''
const createFormData = (image) => {
    var data = new FormData();
    data.append('profilePicture', {
        uri: Platform.OS === "android" ? image.uri : image.uri.replace("file://", ""),
        name: `ony${Date.now()}.jpg`,
        type: 'image/*'
    })
    return data
}
const maps = (lat, long) => {
    if (Platform.OS == 'ios') {
        return (
            <MapView
                style={styles.Maps}
                mapType='standard'
                ref={(ref) => map_refs = ref}
                // provider="google"
                //showsMyLocationButton={true}
                // showsUserLocation={true}
                // initialRegion={{
                //     latitude: lat,
                //     // latitude:''
                //     // longitude:'',
                //     // longitude:76.7018,
                //     // latitude:30.7089,
                //     longitude: long,
                //     latitudeDelta: 0.0922,
                //     longitudeDelta: 0.0421,
                // }}
                // scrollEnabled={true}>
                initialRegion={{
                    latitudeDelta: 0.0922,
                    longitudeDelta: 0.0421,
                    latitude: lat,
                    longitude: long,
                }}
                scrollEnabled={true}>
                {!isNaN(lat) && <Marker
                    coordinate={{
                        latitude: lat,
                        longitude: long,
                    }}
                    >
                    <Image resizeMode='contain' source={Map_Marker} style={{ height: 50, width: 50 }} />
                    {/* <Marker coordinate={{ latitude: '30.7089', longitude: '76.7018' }} /> */}
                </Marker>}
            </MapView>
        )
    } else {
        return (
            <MapView
                style={styles.Maps}
                mapType='standard'
                provider="google"
                showsMyLocationButton={true}
                showsUserLocation={true}
                ref={(ref) => map_refs = ref}
                initialRegion={{
                    latitude: lat,
                    longitude: long,
                    latitudeDelta: 0.0922,
                    longitudeDelta: 0.0421,
                }}
                scrollEnabled={true}>
                    {!isNaN(lat) && <Marker
                    coordinate={{
                        latitude: lat,
                        longitude: long,
                    }}>
                    <Image resizeMode='contain' source={Map_Marker} style={{ height: 50, width: 50 }} />
                    {/* <Marker coordinate={{ latitude: '30.7089', longitude: '76.7018' }} /> */}
                </Marker>}
                {/* <Marker coordinate={{ latitude: lat, longitude: long }} /> */}
            </MapView>
        )
    }
}

class Edit_BusinessProfile extends Component {
    constructor(props) {
        super(props)
        this.state = {
            businessUserId: '',
            businessName: '',
            firstName: '',
            lastName: '',
            address: '',
            email: '',
            mobileNumber: '',
            distance: '',
            availableHours: '',
            websiteLink: '',
            image: '',
            allImages: '',
            zipCode: '',
            isLoading: false,
           // subscriptionType: '',
            businessLocation: [],
            businessLatitude: '',
            businessLongitude: '',
        }
    }

    itemView(item, index) {
        return (
            <View style={{ alignItems: 'center', marginLeft: 15 }}>
                <Image
                    source={{ uri: item.image }}
                    style={{
                        width: 60, height: 60,
                        backgroundColor: APP_GREY, borderRadius: 10
                    }} />
                <TouchableOpacity onPress={() => this.updateImageData(index)}>
                    <Image
                        source={REMOVE_ICON}
                        style={{
                            width: 17, height: 17, borderRadius: 50,
                            backgroundColor: APP_WHITE, marginTop: -10
                        }} />
                </TouchableOpacity>
            </View>
        )
    }

    updateBusiness() {
        console.log('check adress', this.state.address)
        this.setState({ isLoading: true })
        API.UPDATE_BUSINESS(this.state.businessUserId, this.state.businessName, this.state.address, this.state.firstName, this.state.lastName, this.state.mobileNumber, this.state.allImages,
            this.state.zipCode, this.state.availableHours, this.state.websiteLink, this.state.businessLocation[0], this.state.businessLocation[1]).then((response) => {
                console.log("check ressponse of businesss", JSON.stringify(response))
                if (response.status == 1) {
                    this.setState({ isLoading: false }, async () => {
                        await AsyncStorage.setItem("user_info", JSON.stringify(response.data.getBusiness), () => { })
                        this.props.navigation.goBack()
                    })
                } else {

                }
            })
    }

    pickImage() {
        if (Platform.OS === 'android') {
            PermissionsAndroid.requestMultiple(
                [PermissionsAndroid.PERMISSIONS.CAMERA,
                PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
                PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE]
            ).then((result) => {
                if (result['android.permission.CAMERA']
                    && result['android.permission.READ_EXTERNAL_STORAGE']
                    && result['android.permission.WRITE_EXTERNAL_STORAGE'] === 'granted') {
                    ImagePicker.showImagePicker((response) => {
                        if (response.didCancel) {
                            console.log('User cancelled image picker');
                        } else if (response.error) {
                            console.log('ImagePicker Error: ', response.error);
                        } else {
                            const source = response;

                            this.setState({ isLoading: true }, () => {
                                this._compressImage(source)
                            })
                           
                        }
                    })
                } else if (result['android.permission.CAMERA']
                    || result['android.permission.READ_EXTERNAL_STORAGE']
                    || result['android.permission.WRITE_EXTERNAL_STORAGE'] === 'never_ask_again') {
                    alert("Required permissions denied");
                }
            });
        } else {
            ImagePicker.showImagePicker((response) => {
                if (response.didCancel) {
                    console.log('User cancelled image picker');
                } else if (response.error) {
                    console.log('ImagePicker Error: ', response.error);
                } else {
                    const source = response;
                    this.setState({ isLoading: true }, () => {
                        this._compressImage(source)
                    })
                }
            })
        }
    }

    async imageUpload(source) {
        try {
            let response = await fetch(
                API.BASE_URL + "fileupload",
                {
                    'method': 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data',
                    },
                    body: createFormData(source)
                }
            );

            if (response.status == 200) {
                response.json().then(data => {
                    images.push({ 'id': id, 'image': FOR_IMAGE_URL + data.filename })
                    id = id + 1
                    this.setState({ allImages: images, isLoading: false }, () => {
                        console.log('select', JSON.stringify(this.state.allImages))
                    })
                });
            } else {
                console.log('error', response)
            }
        } catch (error) {
            console.error(error);
        }
    }

    updateImageData(index) {
        const elements = this.state.allImages.filter(item => {
            return item.id !== index
        });
        if (elements.length == 0) {
            this.setState({ allImages: '' });
            images = [],
                index = 0
        } else if (elements[0] == "") {
            this.setState({ allImages: '' });
            images = [],
                index = 0
        } else {

            var newArray = []
            elements.map((data, index) => {
                console.log('index', index)
                data.id = index
                newArray.push(data)
            })
            images = []
            newArray.map((data, index) => {
                images.push(data)
            })
            this.setState({ allImages: images }, () => {
                console.log(JSON.stringify(this.state.allImages))
                this.setState({ image: this.state.allImages.reverse()[0].image})
            });
        }
    }

    async _compressImage(source) {
        this.setState({
            image:source.uri
        })
        var v = this;
        let response = await ImageResizer.createResizedImage(source.uri, 400, 400, 'JPEG', 80);
        if (response != null) {
            v.imageUpload(response)
        }
    }

    componentDidMount() {
        AsyncStorage.getItem("user_info").then((value) => {
            var data = JSON.parse(value)
            this.setState({
                businessName: data.businessName,
                address: data.businessAddress,
                email: data.businessuserEmail,
                mobileNumber: data.businessuserPhoneNumber,
                distance: '2 km',
                availableHours: '24 Hours',
                websiteLink: data.businesswebsiteLink,
                image: data.businessImages.reverse()[0].image,
                allImages: data.businessImages,
                businessUserId: data._id,
                firstName: data.firstname,
                lastName: data.lastname,
                zipCode: data.businessuserZipcode,
                subscriptionType: data.businesssubscrption,
                businessLocation: data.businessLocation.coordinates
            }, () => {
                map_refs.animateToRegion({
                    latitude: data.businessLocation.coordinates[0],
                    longitude: data.businessLocation.coordinates[1],
                    latitudeDelta: 0.0922,
                    longitudeDelta: 0.0421
                })
            })
            console.log(JSON.stringify(data, "??????????????"))
        }).done();


    }

    imageSelect() {
        //if (this.state.subscriptionType == '1')
            return (
                <View style={{
                    flexDirection: 'row', paddingTop: 20,
                    flex: 1, backgroundColor: APP_WHITE
                }}>
                    <TouchableOpacity onPress={() => this.pickImage()} style={{
                        width: 65, height: 60,
                        backgroundColor: '#e3e5e8', borderRadius: 10, marginLeft: 15, marginBottom: 10, shadowOffset: {
                            width: 0,
                            height: 1
                        },
                        shadowRadius: 1,
                        shadowOpacity: 0.5, borderTopWidth: 0,
                        elevation: Platform.OS == 'android' ? 5 : 0
                    }}>
                        <Image source={ADD_DOCUMENT} style={{ height: 22, width: 22, alignSelf: 'center', marginTop: 20 }}></Image>
                    </TouchableOpacity>
                    <FlatList
                        data={this.state.allImages}
                        horizontal
                        keyboardShouldPersistTaps={'always'}
                        showsHorizontalScrollIndicator={false}
                        style={{
                            padding: 15, paddingTop: 0,
                            paddingLeft: 5
                        }}
                        renderItem={({ item, index }) =>
                            this.itemView(item, index)}
                        keyExtractor={item => item.id}
                    />
                </View>
            )
    }


    render() {
        return (

            <View style={{ flex: 1, flexDirection: 'column', backgroundColor: APP_WHITE }}>
            <StatusBar barStyle={'dark-content'} />
            <OrientationLoadingOverlay
                visible={this.state.isLoading}
                color="white"
                indicatorSize="large"
            />

            <KeyboardAvoidingView
                style={{
                    flex: 1,
                    flexDirection: 'column',
                }}
                behavior={Platform.OS=== 'ios'?"height":"height"} enabled
                keyboardVerticalOffset={Platform.OS === 'ios' ? -10 : ''}>
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    style={{ marginBottom: 25 }}
                    keyboardShouldPersistTaps='always'
                    >
                    <ImageBackground
                        source={{ uri: this.state.image }}
                        style={styles.Image}>
                        <View style={styles.IconContainer}>
                            <View style={{ backgroundColor: 'transparent', width: "65%", marginTop: 20 }}>
                                <TouchableOpacity style={{ width: 60, height: 30, backgroundColor: 'transparent' }} onPress={() => this.props.navigation.goBack()}>
                                    <Image source={BACK_ICON} style={[styles.Icons], { alignSelf: 'center', tintColor: APP_BLACK }} />
                                </TouchableOpacity>
                            </View>
                            {/* <View style={{flexDirection:'row',width:'35%'}}>
                            <TouchableOpacity onPress={()=>this.props.navigation.navigate('Edit_BusinessProfile')} style={{width:40,height:30,backgroundColor:'transparent'}}>
                                 <Image source={FAVORITE_DETAIL} style={[styles.Icons],{alignSelf:'center'}} />
                                </TouchableOpacity>
                                <TouchableOpacity style={{width:40,height:30,backgroundColor:'transparent'}}>
                                <Image source={SHARE} style={[styles.Icons],{alignSelf:'center'}} />
                                </TouchableOpacity>
                            <TouchableOpacity style={{width:40,height:30,backgroundColor:'transparent'}}>
                                <Image source={EDIT_ICON} style={[styles.Icons],{alignSelf:'center'}} />
                            </TouchableOpacity>
                            </View> */}
                        </View>
                    </ImageBackground>

                    {this.imageSelect()}
                    <View style={{
                        flex: 1, backgroundColor: APP_WHITE,
                        paddingTop: 10
                    }}>
                        <View style={styles.textInputcontainer}>
                            <Text style={styles.titleText}>
                                Business Name
                            </Text>
                            <TextInput
                                style={styles.textInputStyle}
                                //placeholder={'Email'}
                                selectionColor={APP_GREY}
                                placeholderTextColor={APP_GREY}
                                value={this.state.businessName}
                                onChangeText={(text) => this.setState({ businessName: text })}
                            />
                        </View>
                        <View style={styles.textInputcontainer}>
                            <Text style={styles.titleText}>
                                Address
                            </Text>
                            <View style={{
                                flex: 1, width: width - 10,
                                // width: Platform.OS=="android"? 320 : 340, 
                                justifyContent: 'center', alignSelf: 'flex-start', margin: 5, borderBottomColor: '#e3e5e8',
                                borderBottomWidth: 2,
                                paddingVertical: 12,
                        
                            }}>
                                <GooglePlacesAutocomplete
                                    ref={ref => { this.placesRef = ref }}
                                    editable={true}
                                    clearButtonMode={true}
                                    placeholder={this.state.address}
                                    minLength={2}
                                    autoFocus={false}
                                    returnKeyType={'search'}
                                    listViewDisplayed={false}
                                    fetchDetails={true}
                                    keyboardShouldPersistTaps={'always'}
                                    enablePoweredByContainer={false}
                                    listViewDisplayed={false}
                                    onPress={(data, details = null) => {
                                        console.log('data????????', JSON.stringify(details))

                                        let region = {
                                            latitude: details.geometry.location.lat,
                                            longitude: details.geometry.location.lng,
                                            latitudeDelta: 0.0922,
                                            longitudeDelta: 0.0421
                                        }
                                        let coords = [parseFloat(details.geometry.location.lat), parseFloat(details.geometry.location.lng)]
                                        this.setState({
                                            latitude: region.latitude,
                                            longitude: region.longitude,
                                            region: region,
                                            address: data.description,
                                            businessLocation: coords
                                        }, () => {
                                            map_refs.animateToRegion({
                                                latitude: this.state.businessLocation[0],
                                                longitude: this.state.businessLocation[1],
                                                latitudeDelta: 0.0922,
                                                longitudeDelta: 0.0421
                                            })
                                        })
                                        //  let region = {
                                        //  latitude: details.geometry.location.lat,
                                        //  longitude: details.geometry.location.lng,
                                        //  latitudeDelta: LATITUDE_DELTA,
                                        //  longitudeDelta: LONGITUDE_DELTA
                                    }}
                                    textInputProps={{
                                        placeholderTextColor: 'grey',

                                        onChangeText: (text) => {
                                            console.log('onChangeText???', text)

                                            if (text.length === 0) {
                                                console.log('onChangeText??????????')
                                                // this.onUserPinDragEnd(
                                                // {
                                                // latitude: 0,
                                                // longitude: 0
                                                // }, 'changeText'
                                                // )
                                            }
                                        }
                                    }}
                                    getDefaultValue={() => {
                                        if (this.state.region !== null && this.state.region !== undefined) {
                                            return this.state.description;
                                        } // text input default value
                                        return '';
                                    }}

                                    query={{
                                        key: 'AIzaSyAYbSbQqDQm2u092NYdX9-CxcYyRDY6YSk',
                                        language: 'en',
                                    }}

                                    styles={{

                                        textInput: {
                                            backgroundColor: APP_WHITE,
                                            borderColor: '#e3e5e8',
                                            borderBottomWidth: 0.1,
                                            //borderRadius: 30,
                                            height: 25,
                                            //backgroundColor: APP_WHITE
                                        },

                                        horizontal: {
                                            flexDirection: 'row',
                                            justifyContent: 'space-around',
                                        },

                                        listView: {
                                            // zIndex: 1000,
                                            backgroundColor: 'transparent',
                                            color: 'black', //To see where exactly the list is
                                            //position: 'absolute',
                                            //top:43
                                        },
                                        textInputContainer: {
                                            // zIndex: 2000,
                                            // backgroundColor: '',
                                        },
                                        description: {
                                            fontWeight: 'bold',
                                        },
                                        predefinedPlacesDescription: {
                                            // color: 'red'
                                        },

                                    }}

                                    currentLocation={true}
                                    // filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities
                                    debounce={100} // debounce the requests in ms. Set to 0 to remove debounce. By default 0ms.

                                />
                            </View>

                        {/* </View> */}
                        <View style={styles.textInputcontainer}>
                            <Text style={styles.titleText}>
                                Email Address
                            </Text>
                            <TextInput
                                style={styles.textInputStyle}
                                //placeholder={'Email'}
                                selectionColor={APP_GREY}
                                editable={false}
                                placeholderTextColor={APP_GREY}
                                value={this.state.email}
                                onChangeText={(text) => this.setState({ email: text })}
                            />
                        </View>
                        <View style={styles.textInputcontainer}>
                            <Text style={styles.titleText}>
                                Mobile Number
                            </Text>
                            <TextInput
                                style={styles.textInputStyle}
                                //placeholder={'Email'}
                                selectionColor={APP_GREY}
                                placeholderTextColor={APP_GREY}
                                value={this.state.mobileNumber}
                                onChangeText={(text) => this.setState({ mobileNumber: text })}
                            />
                        </View>
                        {/* <View style={styles.textInputcontainer}>
<Text style={styles.titleText}>
Distance
</Text>
<TextInput
style={styles.textInputStyle}
//placeholder={'Email'}
selectionColor={APP_GREY}
placeholderTextColor={APP_GREY}
value={this.state.distance}
onChangeText={(text) => this.setState({ distance: text })}
/>
</View> */}
                        <View style={styles.textInputcontainer}>
                            <Text style={styles.titleText}>
                                Available Hours
                            </Text>
                            <TextInput
                                style={styles.textInputStyle}
                                //placeholder={'Email'}
                                selectionColor={APP_GREY}
                                placeholderTextColor={APP_GREY}
                                value={this.state.availableHours}
                                onChangeText={(text) => this.setState({ availableHours: text })}
                            />


                        </View>
                        <View style={styles.textInputcontainer}>
                            <Text style={[styles.titleText, { marginTop: 10 }]}>
                                Directions
                            </Text>
                            
                        </View>
                        <View style={styles.MapsContainer}>
                                {maps(this.state.businessLocation[0], this.state.businessLocation[1])}
                            </View>

                        <View style={styles.textInputcontainer}>
                            <Text style={styles.titleText}>
                                Website Link
                            </Text>
                            <TextInput
                                style={[styles.textInputStyle, { color: 'dodgerblue' }]}
                                //placeholder={'Email'}
                                selectionColor={APP_GREY}
                                placeholderTextColor={APP_GREY}
                                value={this.state.websiteLink}
                                onChangeText={(text) => this.setState({ websiteLink: text })}
                            />
                        </View>

                        <TouchableOpacity onPress={() => {
                            this.updateBusiness()
                        }} style={[styles.textInputcontainer], {
                            paddingVertical: 17, backgroundColor: '#e3e5e8', width: '90%', alignSelf: 'center', borderRadius: 10, marginTop: 10, marginBottom: 10, shadowOffset: {
                                width: 0,
                                height: 2
                            },
                            shadowRadius: 1,
                            shadowOpacity: 0.5, borderTopWidth: 0, elevation: Platform.OS == 'android' ? 5 : 0
                        }}>
                            <Text style={[styles.titleText], { alignSelf: 'center', color: APP_BLACK, fontFamily: FONT_SEMIBOLD }}>
                                Update
                            </Text>
                        </TouchableOpacity>

                    </View>
                    </View>
                </ScrollView>
            </KeyboardAvoidingView>
        </View >
        )
    }


} const styles = StyleSheet.create({
    titleText: {
        fontSize: 15,
        color: APP_BLACK,
        fontFamily: FONT_MEDIUM,
        marginHorizontal: 15
    },
    textInputStyle: {
        fontSize: 14,
        color: APP_GREY,
        fontFamily: FONT_MEDIUM,
        marginTop: 2,
        marginHorizontal: 15
    },
    textInputcontainer: {
        borderBottomColor: '#e3e5e8',
        borderBottomWidth: 2,
        paddingVertical: 15,

    },
    SafeArea: {
        flex: 1,
        backgroundColor: APP_BLACK,
    },
    Image: {
        width: Dimensions.get('window').width,
        resizeMode: 'cover',
        height:250
    },
    IconContainer: {
        paddingHorizontal: 0,
        marginTop: 20,
        flexDirection: 'row',
        width: '100%',
        backgroundColor: 'transparent',
        //justifyContent:'space-around'
    },
    Icons: {
        width: 18,
        height: 18,
        marginTop: 20,
        resizeMode: 'contain',
        tintColor: APP_WHITE,
    },
    AddIcon: {
        width: 20,
        height: 20,
        marginTop: 0,
        right: 10,
        resizeMode: 'contain',
        tintColor: APP_WHITE,
    },
    Border: {
        backgroundColor: '#2d2e2d',
        width: Dimensions.get('window').width,
        height: 0.4,
    },
    TextContainer: {
        paddingHorizontal: 10,
        paddingVertical: 20,
    },
    TextHeader: {
        //fontWeight: 'normal',
        color: APP_WHITE,
        fontSize: 14,
        fontFamily: FONT_SEMIBOLD
    },
    TextNormal: {
        color: APP_GREY,
        fontFamily: FONT_MEDIUM,
        marginTop: 5,
        fontSize: 14,
    },
    MapsContainer: {
        width: '90%',
        height: 140,
        alignSelf: 'center',
        overflow: 'hidden',
        borderRadius: 10,
        marginTop: 10,

    },
    Maps: { flex: 1 },
    Icon: {
        alignSelf: 'center',
        position: 'absolute',
        right: 20,
        color: APP_WHITE,
        fontSize: 25,
    },
    RatingContainer: {
        flex: 1,
        flexDirection: 'row',
        paddingVertical: 15,
        paddingStart: 20,
    },
    ReviewContainer: {
        flex: 1,
        flexDirection: 'column',
        marginStart: 20,
        marginEnd: 20,
    },
    ReviewName: {
        color: APP_WHITE,
        fontSize: 16,
        fontFamily: FONT_SEMIBOLD
    },
    ReviewText: {
        color: APP_GREY,
        marginTop: 5,
        fontSize: 11,
        fontFamily: FONT_REGULAR
    },
    RatingBar: {
        flex: 1,
        position: 'absolute',
        right: 0,
        alignSelf: 'center',
    },
})
export default Edit_BusinessProfile





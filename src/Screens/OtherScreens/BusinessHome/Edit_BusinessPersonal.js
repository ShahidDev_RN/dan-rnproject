import React, {Component} from 'react';
import {View,
    Dimensions ,
    Text,
    StyleSheet,
    TouchableOpacity,
    Image,
    TextInput,
    BackHandler,
    Alert,
    SafeAreaView,
    Platform,
    ScrollView,
    KeyboardAvoidingView,StatusBar,
    ImageBackground,
    Keyboard} from 'react-native';
import * as API from '../../../Requests/WebService.js'
import AsyncStorage from '@react-native-community/async-storage';
import {FONT_BLACK,FONT_BOLD,FONT_EXTRABOLD,FONT_EXTRALIGHT,FONT_LIGHT,FONT_SEMIBOLD,TOGGLE_BUTTON_OFF,TOGGLE_BUTTON_ON,
    FONT_THIN,APP_WHITE,APP_BLACK,APP_FACEBOOK,BACK_ICON, FONT_REGULAR,FONT_MEDIUM,NOTIFICATION_ICON, APP_GREY} from '../../../Constants/App_Constants.js'
    import CheckBox from 'react-native-check-box';
    let { width, height } = Dimensions.get('window')
    const ASPECT_RATIO = width / height
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
class EditBusinessPersonal extends Component {
    constructor(props){
        super(props);
        this.state={
            longitude:'76.7018',
            latitude:'30.7089',
            deviceToken:'',
            deviceType:'',
            firstname:'',
            lastname:'',
            email:'',
            password:'',
            phonenumber:'',
            zipcode:'',
            deviceType:'',
            user_type:'manual',
            fb_id:'',
            keyboardavoid:false,
            isChecked:false,
            serverError:false,
            serverErrorText:'Error',
            loginText:'',
            avoidEnable:false,
            isloading:false
        }  
      } 
      header(){
        if(Platform.OS=='ios'){
          return(
            <View style={{height:65,width:'100%',backgroundColor:APP_BLACK,justifyContent:'flex-end',borderBottomColor:'#2d2e2d',borderBottomWidth:3}}>
                <View style={{height:'80%',width:'90%',backgroundColor:'transparent',alignSelf:'center'}}>
                    <View style={{flexDirection:'row',justifyContent:'space-between',height:'100%',width:'55%',backgroundColor:'transparent',alignSelf:'flex-start'}}>
                        <TouchableOpacity onPress={()=>this.props.navigation.navigate('Home')}>
                        <Image source={BACK_ICON} style={{marginTop:13, height:17,width:25}}></Image>
                        </TouchableOpacity>
                        <Text style={{color:'white',fontSize:18,marginTop:10,fontFamily:FONT_MEDIUM,width:200,marginLeft:15}}>Edit Business Details</Text>
                    </View>
                </View>
            </View>
          ) 
        }else{
          return(
            <View style={{height:65,width:'100%',backgroundColor:APP_BLACK,justifyContent:'flex-end',borderBottomColor:'#2d2e2d',borderBottomWidth:3}}>
            <View style={{height:'80%',width:'90%',backgroundColor:'transparent',alignSelf:'center'}}>
                <View style={{flexDirection:'row',justifyContent:'space-between',height:'100%',width:'55%',backgroundColor:'transparent',alignSelf:'flex-start'}}>
                    <TouchableOpacity onPress={()=>this.props.navigation.navigate('Home')}>
                    <Image source={BACK_ICON} style={{marginTop:13, height:17,width:25}}></Image>
                    </TouchableOpacity>
                    <Text style={{color:'white',fontSize:18,marginTop:10,fontFamily:FONT_MEDIUM,width:200,marginLeft:20}}>Edit Business Details</Text>
                </View>
            </View>
        </View>
          )
        }
      }

      componentDidMount(){
        if(Platform.OS=='ios'){
            this.setState({deviceType:'ios'})
        }else{
            this.setState({deviceType:'android'})
        }
        if(global.type=='User'){
            this.setState({loginText:'Register'})
        }else{
            this.setState({loginText:'Next'})
        }
        this.setState({deviceToken:'dMHNX87sSuqqEqhtXeT4K:APA91bFxsGOzSiTtGbasYHeeKIt6FvgKKrjd0j1CDOBXtSL6ryKAIKdJ1HtDQJYUMOHbVL5KLBTvbild5XtOTG1yVSnFSXGZZI1uHsvla2eRivH5gtwwwaEsV33654g_VIi0PVPn4TKl'})
    }
    

    handleFirstname=(text)=>{
        this.setState({firstname:(text),serverError:false})
    }
    handleLastname=(text)=>{
        this.setState({lastname:(text),serverError:false})
    }
    handleEmail=(text)=>{
        this.setState({email:(text),serverError:false})
    }
    
    handlePhonenumber=(text)=>{
        this.setState({phonenumber:(text),serverError:false})
    }
    handleZipcode=(text)=>{ 
        this.setState({zipcode:(text),serverError:false})
    }

   
    onFetchRegister(longitude,latitude,firstname,lastname,email,phonenumber,password,zipcode,user_type,fb_id,deviceToken,deviceType){
        // this.setState({isloading:false})
        if(global.type=='User'){
            API.REGISTER(longitude,latitude,firstname,lastname,email,phonenumber,password,zipcode,user_type,fb_id,deviceToken,deviceType).then((response)=>{
                if(response.status==1){
                    AsyncStorage.setItem("user_info", JSON.stringify(response.data.user),()=>{})
                    AsyncStorage.setItem("user_token", (response.data.token),()=>{
                        this.setState({isloading:false},()=>{
                            this.props.navigation.navigate('Bottomtab')
                        })
                        });
                }else if(response.status==0){
                    this.setState({serverError:true,serverErrorText:response.message,isloading:false})
                }
            })
        }else{
            this.props.navigation.navigate('BusinessRegister',{
                firstname:this.state.firstname,
                lastname:this.state.lastname,
                phonenumber: this.state.phonenumber,
                email:this.state.email.toLowerCase(),
                password:this.state.password,
                zipcode:this.state.zipcode
            },()=>{Keyboard.dismiss()})
            this.setState({isloading:false})
        }
    }

    signUp(){
            this.register(this.state.latitude,this.state.longitude,this.state.firstname,this.state.lastname,this.state.email.toLowerCase(),this.state.phonenumber,this.state.password,this.state.zipcode,this.state.user_type,this.state.fb_id,this.state.deviceToken,this.state.deviceType)
            //this.props.navigation.navigate('BusinessRegister')
        }

    
    register=(longitude,latitude,firstname,lastname,email,phonenumber,password,zipcode,user_type,fb_id,deviceToken,deviceType)=>{
            if(email==''&& password==''&&lastname==''&&firstname==''&&phonenumber==''&&zipcode==''){
                this.setState({serverError : true , serverErrorText: 'Fields Empty'});
            }else if(firstname==''){
                this.setState({serverError : true , serverErrorText: 'Enter FirstName'});
            }else if(lastname==''){
                this.setState({serverError : true , serverErrorText: 'Enter LastName'});
            }else if(email==''|| !email.includes('.com') || !email.includes('@')){
                if(email != ''){
                    if(!email.includes('.com')){
                        this.setState({serverError : true , serverErrorText: 'Email Format Not Correct'})
                    }
                    if(!email.includes('@')){
                        this.setState({serverError : true , serverErrorText: 'Email Format Not Correct'})
                    }
                 }else{
                    this.setState({serverError : true , serverErrorText: 'Enter Email'});
                 } 
            }else if(password==''){
                this.setState({serverError : true , serverErrorText: 'Enter Password'});
            }else if(password.length<4){
                this.setState({serverError : true , serverErrorText: 'Password is too short'});
            }
            else if(phonenumber==''){
                this.setState({serverError : true , serverErrorText: 'Enter PhoneNumber'});
            }
            else if(phonenumber.length<9){
                this.setState({serverError : true , serverErrorText: 'Phone Number is too short'});
            }else if(phonenumber.length>15){
                this.setState({serverError : true , serverErrorText: 'Phone Number exceeds the limit'});
            }
            else if(zipcode==''){
                this.setState({serverError : true , serverErrorText: 'Enter Zipcode'});
            }else if(zipcode.length<4){
                this.setState({serverError : true , serverErrorText: 'Enter valid Zipcode'});
            }else{
                this.setState({isloading:true})
                this.onFetchRegister(longitude,latitude,firstname,lastname,email,phonenumber,password,zipcode,user_type,fb_id,deviceToken,deviceType);
            }
    }



    render(){
        return(
            <SafeAreaView forceInset={{ top: 'always' }} style={{flex:1,backgroundColor:APP_WHITE}}>
                <StatusBar barStyle={'light-content'}></StatusBar>
                {this.header()}
                <OrientationLoadingOverlay
                visible={this.state.isloading}
                color="white"
                indicatorSize="large"
                />
                <KeyboardAvoidingView 
                    style={{ height:height, backgroundColor:APP_BLACK,
                    justifyContent: 'center',}} 
                    behavior="padding" enabled={this.state.avoidEnable}  
                    keyboardVerticalOffset={10}> 
               <View style={styles.OptionsView}>
                {/* <Text style={styles.welcomeText}>Welcome to Signup</Text> */}
             
                   <View style={styles.view}> 
                        <View style={styles.feildBorder}>
                            <TextInput  ref={input => { this.textInput = input }}
                                style={styles.textfeild}  
                                placeholder="Enter First Name"
                                onFocus={()=>this.setState({avoidEnable:false})}
                                keyboardType="default"
                                selectionColor={APP_WHITE}
                                placeholderTextColor={APP_WHITE}
                                onChangeText = {this.handleFirstname}
                                /> 
                        </View>
            
                        <View style={styles.feildBorder1}>
                            <TextInput  ref={input => { this.textInput = input }}
                                style={styles.textfeild}  
                                placeholder="Enter Last Name"
                                keyboardType="default"
                                onFocus={()=>this.setState({avoidEnable:false})}
                                selectionColor={APP_WHITE}
                                placeholderTextColor={APP_WHITE}
                                onChangeText = {this.handleLastname}
                                /> 
                        </View>
                        <View style={styles.feildBorder1}>
                            <TextInput  ref={input => { this.textInput = input }}
                                style={styles.textfeild}  
                                placeholder="Enter Email"
                                keyboardType="email-address"
                                onFocus={()=>this.setState({avoidEnable:false})}
                                selectionColor={APP_WHITE}
                                placeholderTextColor={APP_WHITE}
                                onChangeText = {this.handleEmail}
                                /> 
                        </View>
                      
                        <View style={styles.feildBorder1}>
                            <TextInput  ref={input => { this.textInput = input }}
                                style={styles.textfeild}  
                                onFocus={()=>this.setState({avoidEnable:true})}
                                placeholder="Enter Phone Number"
                                keyboardType="phone-pad"
                                returnKeyType="done"
                                selectionColor={APP_WHITE}
                                placeholderTextColor={APP_WHITE}
                                onChangeText = {this.handlePhonenumber}
                                /> 
                        </View>
                        
                        <View style={styles.feildBorder1}>
                            <TextInput  ref={input => { this.textInput = input }}
                                style={styles.textfeild}  
                                placeholder="Enter Zip Code"
                                keyboardType="number-pad"
                                returnKeyType="done"
                                onFocus={()=>this.setState({avoidEnable:true})}
                                selectionColor={APP_WHITE}
                                placeholderTextColor={APP_WHITE}
                                onChangeText = {this.handleZipcode}
                                /> 
                        </View>
                           {this.state.serverError?<View style={{width:'100%', justifyContent:'center', alignItems:'center',height:27,backgroundColor:'transparent',marginTop:10}}>
                                <Text style={{alignSelf:'center',color:"#dd4b39",fontFamily:FONT_SEMIBOLD,fontSize:16,marginTop:10}}>{this.state.serverErrorText}</Text>
                                </View>:null}

                        <View style={styles.button}>
                            <TouchableOpacity onPress={()=>this.signUp()} style={{height:'100%'}}>
                                <Text style={styles.buttonText}>Update</Text>
                            </TouchableOpacity>
                        </View>

                    </View>
                    
                    </View>
                    </KeyboardAvoidingView>
                    
               {/* </View> */}
            </SafeAreaView>
        )
    }
};

const styles = StyleSheet.create({
    OptionsView:{
        height:height,
        width:width,
        backgroundColor:APP_BLACK
    },
    otherText:{
        fontSize:14,
        fontFamily:FONT_MEDIUM,
        color:APP_WHITE
        },
   view:{
        width:"85%",
        height:"80%",
        alignSelf:'center',
        backgroundColor:'transparent',
        marginTop:height<896?40:55,
   },
    feildBorder:{
        width:width-40,
        height:height<896?50:50,
        borderRadius:30,
        borderWidth:1.5,
        borderColor:APP_WHITE,
        backgroundColor:APP_BLACK,
        alignSelf:'center'
    },
    feildBorder1:{
        width:width-40,
        height:height<896?50:50,
        borderRadius:30,
        borderWidth:1.5,
        marginTop:15,
        borderColor:APP_WHITE,
        backgroundColor:APP_BLACK,
        alignSelf:'center'
    },
    textfeild:{
        width:width-80,
        height:height<896?47:47,
        fontSize:13,
        fontFamily:FONT_MEDIUM,
        borderRadius:30,
        alignSelf:'center',
        color:APP_WHITE,
        },
    button:{
        width:width-45,
        height:height<896?50:50,
        borderRadius:30,
        borderWidth:1,
        marginTop:40,
        borderColor:APP_BLACK,
        backgroundColor:APP_WHITE,
        alignSelf:'center'
    },
    buttonText:{
        fontSize:15,
        fontFamily:FONT_SEMIBOLD,
        color:APP_BLACK,
        alignSelf:'center',
        marginTop:height<896?13:13
    },
    welcomeText:{
        alignSelf:'center',
        fontSize:height<896?20:24,
        fontFamily:FONT_MEDIUM,
        backgroundColor:APP_BLACK,
        color:APP_WHITE,
        marginTop:height<896?'15%':'25%'
    },
  
    registerTextUnderline:{
        color:APP_WHITE,
        fontSize:14,
        marginTop:35,
        fontFamily:FONT_SEMIBOLD,
        alignSelf:'center',
    }

});

export default EditBusinessPersonal;

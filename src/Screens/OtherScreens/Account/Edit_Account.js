import React, { Component } from 'react';
import {
    View,
    Dimensions,
    Text,
    StyleSheet,
    TouchableOpacity,
    Image,
    TextInput,
    BackHandler,
    Alert,
    SafeAreaView,
    Platform,
    ScrollView,
    FlatList,
    KeyboardAvoidingView, StatusBar,
    ImageBackground
} from 'react-native';
import {
    FONT_BLACK, FONT_BOLD, FONT_EXTRABOLD, FONT_EXTRALIGHT, FONT_LIGHT, FONT_SEMIBOLD,
    FONT_THIN, APP_WHITE, APP_BLACK, APP_FACEBOOK, BACK_ICON, FONT_REGULAR, FONT_MEDIUM, NOTIFICATION_ICON, APP_GREY, RESTAURANT, FONT_ULTRALIGHT, APP_BORDER
} from '../../../Constants/App_Constants.js'
import AsyncStorage from '@react-native-community/async-storage';
import * as API from '../../../Requests/WebService.js'
let { width, height } = Dimensions.get('window')
const ASPECT_RATIO = width / height
class EditAccount extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userLastname: '',
            userFirstname: '',
            userEmail: '',
            userPassword: '',
            userPhonenumber: '',
            userZipcode: '',
            userDescription: '',
            api_token: '',
            serverError: false,
            serverErrorText: '',
            keyboardAvoid: false
        }
    }

    header() {
        if (Platform.OS == 'ios') {
            return (
                <View style={{ height: 65, width: '100%', backgroundColor: 'white', justifyContent: 'flex-end', borderBottomColor: '#e3e5e8', borderBottomWidth: 3 }}>
                    <View style={{ height: '80%', width: '90%', backgroundColor: 'transparent', alignSelf: 'center' }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', height: '100%', width: '48%', backgroundColor: 'transparent', alignSelf: 'flex-start' }}>
                            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                                <Image source={BACK_ICON} style={{ marginTop: 13, height: 17, width: 24, tintColor: APP_BLACK }}></Image>
                            </TouchableOpacity>
                            <Text style={{ color: APP_BLACK, fontSize: 18, marginTop: 10, fontFamily: FONT_SEMIBOLD }}>Edit Account</Text>
                        </View>
                    </View>
                </View>
            )
        } else {
            return (
                <View style={{ height: 65, width: '100%', backgroundColor: 'white', justifyContent: 'flex-end', borderBottomColor: '#e3e5e8', borderBottomWidth: 3 }}>
                    <View style={{ height: '80%', width: '90%', backgroundColor: 'transparent', alignSelf: 'center' }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', height: '100%', width: '48%', backgroundColor: 'transparent', alignSelf: 'flex-start' }}>
                            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                                <Image source={BACK_ICON} style={{ marginTop: 13, height: 17, width: 24, marginLeft: 5, tintColor: APP_BLACK }}></Image>
                            </TouchableOpacity>
                            <Text style={{ color: APP_BLACK, fontSize: 18, marginTop: 10, fontFamily: FONT_SEMIBOLD, width: 150, marginLeft: 15 }}>Edit Account</Text>
                        </View>
                    </View>
                </View>
            )
        }
    }

    componentDidMount() {
        AsyncStorage.getItem("user_information").then((value) => {
            var data = JSON.parse(value)

            this.setState({
                userFirstname: data.data.user.userFirstName,
                userLastname: data.data.user.userLastName,
                userEmail: data.data.user.userEmail,
                userPhonenumber: data.data.user.userPhoneNumber,
                userZipcode: data.data.user.userZipcode,
                userPassword: data.data.user.userPassword,
                userDescription: data.data.user.userdescription
            })
        }).done();

        AsyncStorage.getItem("user_token").then((value) => {
            this.setState({ api_token: value })
        }).done();
    }

    edit_Account() {
        if (this.state.userFirstname == '' && this.state.userLastname == '' && this.state.userZipcode == '' && this.state.userPhoneNumber == '' && this.state.userDescription == '') {
            this.setState({ serverError: true, serverErrorText: 'Fields Empty' });
        } else if (this.state.userFirstname == '') {
            this.setState({ serverError: true, serverErrorText: 'Enter Valid Firstname' });
        } else if (this.state.userLastname == '') {
            this.setState({ serverError: true, serverErrorText: 'Enter Valid Lastname' });
        } else if (this.state.userZipcode == '') {
            this.setState({ serverError: true, serverErrorText: 'Enter Valid Zipcode' });
        } else if (this.state.userPhonenumber == '') {
            this.setState({ serverError: true, serverErrorText: 'Enter Valid Phonenumber' });
        } else if (this.state.userPhonenumber.length < 9) {
            this.setState({ serverError: true, serverErrorText: 'Phone Number is too short' });
        } else if (this.state.userPhonenumber.length > 15) {
            this.setState({ serverError: true, serverErrorText: 'Phone Number exceeds the limit' });
        }
        else {
            this.setState({ isloading: true })
            API.UPDATE_PROFILE(this.state.api_token, this.state.userFirstname, this.state.userLastname, this.state.userZipcode, this.state.userPhonenumber, this.state.userDescription).then((response) => {
                if (response.status == 1) {
                    this.setState({ isloading: false }, () => {
                        this.props.navigation.goBack()
                    })
                } else if (response.status == 0) {
                    this.setState({ serverError: true, serverErrorText: response.message, isloading: false })
                }
            })
        }

    }

    handleFirstname = (text) => {
        this.setState({ userFirstname: (text), serverError: false })
    }
    handleLastname = (text) => {
        this.setState({ userLastname: (text), serverError: false })
    }
    handlePhonenumber = (text) => {
        this.setState({ userPhonenumber: (text), serverError: false })
    }
    handleZipcode = (text) => {
        this.setState({ userZipcode: (text), serverError: false })
    }
    handleDescription = (text) => {
        this.setState({ userDescription: (text), serverError: false })
    }

    render() {
        return (
            <SafeAreaView forceInset={{ top: 'always' }} style={{ flex: 1, backgroundColor: APP_WHITE }}>
                {this.header()}
                <StatusBar backgroundColor={APP_BLACK}></StatusBar>

                <KeyboardAvoidingView
                    style={{
                        height: height, backgroundColor: APP_WHITE,
                        justifyContent: 'center',
                    }}
                    behavior="padding" enabled={this.state.keyboardAvoid}
                    keyboardVerticalOffset={1}>
                {/* <ScrollView> */}
                    <View scrollEnabled={false} style={styles.OptionsView}>
                        <View style={styles.view}>
                            <View style={styles.feildBorder}>
                                <TextInput ref={input => { this.textInput1 = input }}
                                    style={styles.textfeild}
                                    placeholder="Enter Name"
                                    keyboardType="default"
                                    value={this.state.userFirstname}
                                    onFocus={() => this.setState({ keyboardAvoid: false })}
                                    placeholderTextColor={APP_BLACK}
                                    selectionColor={APP_BLACK}
                                    onChangeText={this.handleFirstname}
                                />
                            </View>
                            <View style={styles.feildBorder1}>
                                <TextInput ref={input => { this.textInput2 = input }}
                                    style={styles.textfeild}
                                    placeholder="Enter Last Name"
                                    keyboardType="default"
                                    onFocus={() => this.setState({ keyboardAvoid: false })}
                                    value={this.state.userLastname}
                                    placeholderTextColor={APP_BLACK}
                                    selectionColor={APP_BLACK}
                                    onChangeText={this.handleLastname}
                                />
                            </View>
                            <View style={styles.feildBorder1}>
                                <TextInput ref={input => { this.textInput3 = input }}
                                    style={styles.textfeild}
                                    placeholder="Enter Email"
                                    editable={false}
                                    keyboardType="email-address"
                                    value={this.state.userEmail}
                                    placeholderTextColor={APP_BLACK}
                                    selectionColor={APP_BLACK}
                                    onChangeText={this.handleEmail}
                                />
                            </View>

                            {/* <View style={styles.feildBorder1}>
                            <TextInput  ref={input => { this.textInput4 = input }}
                                style={styles.textfeild}  
                                placeholder="Password"
                                secureTextEntry={true}
                                editable={false}
                                keyboardType="default" 
                                value={this.state.userPassword}
                                placeholderTextColor={APP_WHITE}
                                selectionColor={APP_WHITE}
                                //onChangeText = {this.handleEmail}
                                /> 
                        </View> */}
                            <View style={styles.feildBorder1}>
                                <TextInput ref={input => { this.textInput5 = input }}
                                    style={styles.textfeild}
                                    placeholder="Enter Phone Number"
                                    keyboardType="phone-pad"
                                    returnKeyType='done'
                                    value={this.state.userPhonenumber}
                                    placeholderTextColor={APP_BLACK}
                                    selectionColor={APP_BLACK}
                                    onFocus={() => this.setState({ keyboardAvoid: true })}
                                    onChangeText={this.handlePhonenumber}
                                />
                            </View>
                            <View style={styles.feildBorder1}>
                                <TextInput ref={input => { this.textInput6 = input }}
                                    style={styles.textfeild}
                                    placeholder="Enter Zip Code"
                                    keyboardType="phone-pad"
                                    returnKeyType='done'
                                    value={this.state.userZipcode}
                                    placeholderTextColor={APP_BLACK}
                                    selectionColor={APP_BLACK}
                                    onFocus={() => this.setState({ keyboardAvoid: true })}
                                    onChangeText={this.handleZipcode}
                                />
                            </View>
                            <View style={styles.feildBorder1}>
                                <TextInput ref={input => { this.textInput7 = input }}
                                    style={styles.textfeild}
                                    placeholder="Description"
                                    keyboardType="default"
                                    value={this.state.userDescription}
                                    placeholderTextColor={APP_BLACK}
                                    selectionColor={APP_BLACK}
                                    onFocus={() => this.setState({ keyboardAvoid: true })}
                                    onChangeText={this.handleDescription}
                                />
                            </View>
                            {this.state.serverError ? <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center', height: 20, backgroundColor: 'tra', marginTop: 10 }}>
                                <Text style={{ alignSelf: 'center', color: "#dd4b39", fontFamily: FONT_SEMIBOLD, fontSize: 16 }}>{this.state.serverErrorText}</Text>
                            </View> : null}
                            <View style={styles.button}>
                                <TouchableOpacity onPress={() => this.edit_Account()} style={{ height: '100%' }}>
                                    <Text style={styles.buttonText}>Update</Text>
                                </TouchableOpacity>
                            </View>

                        </View>
                    </View>
                {/* </ScrollView> */}
                </KeyboardAvoidingView>
            </SafeAreaView>
        )
    }
};

const styles = StyleSheet.create({
    OptionsView: {
        height: height,
        width: width,
        backgroundColor: APP_WHITE
    },
    view: {
        width: "85%",
        height: "80%",
        alignSelf: 'center',
        backgroundColor: 'transparent',
        marginTop: 15
    },
    feildBorder: {
        width: width - 40,
        height: 50,
        borderRadius: 30,
        borderWidth: 1,
        borderColor: APP_GREY,
        backgroundColor: '#e3e5e8',
        alignSelf: 'center',

    },
    feildBorder1: {
        width: width - 40,
        height: 50,
        borderRadius: 30,
        borderWidth: 1,
        marginTop: 15,
        borderColor: APP_GREY,
        backgroundColor: '#e3e5e8',
        alignSelf: 'center',

    },
    textfeild: {
        width: width - 80,
        height: 47,
        fontSize: 13,
        fontFamily: FONT_MEDIUM,
        borderRadius: 30,
        alignSelf: 'center',
        color: APP_BLACK,

    },
    button: {
        width: width - 45,
        height: 50,
        borderRadius: 30,
        //borderWidth:1,
        marginTop: 30,
        //borderColor:,
        backgroundColor: '#e3e5e8',
        alignSelf: 'center',
        shadowOffset: {
            width: 0,
            height: 1
        },
        shadowRadius: 1,
        shadowOpacity: 0.5, borderTopWidth: 0,
        elevation: Platform.OS == 'android' ? 5 : 0
    },
    buttonText: {
        fontSize: 15,
        fontFamily: FONT_SEMIBOLD,
        color: APP_BLACK,
        alignSelf: 'center',
        marginTop: 13
    },

});

export default EditAccount;

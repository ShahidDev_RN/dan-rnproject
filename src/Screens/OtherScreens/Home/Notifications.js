import React, {Component} from 'react';
import {View,
    Dimensions ,
    Text,
    StyleSheet,
    TouchableOpacity,
    Image,
    TextInput,
    BackHandler,
    Alert,
    SafeAreaView,
    Platform,
    ScrollView,
    FlatList,
    KeyboardAvoidingView,StatusBar,
    ImageBackground} from 'react-native';
import {FONT_BLACK,FONT_BOLD,FONT_EXTRABOLD,FONT_EXTRALIGHT,FONT_LIGHT,FONT_SEMIBOLD,
    FONT_THIN,APP_WHITE,APP_BLACK,APP_FACEBOOK,BACK_ICON, FONT_REGULAR,FONT_MEDIUM,NOTIFICATION_ICON, APP_GREY, RESTAURANT} from '../../../Constants/App_Constants.js'
    let { width, height } = Dimensions.get('window')
    const ASPECT_RATIO = width / height
class Notifications extends Component {
    constructor(props){
        super(props);
        this.state={
            username:'',
            password:'',
            data:[1,2,3,4,5,5,6,6,6]
        }  
      } 

      header(){
        if(Platform.OS=='ios'){
          return(
            <View style={{height:65,width:'100%',backgroundColor:'white',justifyContent:'flex-end',borderBottomColor:'#e3e5e8',borderBottomWidth:3}}>
                <View style={{height:'80%',width:'90%',backgroundColor:'transparent',alignSelf:'center'}}>
                    <View style={{flexDirection:'row',justifyContent:'space-between',height:'100%',width:'48%',backgroundColor:'transparent',alignSelf:'flex-start'}}>
                        <TouchableOpacity onPress={()=>this.props.navigation.goBack()}>
                        <Image source={BACK_ICON} style={{marginTop:13, height:17,width:24,tintColor:APP_BLACK}}></Image>
                        </TouchableOpacity>
                        <Text style={{color:APP_BLACK,fontSize:18,marginTop:9,fontFamily:FONT_MEDIUM,marginRight:20}}>Notifications</Text>
                    </View>
                </View>
            </View>
          )
        }else{
          return(
            <View style={{height:65,width:'100%',backgroundColor:'white',justifyContent:'flex-end',borderBottomColor:'#e3e5e8',borderBottomWidth:3}}>
            <View style={{height:'80%',width:'90%',backgroundColor:'transparent',alignSelf:'center'}}>
                <View style={{flexDirection:'row',justifyContent:'space-between',height:'100%',width:'48%',backgroundColor:'transparent',alignSelf:'flex-start'}}>
                    <TouchableOpacity onPress={()=>this.props.navigation.goBack()}>
                        <Image source={BACK_ICON} style={{marginTop:13, height:17,width:24,tintColor:APP_BLACK}}></Image>
                    </TouchableOpacity>
                    <Text style={{color:APP_BLACK,fontSize:18,marginTop:10,fontFamily:FONT_MEDIUM,width:150,marginLeft:10}}>Notifications</Text>
                </View>
            </View>
        </View>
          )
        }
      }



    render(){
        return(
            <SafeAreaView forceInset={{ top: 'always' }} style={{flex:1,backgroundColor:APP_WHITE}}>
                {this.header()}
                <StatusBar backgroundColor={APP_BLACK}></StatusBar>
               <View style={styles.OptionsView}>
                   <View style={styles.view}>
                        <FlatList
                            data={this.state.data} 
                            showsVerticalScrollIndicator={false}
                            contentContainerStyle={{backgroundColor:APP_WHITE,width:'100%',alignSelf:'center',marginBottom:10,shadowColor: '#000',
                            shadowOffset: { width: 0, height: 0 },
                            shadowOpacity: 0.8,
                            shadowRadius: 0.5, }}
                            renderItem={({ item: data, index }) => {
                                return( 
                                    <View style={styles.notificationView}>
                                        <View style={styles.notifImage}>
                                            <Image source={RESTAURANT} style={{ height:"100%",width:'100%',borderRadius:50}}></Image>
                                        </View>
                                        <View style={styles.textView}>
                                            <Text style={styles.NotifText}>Lorem Ipsum is simply text</Text>
                                            <Text style={styles.TimeText}>8 mins ago</Text>
                                        </View>
                                    </View>
                                )
                            }}keyExtractor={item => item.id}/>    

                   </View>
               </View>
            </SafeAreaView>
        )
    }
};

const styles = StyleSheet.create({
    OptionsView:{
        height:height,
        width:width,
        backgroundColor:APP_WHITE
    },
   view:{
    width:"100%",
    height:"100%",
    alignSelf:'center',
    backgroundColor:APP_WHITE,
   },
   Middle:{
        height:3,
        width:width,
        backgroundColor:"#f0e9e9"
    },
    notificationView:{
        width:'100%',
        height:70,
        flexDirection:'row',
        marginTop:0,
        borderBottomWidth:1.5,
        borderBottomColor:'#e3e5e8',
        justifyContent:'space-around',
        backgroundColor:APP_WHITE,
   
    },
    notifImage:{
        height:45,
        width:45,
        marginTop:9,
        borderRadius:50,
    },
    textView:{
        height: 60 ,  
        backgroundColor:APP_WHITE,
        width:270,paddingTop:17
    },
    NotifText:{
        color:APP_BLACK,
        fontSize:14,
        fontFamily:FONT_MEDIUM
    },
    TimeText:{
        color:APP_BLACK,
        fontSize:9,
        fontFamily:FONT_MEDIUM
    }

});

export default Notifications;

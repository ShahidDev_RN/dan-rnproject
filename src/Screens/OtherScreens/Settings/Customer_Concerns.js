import React, { Component } from 'react';
import {
    View,
    Dimensions,
    Text,
    StyleSheet,
    TouchableOpacity,
    Image,
    TextInput,
    BackHandler,
    Alert,
    SafeAreaView,
    Platform,
    ScrollView,
    KeyboardAvoidingView, StatusBar,
    ImageBackground,
    Keyboard
} from 'react-native';

import AsyncStorage from '@react-native-community/async-storage';
import * as API from '../../../Requests/WebService.js'
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
import * as Animatable from 'react-native-animatable';
import { FONT_BLACK, FONT_BOLD, FONT_EXTRABOLD, FONT_EXTRALIGHT, FONT_LIGHT, FONT_SEMIBOLD, FONT_THIN, APP_WHITE, APP_BLACK, APP_FACEBOOK, CHECK_BOX_CHECK, CHECK_BOX_UNCHECK, FONT_REGULAR, FONT_MEDIUM, LOGIN, HEADER_ICON, APP_GREY, BACK_ICON } from '../../../Constants/App_Constants.js'
let { width, height } = Dimensions.get('window')
const ASPECT_RATIO = width / height
class Customer_Concerns extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            concern: ''
        }
    }
    header() {
        if (Platform.OS == 'ios') {
            return (
                <View style={{ height: 65, width: '100%', backgroundColor: 'white', justifyContent: 'flex-end', borderBottomColor: '#e3e5e8', borderBottomWidth: 3 }}>
                    <View style={{ height: '80%', width: '95%', backgroundColor: 'transparent', alignSelf: 'center' }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', height: '100%', width: '50%', backgroundColor: 'transparent', alignSelf: 'flex-start' }}>
                            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                                <Image source={BACK_ICON} style={{ marginTop: 14, height: 17, width: 25, tintColor: APP_BLACK }}></Image>
                            </TouchableOpacity>
                            <Text style={{ color: APP_BLACK, fontSize: 18, marginTop: 10, fontFamily: FONT_SEMIBOLD, width: 200, marginLeft: height < 896 ? 20 : 20 }}>Customer Concerns</Text>
                        </View>
                    </View>
                </View>
            )
        } else {
            return (
                <View style={{ height: 65, width: '100%', backgroundColor: 'white', justifyContent: 'flex-end', borderBottomColor: '#e3e5e8', borderBottomWidth: 3 }}>
                    <View style={{ height: '80%', width: '95%', backgroundColor: 'transparent', alignSelf: 'center' }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', height: '100%', width: '50%', backgroundColor: 'transparent', alignSelf: 'flex-start' }}>
                            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                                <Image source={BACK_ICON} style={{ marginTop: 14, height: 17, width: 25, tintColor: APP_BLACK }}></Image>
                            </TouchableOpacity>
                            <Text style={{ color: APP_BLACK, fontSize: 18, marginTop: 10, fontFamily: FONT_SEMIBOLD, width: 200, marginLeft: 20 }}>Customer Concerns</Text>
                        </View>
                    </View>
                </View>
            )
        }
    }

    render() {
        return (
            <SafeAreaView forceInset={{ top: 'always' }} style={{ flex: 1, backgroundColor: APP_WHITE }}>
                <StatusBar barStyle={'dark-content'}></StatusBar>
                {this.header()}
                <OrientationLoadingOverlay
                    visible={this.state.isloading}
                    color="white"
                    indicatorSize="large"
                />

                <KeyboardAvoidingView
                    style={{
                        height: height, backgroundColor: APP_WHITE,
                        justifyContent: 'center',
                    }}
                    behavior="padding" enabled
                    keyboardVerticalOffset={10}>
                    <View style={styles.background}>
                        <View style={styles.feildView}>
                            <View style={styles.feildBorder}>
                                <TextInput ref={input => { this.textInput = input }}
                                    style={styles.textfeild}
                                    placeholder="Enter Email"
                                    keyboardType="email-address"
                                    placeholderTextColor={APP_BLACK}
                                    selectionColor={APP_BLACK}
                                //onChangeText = {this.handleEmail}
                                />

                            </View>
                            <View style={styles.feildBorderPassword}>
                                <TextInput ref={input => { this.textInput1 = input }}
                                    style={styles.multilinetextfeild}
                                    multiline={true}
                                    placeholder="Concerns"
                                    secureTextEntry={true}
                                    placeholderTextColor={APP_BLACK}
                                    selectionColor={APP_BLACK}
                                    returnKeyType={"done"}
                                //onChangeText = {this.handlePassword}
                                />
                            </View>


                            {this.state.serverError ? <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center', height: 20, backgroundColor: 'transparent' }}>
                                <Text style={{ alignSelf: 'center', color: "#dd4b39", fontFamily: FONT_SEMIBOLD, fontSize: 16 }}>{this.state.serverErrorText}</Text>
                            </View> : null}

                            <View style={styles.button}>
                                <TouchableOpacity style={{ height: '100%' }}>
                                    <Text style={styles.buttonText}>Submit</Text>
                                </TouchableOpacity>
                            </View>

                            {/* <TouchableOpacity style={{borderBottomColor:APP_WHITE,marginTop:10}} onPress={()=>this.bussinessLogin()}>
                                        <Text style={styles.registerTextUnderline}>Login as a business</Text>
                            </TouchableOpacity> */}
                        </View>
                    </View>
                </KeyboardAvoidingView>
            </SafeAreaView>
        )
    }
};

const styles = StyleSheet.create({
    background: {
        height: height,
        width: width,
        alignItems: 'center'
    },
    otherText: {
        fontSize: 13,
        fontFamily: FONT_BOLD,
        color: APP_WHITE
    },
    welcomeText: {
        alignSelf: 'center',
        fontSize: 28,
        fontFamily: FONT_MEDIUM,
        color: APP_WHITE,
        //backgroundColor:APP_WHITE,
        marginTop: height < 896 ? '18%' : '38%'
    },
    feildView: {
        width: width,
        height: 520,
        backgroundColor: "transparent",
        marginTop: height < 896 ? 60 : 60
    },
    feildBorder: {
        width: width - 40,
        height: 50,
        borderRadius: 30,
        borderWidth: 1,
        borderColor: APP_GREY,
        backgroundColor: '#e3e5e8',
        alignSelf: 'center'
    },
    feildBorderPassword: {
        width: width - 45,
        height: 150,
        borderRadius: 20,
        borderWidth: 1,
        marginTop: 17,
        borderColor: APP_GREY,
        backgroundColor: '#e3e5e8',
        alignSelf: 'center'
    },
    textfeild: {
        width: width - 80,
        height: 47,
        fontSize: 14,
        fontFamily: FONT_SEMIBOLD,
        borderRadius: 30,
        alignSelf: 'center',
        color: APP_WHITE,
    },
    multilinetextfeild: {
        width: width - 70,
        minHeight: 50,
        fontSize: 13,
        fontFamily: FONT_SEMIBOLD,
        borderRadius: 30,
        alignSelf: 'center',
        color: APP_WHITE,
        marginTop: 20
        //backgroundColor:'red'
        //fontWeight:'300',
    },
    forgetPassword: {
        width: width - 45,
        height: 40,
        backgroundColor: "transparent",
        alignSelf: 'center',
        marginTop: 20,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    button: {
        width: width - 45,
        height: 52,
        borderRadius: 30,
        //borderWidth:1,
        marginTop: 20,
        //borderColor:APP_BLACK,
        backgroundColor: '#e3e5e8',
        alignSelf: 'center',
        shadowOffset: {
            width: 0,
            height: 1
        },
        shadowRadius: 1,
        shadowOpacity: 0.5,
        elevation: Platform.OS == 'android' ? 5 : 0
    },
    buttonText: {
        fontSize: 18,
        fontFamily: FONT_SEMIBOLD,
        color: APP_BLACK,
        alignSelf: 'center',
        marginTop: 14
    },
    ORView: {
        width: 360,
        height: 45,
        alignItems: 'center',
        backgroundColor: 'transparent',
        alignSelf: 'center'
    },
    ORText: {
        fontSize: 12,
        fontWeight: '600',
        marginTop: 15,
        color: APP_WHITE
    },
    facebookButton: {
        width: width - 45,
        height: 52,
        borderRadius: 30,
        borderColor: APP_BLACK,
        backgroundColor: APP_FACEBOOK,
        alignSelf: 'center'
    },
    facebookbuttonText: {
        fontSize: 17,
        color: APP_WHITE,
        fontFamily: FONT_SEMIBOLD,
        alignSelf: 'center',
        marginTop: 15
    },
    registerText: {
        color: APP_WHITE,
        fontSize: 14,
        marginTop: 10,
        fontFamily: FONT_SEMIBOLD,
        alignSelf: 'center',
    },
    registerTextUnderline: {
        color: APP_WHITE,
        fontSize: 14,
        marginTop: 0,
        fontFamily: FONT_SEMIBOLD,
        alignSelf: 'center',
    }
});

export default Customer_Concerns;

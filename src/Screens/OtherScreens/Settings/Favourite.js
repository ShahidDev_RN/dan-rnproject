import React, { Component } from 'react'
import {View, Text, TouchableOpacity,Image,StatusBar} from 'react-native'
import {FONT_BLACK, FONT_BOLD, FONT_EXTRABOLD, FONT_EXTRALIGHT, FONT_LIGHT, FONT_SEMIBOLD, APP_BORDER,
    FONT_THIN, APP_WHITE, APP_BLACK, APP_FACEBOOK, BACK_ICON, FONT_REGULAR, FONT_MEDIUM, NOTIFICATION_ICON, APP_GREY, HEADER_ICON}
    from '../../../Constants/App_Constants'
    import * as API from '../../../Requests/WebService.js'
class Favourite extends Component{
    constructor(){
        super()
        this.state={
            // liked:false,
            
        };
        // touchLike = () => this.setState(state => ({liked: !this.state.liked}));
    }
  
    header() {
        if (Platform.OS == 'ios') {
            return (
                <View style={{ height: 90, width: '100%', backgroundColor: 'white', justifyContent: 'flex-end', borderBottomColor: '#e3e5e8', borderBottomWidth: 3 }}>
                    <View style={{ height: '80%', width: '95%', backgroundColor: 'transparent', alignSelf: 'center' }}>
                        <View style={{ padding:10,flexDirection: 'row', justifyContent: 'space-between', height: '100%', width: '50%', backgroundColor: 'transparent', alignSelf: 'flex-start' }}>
                            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                                <Image source={BACK_ICON} style={{ margin: 20, height: 20, width: 28,tintColor:"black" }}></Image>
                            </TouchableOpacity>
                            <Text style={{ color: APP_BLACK, fontSize: 18, marginTop: 20, fontFamily: FONT_SEMIBOLD, width: 150,  }}>Settings</Text>
                        </View>
                    </View>
                </View>
            )
        } else {
            return (
                <View style={{ height: 65, width: '100%', backgroundColor: 'white', justifyContent: 'flex-end', borderBottomColor: '#e3e5e8', borderBottomWidth: 3 }}>
                    <View style={{ height: '80%', width: '95%', backgroundColor: 'transparent', alignSelf: 'center' }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', height: '100%', width: '50%', backgroundColor: 'transparent', alignSelf: 'flex-start' }}>
                            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                                <Image source={BACK_ICON} style={{ marginTop: 13, height: 20, width: 28,tintColor:"black" }}></Image>
                            </TouchableOpacity>
                            <Text style={{ color: APP_BLACK, fontSize: 18, marginTop: 10, fontFamily: FONT_SEMIBOLD, width: 175, marginLeft: 20 }}>Settings</Text>
                        </View>
                    </View>
                </View>
            )
        }
    }

    componentDidMount(){
      
        API.GET_WISHLIST().then((response) => {
            if(response.status==1){
                console.log("Getttttttt", JSON.stringify(response))
            }else if(response.status==0){
                console.log("errorrrrrrr")
            }
        })
    }

  

   
    render(){   
        return( 
            <View>
        
               {this.header()}     
            </View>
        )
    }
}
export default Favourite; 
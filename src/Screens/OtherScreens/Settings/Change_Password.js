import React, {Component} from 'react';
import {View,
    Dimensions ,
    Text,
    StyleSheet,
    TouchableOpacity,
    Image,
    TextInput,
    BackHandler,
    Alert,
    SafeAreaView,
    Platform,
    ScrollView,
    KeyboardAvoidingView,StatusBar,
    ImageBackground} from 'react-native';
import * as API from '../../../Requests/WebService.js'
import AsyncStorage from '@react-native-community/async-storage';
import {FONT_BLACK,FONT_BOLD,FONT_EXTRABOLD,FONT_EXTRALIGHT,FONT_LIGHT,FONT_SEMIBOLD,
    FONT_THIN,APP_WHITE,APP_BLACK,APP_FACEBOOK,BACK_ICON, FONT_REGULAR,FONT_MEDIUM,NOTIFICATION_ICON, APP_GREY, APP_BORDER} from '../../../Constants/App_Constants.js'
    let { width, height } = Dimensions.get('window')
    const ASPECT_RATIO = width / height
class ChangePassword extends Component {
    constructor(props){
        super(props);
        this.state={
            user_id:'',
            newpassword:'',
            oldpassword:'',
            confirmpassword:'',
            serverError:false,
            serverErrorText:''
        }  
      } 

      navigation(){
          this.props.navigation.goBack()
          //alert(global.type)
        //   if(global.type=='User'){
        //     this.props.navigation.navigate('Settings')
        //   }else{
        //     this.props.navigation.navigate('BusinessSetings')
        //   }
      }

      header(){
        if(Platform.OS=='ios'){
          return(
            <View style={{height:65,width:'100%',backgroundColor:'white',justifyContent:'flex-end',borderBottomColor:'#e3e5e8',borderBottomWidth:3}}>
                <View style={{height:'80%',width:'90%',backgroundColor:'transparent',alignSelf:'center'}}>
                    <View style={{flexDirection:'row',justifyContent:'space-between',height:'100%',width:'55%',backgroundColor:'transparent',alignSelf:'flex-start'}}>
                        <TouchableOpacity onPress={()=>this.navigation()}>
                            <Image source={BACK_ICON} style={{marginTop:13, height:17,width:24,tintColor:APP_BLACK}}></Image>
                        </TouchableOpacity>
                        <Text style={{color:APP_BLACK,fontSize:18,marginTop:10,fontFamily:FONT_MEDIUM,marginLeft:10,width:'auto'}}>Change password</Text>
                    </View>
                </View>
            </View>
          )
        }else{
          return(
            <View style={{height:65,width:'100%',backgroundColor:'white',justifyContent:'flex-end',borderBottomColor:'#e3e5e8',borderBottomWidth:3}}>
            <View style={{height:'80%',width:'90%',backgroundColor:'transparent',alignSelf:'center'}}>
                <View style={{flexDirection:'row',justifyContent:'space-between',height:'100%',width:'55%',backgroundColor:'transparent',alignSelf:'flex-start'}}>
                    <TouchableOpacity onPress={()=>this.navigation()}>
                        <Image source={BACK_ICON} style={{marginTop:13, height:17,width:24,tintColor:APP_BLACK}}></Image>
                    </TouchableOpacity>
                    <Text style={{color:APP_BLACK,fontSize:18,marginTop:10,fontFamily:FONT_MEDIUM,width:190,marginLeft:10,width:'auto'}}>Change password</Text>
                </View>
            </View>
        </View>
          )
        }
      }

    changepassword(){
        if(this.state.oldpassword.trim()==''&&this.state.newpassword.trim()==''&&this.state.confirmpassword.trim()==''){
            this.setState({serverError : true , serverErrorText: 'Fields Empty'});
        }else if(this.state.oldpassword.trim()==''){
            this.setState({serverError : true , serverErrorText: 'Enter the Old Password'});
        }else if(this.state.newpassword.trim()==''){
            this.setState({serverError : true , serverErrorText: 'Enter the New Password'});
        }else if(this.state.newpassword.trim().length<4){
            this.setState({serverError : true , serverErrorText: 'New Password is too short'});
        }else if(this.state.confirmpassword.trim()==''){
            this.setState({serverError : true , serverErrorText: 'Enter your new password again to confirm'});
        }else if(this.state.newpassword.trim()!=this.state.confirmpassword.trim()){
            this.setState({serverError : true , serverErrorText: 'Confirm password does not match'});
        }else if(this.state.oldpassword.trim()==this.state.newpassword.trim()){
            this.setState({serverError : true , serverErrorText: 'You are entering an old password again'});
        }else{
            if(global.type=='User'){
                API.CHANGE_PASSWORD(this.state.user_id,this.state.oldpassword,this.state.newpassword).then((response)=>{


                    console.log("check rewsppnse of change passoerfd////",response)
                    if(response.status==1){
                        Alert.alert(
                            '',
                            'Password Changed',
                            [
                   
                                { text: 'Ok', onPress: () =>  this.props.navigation.navigate('AppLogin') },
                            ],
                            {
                                cancelable: false,
                            },
                        );
                        return true;
                        // this.props.navigation.navigate('Bottomtab')
                    }else if(response.status==0){
                        this.setState({serverError:true,serverErrorText:response.message,isloading:false})
                    }
                })
            }else{
                API.CHANGE_PASSWORD_BUSINESS(this.state.user_id,this.state.oldpassword,this.state.newpassword).then((response)=>{
                    if(response.status==1){
                        this.props.navigation.navigate('BusinessBottomtab')
                    }else if(response.status==0){
                        this.setState({serverError:true,serverErrorText:response.message,isloading:false})
                    }
                })
            }
          
        }
    }
    handlecurrentpassword=(text)=>{
        this.setState({newpassword:(text),serverError:false})
    }
    handleoldpassword=(text)=>{
        this.setState({oldpassword:(text),serverError:false})
    }
    handleconfirmpassword=(text)=>{
        this.setState({confirmpassword:(text),serverError:false})
    }

    componentDidMount(){
        if(global.type=='User'){
        AsyncStorage.getItem("user_info").then((value) => {
            var data= JSON.parse(value)
                this.setState({user_id:data._id})
        }).done();
    }else{
        AsyncStorage.getItem("user_info").then((value) => {
            var data= JSON.parse(value)
            this.setState({user_id:data._id})
        }).done();
    }
        
    }

    render(){
        return(
            <SafeAreaView forceInset={{ top: 'always' }} style={{flex:1,backgroundColor:APP_WHITE}}>
                {this.header()}
                <StatusBar barStyle={'dark-content'}></StatusBar>
               <View style={styles.OptionsView}>
                   <View style={styles.view}>
                   <View style={styles.feildBorder}>
                            <TextInput  ref={input => { this.textInput = input }}
                                style={styles.textfeild}  
                                placeholder="Enter Old Password"
                                keyboardType="default"
                                // secureTextEntry={true}
                                selectionColor={APP_BLACK}
                                placeholderTextColor={APP_BLACK}
                                onChangeText = {this.handleoldpassword}
                                /> 
                        </View>
                        <View style={styles.feildBorder1}>
                            <TextInput  ref={input => { this.textInput = input }}
                                style={styles.textfeild}  
                                placeholder="Enter New Password"
                                keyboardType="default"
                                // secureTextEntry={true}
                                selectionColor={APP_BLACK}
                                placeholderTextColor={APP_BLACK}
                                onChangeText = {this.handlecurrentpassword}
                                /> 
                        </View>

                        <View style={styles.feildBorder1}>
                            <TextInput  ref={input => { this.textInput = input }}
                                style={styles.textfeild}  
                                placeholder="Enter Confirm Password"
                                keyboardType="default"
                                // secureTextEntry={true}
                                selectionColor={APP_BLACK}
                                placeholderTextColor={APP_BLACK}
                                onChangeText = {this.handleconfirmpassword}
                                /> 
                        </View>
                        {this.state.serverError?<View style={{width:'100%', justifyContent:'center', alignItems:'center',height:27,backgroundColor:'transparent',marginTop:10}}>
                                <Text style={{alignSelf:'center',color:"#dd4b39",fontFamily:FONT_SEMIBOLD,fontSize:15,marginTop:10}}>{this.state.serverErrorText}</Text>
                                </View>:null}
                        <View style={styles.button}>
                            <TouchableOpacity onPress={()=>this.changepassword()} style={{height:'100%'}}>
                                <Text style={styles.buttonText}>Change Password</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
               </View>
            </SafeAreaView>
        )
    }
};

const styles = StyleSheet.create({
    OptionsView:{
        height:400,
        width:width,
        backgroundColor:APP_WHITE
    },
   view:{
    width:"85%",
    height:"80%",
    alignSelf:'center',
    backgroundColor:'transparent',
    marginTop:50
   },
    feildBorder:{
        width:width-40,
        height:50,
        borderRadius:30,
        borderWidth:1,
        borderColor:APP_GREY,
        backgroundColor:'#e3e5e8',
        alignSelf:'center'
    },
    feildBorder1:{
        width:width-40,
        height:50,
        borderRadius:30,
        borderWidth:1,
        marginTop:15,
        borderColor:APP_GREY,
        backgroundColor:'#e3e5e8',
        alignSelf:'center'
    },
    textfeild:{
        width:width-80,
        height:47,
        fontSize:13,
        fontFamily:FONT_MEDIUM,
        borderRadius:30,
        alignSelf:'center',
        color:APP_BLACK,
        
        },
    button:{
        width:width-45,
        height:50,
        borderRadius:30,
        //borderWidth:1,
        marginTop:50,
        //borderColor:APP_BLACK,
        backgroundColor:'#e3e5e8',
        alignSelf:'center',
        shadowOffset: {
            width: 0,
            height: 1
          },
          shadowRadius:1,
          shadowOpacity: 0.5,
          elevation:Platform.OS=='android'?5:0
    },
    buttonText:{
        fontSize:15,
        fontFamily:FONT_SEMIBOLD,
        color:APP_BLACK,
        alignSelf:'center',
        marginTop:13
    },

});

export default ChangePassword;
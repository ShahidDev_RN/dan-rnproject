import React, {Component} from 'react';
import {View,
    Dimensions ,
    Text,
    StyleSheet,
    TouchableOpacity,
    Image,
    TextInput,
    BackHandler,
    Alert,
    SafeAreaView,
    Platform,
    ScrollView,
    KeyboardAvoidingView,StatusBar,
    ImageBackground} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {FONT_BLACK,FONT_BOLD,FONT_EXTRABOLD,FONT_EXTRALIGHT,FONT_LIGHT,FONT_SEMIBOLD,
    FONT_THIN,APP_WHITE,APP_BLACK,APP_FACEBOOK,BACK_ICON, FONT_REGULAR,FONT_MEDIUM,NOTIFICATION_ICON, APP_GREY} from '../../../Constants/App_Constants.js'
import * as API from '../../../Requests/WebService.js'
    let { width, height } = Dimensions.get('window')
    const ASPECT_RATIO = width / height
class ContactUs extends Component {
    constructor(props){
        super(props);
        this.state={
            username:'',
            useremail:'',
            userphonenumber:'',
            usercomment:'',
            user_id:'',
            api_token:'',
            serverError:false,
            serverErrorText:''
        }  
      } 

      header(){
        if(Platform.OS=='ios'){
          return(
            <View style={{height:65,width:'100%',backgroundColor:'white',justifyContent:'flex-end',borderBottomColor:'#e3e5e8',borderBottomWidth:3}}>
                <View style={{height:'80%',width:'90%',backgroundColor:'transparent',alignSelf:'center'}}>
                    <View style={{flexDirection:'row',justifyContent:'space-between',height:'100%',width:'40%',backgroundColor:'transparent',alignSelf:'flex-start'}}>
                        <TouchableOpacity onPress={()=>this.props.navigation.navigate('Settings')}>
                            <Image source={BACK_ICON} style={{marginTop:13, height:17,width:24,tintColor:APP_BLACK}}></Image>
                        </TouchableOpacity>
                        <Text style={{color:APP_BLACK,fontSize:18,marginTop:9,fontFamily:FONT_MEDIUM}}>Contact Us</Text>
                    </View>
                </View>
            </View>
          )
        }else{
          return(
            <View style={{height:65,width:'100%',backgroundColor:'white',justifyContent:'flex-end',borderBottomColor:'#e3e5e8',borderBottomWidth:3}}>
            <View style={{height:'80%',width:'90%',backgroundColor:'transparent',alignSelf:'center'}}>
                <View style={{flexDirection:'row',justifyContent:'space-between',height:'100%',width:'40%',backgroundColor:'transparent',alignSelf:'flex-start'}}>
                    <TouchableOpacity onPress={()=>this.props.navigation.navigate('Settings')}>
                        <Image source={BACK_ICON} style={{marginTop:13, height:17,width:24,tintColor:APP_BLACK}}></Image>
                    </TouchableOpacity>
                    <Text style={{color:APP_BLACK,fontSize:18,marginTop:10,fontFamily:FONT_MEDIUM,width:'auto'}}>Contact Us</Text>
                </View>
            </View>
        </View>
          )
        }
      }

    handlename=(text)=>{
        this.setState({username:(text),serverError:false})
    }
    handleemail=(text)=>{
        this.setState({useremail:(text),serverError:false})
    }
    handlephonenumber=(text)=>{
        this.setState({userphonenumber:(text),serverError:false})
    }
    handlecomment=(text)=>{
        this.setState({usercomment:(text),serverError:false})
    }


    componentDidMount(){
        AsyncStorage.getItem("user_info").then((value) => {
            var data= JSON.parse(value)
            this.setState({user_id:data._id})
        }).done(); 
        AsyncStorage.getItem("user_token").then((value) => {
            this.setState({api_token:value})
        }).done(); 
    }

      contact_us(){
        if(this.state.username==''&& this.state.useremail==''&&this.state.userphonenumber==''&& this.state.usercomment==''){
            this.setState({serverError : true , serverErrorText: 'Fields Empty'});
        }else if(this.state.username==''){
            this.setState({serverError : true , serverErrorText: 'Enter Name'});
        }else if(this.state.useremail==''){
            this.setState({serverError : true , serverErrorText: 'Enter Email'});
        }else if(this.state.userphonenumber==''){
            this.setState({serverError : true , serverErrorText: 'Enter Phonenumber'});
        }else if(this.state.usercomment==''){
            this.setState({serverError : true , serverErrorText: 'Enter Comment'});
        }
        else{
            this.setState({isloading:true})
            if(global.type=='User'){
                API.CONTACT_US(this.state.api_token,this.state.user_id,this.state.username,this.state.useremail,this.state.usercomment).then((response)=>{
                    if(response.status==1){
                       this.props.navigation.navigate('Bottomtab')
                    }else if(response.status==0){
                        this.setState({serverError:true,serverErrorText:response.message,isloading:false})
                    }
                })
            }else{
                API.CONTACT_US(this.state.api_token,this.state.user_id,this.state.username,this.state.useremail,this.state.usercomment).then((response)=>{
                    if(response.status==1){
                       this.props.navigation.navigate('Bottomtab')
                    }else if(response.status==0){
                        this.setState({serverError:true,serverErrorText:response.message,isloading:false})
                    }
                })
            }
           
        }
       
    }


    render(){
        return(
            <SafeAreaView forceInset={{ top: 'always' }} style={{flex:1,backgroundColor:APP_WHITE}}>
                {this.header()}
                {/* <StatusBar barStyle={'light-content'}></StatusBar> */}
               <View style={styles.OptionsView}>
                   <View style={styles.view}>
                        <View style={styles.feildBorder}>
                            <TextInput  ref={input => { this.textInput = input }}
                                style={styles.textfeild}  
                                placeholder="Enter Name"
                                keyboardType="default"
                                selectionColor={APP_BLACK}
                                placeholderTextColor={APP_BLACK}
                                onChangeText = {this.handlename}
                                /> 
                        </View>
                        <View style={styles.feildBorder1}>
                            <TextInput  ref={input => { this.textInput = input }}
                                style={styles.textfeild}  
                                placeholder="Enter Email"
                                keyboardType="email-address"
                                selectionColor={APP_BLACK}
                                placeholderTextColor={APP_BLACK}
                                onChangeText = {this.handleemail}
                                /> 
                        </View>
                        <View style={styles.feildBorder1}>
                            <TextInput  ref={input => { this.textInput = input }}
                                style={styles.textfeild}  
                                placeholder="Enter Phone Number"
                                keyboardType="phone-pad"
                                selectionColor={APP_BLACK}
                                placeholderTextColor={APP_BLACK}
                                onChangeText = {this.handlephonenumber}
                                /> 
                        </View>
                        <View style={styles.feildBorder1}>
                            <TextInput  ref={input => { this.textInput = input }}
                                style={styles.textfeild}  
                                placeholder="Comment"
                                keyboardType="default"
                                selectionColor={APP_BLACK}
                                placeholderTextColor={APP_BLACK}
                                onChangeText = {this.handlecomment}
                                /> 
                        </View>
                        {this.state.serverError?<View style={{width:'100%', justifyContent:'center', alignItems:'center',height:20,backgroundColor:'tra',marginTop:10}}>
                        <Text style={{alignSelf:'center',color:"#dd4b39",fontFamily:FONT_SEMIBOLD,fontSize:16}}>{this.state.serverErrorText}</Text>
                        </View>:null}
                        <View style={styles.button}>
                            <TouchableOpacity onPress={()=>this.contact_us()} style={{height:'100%'}}>
                                <Text style={styles.buttonText}>Submit</Text>
                            </TouchableOpacity>
                        </View>

                    </View>
               </View>
            </SafeAreaView>
        )
    }
};

const styles = StyleSheet.create({
    OptionsView:{
        height:400,
        width:width,
        backgroundColor:APP_WHITE
    },
   view:{
    width:"85%",
    height:"80%",
    alignSelf:'center',
    backgroundColor:'transparent',
    marginTop:40
   },
   feildBorder:{
    width:width-40,
    height:50,
    borderRadius:30,
    borderWidth:1,
    borderColor:APP_GREY,
    backgroundColor:'#e3e5e8',
    alignSelf:'center'
},
feildBorder1:{
    width:width-40,
    height:50,
    borderRadius:30,
    borderWidth:1,
    marginTop:15,
    borderColor:APP_GREY,
    backgroundColor:'#e3e5e8',
    alignSelf:'center'
},
textfeild:{
    width:width-80,
    height:47,
    fontSize:13,
    fontFamily:FONT_MEDIUM,
    borderRadius:30,
    alignSelf:'center',
    color:APP_BLACK,
    
    },
    button:{
        width:width-45,
        height:50,
        borderRadius:30,
        //borderWidth:1,
        marginTop:50,
        //borderColor:APP_BLACK,
        backgroundColor:'#e3e5e8',
        alignSelf:'center',
        shadowOffset: {
            width: 0,
            height: 1
          },
          shadowRadius:1,
          shadowOpacity: 0.5,
          elevation:Platform.OS=='android'?5:0
    },
    buttonText:{
        fontSize:15,
        fontFamily:FONT_SEMIBOLD,
        color:APP_BLACK,
        alignSelf:'center',
        marginTop:13
    },

});

export default ContactUs;

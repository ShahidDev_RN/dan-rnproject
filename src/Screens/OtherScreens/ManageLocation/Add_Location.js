import React, { Component } from 'react';
import {
    View,
    Dimensions,
    Text,
    StyleSheet,
    TouchableOpacity,
    Image,
    TextInput,
    BackHandler,
    Alert,
    SafeAreaView,
    Platform,
    ScrollView,
    KeyboardAvoidingView, StatusBar, FlatList,
    ImageBackground
} from 'react-native';
import ImagePicker from 'react-native-image-picker';
import * as API from '../../../Requests/WebService.js'
import ImageResizer from 'react-native-image-resizer';
import AsyncStorage from '@react-native-community/async-storage';
import DocumentPicker from 'react-native-document-picker';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';

import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
import {
    FONT_BLACK, FONT_BOLD, FONT_EXTRABOLD, FONT_EXTRALIGHT, FONT_LIGHT, FONT_SEMIBOLD, RADIO_OFF, RADIO_ON, ADD_DOCUMENT, FOR_IMAGE_URL, REMOVE_ICON,
    FONT_THIN, APP_WHITE, APP_BLACK, APP_FACEBOOK, BACK_ICON, FONT_REGULAR, FONT_MEDIUM, NOTIFICATION_ICON, APP_GREY, RADIO_BUTTON_ON, RADIO_BUTTON_OFF, APP_BORDER
} from '../../../Constants/App_Constants.js'
let { width, height } = Dimensions.get('window')
const ASPECT_RATIO = width / height
const LATITUDE_DELTA = 0.005
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO
const createFormData = (image) => {
    var data = new FormData();
    data.append('profilePicture', {
        uri: Platform.OS === "android" ? image.fileCopyUri : image.fileCopyUri,
        name: `ony${Date.now()}.` + image.type.slice(image.type.indexOf('/') + 1),
        type: '*/*'
    })

    return data
}
var images = []
var id = 0
class AddLocation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            longitude: '76.7018',           // businessLong
            latitude: '30.7089',
            zipcode: '',
            address: '',
            userid: '',
            businessDoc: '',
            subscriptionType: 0,
            subscriptionIconFree: RADIO_ON,
            subscriptionIconPaid: RADIO_OFF,
            isloading: false,
            serverError: false,
            serverErrorText: '',
            isfeatured: "0",
            onSelectedLoc:''
        }
    }

    selectSubRadioFree() {
        this.setState({ subscriptionType: 0, subscriptionIconFree: RADIO_ON, subscriptionIconPaid: RADIO_OFF })
    }
    selectSubRadioPaid() {
        this.setState({ subscriptionType: 1, subscriptionIconPaid: RADIO_ON, subscriptionIconFree: RADIO_OFF })
    }

    handleZipcode = (text) => {
        this.setState({ zipcode: (text), serverError: false })
    }

    handleAddress = (text) => {
        this.setState({ address: (text), serverError: false })
    }


    componentDidMount() {
        AsyncStorage.getItem("user_info").then((value) => {
            var data = JSON.parse(value)
            console.log(JSON.stringify(data._id))
            this.setState({ userid: data._id })
        }).done();
    }

    verifyHit() {
        this.setState({ isloading: true }, () => {
            this.verify(this.state.zipcode, this.state.address)
        })
    }

    verify = (zipcode, address) => {
        if (address == '') {
            this.setState({ serverError: true, serverErrorText: 'Fields Empty', isloading: false });
        // } else if (zipcode == '') {
        //     this.setState({ serverError: true, serverErrorText: 'Enter Zipcode', isloading: false });
        } else if (address == '') {
            this.setState({ serverError: true, serverErrorText: 'Enter Address', isloading: false });
        // } else if (zipcode.length < 3) {
        //     this.setState({ serverError: true, serverErrorText: 'Enter Valid Zipcode', isloading: false });
        } else {
            //this.setState({isloading:true})
            //this.onFetchLoginRecords(email,password,deviceToken,type); 
            API.ADD_LOCATION(this.state.userid, this.state.latitude, this.state.longitude, this.state.zipcode, this.state.isfeatured, this.state.address, this.state.businessDoc[0]).then((response) => {
                if (response.status == 1) {
                    this.setState({ isloading: false })
                    this.props.navigation.goBack()
                } else {
                    console.log(JSON.stringify(response))
                }
            })
        }
    }

    header() {
        if (Platform.OS == 'ios') {
            return (
                <View style={{ height: 75, width: '100%', backgroundColor: 'white', justifyContent: 'flex-end', borderBottomColor: '#e3e5e8', borderBottomWidth: 3 }}>
                    <View style={{ height: '80%', width: '90%', backgroundColor: 'transparent', alignSelf: 'center' }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', height: '100%', width: '55%', backgroundColor: 'transparent', alignSelf: 'flex-start' }}>
                            <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={{ width: 40 }}>
                                <Image source={BACK_ICON} style={{ marginTop: 17, height: 17, width: 24, tintColor: APP_BLACK }}></Image>
                            </TouchableOpacity>
                            <Text style={{ color: APP_BLACK, fontSize: 19, marginTop: 15, fontFamily: FONT_MEDIUM, width: "100%" }}>Add Location</Text>
                        </View>
                    </View>
                </View>
            )
        } else {
            return (
                <View style={{ height: 75, width: '100%', backgroundColor: 'white', justifyContent: 'flex-end', borderBottomColor: '#e3e5e8', borderBottomWidth: 3 }}>
                    <View style={{ height: '80%', width: '90%', backgroundColor: 'transparent', alignSelf: 'center' }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', height: '100%', width: '55%', backgroundColor: 'transparent', alignSelf: 'flex-start' }}>
                            <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={{ width: 40 }}>
                                <Image source={BACK_ICON} style={{ marginTop: 17, height: 17, width: 24, tintColor: APP_BLACK }}></Image>
                            </TouchableOpacity>
                            <Text style={{ color: APP_BLACK, fontSize: 19, marginTop: 15, fontFamily: FONT_MEDIUM, width: 190 }}>Add Location</Text>
                        </View>
                    </View>
                </View>
            )
        }
    }

    async _compressImage(source) {
        var v = this;
        let response = await ImageResizer.createResizedImage(source.uri, 400, 400, 'JPEG', 80);
        //console.log(response)
        if (response != null) {
            v.imageUpload(response)
        }
    }

    picDocument() {
        if (this.state.businessDoc.length < 1) {

            this.documentPicker()
        } else {
            alert('Only one document file is allowed')
        }
    }

    async documentPicker() {
        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.allFiles],
            });

            this.setState({
                isloading: true
            }, () => {
                this.imageUpload(res)
            })

        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                // User cancelled the picker, exit any dialogs or menus and move on
            } else {
                throw err;
            }
        }
    }

    async imageUpload(source) {
        try {
            let response = await fetch(
                API.BASE_URL + "fileupload",
                {
                    'method': 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data',
                    },
                    body: createFormData(source)
                }
            );
            if (response.status == 200) {
                response.json().then(data => {
                    images = []
                    images.push(FOR_IMAGE_URL + data.filename)
                    this.setState({ businessDoc: images, isloading: false })

                });
            } else {
                console.log('error', response)
            }
        } catch (error) {
            console.error(error);
        }
    }


    imageDelete() {
        images.shift()
        this.setState({ businessDoc: '' })
    }


    showImage() {
        return (
            <FlatList
                data={this.state.businessDoc}
                showsVerticalScrollIndicator={false}
                numColumns={3}
                contentContainerStyle={{ backgroundColor: 'transparent', width: '95%', alignSelf: 'center', height: '85%' }}
                renderItem={({ item: data, index }) => {
                    return (
                        <View style={{ height: '100%', width: 72, backgroundColor: APP_BLACK, alignSelf: 'flex-start', borderRadius: 10, marginLeft: 10, borderRadius: 1, borderColor: APP_WHITE }}>
                            <Image source={require('../../../Assets/document.png')} style={{ height: '100%', width: '90%', alignSelf: 'center', borderRadius: 10 }}></Image>
                            <TouchableOpacity onPress={() => this.imageDelete()} style={{ height: 17, width: 17, alignSelf: 'center', position: 'absolute', backgroundColor: 'white', borderRadius: 10, top: 55 }}>
                                <Image source={REMOVE_ICON} style={{ height: 17, width: 17, borderRadius: 10 }}></Image>
                            </TouchableOpacity>
                        </View>
                    )
                }}
                keyExtractor={(item, index) => index.toString()} />)
    }


    render() {
        return (
            <SafeAreaView forceInset={{ top: 'always' }} style={{ flex: 1, backgroundColor: APP_WHITE }}>
                {this.header()}
                <StatusBar barStyle={'dark-content'}></StatusBar>
                <OrientationLoadingOverlay
                    visible={this.state.isloading}
                    color="white"
                    indicatorSize="large"
                />
                <ScrollView keyboardShouldPersistTaps={'always'}>
                <View style={styles.OptionsView}>
                    {/* <View style={styles.view}> */}
                        {/* <View style={styles.feildBorder}>
                            <TextInput ref={input => { this.textInput = input }}
                                style={styles.textfeild}
                                placeholder="Enter Zip Code"
                                keyboardType="number-pad"
                                returnKeyType='done'
                                placeholderTextColor={APP_BLACK}
                                selectionColor={APP_BLACK}
                                onChangeText={this.handleZipcode}
                            />
                        </View> */}

                        <View style={{
                            //minHeight:170,
                            width: width - 40,
                            //  width: Platform.OS=="android"? 320 : 340, 
                            justifyContent: 'center', alignSelf: 'center', marginTop: 20,
                            //height:200,
                           // flex:1
                        }}>

                            <GooglePlacesAutocomplete
                                ref={ref => { this.placesRef = ref }}
                                editable={true}
                                clearButtonMode={true}
                                placeholder='Address'
                                minLength={2}
                                autoFocus={false}
                                returnKeyType={'search'}
                                listViewDisplayed={false}
                                fetchDetails={true}
                                keyboardShouldPersistTaps={'always'}
                                enablePoweredByContainer={false}
                                listViewDisplayed={false}
                                onPress={(data, details = null) => {
                                        console.log("dataaaa", details)
                                    let region = {
                                        latitude: details.geometry.location.lat,
                                        longitude: details.geometry.location.lng,
                                        latitudeDelta: LATITUDE_DELTA,
                                        longitudeDelta: LONGITUDE_DELTA
                                    }
                                    this.setState({
                                        latitude: region.latitude,
                                        longitude: region.longitude,
                                        region: region,
                                        businessAddress: data.description,
                                        //onSelectedLoc:'',
                                        address: data.description
                                        
                                    })
                                   
                                    console.log("check selected ",this.state.onSelectedLoc)
                                }}
                                // textInputProps={{
                                //     placeholderTextColor: 'black',
                                    
                                //     onChangeText: (text) => {
                                //         console.log('onChangeText???', text)
                                //         this.setState({
                                //             //onSelectedLoc:text
                                //         })
                                //         if (text.length === 0) {
                                //             console.log('onChangeText??????????')
                                           
                                //         }
                                //     }
                                // }}
                                // getDefaultValue={() => {
                                //      if (this.state.region !== null && this.state.region !== undefined) {
                                //         return this.state.description;
                                //     } // text input default value
                                //     return '';
                                // }}

                                query={{
                                    key: 'AIzaSyAYbSbQqDQm2u092NYdX9-CxcYyRDY6YSk',
                                    language: 'en',
                                }}

                                styles={{
                                    // containerTop: {
                                    // position: 'absolute',
                                    // top: 0,
                                    // left: 0,
                                    // right: 0,
                                    // alignItems: 'center',
                                    // justifyContent: 'flex-start',
                                    // },
                                    container:{
                                    //flex:1,
                                    
                                    }
                                    ,
                                    textInput: {
                                        backgroundColor:  '#e3e5e8',
                                        borderColor: APP_GREY,
                                        borderWidth: 1,
                                        borderRadius: 30,
                                        height: 50,
                                        
                                    },

                                    // horizontal: {
                                    //     flexDirection: 'row',
                                    //     justifyContent: 'space-around',
                                    // },

                                    listView: {
                                         zIndex: 2000,
                                        backgroundColor: 'transparent',
                                        color: 'black', //To see where exactly the list is
                                        //position: 'relative',
                                        //top:43,
                                       // height:200,
                                        
                                    },
                                    textInputContainer: {
                                        // zIndex: 2000,
                                        // backgroundColor: '',
                                    },
                                    description: {
                                        fontWeight: 'bold',
                                    },
                                    predefinedPlacesDescription: {
                                        // color: 'red'
                                    },

                                }}

                                currentLocation={true}
                                // filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities
                                debounce={100} // debounce the requests in ms. Set to 0 to remove debounce. By default 0ms.

                            />
                        </View>

                        {/* <View>
                            <Text style={{color:"red"}}>Mobile check</Text>
                        </View> */}
                        <View style={{padding:20}}>
                        <Text style={styles.TextHeader}>Upload documents for verification</Text>
                        <View style={styles.ImageContainer}>
                                <TouchableOpacity onPress={() => this.picDocument()} style={{
                                    height: '85%', width: 72, backgroundColor: '#e3e5e8', borderRadius: 10, shadowOffset: {
                                        width: 0,
                                        height: 1
                                    },
                                    shadowRadius: 1,
                                    shadowOpacity: 0.5, borderTopWidth: 0,
                                    elevation: Platform.OS == 'android' ? 5 : 0
                                }}>
                                    <Image source={ADD_DOCUMENT} style={{ height: 22, width: 22, alignSelf: 'center', marginTop: 20 }}></Image>
                                </TouchableOpacity>
                                {this.showImage()}
                            </View>
                        </View>
                            <View style={styles.TextContainer}>
                            <Text style={styles.TextHeader}>Select Subscription Plan</Text>
                            <View style={[styles.ImageContainer, { marginTop: 5 }]}>
                                <TouchableOpacity onPress={() => this.selectSubRadioFree()} style={{ width: '50%', height: '100%', backgroundColor: 'transparent', flexDirection: 'row' }}>
                                    <Image style={{ height: 25, width: 25, alignSelf: 'center', tintColor: APP_BLACK }} source={this.state.subscriptionIconFree}></Image>
                                    <Text style={{ fontFamily: FONT_MEDIUM, fontSize: 16, color: APP_BLACK, marginLeft: 10, marginTop: 0, alignSelf: 'center' }}>Free  <Text onPress={() => alert('This is your free subscription')} style={{ color: APP_GREY, fontFamily: FONT_MEDIUM, fontSize: 14, color: 'dodgerblue' }}>Detail</Text></Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.selectSubRadioPaid()} style={{ width: '50%', height: '100%', backgroundColor: 'transparent', flexDirection: 'row' }}>
                                    <Image style={{ height: 25, width: 25, alignSelf: 'center', tintColor: APP_BLACK }} source={this.state.subscriptionIconPaid}></Image>
                                    <Text style={{ fontFamily: FONT_MEDIUM, fontSize: 16, color: APP_BLACK, marginLeft: 10, marginTop: 0, alignSelf: 'center' }}>Paid($20)  <Text onPress={() => alert('This is your paid subscription')} style={{ color: APP_GREY, fontFamily: FONT_MEDIUM, fontSize: 14, color: 'dodgerblue' }}>Detail</Text></Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        {this.state.serverError ? <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center', height: 20, backgroundColor: 'transparent' }}>
                            <Text style={{ alignSelf: 'center', color: "#dd4b39", fontFamily: FONT_SEMIBOLD, fontSize: 16 }}>{this.state.serverErrorText}</Text>
                        </View> : null}
                        <View style={styles.button}>
                            <TouchableOpacity onPress={() => this.verifyHit()} style={{ height: '100%' }}>
                                <Text style={styles.buttonText}>Verify</Text>
                            </TouchableOpacity>
                        </View>
                        {/* <View style={{bottom:this.state.onSelectedLoc==''? 120:0}}>
                        <View style={[styles.TextContainerImages,{marginLeft:20}]}>
                            <Text style={styles.TextHeader}>Upload documents for verification</Text>
                            <View style={styles.ImageContainer}>
                                <TouchableOpacity onPress={() => this.picDocument()} style={{
                                    height: '85%', width: 72, backgroundColor: '#e3e5e8', borderRadius: 10, shadowOffset: {
                                        width: 0,
                                        height: 1
                                    },
                                    shadowRadius: 1,
                                    shadowOpacity: 0.5, borderTopWidth: 0,
                                    elevation: Platform.OS == 'android' ? 5 : 0
                                }}>
                                    <Image source={ADD_DOCUMENT} style={{ height: 22, width: 22, alignSelf: 'center', marginTop: 20 }}></Image>
                                </TouchableOpacity>
                                {this.showImage()}
                            </View>
                        </View>

                        <View style={styles.TextContainer}>
                            <Text style={styles.TextHeader}>Select Subscription Plan</Text>
                            <View style={[styles.ImageContainer, { marginTop: 5 }]}>
                                <TouchableOpacity onPress={() => this.selectSubRadioFree()} style={{ width: '50%', height: '100%', backgroundColor: 'transparent', flexDirection: 'row' }}>
                                    <Image style={{ height: 25, width: 25, alignSelf: 'center', tintColor: APP_BLACK }} source={this.state.subscriptionIconFree}></Image>
                                    <Text style={{ fontFamily: FONT_MEDIUM, fontSize: 16, color: APP_BLACK, marginLeft: 10, marginTop: 0, alignSelf: 'center' }}>Free  <Text onPress={() => alert('This is your free subscription')} style={{ color: APP_GREY, fontFamily: FONT_MEDIUM, fontSize: 14, color: 'dodgerblue' }}>Detail</Text></Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.selectSubRadioPaid()} style={{ width: '50%', height: '100%', backgroundColor: 'transparent', flexDirection: 'row' }}>
                                    <Image style={{ height: 25, width: 25, alignSelf: 'center', tintColor: APP_BLACK }} source={this.state.subscriptionIconPaid}></Image>
                                    <Text style={{ fontFamily: FONT_MEDIUM, fontSize: 16, color: APP_BLACK, marginLeft: 10, marginTop: 0, alignSelf: 'center' }}>Paid($20)  <Text onPress={() => alert('This is your paid subscription')} style={{ color: APP_GREY, fontFamily: FONT_MEDIUM, fontSize: 14, color: 'dodgerblue' }}>Detail</Text></Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        {this.state.serverError ? <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center', height: 20, backgroundColor: 'transparent' }}>
                            <Text style={{ alignSelf: 'center', color: "#dd4b39", fontFamily: FONT_SEMIBOLD, fontSize: 16 }}>{this.state.serverErrorText}</Text>
                        </View> : null}
                        <View style={styles.button}>
                            <TouchableOpacity onPress={() => this.verifyHit()} style={{ height: '100%' }}>
                                <Text style={styles.buttonText}>Verify</Text>
                            </TouchableOpacity>
                        </View>
                     </View> */}
                {/* </View> */}
                </View> 
              </ScrollView>
            </SafeAreaView>
        )
    }
};

const styles = StyleSheet.create({
    OptionsView: {
        height: "100%",
        width: width,
        backgroundColor: APP_WHITE
    },
    view: {
        width: "90%",
        height: "80%",
        alignSelf: 'center',
        backgroundColor: 'transparent',
        marginTop: 30
    },
    feildBorder: {
        width: width - 40,
        height: 50,
        borderRadius: 30,
        borderWidth: 1.5,
        borderColor: APP_GREY,
        backgroundColor: '#e3e5e8',
        alignSelf: 'center'
    },
    textfeild: {
        width: width - 80,
        height: 47,
        fontSize: 14,

        fontFamily: FONT_MEDIUM,
        borderRadius: 30,
        alignSelf: 'center',
        color: APP_BLACK,
    },

    button: {
        width: width - 45,
        height: 55,
        borderRadius: 30,
        //borderWidth:1,
        marginTop: 40,
        //borderColor:APP_BLACK,
        backgroundColor: '#e3e5e8',
        alignSelf: 'center',
        shadowOffset: {
            width: 0,
            height: 1
        },
        shadowRadius: 1,
        shadowOpacity: 0.5,
        elevation: Platform.OS == 'android' ? 5 : 0

    },
    buttonText: {
        fontSize: 18,
        fontFamily: FONT_MEDIUM,
        color: APP_BLACK,
        alignSelf: 'center',
        marginTop: 15
    },
    TextContainer: {
        width: width - 40,
        alignSelf: 'center',
        paddingHorizontal: 0,
        paddingVertical: 0,
        backgroundColor: 'transparent'
    },
    TextContainerImages: {
        paddingHorizontal: 0,
        paddingVertical: 0,
        backgroundColor: 'transparent',
        
    },
    TextHeader: {
        color: APP_BLACK,
        fontSize: 15,
        fontFamily: FONT_MEDIUM
    },
    ImageContainer: {
        width: '100%',
        height: 75,
        backgroundColor: 'transparent',
        alignSelf: 'center',
        overflow: 'hidden',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        marginTop: 17,
    },

});

export default AddLocation;

import React, {Component} from 'react';
import {View,
    Dimensions ,
    Text,
    StyleSheet,
    TouchableOpacity,
    Image,
    TextInput,
    BackHandler,
    Alert,
    SafeAreaView,
    Platform,
    ScrollView,
    KeyboardAvoidingView,
    ImageBackground,FlatList} from 'react-native';
    import AsyncStorage from '@react-native-community/async-storage';
    import {FONT_BLACK,FONT_BOLD,FONT_EXTRABOLD,FONT_EXTRALIGHT,FONT_LIGHT,FONT_SEMIBOLD,SEARCH_ICON_INACTIVE,FILTER,RESTAURANT,HEADER_ICON,
        FONT_THIN,APP_WHITE,APP_BLACK,APP_FACEBOOK,BACK_ICON, FONT_REGULAR,FONT_MEDIUM,NOTIFICATION_ICON, APP_GREY, RATTING,} from '../../Constants/App_Constants.js'
    let { width, height } = Dimensions.get('window')
    const ASPECT_RATIO = width / height
    var data=[1,2]
class FeaturedBusiness extends Component {
    constructor(props){
        super(props);
        this.state={
            username:'',
            password:'',
            data:''
        }  
      } 
      
      header(){
        if(Platform.OS=='ios'){
          return(
            <View style={{height:65,width:'100%',backgroundColor:'white',justifyContent:'flex-end',borderBottomColor:'#e3e5e8',borderBottomWidth:3}}>
                <View style={{height:'80%',width:'95%',backgroundColor:'transparent',alignSelf:'center'}}>
                    <View style={{flexDirection:'row',justifyContent:'space-between',height:'100%',width:'100%',backgroundColor:'transparent',alignSelf:'flex-start'}}>
                        <View style={{flexDirection:'row',width:200,backgroundColor:'transparent',justifyContent:'flex-start'}}>
                            <TouchableOpacity onPress={()=>this.props.navigation.goBack()} style={{height:'100%',backgroundColor:'transparent',width:40,alignItems:'center'}}>
                                <Image source={BACK_ICON} style={{marginTop:15, height:13,width:19,tintColor:APP_BLACK}}></Image>
                            </TouchableOpacity>
                            <Image source={HEADER_ICON} style={{marginTop:5, height:32,width:32}}></Image>
                            <Text style={{color:APP_BLACK,fontSize:18,marginLeft:7,marginTop:10,fontFamily:FONT_MEDIUM,backgroundColor:'transparent'}}>Features Business</Text>
                        </View>
                        <TouchableOpacity onPress={()=>this.props.navigation.navigate('Notifications')} style={{height:'100%',width:30}}>
                        <Image source={NOTIFICATION_ICON} style={{marginTop:13, height:17,width:15,alignSelf:'center'}}></Image>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
          )
        }else{
          return(
            <View style={{height:65,width:'100%',backgroundColor:'white',justifyContent:'flex-end',borderBottomColor:'#e3e5e8',borderBottomWidth:3}}>
                <View style={{height:'80%',width:'95%',backgroundColor:'transparent',alignSelf:'center'}}>
                    <View style={{flexDirection:'row',justifyContent:'space-between',height:'100%',width:'100%',backgroundColor:'transparent',alignSelf:'flex-start'}}>
                        <View style={{flexDirection:'row',width:200,backgroundColor:'transparent',justifyContent:'flex-start'}}>
                            <TouchableOpacity onPress={()=>this.props.navigation.goBack()} style={{height:'100%',backgroundColor:'transparent',width:40,alignItems:'center'}}>
                                <Image source={BACK_ICON} style={{marginTop:15, height:13,width:19,tintColor:APP_BLACK}}></Image>
                            </TouchableOpacity>
                            <Image source={HEADER_ICON} style={{marginTop:5, height:32,width:32}}></Image>
                            <Text style={{color:APP_BLACK,fontSize:18,marginLeft:7,marginTop:10,fontFamily:FONT_MEDIUM,backgroundColor:'transparent'}}>Features Business</Text>
                        </View>
                        
                        <TouchableOpacity onPress={()=>this.props.navigation.navigate('Notifications')} style={{height:'100%',width:30}}>
                        <Image source={NOTIFICATION_ICON} style={{marginTop:13, height:17,width:15,alignSelf:'center'}}></Image>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
          )
        }
      }

      componentDidMount(){
        AsyncStorage.getItem("featuredData").then((value) => { 
            var data= JSON.parse(value)
          console.log(JSON.stringify(data))
          this.setState({data:data})
        }).done();
      }

    render(){
        return(
            <SafeAreaView forceInset={{ top: 'always' }} style={{flex:1,backgroundColor:APP_WHITE}}>
                {this.header()}
                <View style={styles.nearbyTextView}>      
                    <View style={styles.listView}>
                    <FlatList
                        data={this.state.data}
                        showsHorizontalScrollIndicator={false}
                        //horizontal={true}
                        numColumns={0}
                        contentContainerStyle={{width:'98%',backgroundColor:APP_WHITE,alignSelf:'center',height:'100%',alignItems:'center'}}
                        renderItem={({ item: data, index }) => {
                            var margin=0
                            var marginTop=20
                            // if(index%2==0||index==0){
                            //     margin=0
                            // }else{
                            //     margin=height<896?'5%':'5%'
                            // }
                            // if(index==0||index==1){
                            //     marginTop=20
                            // }else{
                            //     marginTop=10
                            // }
                           //{data.firstname} {data.lastname}
                           //console.log(data.businessImages)
                            return( 
                                
                                <TouchableOpacity onPress={()=>this.props.navigation.navigate('SingleDetail',{userid:data._id})} style={[{marginLeft:margin,marginTop:marginTop},styles.FeaturedView]}>
                                    <Image style={{height:'73%',width:'95%',borderRadius:12,borderColor:APP_GREY,borderWidth:1,alignSelf:'center'}} source={{uri:data.businessImages[0].image }}>

                                    </Image>
                                    <View style={styles.downView}>
                                        <Text style={styles.textName}>{data.firstname} {data.lastname}</Text>
                                        <Text style={styles.textDiscription}>{data.businessAddress}</Text>
                                        <View style={styles.rattingView}>
                                            <Image style={styles.imageStar} source={RATTING}></Image>
                                            <Text style={styles.rattingNumber}>0.5</Text>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            )
                        }}keyExtractor={item => item.id}/>
                
                </View>
                     
                  
                </View>

            </SafeAreaView>
        )
    }
};

const styles = StyleSheet.create({
  
  feildBorder:{
      width:290,
      height:35,
      borderRadius:30,
      borderColor:APP_BLACK,
      backgroundColor:APP_WHITE,
      //alignSelf:'center',
      marginTop:9,
      justifyContent:"space-between",
      flexDirection:'row'
  },
 
textfeild:{
    width:'88%',
    height:34,
    fontSize:11,
    fontFamily:FONT_MEDIUM,
    borderRadius:30,
    alignSelf:'center',
    color:APP_BLACK,
    //fontWeight:'400',
    backgroundColor:APP_WHITE
    },

nearbyTextView:{
    height:height,
    width:width,
    backgroundColor:APP_WHITE
},
nearbyTextInnerView:{
    height:50,
    width:'90%',
    backgroundColor:APP_WHITE,
    alignSelf:'center'
},
nearbyText:{
    fontFamily:FONT_MEDIUM,
    fontSize:14,
    color:APP_BLACK,
    alignSelf:'flex-start',
    marginTop:18
},
listView:{
    height:height,
    width:'100%',
    alignSelf:'center',
    backgroundColor:APP_WHITE
},
singleListview:{
    height:220,
    width:'100%',
    backgroundColor:APP_WHITE,
    marginBottom:15
},
NearbyView:{
    height:160,
    width:'100%',
    borderRadius:15,
    alignSelf:'flex-start',
    backgroundColor:'transparent'
},
downView:{
    height:60,
    backgroundColor:"transparent",
    width:'95%',alignSelf:'center',
    
},
textName:{
    fontFamily:FONT_SEMIBOLD,
    fontSize:13,
    marginTop:8,
    marginLeft:1,
    color:APP_BLACK
},
textDiscription:{
    fontFamily:FONT_REGULAR,
    fontSize:11,
    marginTop:3,
    color:APP_GREY,
    marginLeft:1
},
imageStar:{
    height:9,
    width:9,
    marginTop:5,
    marginLeft:3
},
rattingView:{
    width:"100%",
    height:'auto',
    backgroundColor:APP_WHITE,
    flexDirection:'row',
    justifyContent:'space-between'
},
rattingNumber:{
    color:APP_BLACK,
    fontSize:10,
    marginRight:5,
    fontFamily:FONT_MEDIUM,
    marginTop:4
},
FeaturedView:{
    height:height<896?260:250,
    width:height<896?350:380,
    alignSelf:'center',
    borderBottomWidth:2,
    backgroundColor:'transparent',
    borderColor:'#e3e5e8'
    
}

});

export default FeaturedBusiness;

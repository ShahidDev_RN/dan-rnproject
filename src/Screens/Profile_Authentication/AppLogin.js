import React, { Component } from 'react';
import {
    View,
    Dimensions,
    Text,
    StyleSheet,
    TouchableOpacity,
    Image,
    TextInput,
    BackHandler,
    Alert,
    SafeAreaView,
    Platform,
    ScrollView,
    KeyboardAvoidingView, StatusBar,
    ImageBackground,
    Keyboard,
    ToastAndroid
} from 'react-native';
import Splash from '../Splash/Splash.js'
import Video from 'react-native-video'
import CheckBox from 'react-native-check-box';
import { LoginManager, AccessToken, GraphRequest, GraphRequestManager } from "react-native-fbsdk";
import AsyncStorage from '@react-native-community/async-storage';
import * as API from '../../Requests/WebService.js'
import Geolocation from '@react-native-community/geolocation';
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
import * as Animatable from 'react-native-animatable';
import NetInfo from '@react-native-community/netinfo'
import { FONT_BLACK, FONT_BOLD, FONT_EXTRABOLD, FONT_EXTRALIGHT, FONT_LIGHT, FONT_SEMIBOLD, FONT_THIN, APP_WHITE, APP_BLACK, APP_FACEBOOK, CHECK_BOX_CHECK, CHECK_BOX_UNCHECK, FONT_REGULAR, FONT_MEDIUM, LOGIN, LOGIN_BACKGROUND, APP_GREY } from '../../Constants/App_Constants.js'
let { width, height } = Dimensions.get('window')
const ASPECT_RATIO = width / height
class AppLogin extends Component {
    constructor(props) {
        super(props);
        this.state = {
            longitude: '76.7018',
            latitude: '30.7089',
            internet: true,
            email: '',
            password: '',
            firstname: '',
            lastname: '',
            phonenumber: '',
            zipcode: '',
            user_type: '',
            fb_id: '',
            isChecked: false,
            serverError: false,
            serverErrorText: '',
            type: '',
            deviceToken: '',
            isloading: false,
            scrollEnabled: false,
            flexDirectValue: 'flex-start',
            splash: true,
            pauseVideo: false
        }
    }

    handleFacebookLogin() {
        var self = this;
        LoginManager.logInWithPermissions(['public_profile', 'email']).then(
            function (result) {
                if (result.isCancelled) {
                    console.log('Login cancelled', JSON.stringify(result))
                } else {
                    AccessToken.getCurrentAccessToken().then((data) => {
                        const responseInfoCallback = (error, result) => {
                            if (error) {
                                console.log('Error fetching data=', error.toString());
                            } else {
                                console.log('result fetching data=', result);
                                var firstname = result.name.substring(0, result.name.indexOf(' '))
                                var lastname = result.name.substring(result.name.indexOf(' ')).trim()
                                var email = result.email.toLowerCase()
                                var id = result.id
                                self.setState({
                                    firstname: firstname,
                                    lastname: lastname,
                                    email: email,
                                    fb_id: id,
                                    user_type: 'facebook',
                                    zipcode: '',
                                    password: '',
                                    isloading: true
                                }, () => {
                                    self.onFacebookRegister(self.state.latitude, self.state.longitude, self.state.firstname, self.state.lastname, self.state.email, self.state.phonenumber, self.state.password, self.state.zipcode, self.state.user_type,
                                        self.state.fb_id, self.state.deviceToken, self.state.deviceType)
                                })

                            }
                        };
                        const infoRequest = new GraphRequest(
                            '/me',
                            {
                                data,
                                parameters: {
                                    fields: {
                                        string: 'email,name,picture.type(large)',
                                    },
                                },
                            },
                            responseInfoCallback,
                        );
                        new GraphRequestManager().addRequest(infoRequest).start();
                    }
                    )

                }
            },
            function (error) {
                console.log('Login fail with error: ' + error)
            }
        )
    }

    location() {
        if (Platform.OS === 'ios') {
            Geolocation.getCurrentPosition(
                (position) => {
                    console.log('coords : ', position.coords)
                },
                (error) => {
                    console.log(error.code, error.message);
                },
                { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
            );
        }
    }

    // getToken(){
    //     getTokenFromAsync().then(value=>{
    //       console.log('token',value)
    //     })
    // }

    checkInternet() {
        const netInfo = NetInfo.addEventListener(state => {
            console.log("Connection type", state);
            console.log("Is connected?", state.isConnected);
            if (!state.isConnected) {
                this.setState({ internet: false })
                if (Platform.OS == 'android') {
                    ToastAndroid.showWithGravity('No Internet !', ToastAndroid.SHORT)
                } else {
                    Alert.alert('No Internet !')
                }
            } else {
                this.setState({ internet: true })
            }
        });
    }

    componentDidMount() {
        this.location()
        global.type == 'User'
        if (Platform.OS == 'ios') {
            this.setState({ type: 'ios' })
        } else {
            this.setState({ type: 'android' })
        }
        if (height == 667) {
            this.setState({ scrollEnabled: true, flexDirectValue: 'center' })
        } else {
            this.setState({ scrollEnabled: false })
        }
        //this.getToken()
        this.checkInternet()
        setTimeout(() => {
            this.setState({ splash: false })
        }, 3500)
        this.setState({ pauseVideo: false })
        this.setState({ deviceToken: 'dMHNX87sSuqqEqhtXeT4K:APA91bFxsGOzSiTtGbasYHeeKIt6FvgKKrjd0j1CDOBXtSL6ryKAIKdJ1HtDQJYUMOHbVL5KLBTvbild5XtOTG1yVSnFSXGZZI1uHsvla2eRivH5gtwwwaEsV33654g_VIi0PVPn4TKl' })
    }

    onFacebookRegister(longitude, latitude, firstname, lastname, email, phonenumber, password, zipcode, user_type, fb_id, deviceToken, deviceType) {
        if (this.state.internet) {
            API.REGISTER(longitude, latitude, firstname, lastname, email, phonenumber, password, zipcode, user_type, fb_id, deviceToken, deviceType).then((response) => {
                if (response.status == 1) {
                    AsyncStorage.setItem("user_token", (response.data.user[1].token), () => {
                        AsyncStorage.setItem("user_info", JSON.stringify(response.data.user[0]), () => { })
                        AsyncStorage.setItem("session", 'User')
                        //console.log('data',JSON.stringify(response.data.user))
                        this.setState({ isloading: false }, () => {
                            this.props.navigation.navigate('Bottomtab')
                        })
                    });

                } else if (response.status == 0) {
                    this.setState({ serverError: true, serverErrorText: response.message, isloading: false })
                }
            })
        } else {
            alert('Check internet connection');
        }

    }

    onFetchLoginRecords(email, password, deviceToken, type) {
        if (this.state.internet) {
            this.setState({ pauseVideo: true })
            API.LOGIN(email, password, deviceToken, type).then((response) => {
                 console.log("LOOOGIN",JSON.stringify(response))
                //.log("checkkkkkk>>>>>>>>>",response)
        if(response.status==1){


                if (response.data.user !== undefined) {
                    // if (response.data.user[0].length != 0 && response.data.business[0].length == 0) {
                        AsyncStorage.setItem("user_token", (response.data.user[1].token), () => {
                            AsyncStorage.setItem("user_info", JSON.stringify(response.data.user[0]), () => { })
                            AsyncStorage.setItem("session", response.data.user[1].type)
                            this.setState({ isloading: false, email: '', password: '' }, () => {
                                this.props.navigation.navigate('Bottomtab')
                                global.type = 'User'
                                this.textInput.clear()
                                this.textInput1.clear()
                                Keyboard.dismiss()
                            })
                        });
                    } 
                    
                    
                    
                    
                    
                    // else if (response.data.business[0].length != 0 && response.data.user[0].length == 0) {

                    else if(response){
                        AsyncStorage.setItem("user_info", JSON.stringify(response.data.business), () => { })
                        AsyncStorage.setItem("user_token", (response.data.business.businessdeviceToken[0].token), () => {
                             AsyncStorage.setItem("session", response.data.business.businessdeviceToken[1].deviceType)
                            this.setState({ isloading: false, email: '', password: '' }, () => {
                                this.props.navigation.navigate('BusinessBottomtab')
                                global.type = 'Bussiness'
                                this.textInput.clear()
                                this.textInput1.clear()
                                Keyboard.dismiss()
                            })
                        });
                    } 

                      
                    
                    else {
                        this.setState({ isloading: false })
                      //  console.log('data', JSON.stringify(response))
                    }

                }
                
                else  {
                    this.setState({ serverError: true, serverErrorText: response.message, isloading: false })
                    //console.log('data0', JSON.stringify(response))
                }
              
                
                
                // else{
                //     console.log('data0Else', JSON.stringify(response))
                //     this.setState({isloading:false},()=>alert('Something went wrong'))
                // }
            })


        } else {
            alert('check internet connection')
        }
    }

    user() {
        
        global.type = 'User'
        this.setState({ serverError: false }, () => {
            this.props.navigation.navigate('Register')
        })
    }
    bussiness() {
        global.type = 'Bussiness'
        this.setState({ serverError: false }, () => {
            this.props.navigation.navigate('Register')
        })
    }
    bussinessLogin() {
        global.type = 'Bussiness'
        this.setState({ serverError: false }, () => {
            this.props.navigation.navigate('LoginBusiness')
        })
    }
    facebookButton() {
        return (
            <View style={styles.facebookButton}>
                <TouchableOpacity onPress={() => this.handleFacebookLogin()} style={{ height: '100%' }}>
                    <Text style={styles.facebookbuttonText}>Login with Facebook</Text>
                </TouchableOpacity>
            </View>
        )
    }
    orText() {
        return (
            <View style={styles.ORView}>
                <Text style={styles.ORText}>OR</Text>
            </View>
        )
    }

    LoginText() {
        if (!this.state.splash)
            return (
                <Text style={styles.welcomeText}>Login</Text>
            )
    }

    LoginOptions() {
        if (!this.state.splash)
            return (
                <View style={{ alignSelf: 'center', marginTop:20,
                 marginTop: height < 896 ? 45 : 35
                 }}>
                    <Text style={styles.registerText}>Don't have an account?</Text>
                    {/* <View style={{ width: width, backgroundColor: 'transparent', height: 40, marginTop: 0 , backgroundColor:'red'}}> */}
                    <View style={{ width: width, height: 60, marginTop: 0 }}>
                        <View style={{ width: '95%', alignSelf: 'center', flexDirection: 'row', justifyContent: 'space-around', marginTop: 7 }}>
                            
                        <View style={{backgroundColor:APP_WHITE,height:45,width:140,borderRadius:40,marginTop:10,alignItems:'center',justifyContent:'center'}}>
                            <TouchableOpacity style={{}} onPress={() => this.user()}>
                                
                                <Text style={styles.registerTextUnderline}>Sign up as a user</Text>
                                
                            </TouchableOpacity>
                            </View>
                            {/* <TouchableOpacity style={{ borderBottomWidth: 1, borderBottomColor: APP_WHITE }} onPress={() => this.bussiness()}> */}
                            <View style={{backgroundColor:APP_WHITE,height:45,width:140,borderRadius:20,alignItems:'center',justifyContent:'center',margin:10}}>
                            <TouchableOpacity onPress={() => this.bussiness()}>
                               
                                <Text style={styles.registerTextUnderline2}>Register your business</Text>
                                
                            </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </View>
            )
    }

    LoginView() {
        if (!this.state.splash)
            return (
                <Animatable.View 
                    delay={10}
                    animation="fadeIn"
                    iterationCount={1}
                    direction="alternate"
                    style={{
                        height: 420,
                        width: '90%',
                        alignSelf: 'center',
                        backgroundColor: 'white',
                        // backgroundColor: 'yellow',
                        marginTop: Platform.OS=='android' ? 40:20, borderRadius: 20,
                        shadowColor: APP_GREY,
                        shadowOffset: {
                            width: 0,
                            height: 2
                        },
                        shadowRadius: 2,
                        shadowOpacity: 1,
                        opacity: 0.8,
                        // opacity: 0.15,
                        elevation: Platform.OS == 'android' ? 5 : 5
                    }}>

                    <View style={styles.feildBorder}>
                        <TextInput ref={input => { this.textInput = input }}
                            style={styles.textfeild}
                            placeholder="Enter Email"
                            value={this.state.email}
                            keyboardType="email-address"
                            placeholderTextColor={APP_BLACK}
                            selectionColor={APP_BLACK}
                            onChangeText={this.handleEmail}
                        />
                    </View>
                    <View style={styles.feildBorderPassword}>
                        <TextInput ref={input => { this.textInput1 = input }}
                            style={styles.textfeild}
                            secureTextEntry={true}
                            value={this.state.password}
                            placeholder="Password"
                            keyboardType="default"
                            placeholderTextColor={APP_BLACK}
                            selectionColor={APP_BLACK}
                            onChangeText={this.handlePassword}
                        />
                    </View>

                    {this.state.serverError ? <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center', height: 20, backgroundColor: 'transparent', marginTop: 10 }}>
                        <Text style={{ alignSelf: 'center', color: "#dd4b39", fontFamily: FONT_SEMIBOLD, fontSize: 16 }}>{this.state.serverErrorText}</Text>
                    </View> : null}

                    <View style={styles.button}>
                        <TouchableOpacity onPress={() => this.login(this.state.email.toLowerCase(), this.state.password, this.state.deviceToken, this.state.type)} style={{ height: '100%' }}>
                            <Text style={styles.buttonText}>Login</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={styles.forgetPassword}>
                        {/* <CheckBox
                            style={{ flex: 1, padding: 0 }}
                            rightText={'Remember Me'}
                            rightTextStyle={styles.otherText}
                            onClick={() => {
                                this.setState({
                                    isChecked: !this.state.isChecked
                                })
                            }}
                            isChecked={this.state.isChecked}
                            checkedImage={<Image style={{ tintColor: APP_BLACK }} source={CHECK_BOX_CHECK} />}
                            unCheckedImage={<Image style={{ tintColor: APP_BLACK }} source={CHECK_BOX_UNCHECK} />} /> */}

                        <TouchableOpacity onPress={() => this.props.navigation.navigate('ForgotPassword')}>
                            <Text style={[styles.otherText,{textAlign:"center"}]}>Forgot Password?</Text>
                        </TouchableOpacity>

                    </View>
                    {this.facebookButton()}
                </Animatable.View>
            )
    }

    handleEmail = (text) => {
        this.setState({ email: (text), serverError: false })
    }

    handlePassword = (text) => {
        this.setState({ password: (text), serverError: false })
    }

    login = (email, password, deviceToken, type) => {
        if (email == '' && password == '') {
            this.setState({ serverError: true, serverErrorText: 'Fields Empty' });
        } else if (password == '') {
            this.setState({ serverError: true, serverErrorText: 'Enter Password' });
        } else if (email == '' || !email.includes('.com') || !email.includes('@')) {
            if (email != '') {
                if (!email.includes('.com')) {
                    this.setState({ serverError: true, serverErrorText: 'Email Format Not Correct' })
                }
                if (!email.includes('@')) {
                    this.setState({ serverError: true, serverErrorText: 'Email Format Not Correct' })
                }
            } else {
                this.setState({ serverError: true, serverErrorText: 'Enter Email' });
            }
        } else {
            this.setState({ isloading: true })
            this.onFetchLoginRecords(email, password, deviceToken, type);
        }
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'white' }}>
                <StatusBar barStyle={'light-content'}></StatusBar>

                <OrientationLoadingOverlay
                    visible={this.state.isloading}
                    color="white"
                    indicatorSize="large" />

                <Animatable.View animation="fadeIn" iterationCount={1} direction="alternate">
                
                    <ScrollView
                        bounces={false}
                        scrollEnabled={this.state.scrollEnabled}
                        showsVerticalScrollIndicator={false}
                        style={{ backgroundColor: 'red' }}>

                        <View
                            style={{
                                height: '100%',
                                backgroundColor: 'white',
                                justifyContent: this.state.flexDirectValue,
                                marginBottom: 50
                            }}>

                            <View style={styles.background}>
                               
                                <Video
                                    style={{
                                        position: 'absolute',
                                        top: 0,
                                        left: 0,
                                        bottom: 0,
                                        right: 0,
                                        backgroundColor: APP_BLACK,
                                        height: 'auto',

                                    }}
                                    fullscreen={true}
                                    resizeMode='cover'
                                    //ignoreSilentSwitch={"ignore"}
                                    //onBuffer={this.onBuffer}
                                    repeat={false}
                                    source={require('../../Assets/Splash2.mp4')}
                                    controls={false}
                                    playInBackground={true} />

                                <View
                                    style={{
                                        height: height,
                                        width: width,
                                        backgroundColor: 'transparent',
                                        opacity: 0.8,
                                        
                                    }}>
                                    {this.LoginText()}
                                    {this.LoginView()}
                                    {this.LoginOptions()}
                                </View>

                            </View>

                        </View>
                    </ScrollView>
                </Animatable.View>

            </View>
        )
    }

};

const styles = StyleSheet.create({
    background: {
        height: '100%',
        width: width,
        backgroundColor: 'white',
        alignItems: 'center'
    },
    otherText: {
        fontSize: 13,
        fontFamily: FONT_MEDIUM,
        color: APP_BLACK
    },
    welcomeText: {
        alignSelf: 'flex-start',
        fontSize: 30,
        width: 100,
        marginLeft: 20,
        fontFamily: FONT_BOLD,
        color: APP_WHITE,
        marginTop: height < 896 ? '25%' : '55%'
    },
    feildView: {
        width: width,
        height: 520,
        backgroundColor: "transparent",
        marginTop: height < 896 ? 60 : 100
    },
    feildBorder: {
        width: width - 75,
        height: 53,
        borderRadius: 30,
        borderWidth: 0.8,
        marginTop: 35,
        borderColor: APP_BLACK,
        backgroundColor: 'transparent',
        alignSelf: 'center'
    },
    feildBorderPassword: {
        width: width - 75,
        height: 53,
        borderRadius: 30,
        borderWidth: 0.8,
        marginTop: 18,
        borderColor: APP_BLACK,
        backgroundColor: 'transparent',
        alignSelf: 'center'
    },
    textfeild: {
        width: width - 120,
        height: 51,
        fontSize: 14,
        fontFamily: FONT_REGULAR,
        borderRadius: 30,
        alignSelf: 'center',
        color: APP_BLACK,
        backgroundColor: 'transparent'
    },
    forgetPassword: {
        width: width - 75,
        height: 42,
        backgroundColor: "transparent",
        alignSelf: 'center',
        marginTop: 30,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    button: {
        width: width - 75,
        height: 53,
        borderRadius: 30,
        borderWidth: 1,
        marginTop: 37,
        borderColor: APP_BLACK,
        backgroundColor: APP_BLACK,
        alignSelf: 'center'
    },
    buttonText: {
        fontSize: 18,
        fontFamily: FONT_SEMIBOLD,
        color: APP_WHITE,
        alignSelf: 'center',
        marginTop: 14
    },
    ORView: {
        width: 360,
        height: 45,
        alignItems: 'center',
        backgroundColor: 'transparent',
        alignSelf: 'center'
    },
    ORText: {
        fontSize: 12,
        fontWeight: '600',
        marginTop: 15,
        color: APP_WHITE
    },
    facebookButton: {
        width: width - 75,
        height: 53,
        borderRadius: 30,
        marginTop: 5,
        borderColor: APP_BLACK,
        backgroundColor: APP_FACEBOOK,
        alignSelf: 'center'
    },
    facebookbuttonText: {
        fontSize: 17,
        color: APP_WHITE,
        fontFamily: FONT_SEMIBOLD,
        alignSelf: 'center',
        marginTop: 15
    },
    registerText: {
        color: APP_WHITE,
        fontSize: 14,
        marginTop: 0,
        fontFamily: FONT_MEDIUM,
        alignSelf: 'center',
    },
    registerTextUnderline: {
        // color: APP_WHITE,
        color: APP_BLACK,
        fontSize: 14,
        marginTop: 0,
        fontFamily:FONT_EXTRABOLD,
        
        // fontFamily: FONT_SEMIBOLD,
        alignSelf: 'center',
        textAlign:'center'
    },
    registerTextUnderline2:{
        color: APP_BLACK,
        fontSize: 14,
        marginTop: 0,
        fontFamily:FONT_EXTRABOLD,
        
        // fontFamily: FONT_SEMIBOLD,
        alignSelf: 'center',
        textAlign:'center'
    }
});

export default AppLogin;

import React, { Component } from 'react';
import {
    View,
    Dimensions,
    Text,
    StyleSheet,
    TouchableOpacity,
    Image,
    TextInput,
    BackHandler,
    Alert,
    SafeAreaView,
    Platform,
    ScrollView,
    KeyboardAvoidingView, StatusBar,
    ImageBackground,
    Keyboard,
} from 'react-native';
import * as API from '../../Requests/WebService.js'
import AsyncStorage from '@react-native-community/async-storage';
import {
    FONT_BLACK, FONT_BOLD, FONT_EXTRABOLD, FONT_EXTRALIGHT, FONT_LIGHT, FONT_SEMIBOLD, TOGGLE_BUTTON_OFF, TOGGLE_BUTTON_ON, LOGIN, SIGNUP_USER,
    FONT_THIN, APP_WHITE, APP_BLACK, APP_FACEBOOK, BACK_ICON, FONT_REGULAR, FONT_MEDIUM, NOTIFICATION_ICON, APP_GREY, SIGNUP_BG, SIGNUP_NEW,
} from '../../Constants/App_Constants.js'
import CheckBox from 'react-native-check-box';
let { width, height } = Dimensions.get('window')
const ASPECT_RATIO = width / height
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
const EMAIL_EXPRESSION_CHECK = /(?!.*\.{2})^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([\t]*\r\n)?[\t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([\t]*\r\n)?[\t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;

class Register extends Component {
    constructor(props) {
        super(props);
        this.state = {
            longitude: '30.7089',
            latitude: '76.7018',
            deviceToken: '',
            deviceType: '',
            firstname: '',
            lastname: '',
            email: '',
            password: '',
            phonenumber: '',
            zipcode: '',
            deviceType: '',
            user_type: 'manual',
            fb_id: '',
            keyboardavoid: false,
            isChecked: false,
            serverError: false,
            serverErrorText: 'Error',
            loginText: '',
            avoidEnable: false,
            isloading: false
        }
    }

  

    componentDidMount() {
        if (Platform.OS == 'ios') {
            this.setState({ deviceType: 'ios' })
        } else {
            this.setState({ deviceType: 'android' })
        }
        if (global.type == 'User') {
            this.setState({ loginText: 'Register' })
        } else {
            this.setState({ loginText: 'Next' })
        }
        this.setState({ deviceToken: 'dMHNX87sSuqqEqhtXeT4K:APA91bFxsGOzSiTtGbasYHeeKIt6FvgKKrjd0j1CDOBXtSL6ryKAIKdJ1HtDQJYUMOHbVL5KLBTvbild5XtOTG1yVSnFSXGZZI1uHsvla2eRivH5gtwwwaEsV33654g_VIi0PVPn4TKl' })
    }


    handleFirstname = (text) => {
        this.setState({ firstname: (text), serverError: false })
    }
    handleLastname = (text) => {
        this.setState({ lastname: (text), serverError: false })
    }
    handleEmail = (text) => {
        this.setState({ email: (text), serverError: false })
    }
    handlePassword = (text) => {
        this.setState({ password: (text), serverError: false })
    }
    handlePhonenumber = (text) => {
        this.setState({ phonenumber: (text), serverError: false })
    }
    handleZipcode = (text) => {
        this.setState({ zipcode: (text), serverError: false })
    }


    onFetchRegister(longitude, latitude, firstname, lastname, email, phonenumber, password, zipcode, user_type, fb_id, deviceToken, deviceType) {
        // this.setState({isloading:false})
        if (global.type == 'User') {
            API.REGISTER(longitude, latitude, firstname, lastname, email, phonenumber, password, zipcode, user_type, fb_id, deviceToken, deviceType).then((response) => {
                console.log('ref', response.data.user[1].token)
                if (response.status == 1) {
                    AsyncStorage.setItem("user_info", JSON.stringify(response.data.user[0]), () => { })
                    AsyncStorage.setItem("session", 'User')
                    AsyncStorage.setItem("user_token", (response.data.user[1].token), () => {
                        this.setState({ isloading: false }, () => {
                            this.props.navigation.navigate('Bottomtab')
                        })
                    });

                } else if (response.status == 0) {
                    this.setState({ serverError: true, serverErrorText: response.message, isloading: false })
                }
            })
        } else {
            this.props.navigation.navigate('BusinessRegister', {
                firstname: this.state.firstname,
                lastname: this.state.lastname,
                phonenumber: this.state.phonenumber,
                email: this.state.email.toLowerCase(),
                password: this.state.password,
                zipcode: this.state.zipcode
            }, () => { Keyboard.dismiss() })
            this.setState({ isloading: false })
        }
    }

    signUp() {
        this.register(this.state.latitude, this.state.longitude, this.state.firstname, this.state.lastname, this.state.email.toLowerCase(), this.state.phonenumber, this.state.password, this.state.zipcode, this.state.user_type, this.state.fb_id, this.state.deviceToken, this.state.deviceType)
        //this.props.navigation.navigate('BusinessRegister')
    }


    register = (longitude, latitude, firstname, lastname, email, phonenumber, password, zipcode, user_type, fb_id, deviceToken, deviceType) => {
        if (email == '' && password == '' && lastname == '' && firstname == '' && phonenumber == '' && zipcode == '') {
            this.setState({ serverError: true, serverErrorText: 'Fields Empty' });
        } else if (firstname == '') {
            this.setState({ serverError: true, serverErrorText: 'Enter FirstName' });
        } else if (lastname == '') {
            this.setState({ serverError: true, serverErrorText: 'Enter LastName' });
        } else if (email == '' || !email.includes('.com') || !email.includes('@')) {
            if(!EMAIL_EXPRESSION_CHECK.test(this.state.email.trim().toLowerCase())){
                this.setState({ serverError: true, serverErrorText: 'Please enter a valid email' })
            }
             else {
                this.setState({ serverError: true, serverErrorText: 'Enter Email' });
            }
        } else if (password == '') {
            this.setState({ serverError: true, serverErrorText: 'Enter Password' });
        } else if (password.length < 4) {
            this.setState({ serverError: true, serverErrorText: 'Password is too short' });
        }
        else if (phonenumber == '') {
            this.setState({ serverError: true, serverErrorText: 'Enter PhoneNumber' });
        }
        else if (phonenumber.length < 9) {
            this.setState({ serverError: true, serverErrorText: 'Phone Number is too short' });
        } else if (phonenumber.length > 15) {
            this.setState({ serverError: true, serverErrorText: 'Phone Number exceeds the limit' });
        }
        else if (zipcode == '') {
            this.setState({ serverError: true, serverErrorText: 'Enter Zipcode' });
        } else if (zipcode.length < 4) {
            this.setState({ serverError: true, serverErrorText: 'Enter valid Zipcode' });
        } else {
            this.setState({ isloading: true })
            this.onFetchRegister(longitude, latitude, firstname, lastname, email, phonenumber, password, zipcode, user_type, fb_id, deviceToken, deviceType);
        }
    }

    //angriest scam

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <StatusBar barStyle={'dark-content'}></StatusBar>
                <OrientationLoadingOverlay
                    visible={this.state.isloading}
                    color="white"
                    indicatorSize="large"
                />
                <KeyboardAvoidingView
                    style={{
                        // height: height,
                        backgroundColor: APP_BLACK,
                        justifyContent: 'center',
                    }}
                     behavior={Platform.OS == 'ios'?'height':''} 
                     enabled
                    // //enabled={this.state.avoidEnable}
                     keyboardVerticalOffset={Platform.OS == 'ios'?10:10}
                    >
                    <ImageBackground style={{ height: "100%", width: "100%", }} source={global.type == 'User' ? SIGNUP_USER : SIGNUP_BG}>
                        <ScrollView>
                        {/* <View style={{ padding:20,backgroundColor:"transparent"}}> */}
                        <View style={{ padding:20}}>
                        {/* <View style={{ padding:20}}> */}
                              <View>
                                <Text style={styles.welcomeText}>Welcome to Signup</Text>
                                </View>
                     {/* <KeyboardAwareScrollView> */}
                            <View>
                                <View style={styles.view}>
                                    <View style={styles.feildBorder}>
                                        <TextInput ref={input => { this.textInput = input }}
                                            style={styles.textfeild}
                                            placeholder="Enter First Name"
                                            onFocus={() => this.setState({ avoidEnable: false })}
                                            keyboardType="default"
                                            selectionColor={APP_BLACK}
                                            placeholderTextColor={APP_BLACK}
                                            onChangeText={this.handleFirstname}
                                        />
                                    </View>

                                    <View style={styles.feildBorder1}>
                                        <TextInput ref={input => { this.textInput = input }}
                                            style={styles.textfeild}
                                            placeholder="Enter Last Name"
                                            keyboardType="default"
                                            onFocus={() => this.setState({ avoidEnable: false })}
                                            selectionColor={APP_BLACK}
                                            placeholderTextColor={APP_BLACK}
                                            onChangeText={this.handleLastname}
                                        />
                                    </View>

                                    <View style={styles.feildBorder1}>
                                        <TextInput ref={input => { this.textInput = input }}
                                            style={styles.textfeild}
                                            placeholder="Enter Email"
                                            keyboardType="email-address"
                                            onFocus={() => this.setState({ avoidEnable: false })}
                                            selectionColor={APP_BLACK}
                                            placeholderTextColor={APP_BLACK}
                                            onChangeText={this.handleEmail}
                                        />
                                    </View>

                                    <View style={styles.feildBorder1}>
                                        <TextInput ref={input => { this.textInput = input }}
                                            style={styles.textfeild}
                                            placeholder="Enter Password"
                                            keyboardType="default"
                                            onFocus={() => this.setState({ avoidEnable: false })}
                                            secureTextEntry={true}
                                            selectionColor={APP_BLACK}
                                            placeholderTextColor={APP_BLACK}
                                            onChangeText={this.handlePassword}
                                        />
                                    </View>

                                    <View style={styles.feildBorder1}>
                                        <TextInput ref={input => { this.textInput = input }}
                                            style={styles.textfeild}
                                            onFocus={() => this.setState({ avoidEnable: true })}
                                            placeholder="Enter Phone Number"
                                            keyboardType="phone-pad"
                                            returnKeyType="done"
                                            selectionColor={APP_BLACK}
                                            placeholderTextColor={APP_BLACK}
                                            onChangeText={this.handlePhonenumber}
                                        />
                                    </View>

                                    <View style={styles.feildBorder1}>
                                        <TextInput ref={input => { this.textInput = input }}
                                            style={styles.textfeild}
                                            placeholder="Enter Zip Code"
                                            keyboardType="number-pad"
                                            returnKeyType="done"
                                            onFocus={() => this.setState({ avoidEnable: true })}
                                            selectionColor={APP_BLACK}
                                            placeholderTextColor={APP_BLACK}
                                            onChangeText={this.handleZipcode}
                                        />
                                    </View>
                                    {this.state.serverError ? <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center', height: 20, backgroundColor: 'transparent', marginTop: 10 }}>
                                      <Text style={{ alignSelf: 'center', color: "#dd4b39", fontFamily: FONT_SEMIBOLD, fontSize: 16,  }}>{this.state.serverErrorText}</Text>
                                    </View> : null}
                                    <View>
                                        <View style={[styles.button, { marginTop: height < 896 ? 35 : 70 }]}>
                                            <TouchableOpacity onPress={() => this.signUp()} style={{ height: '100%' }}>
                                                <Text style={styles.buttonText}>{this.state.loginText}</Text>
                                            </TouchableOpacity>
                                        </View>
                                        <View style={{  alignSelf: 'center', }}>
                                            <View style={{ borderBottomWidth: 1, marginLeft: 3, borderBottomColor: APP_BLACK }}><Text onPress={() => this.props.navigation.goBack()} style={styles.registerTextUnderline}>Back to Login</Text></View>
                                        </View>
                                    </View>
                                </View>
                            </View>
                            {/* </KeyboardAwareScrollView> */}
                            </View>
                            
                        </ScrollView>
                    </ImageBackground>
                </KeyboardAvoidingView>
            </SafeAreaView>

            //                   <SafeAreaView style={{flex:1,}}>
            //                  <StatusBar barStyle={'dark-content'}/>
            //                  <OrientationLoadingOverlay
            //                     visible={this.state.isloading}
            //                     color="white"
            //                     indicatorSize="large"
            //                 />
            //                 <KeyboardAvoidingView
            //                     style={{
            //                          height: height,
            //                          backgroundColor: APP_BLACK,
            //                          justifyContent: 'center',
            //                     }}
            //                     // behavior={Platform.OS == 'android'?'':''} 
            //                     // enabled
            //                     //enabled={this.state.avoidEnable}
            //                      keyboardVerticalOffset={10}> 
            //                      {/* <ScrollView contentContainstyleerStyle={{flex:1}}>     */}
            //                     <ImageBackground style={styles.background}  source={global.type == 'User' ? SIGNUP_NEW : SIGNUP_BG}>
            //                         <ScrollView style={{flex:1}}>
            //                         {/* <ScrollView keyboardShouldPersistTaps={'always'}> */}
            //                                 <View style={{flex:1, padding:20,}}>
            //                                 <View>
            //                                     <Text style={styles.welcomeText}>Welcome to Signup</Text>
            //                                 </View>

            //                                 <View>
            //                                 <View style={styles.view}>
            //                                 <View style={styles.feildBorder}>
            //                                     <TextInput ref={input => { this.textInput = input }}
            //                                         style={styles.textfeild}
            //                                         placeholder="Enter First Name"
            //                                         onFocus={() => this.setState({ avoidEnable: false })}
            //                                         keyboardType="default"
            //                                         selectionColor={APP_BLACK}
            //                                         placeholderTextColor={APP_BLACK}
            //                                         onChangeText={this.handleFirstname}
            //                                     />
            //                                 </View>
            //                                 <View style={styles.feildBorder1}>
            //                                     <TextInput ref={input => { this.textInput = input }}
            //                                         style={styles.textfeild}
            //                                         placeholder="Enter Last Name"
            //                                         keyboardType="default"
            //                                         onFocus={() => this.setState({ avoidEnable: false })}
            //                                         selectionColor={APP_BLACK}
            //                                         placeholderTextColor={APP_BLACK}
            //                                         onChangeText={this.handleLastname}
            //                                     />
            //                                 </View>

            //                                 <View style={styles.feildBorder1}>
            //                                     <TextInput ref={input => { this.textInput = input }}
            //                                         style={styles.textfeild}
            //                                         placeholder="Enter Email"
            //                                         keyboardType="email-address"
            //                                         onFocus={() => this.setState({ avoidEnable: false })}
            //                                         selectionColor={APP_BLACK}
            //                                         placeholderTextColor={APP_BLACK}
            //                                         onChangeText={this.handleEmail}
            //                                     />
            //                                 </View>
            //                                 <View style={styles.feildBorder1}>
            //                                     <TextInput ref={input => { this.textInput = input }}
            //                                         style={styles.textfeild}
            //                                         placeholder="Enter Password"
            //                                         keyboardType="default"
            //                                         onFocus={() => this.setState({ avoidEnable: false })}
            //                                         secureTextEntry={true}
            //                                         selectionColor={APP_BLACK}
            //                                         placeholderTextColor={APP_BLACK}
            //                                         onChangeText={this.handlePassword}
            //                                     />
            //                                 </View>

            //                                 <View style={styles.feildBorder1}>
            //                                     <TextInput ref={input => { this.textInput = input }}
            //                                         style={styles.textfeild}
            //                                         onFocus={() => this.setState({ avoidEnable: true })}
            //                                         placeholder="Enter Phone Number"
            //                                         keyboardType="phone-pad"
            //                                         returnKeyType="done"
            //                                         selectionColor={APP_BLACK}
            //                                         placeholderTextColor={APP_BLACK}
            //                                         onChangeText={this.handlePhonenumber}
            //                                     />
            //                                 </View>

            //                                 <View style={styles.feildBorder1}>
            //                                     <TextInput ref={input => { this.textInput = input }}
            //                                         style={styles.textfeild}
            //                                         placeholder="Enter Zip Code"
            //                                         keyboardType="number-pad"
            //                                         returnKeyType="done"
            //                                         onFocus={() => this.setState({ avoidEnable: true })}
            //                                         selectionColor={APP_BLACK}
            //                                         placeholderTextColor={APP_BLACK}
            //                                         onChangeText={this.handleZipcode}
            //                                     />
            //                                 </View>
            //                                 </View>
            //                                 {this.state.serverError ? <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center', height: 20, backgroundColor: 'transparent', marginTop: 10 }}>
            //                                     <Text style={{ alignSelf: 'center', color: "#dd4b39", fontFamily: FONT_SEMIBOLD, fontSize: 16,  }}>{this.state.serverErrorText}</Text>
            //                                 </View> : null}

            //                                 <View>
            //                                 <View style={[styles.button,{marginTop: height < 896 ? 35 : 70}]}>
            //                                     <TouchableOpacity onPress={() => this.signUp()} style={{ height: '100%' }}>
            //                                         <Text style={styles.buttonText}>{this.state.loginText}</Text>
            //                                     </TouchableOpacity>
            //                                 </View>

            //                                 <View style={{ flexDirection: 'row', alignSelf: 'center', }}>
            //                                     <View style={{ borderBottomWidth: 1, marginLeft: 3, borderBottomColor: APP_BLACK }}><Text onPress={() => this.props.navigation.goBack()} style={styles.registerTextUnderline}>Back to Login</Text></View>
            //                                 </View>
            //                              </View>   
            //                                 </View>
            //                               </View>  

            //                         </ScrollView> 
            //                     </ImageBackground>
            //                     {/* </ScrollView> */}
            //                 </KeyboardAvoidingView>
            //                 </SafeAreaView>               
        )
    }
};

const styles = StyleSheet.create({
    background: {
        height: height,
        width: width,
        alignItems: 'center',
        // height:'100%',
        // width:"100%"

    },
    OptionsView: {
        height: height,
        width: width,
        backgroundColor: "transparent"
    },
    otherText: {
        fontSize: 14,
        fontFamily: FONT_MEDIUM,
        color: APP_WHITE
    },
    view: {
        width: "100%",
        height: "100%",
        alignSelf: 'center',
        backgroundColor: 'transparent',
        marginTop: height < 896 ? 40 : 55,
        flex: 1
    },
    feildBorder: {
        width: width - 40,
        height: height < 896 ? 50 : 55,
        borderRadius: 30,
        borderWidth: 1,
        borderColor: APP_BLACK,
        //backgroundColor:APP_BLACK,
        alignSelf: 'center'
    },
    feildBorder1: {
        width: width - 40,
        height: height < 896 ? 50 : 55,
        borderRadius: 30,
        borderWidth: 1,
        marginTop: 15,
        borderColor: APP_BLACK,
        //backgroundColor:APP_BLACK,
        alignSelf: 'center'
    },
    textfeild: {
        width: width - 80,
        height: height < 896 ? 45 : 50,
        fontSize: 15,
        fontFamily: FONT_REGULAR,
        borderRadius: 30,
        alignSelf: 'center',
        color: APP_BLACK,

    },
    button: {
        width: width - 45,
        height: height < 896 ? 50 : 55,
        borderRadius: 30,
        borderWidth: 1,
        marginTop: height < 896 ? 40 : 70,
        borderColor: APP_BLACK,
        backgroundColor: APP_BLACK,
        alignSelf: 'center'
    },
    buttonText: {
        fontSize: 17,
        fontFamily: FONT_SEMIBOLD,
        color: APP_WHITE,
        alignSelf: 'center',
        marginTop: height < 896 ? 13 : 15
    },
    welcomeText: {
        alignSelf: 'center',
        fontSize: height < 896 ? 20 : 24,
        fontFamily: FONT_MEDIUM,
        //backgroundColor:APP_BLACK,
        color: APP_BLACK,
        marginTop: height < 896 ? '15%' : '25%'
    },

    registerTextUnderline: {
        color: APP_BLACK,
        fontSize: 14,
        marginTop: 20,
        fontFamily: FONT_SEMIBOLD,
        alignSelf: 'center',
    }

});

export default Register;



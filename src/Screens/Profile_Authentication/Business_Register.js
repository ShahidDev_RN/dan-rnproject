import React, { Component } from 'react';
import {
    View,
    Dimensions,
    Text,
    StyleSheet,
    TouchableOpacity,
    Image,
    TextInput,
    BackHandler,
    Alert,
    SafeAreaView,
    Platform,
    ScrollView, FlatList,
    KeyboardAvoidingView, StatusBar, PermissionsAndroid, Modal,
    ImageBackground
} from 'react-native';
import * as API from '../../Requests/WebService.js'
import ImagePicker from 'react-native-image-picker';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import {
    FONT_BLACK, FONT_BOLD, FONT_EXTRABOLD, FONT_EXTRALIGHT, FONT_LIGHT, FONT_SEMIBOLD, TOGGLE_BUTTON_OFF, TOGGLE_BUTTON_ON, FOR_IMAGE_URL, RADIO_ON, RADIO_OFF, LOGIN,
    FONT_THIN, APP_WHITE, APP_BLACK, APP_FACEBOOK, BACK_ICON, FONT_REGULAR, FONT_MEDIUM, NOTIFICATION_ICON, APP_GREY, ADD_DOCUMENT, REMOVE_ICON, SIGNUP_BG, Map_Marker
} from '../../Constants/App_Constants.js'
let { width, height } = Dimensions.get('window')
import Geolocation from '@react-native-community/geolocation';
const ASPECT_RATIO = width / height
import ImageResizer from 'react-native-image-resizer';
import MapView, { Marker } from 'react-native-maps';
import DocumentPicker from 'react-native-document-picker';
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
import DatePicker from 'react-native-date-picker'
import AsyncStorage from '@react-native-community/async-storage';


const EMAIL_EXPRESSION_CHECK = /(?!.*\.{2})^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([\t]*\r\n)?[\t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([\t]*\r\n)?[\t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;

const createFormData = (image) => {
    var data = new FormData();
    data.append('profilePicture', {
        uri: Platform.OS === "android" ? image.uri : image.uri.replace("file://", ""),
        name: `ony${Date.now()}.jpg`,
        type: 'image/*'
    })
    return data
}
const createFormDataDoc = (image) => {
    var data = new FormData();
    data.append('profilePicture', {
        uri: Platform.OS === "android" ? image.fileCopyUri : image.fileCopyUri,
        name: `ony${Date.now()}.` + image.type.slice(image.type.indexOf('/') + 1),
        type: '*/*'
    })

    return data
}
var images = []
var doc = []
var id = 0
var choosenDate = [{ 'day': 'Monday', 'open': '00', 'close': '00' }, { 'day': 'Tuesday', 'open': '00', 'close': '00' },
{ 'day': 'Wednesday', 'open': '00', 'close': '00' }, { 'day': 'Thursday', 'open': '00', 'close': '00' },
{ 'day': 'Friday', 'open': '00', 'close': '00' }, { 'day': 'Saturday', 'open': '00', 'close': '00' },
{ 'day': 'Sunday', 'open': '00', 'close': '00' }]
var selectedHour = [
    { id: 1, title: 'No Hour Available', subTitle: 'Visitors would not see business hours on this Page ' },
    { id: 2, title: 'Always Open', subTitle: 'e.g. Parks, beaches, streets' },
    { id: 4, title: 'Open on Selected Hours', subTitle: 'Input your own hours' }
]
const LATITUDE_DELTA = 0.005
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO
class BusinessRegister extends Component {
    constructor(props) {
        super(props);
        this.state = {
            longitude: 0,
            latitude: 0, // businessLong
            // latitude:'', // businessLat
            firstname: '',
            lastname: '',
            password: '',
            phonenumber: '',
            zipcode: '',
            email: '',
            businessName: '', // businessName
            businessAddress: '', // businessAddress
            businessLink: '', // businessLink
            hours: '',
            categories: '', // allCategories
            businessImages: '', // imagesForVerification
            businessDoc: '',
            selectedCategory: '', // category
            selectedCategoryName: 'Select Category',
            subscriptionType: '0', // free/paid
            deviceToken: '',
            deviceType: '',
            loginText: 'Register',
            openPicker: false,
            showHours: false,
            showCategory: false,
            subscriptionIconFree: RADIO_ON,
            subscriptionIconPaid: RADIO_OFF,
            serverError: false,
            serverErrorText: 'Error',
            isloading: false,
            date: new Date(),
            showDatePicker: false,
            showCloseDatePicker: false,
            indexSelected: 0,
            placeaddress: '',
            userLocation: '',
            region: {
                latitude: 76.7018,
                longitude: 30.7089,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA
            },
            oldRegion: {
                description: ''
            },
            marker: {
                latitude: 0,
                longitude: 0,
                // latitude: 76.7018,
                // longitude: 30.7089,
            },
            description: '',
            scrollEnabled: true,
            loaded: false,
            operationalHour: 'Select Your Operational Hours',
            showOperationalHour: false,
            selectedHourId: 0

        }
    }

   async componentDidMount() {
       await this.location()
        this.getCategories()
        if (Platform.OS == 'ios') {
            this.setState({ deviceType: 'ios', deviceToken: 'dMHNX87sSuqqEqhtXeT4K:APA91bFxsGOzSiTtGbasYHeeKIt6FvgKKrjd0j1CDOBXtSL6ryKAIKdJ1HtDQJYUMOHbVL5KLBTvbild5XtOTG1yVSnFSXGZZI1uHsvla2eRivH5gtwwwaEsV33654g_VIi0PVPn4TKl' }, () => {
                this.setState({
                    firstname: this.props.navigation.getParam('firstname', ''), lastname: this.props.navigation.getParam('lastname', ''), email: this.props.navigation.getParam('email', ''),
                    password: this.props.navigation.getParam('password', ''), phonenumber: this.props.navigation.getParam('phonenumber', ''), zipcode: this.props.navigation.getParam('zipcode', '')
                })
            })
        } else {
            this.setState({
                deviceType: 'android',
                deviceToken: 'dMHNX87sSuqqEqhtXeT4K:APA91bFxsGOzSiTtGbasYHeeKIt6FvgKKrjd0j1CDOBXtSL6ryKAIKdJ1HtDQJYUMOHbVL5KLBTvbild5XtOTG1yVSnFSXGZZI1uHsvla2eRivH5gtwwwaEsV33654g_VIi0PVPn4TKl'
            }, () => {
                this.setState({
                    firstname: this.props.navigation.getParam('firstname', ''), lastname: this.props.navigation.getParam('lastname', ''), email: this.props.navigation.getParam('email', ''),
                    password: this.props.navigation.getParam('password', ''), phonenumber: this.props.navigation.getParam('phonenumber', ''), zipcode: this.props.navigation.getParam('zipcode', '')
                })
            })
        }
        this.setState({
            hours: [{ 'day': 'Monday', 'open': '00', 'close': '00' }, { 'day': 'Tuesday', 'open': '00', 'close': '00' },
            { 'day': 'Wednesday', 'open': '00', 'close': '00' }, { 'day': 'Thursday', 'open': '00', 'close': '00' },
            { 'day': 'Friday', 'open': '00', 'close': '00' }, { 'day': 'Saturday', 'open': '00', 'close': '00' },
            { 'day': 'Sunday', 'open': '00', 'close': '00' }]
        })
        // this.location()
    }


   




    getCategories() {
        API.GET_CATEGORIES().then((response) => {
            if (response.status == 200) {
                //console.log('categories----->',response.data)
                this.setState({ categories: response.data })
            }
        })
    }


    async location() {
   // if (Platform.OS === 'ios') {
        Geolocation.getCurrentPosition(
        (position) => {
        //console.log('coords : ',position.coords)
        this.setState({ latitude: position.coords.latitude, longitude: position.coords.longitude }, () => {
        })
        },
        (error) => {
        console.log(error.code, error.message);
        },
        { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
        );
        }
  //  }

    handleBusinessname = (text) => {
        this.setState({ businessName: (text), serverError: false })
    }
    handleAddress = (text) => {
        this.setState({ businessAddress: (text), serverError: false })
    }
    handleWebsite = (text) => {
        this.setState({ businessLink: (text), serverError: false })
    }

    handleConfirm = (date) => {
        console.log("A date has been picked: ", date);

    };



    changeSearchText = (text) => {
        console.log("check ??????????????? ", text)

        console.log('text', text + '-' + text.length + '-' + this.GooglePlacesRef.isFocused())
        if (this.GooglePlacesRef.isFocused()) {
            this.setState({
                placeaddress: text
            })
        }
    }

    changeLocation(data, details) {
        console.log(data, 'comeessss')

        //let coord = [parseFloat(details.geometry.location.lat), parseFloat(details.geometry.location.lng)]
        console.log('changeLocationAdd', JSON.stringify(coord))
        this.setState({
            placeaddress: data.description
        }
        )
    }



    ////////hours available///////
    showHours() {
        if (this.state.showHours)
            return (
                <View style={styles.availableHours}>
                    <FlatList
                        data={this.state.hours}
                        showsVerticalScrollIndicator={false}
                        contentContainerStyle={{ backgroundColor: 'transparent', width: '90%', alignSelf: 'center', marginTop: 10 }}
                        renderItem={({ item: data, index }) => {
                            return (
                                <View style={{ flexDirection: 'row', alignSelf: 'center', height: 23, backgroundColor: 'transparent', width: '100%', marginTop: 2, justifyContent: 'space-between' }}>
                                    <Text style={{ fontSize: 16, fontFamily: FONT_MEDIUM }}>{data.day}</Text>
                                    <Text onPress={() => this.setState({ showDatePicker: true, indexSelected: index })} style={{ fontSize: 16, fontFamily: FONT_MEDIUM }}>{data.open} to {data.close}</Text>
                                </View>
                            )
                        }}
                        keyExtractor={(item, index) => index.toString()} />
                </View>
            )
    }

    showCategory() {
        if (this.state.showCategory == '1')
            return (
                // <View style={styles.availableCategories}>
                <FlatList
                    data={this.state.categories}
                    showsVerticalScrollIndicator={false}
                    style={{ backgroundColor: APP_WHITE, width: 350, alignSelf: 'center', marginTop: 8, height: "100%", borderRadius: 10, paddingHorizontal: 12, paddingTop: 5 }}
                    renderItem={({ item: data, index }) => {
                        return (
                            <View>
                                <TouchableOpacity onPress={() => this.setState({ selectedCategory: data._id, showCategory: false, selectedCategoryName: data.categoryName })} style={{ alignSelf: 'center', height: 23, backgroundColor: 'transparent', width: '100%', marginTop: 2 }}>
                                    <Text style={{ fontSize: 15, fontFamily: FONT_MEDIUM }}>{data.categoryName}</Text>
                                </TouchableOpacity>
                                <View style={{ width: '100%', height: 0.5, backgroundColor: 'black', marginVertical: 3, opacity: 0.5 }} />
                            </View>
                        )
                    }}
                    keyExtractor={(item, index) => index.toString()} />
                // </View>
            )
    }

    // imageDelete(index) {
    // const elements = this.state.businessImages.filter(item => {
    // return item.id !== index
    // });
    // if (elements.length == 0) {
    // this.setState({ businessImages: '' });
    // images = [],
    // id = 0
    // index = 0
    // } else {
    // var newArray = []
    // elements.map((data, index) => {
    // console.log('index', index)
    // data.id = index
    // newArray.push(data)
    // })
    // images = []
    // newArray.map((data, index) => {
    // images.push(data)
    // })
    // this.setState({ businessImages: images }, () => {
    // console.log(JSON.stringify(this.state.businessImages))
    // });
    // }
    // }

    docDelete() {
        doc.shift()
        this.setState({ businessDoc: '' })
    }

    showImage() {
        return (
            <FlatList
                data={this.state.businessImages}
                showsVerticalScrollIndicator={false}
                numColumns={3}
                contentContainerStyle={{ backgroundColor: 'transparent', width: '95%', alignSelf: 'center', height: '85%' }}
                renderItem={({ item: data, index }) => {
                    return (
                        <View style={{ height: '100%', width: 72, backgroundColor: APP_GREY, alignSelf: 'flex-start', borderRadius: 10, marginLeft: 10 }}>
                            <Image source={{ uri: data.image }} style={{ height: '100%', width: '100%', alignSelf: 'center', borderRadius: 10 }}></Image>
                            <TouchableOpacity onPress={() => this.imageDelete(index)} style={{ height: 17, width: 17, alignSelf: 'center', position: 'absolute', backgroundColor: 'white', borderRadius: 10, top: 55 }}>
                                <Image source={REMOVE_ICON} style={{ height: 17, width: 17, borderRadius: 10 }}></Image>
                            </TouchableOpacity>
                        </View>
                    )
                }}
                keyExtractor={(item, index) => index.toString()} />)
    }

    showDoc() {
        return (
            <FlatList
                data={this.state.businessDoc}
                showsVerticalScrollIndicator={false}
                numColumns={3}
                contentContainerStyle={{ backgroundColor: 'transparent', width: '95%', alignSelf: 'center', height: '85%' }}
                renderItem={({ item: data, index }) => {
                    return (
                        <View style={{ height: '100%', width: 72, backgroundColor: APP_BLACK, alignSelf: 'flex-start', borderRadius: 10, marginLeft: 10, borderRadius: 1, borderColor: APP_WHITE }}>
                            <Image source={require('../../Assets/document.png')} style={{ height: '100%', width: '90%', alignSelf: 'center', borderRadius: 10 }}></Image>
                            <TouchableOpacity onPress={() => this.docDelete()} style={{ height: 17, width: 17, alignSelf: 'center', position: 'absolute', backgroundColor: 'white', borderRadius: 10, top: 55 }}>
                                <Image source={REMOVE_ICON} style={{ height: 17, width: 17, borderRadius: 10 }}></Image>
                            </TouchableOpacity>
                        </View>
                    )
                }}
                keyExtractor={(item, index) => index.toString()} />)
    }

    selectSubRadioFree() {
        this.setState({ subscriptionType: '0', subscriptionIconFree: RADIO_ON, subscriptionIconPaid: RADIO_OFF })
    }
    selectSubRadioPaid() {
        this.setState({ subscriptionType: '1', subscriptionIconPaid: RADIO_ON, subscriptionIconFree: RADIO_OFF })
    }

    // address() {
    // if (this.state.subscriptionType == '1')
    // return (
    // // <View style={styles.feildBorder1}>
    // // <TextInput ref={input => { this.textInput = input }}
    // // style={styles.textfeild}
    // // placeholder="Address"
    // // keyboardType="default"
    // // selectionColor={APP_BLACK}
    // // placeholderTextColor={APP_BLACK}
    // // onChangeText = {this.handleAddress}
    // // />
    // // </View>
    // // </View>
    // )
    // }


    category() {
        if (this.state.subscriptionType == '1')

            return (
                <View style={{
                    width: width - 40,
                     height: height < 896 ? 45 : 40,
                    borderRadius: 30,
                    borderWidth: 1,
                    // marginTop: 20,
                    borderColor: APP_BLACK,
                    alignSelf: 'center',
                    marginTop: 15
                }}>
                    {/* <View style={{marginTop:20,backgroundColor:'black'}}> */}
                    <TouchableOpacity onPress={() => this.setState({ showCategory: !this.state.showCategory, showHours: false })}
                        style={{ height: '100%', }}>
                        <Text style={{ color: APP_BLACK, fontFamily: FONT_SEMIBOLD, paddingTop: 13, paddingLeft: 20 }}>{this.state.selectedCategoryName}</Text>
                    </TouchableOpacity>
                </View>

            )

    }
    websiteLink() {
        if (this.state.subscriptionType == '1')
            return (
                <View style={styles.feildBorder1}>
                    <TextInput ref={input => { this.textInput = input }}
                        style={styles.textfeild}
                        placeholder="Website Link"
                        keyboardType="default"
                        selectionColor={APP_BLACK}
                        placeholderTextColor={APP_BLACK}
                        onChangeText={this.handleWebsite}
                    />
                </View>
            )
    }
    uploadDocuments() {
        if (this.state.subscriptionType == '1')
            return (
        <View style={{marginLeft:20}}>
                    <Text style={styles.TextHeader}>Upload documents for Verifications</Text>
                    <View style={styles.ImageContainer}>
                        <TouchableOpacity onPress={() => this.picDocument()} style={{ height: '85%', width: 72, backgroundColor: APP_BLACK, alignSelf: 'flex-start', borderRadius: 10 }}>
                            <Image source={ADD_DOCUMENT} style={{ height: 22, width: 22, alignSelf: 'center', marginTop: 22, tintColor: APP_WHITE }}></Image>
                        </TouchableOpacity>
                        {this.showDoc()}
                    </View>
                </View>
            )
    }
    uploadImage() {
        if (this.state.subscriptionType == '1')
            return (
                <View style={{marginLeft:20}}>
                    <Text style={styles.TextHeader}>Upload Photos</Text>
                    <View style={styles.ImageContainer}>
                        <TouchableOpacity onPress={() => this.pickImage()} style={{ height: '85%', width: 72, backgroundColor: APP_BLACK, alignSelf: 'flex-start', borderRadius: 10 }}>
                            <Image source={ADD_DOCUMENT} style={{ height: 22, width: 22, alignSelf: 'center', marginTop: 22, tintColor: APP_WHITE }}></Image>
                        </TouchableOpacity>
                        {this.showImage()}
                    </View>
                </View>
            )
    }

    pickImage() {
        if (Platform.OS === 'android') {
            PermissionsAndroid.requestMultiple(
                [PermissionsAndroid.PERMISSIONS.CAMERA,
                PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
                PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE]
            ).then((result) => {
                if (result['android.permission.CAMERA']
                    && result['android.permission.READ_EXTERNAL_STORAGE']
                    && result['android.permission.WRITE_EXTERNAL_STORAGE'] === 'granted') {
                    ImagePicker.showImagePicker((response) => {
                        if (response.didCancel) {
                            console.log('User cancelled image picker');
                        } else if (response.error) {
                            console.log('ImagePicker Error: ', response.error);
                        } else {
                            const source = response;

                            this.setState({ isloading: true }, () => {
                                this._compressImage(source)
                            })
                        }
                    })
                } else if (result['android.permission.CAMERA']
                    || result['android.permission.READ_EXTERNAL_STORAGE']
                    || result['android.permission.WRITE_EXTERNAL_STORAGE'] === 'never_ask_again') {
                    alert("Required permissions denied");
                }
            });
        } else {
            ImagePicker.showImagePicker((response) => {
                if (response.didCancel) {
                    console.log('User cancelled image picker');
                } else if (response.error) {
                    console.log('ImagePicker Error: ', response.error);
                } else {
                    const source = response;
                    this.setState({ isloading: true }, () => {
                        this._compressImage(source)
                    })
                }
            })
        }
    }

    async imageUpload(source) {
        try {
            let response = await fetch(
                API.BASE_URL + "fileupload",
                {
                    'method': 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data',
                    },
                    body: createFormData(source)
                }
            );

            if (response.status == 200) {
                response.json().then(data => {
                    images.push({ 'id': id, 'image': FOR_IMAGE_URL + data.filename })
                    id = id + 1
                    this.setState({ businessImages: images, isloading: false }, () => {
                        console.log('select', JSON.stringify(this.state.businessImages))
                    })
                });
            } else {
                console.log('error', response)
            }
        } catch (error) {
            console.error(error);
        }
    }

    async docUpload(source) {
        try {
            let response = await fetch(
                API.BASE_URL + "fileupload",
                {
                    'method': 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data',
                    },
                    body: createFormDataDoc(source)
                }
            );

            if (response.status == 200) {
                response.json().then(data => {
                    if (this.state.businessDoc.length < 1) {
                        doc.push(FOR_IMAGE_URL + data.filename)

                        this.setState({ businessDoc: doc, isloading: false }, () => {
                            console.log(this.state.businessDoc)
                        })
                    }

                });
            } else {
                console.log('error', response)
            }
        } catch (error) {
            console.error(error);
        }
    }

    async _compressImage(source) {
        var v = this;
        let response = await ImageResizer.createResizedImage(source.uri, 400, 400, 'JPEG', 80);
        if (response != null) {
            v.imageUpload(response)
        }
    }

    newAccount() {
        if (this.state.subscriptionType == '0') {
            console.log('register coming---->')
            images.push({ 'id': 0, image: 'https://apps.appsmaventech.com/onyx/1611148303104-placeholder@3x.png' })
            this.setState({ businessImages: images }, () => {
                this.onRegisterBussiness(this.state.latitude, this.state.longitude, this.state.email, this.state.firstname, this.state.lastname, this.state.hours, this.state.phonenumber, this.state.password, this.state.zipcode, this.state.businessName,
                    this.state.businessAddress, this.state.businessLink, this.state.subscriptionType, this.state.businessDoc, this.state.businessImages, this.state.selectedCategory, this.state.deviceToken, this.state.deviceType)
            })
        } else {
            this.onRegisterBussiness(this.state.latitude, this.state.longitude, this.state.email, this.state.firstname, this.state.lastname, this.state.hours, this.state.phonenumber, this.state.password, this.state.zipcode, this.state.businessName,
                this.state.businessAddress, this.state.businessLink, this.state.subscriptionType, this.state.businessDoc, this.state.businessImages, this.state.selectedCategory, this.state.deviceToken, this.state.deviceType)
        }

    }


    onRegisterBussiness(LATITUDE, LONGITUDE, EMAIL, FIRST_NAME, LAST_NAME, BUSINESS_HOURS, PHONE_NUMBER, PASSWORD, ZIP_CODE, BUSINESS_NAME,
        BUSINESS_ADDRESS, BUSINESS_LINK, BUSINESS_SUB, BUSINESS_DOC, BUSINESS_IMAGES, BUSINESS_TYPE, DEVICE_TOKEN, DEVICE_TYPE) {
        if (this.state.subscriptionType == '1') {
            if (BUSINESS_NAME == '', BUSINESS_ADDRESS == '', BUSINESS_LINK == '') {
                this.setState({ serverError: true, serverErrorText: 'Enter Details' })
            } else if (BUSINESS_NAME == '') {
                this.setState({ serverError: true, serverErrorText: 'Enter Business Name' })
            } else if (BUSINESS_ADDRESS == '') {
                this.setState({ serverError: true, serverErrorText: 'Enter Business Address' })
            }else if(!EMAIL_EXPRESSION_CHECK.test(this.state.email.trim().toLowerCase())){
                this.setState({ serverError: true, serverErrorText: 'Please enter a valid email' })
            }else if (BUSINESS_LINK == '') {
                this.setState({ serverError: true, serverErrorText: 'Enter Business Link' })
            } else if (BUSINESS_IMAGES.length == 0) {
                this.setState({ serverError: true, serverErrorText: 'Select atleast one business image' })
            } else if (BUSINESS_DOC.length == 0) {
                this.setState({ serverError: true, serverErrorText: 'Upload valid documents for verifications' })
            } else if (LATITUDE == '') {
                this.setState({ serverError: true, serverErrorText: 'Location not found' })
            } else {
                this.setState({ isloading: true })
                API.BUSINESS_REGISTER(LATITUDE, LONGITUDE, EMAIL, FIRST_NAME, LAST_NAME, BUSINESS_HOURS, PHONE_NUMBER, PASSWORD, ZIP_CODE, BUSINESS_NAME,
                    BUSINESS_ADDRESS, BUSINESS_LINK, BUSINESS_SUB, BUSINESS_DOC, BUSINESS_IMAGES, BUSINESS_TYPE, DEVICE_TOKEN, DEVICE_TYPE).then((response) => {
                        console.log('businesRegister', JSON.stringify(response))
                        if (response.status == 1) {
                            this.setState({ isloading: false }, () => {
                                AsyncStorage.setItem("user_info", JSON.stringify(response.data.business), () => { })
                                AsyncStorage.setItem("session", 'Business')
                                this.props.navigation.navigate('BusinessBottomtab')
                            })
                        } else if (response.status == 0) {
                            this.setState({ serverError: true, serverErrorText: response.message, isloading: false })
                        }
                        console.log('sub1', JSON.stringify(response))
                    })
            }
        } else {
            if (BUSINESS_NAME == '') {
                this.setState({ serverError: true, serverErrorText: 'Enter Business Name' })
            } else {
                this.setState({ isloading: true })
                API.BUSINESS_REGISTER(LATITUDE, LONGITUDE, EMAIL, FIRST_NAME, LAST_NAME, BUSINESS_HOURS, PHONE_NUMBER, PASSWORD, ZIP_CODE, BUSINESS_NAME,
                    BUSINESS_ADDRESS, BUSINESS_LINK, BUSINESS_SUB, BUSINESS_DOC, BUSINESS_IMAGES, BUSINESS_TYPE, DEVICE_TOKEN, DEVICE_TYPE).then((response) => {
                        if (response.status == 1) {
                            console.log('sbusinessRegisterub0', JSON.stringify(response))
                            this.setState({ isloading: false }, () => {
                                AsyncStorage.setItem("user_info", JSON.stringify(response.data.business), () => { })
                                AsyncStorage.setItem("session", 'Business')
                                this.props.navigation.navigate('BusinessBottomtab')
                            })
                        } else if (response.status == 0) {
                            this.setState({ serverError: true, serverErrorText: response.message, isloading: false })
                        }
                       // console.log('sub0', JSON.stringify(response))
                    })
            }
        }

    }

    onRegionChange(region) {
        this.setState({ latitude: region.latitude, longitude: region.longitude })
    }

    map = () => {
        let lat = parseFloat(this.state.latitude)
        let long = parseFloat(this.state.longitude)
        //let driverlat = parseFloat()
        //let driverlong = parseFloat()
        if (Platform.OS == 'ios') {
            if (this.state.latitude != '')
                return (

                    <MapView
                        style={styles.Maps}
                        mapType='standard'
                        //provider="google"
                        showsMyLocationButton={true}
                        ref={(ref) => this.map_refs = ref}
                       // initialRegion={this.state.region}
                        //region={this.state.region}
                        initialRegion={{
                            // latitude: 30.7089,
                            // longitude: 76.7018,
                            latitude: lat,
                            longitude: long,
                            latitudeDelta: 0.0922,
                            longitudeDelta: 0.0421,
                          }}
                        scrollEnabled={true}>
                        {!isNaN(this.state.latitude) && <Marker
                            coordinate={{
                                latitude: this.state.latitude,
                                longitude: this.state.longitude,
                            }}>
                            <Image resizeMode='contain' source={Map_Marker} style={{ height: 50, width: 50 }} />
                        </Marker>}
                        {/* {!isNaN(lat)&&
                        <Marker draggable coordinate={{ latitude: this.state.latitude, longitude: this.state.longitude }}
                            onDragEnd={(e) => this.onRegionChange(e.nativeEvent.coordinate)} />
                        } */}
                    </MapView>
                )
        } else {
            if (this.state.latitude != null)
                return (
                    <MapView
                        style={styles.Maps}
                        mapType='standard'
                        provider="google"
                        ref={(ref) => this.map_refs = ref}
                        showsMyLocationButton={true}

                        initialRegion={{
                            latitude: lat,
                            //latitude: 30.7089,
                            //longitude: 76.7018,
                            longitude: long,
                            latitudeDelta: 0.0922,
                            longitudeDelta: 0.0421,
                          }}
                        //scrollEnabled={false}>
                        scrollEnabled={true}>
                        {!isNaN(this.state.latitude) && <Marker
                            coordinate={{
                                latitude: this.state.latitude,
                                longitude: this.state.longitude,
                            }}>
                            <Image resizeMode='contain' source={Map_Marker} style={{ height: 50, width: 50 }} />
                        </Marker>}
                        {/* {!isNaN(lat)&&
                            <Marker draggable coordinate={{ latitude: this.state.latitude, longitude: this.state.longitude }} onDragEnd={(e) => this.onRegionChange(e.nativeEvent.coordinate)} />
                        } */}
                    </MapView>
                )
        }
    }

    picDocument() {
        if (this.state.businessDoc.length < 1) {

            this.documentPicker()

        } else {
            alert('Only one document file is allowed')
        }
    }

    async documentPicker() {
        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.allFiles],
            });
            this.setState({ isloading: true }, () => {
                this.docUpload(res)
            })
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                // User cancelled the picker, exit any dialogs or menus and move on
            } else {
                throw err;
            }
        }
    }

    setDate(date, index, type) {
        var i = 0
        for (i = 0; i <= choosenDate.length - 1; i++) {
            if (i == index) {
                if (type == 'open') {
                    choosenDate[i].open = date
                } else {
                    choosenDate[i].close = date
                }
            }
        }
        this.setState({ hours: choosenDate }, () => {
           // console.log(JSON.stringify(this.state.hours))
        })
    }

    showDatePicker() {
        var v = ''
        return (
            <View style={{ flex: 1, flexDirection: "column", position: 'absolute', height: '100%', width: '100%', alignItems: 'center', justifyContent: 'center' }}>
                <View style={{ flex: 1, backgroundColor: APP_BLACK, opacity: .5, height: '100%', width: '100%', justifyContent: 'center', position: 'absolute', alignSelf: 'center' }}>
                </View>
                <View style={{ height: 300, width: 300, alignSelf: 'center', position: 'absolute', backgroundColor: 'white', justifyContent: 'space-between', borderRadius: 10 }}>
                    <View style={{ height: 50, width: '100%', backgroundColor: APP_BLACK, borderTopRightRadius: 10, borderTopLeftRadius: 10 }}>
                        <Text style={{ fontFamily: FONT_MEDIUM, fontSize: 20, color: APP_WHITE, alignSelf: 'center', marginTop: 10, }}>OPEN TIME</Text>
                    </View>
                    <DatePicker
                        style={{ width: 200, backgroundColor: 'white', marginTop: 0, alignSelf: 'center' }}
                        date={this.state.date}
                        mode="time"
                        placeholder="select date"
                        format="YYYY-MM-DD"
                        // minDate="2016-05-01"
                        // maxDate="2016-06-01"
                        onDateChange={(time) => { v = time.toString().slice(16, 21) }}
                    />
                    {/* */}
                    <View style={{ width: '100%', height: 50, backgroundColor: 'transparent', alignSelf: 'center', justifyContent: 'flex-start', borderBottomRightRadius: 10, borderBottomLeftRadius: 10, flexDirection: 'row' }}>
                        <TouchableOpacity onPress={() => this.setState({ showDatePicker: false })}
                            style={{ width: '50%', height: "100%", backgroundColor: APP_GREY, alignSelf: 'center', justifyContent: 'center', borderBottomLeftRadius: 10, }}>
                            <Text style={{ color: APP_BLACK, alignSelf: 'center', fontFamily: FONT_SEMIBOLD, fontSize: 15 }}>CANCEL</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.setState({ showDatePicker: false }, () => {
                            if (v == '') {
                                var time = ''
                                time = new Date().toString().slice(16, 21)
                                this.setDate(time, this.state.indexSelected, 'open')
                            } else {
                                this.setDate(v, this.state.indexSelected, 'open')
                            }
                            this.setState({ showCloseDatePicker: true })
                        })}
                            style={{ width: '50%', height: "100%", backgroundColor: APP_BLACK, alignSelf: 'center', justifyContent: 'center', borderBottomRightRadius: 10 }}>
                            <Text style={{ color: APP_WHITE, alignSelf: 'center', fontFamily: FONT_SEMIBOLD, fontSize: 15 }}>SELECT</Text>
                        </TouchableOpacity>
                    </View>
                </View>

            </View>)
    }

    showCloseDatePicker() {
        var v = ''
        return (
            <View style={{ flex: 1, flexDirection: "column", position: 'absolute', height: '100%', width: '100%', alignItems: 'center', justifyContent: 'center' }}>
                <View style={{ flex: 1, backgroundColor: APP_BLACK, opacity: .5, height: '100%', width: '100%', justifyContent: 'center', position: 'absolute', alignSelf: 'center' }}>
                </View>
                <View style={{ height: 300, width: 300, alignSelf: 'center', position: 'absolute', backgroundColor: 'white', justifyContent: 'space-between', borderRadius: 10 }}>
                    <View style={{ height: 50, width: '100%', backgroundColor: APP_BLACK, borderTopRightRadius: 10, borderTopLeftRadius: 10 }}>
                        <Text style={{ fontFamily: FONT_MEDIUM, fontSize: 20, color: APP_WHITE, alignSelf: 'center', marginTop: 10, }}>CLOSE TIME</Text>
                    </View>
                    <DatePicker
                        style={{ width: 200, backgroundColor: 'white', marginTop: 0, alignSelf: 'center' }}
                        date={this.state.date}
                        mode="time"
                        placeholder="select date"
                        format="YYYY-MM-DD"
                        // minDate="2016-05-01"
                        // maxDate="2016-06-01"
                        onDateChange={(time) => { v = time.toString().slice(16, 21) }}
                    />

                    <View style={{ width: '100%', height: 50, backgroundColor: 'transparent', alignSelf: 'center', justifyContent: 'flex-start', borderBottomRightRadius: 10, borderBottomLeftRadius: 10, flexDirection: 'row' }}>
                        <TouchableOpacity onPress={() => this.setState({ showDatePicker: false })}
                            style={{ width: '50%', height: "100%", backgroundColor: APP_GREY, alignSelf: 'center', justifyContent: 'center', borderBottomLeftRadius: 10, }}>
                            <Text style={{ color: APP_BLACK, alignSelf: 'center', fontFamily: FONT_SEMIBOLD, fontSize: 15 }}>CANCEL</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.setState({ showCloseDatePicker: false, operationalHour: 'Selected Input Time', showHours: false, selectedHourId: 4 }, () => {
                            if (v == '') {
                                var time = ''
                                time = new Date().toString().slice(16, 21)
                                this.setDate(time, this.state.indexSelected, 'close')
                            } else {
                                this.setDate(v, this.state.indexSelected, 'close')
                            }
                        })}
                            style={{ width: '50%', height: "100%", backgroundColor: APP_BLACK, alignSelf: 'center', justifyContent: 'center', borderBottomRightRadius: 10 }}>
                            <Text style={{ color: APP_WHITE, alignSelf: 'center', fontFamily: FONT_SEMIBOLD, fontSize: 15 }}>SELECT</Text>
                        </TouchableOpacity>
                    </View>
                </View>

            </View>)
    }

    showDatePickerPopUp() {
        return (
            <Modal
                animationType="fade"
                transparent={true}
                visible={this.state.showDatePicker}
                onRequestClose={() => {
                    // Alert.alert('Modal has been closed.');
                }}>
                {this.showDatePicker()}
            </Modal>)
    }

    showDatePickerClosePopUp() {
        return (
            <Modal
                animationType="fade"
                transparent={true}
                visible={this.state.showCloseDatePicker}
                onRequestClose={() => {
                    // Alert.alert('Modal has been closed.');
                }}>
                {this.showCloseDatePicker()}
            </Modal>)
    }

    selectedHourFun = (value) => {
        if (value.id == 4) {
            this.setState({
                selectedHourId: value.id,
                showHours: true, showOperationalHour: false
            })

        }
        else {

            this.setState({
                selectedHourId: value.id,
                operationalHour: value.title,
                showOperationalHour: false
            })
        }

    }


    render() {
        return (
            <SafeAreaView forceInset={{ top: 'always' }} style={{ flex: 1, backgroundColor: APP_BLACK }}>
                <>
                    <StatusBar barStyle={'dark-content'}></StatusBar>
                    <OrientationLoadingOverlay
                        visible={this.state.isloading}
                        color="white"
                        indicatorSize="large"
                    />
                    <ImageBackground style={styles.background} source={SIGNUP_BG}>
                        <ScrollView keyboardShouldPersistTaps={'always'} style={styles.OptionsView}>
                            {/* <View style={{height:"100%",width:"100%"}}> */}
                            {/* <ScrollView> */}
                            <Text style={styles.welcomeText}>Register As Business</Text>
                            {/* <View style={styles.view}> */}
                            <View style={{ flex: 1 }}>

                                <View style={styles.feildBorder}>
                                    <TextInput ref={input => { this.textInput = input }}
                                        style={styles.textfeild}
                                        placeholder="Enter Business Name"
                                        keyboardType="default"
                                        selectionColor={APP_BLACK}
                                        placeholderTextColor={APP_BLACK}
                                        onChangeText={this.handleBusinessname}
                                    />
                                </View>

                                <View style={{
                                    width: width - 40,
                                    height: height < 896 ? 45 : 50,
                                    borderRadius: 30,
                                    borderWidth: 1,
                                    marginTop: 20,
                                    borderColor: APP_BLACK,
                                    alignSelf: 'center'
                                }}>
                                    <View style={{}}>
                                        <TouchableOpacity onPress={() => this.setState({ showCategory: !this.state.showCategory, showHours: false })} style={{ height: '100%' }}>
                                            <Text style={{ color: APP_BLACK, fontFamily: FONT_SEMIBOLD, paddingTop: 13, paddingLeft: 20 }}>{this.state.selectedCategoryName}</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                {/* {this.category()} */}
                                {this.showCategory()}
                                {/* address */}

                                {this.state.subscriptionType == '1' ?


                                    <View style={{
                                        flex: 1,
                                        width: width - 40,
                                        //  width: Platform.OS=="android"? 320 : 340, 
                                        justifyContent: 'center', alignSelf: 'center', marginTop: 20,
                                    }}>
                                        <GooglePlacesAutocomplete
                                            ref={ref => { this.placesRef = ref }}
                                            editable={true}
                                            clearButtonMode={true}
                                            placeholder='Address'
                                            minLength={2}
                                            autoFocus={false}
                                            returnKeyType={'search'}
                                            listViewDisplayed={false}
                                            fetchDetails={true}
                                            keyboardShouldPersistTaps={'always'}
                                            enablePoweredByContainer={false}
                                            listViewDisplayed={false}
                                            onPress={(data, details = null) => {
                                                console.log('data????????', details)

                                                let region = {
                                                    latitude: details.geometry.location.lat,
                                                    longitude: details.geometry.location.lng,
                                                    latitudeDelta: LATITUDE_DELTA,
                                                    longitudeDelta: LONGITUDE_DELTA
                                                }
                                                this.setState({
                                                    latitude: details.geometry.location.lat,
                                                    longitude: details.geometry.location.lng,
                                                    region: region,
                                                    businessAddress: data.description
                                                },()=>{
                                                    this.map_refs.animateToRegion({
                                                        latitude:this.state.latitude,
                                                         longitude:this.state.longitude,
                                                         latitudeDelta: 0.0922,
                                                         longitudeDelta: 0.0421
                                                     })
                                                })
                                                // this.onUserPinDragEnd(
                                                // {
                                                // latitude: details.geometry.location.lat,
                                                // longitude: details.geometry.location.lng
                                                // }, 'onPress'
                                                // )
                                            }}
                                            textInputProps={{
                                                placeholderTextColor: 'black',
                                                // onFocus: () => {
                                                // // console.log('hiiiiiii...............onFocus')
                                                // // this.setState({
                                                // // onFocus: {
                                                // // latitude: this.state.region.latitude,
                                                // // longitude: this.state.region.longitude
                                                // // }
                                                // // })
                                                // // this.onUserPinDragEnd(
                                                // // {
                                                // // latitude: 0,
                                                // // longitude: 0
                                                // // }, 'onFocus'
                                                // // )

                                                // },
                                                // onBlur: () => {
                                                // //console.log('onBlur///////////////')
                                                // this.setState({
                                                // scrollEnabled: true
                                                // })

                                                //},
                                                // onChangeText: (text) => onChange(text)
                                                onChangeText: (text) => {
                                                    console.log('onChangeText???', text)

                                                    if (text.length === 0) {
                                                        console.log('onChangeText??????????')
                                                        // this.onUserPinDragEnd(
                                                        // {
                                                        // latitude: 0,
                                                        // longitude: 0
                                                        // }, 'changeText'
                                                        // )
                                                    }
                                                }
                                            }}
                                            getDefaultValue={() => {
                                                if (this.state.region !== null && this.state.region !== undefined) {
                                                    return this.state.description;
                                                } // text input default value
                                                return '';
                                            }}

                                            query={{
                                                key: 'AIzaSyAYbSbQqDQm2u092NYdX9-CxcYyRDY6YSk',
                                                language: 'en',
                                            }}

                                            styles={{
                                                // containerTop: {
                                                // position: 'absolute',
                                                // top: 0,
                                                // left: 0,
                                                // right: 0,
                                                // alignItems: 'center',
                                                // justifyContent: 'flex-start',
                                                // },
                                                textInput: {
                                                    backgroundColor: 'transparent',
                                                    borderColor: 'black',
                                                    borderWidth: 1,
                                                    borderRadius: 30,
                                                    height: 50
                                                },

                                                horizontal: {
                                                    flexDirection: 'row',
                                                    justifyContent: 'space-around',
                                                },

                                                listView: {
                                                    // zIndex: 1000,
                                                    backgroundColor: 'transparent',
                                                    color: 'black', //To see where exactly the list is
                                                    //position: 'absolute',
                                                    //top:43
                                                },
                                                textInputContainer: {
                                                    // zIndex: 2000,
                                                    // backgroundColor: '',
                                                },
                                                description: {
                                                    fontWeight: 'bold',
                                                },
                                                predefinedPlacesDescription: {
                                                    // color: 'red'
                                                },

                                            }}

                                            currentLocation={true}
                                            // filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities
                                            debounce={100} // debounce the requests in ms. Set to 0 to remove debounce. By default 0ms.

                                        />
                                    </View>
                                    : null}


                                {/* {this.GoogleSearch()} */}

                                {/* category */}
                                {/* {this.category()}
                                {this.showCategory()} */}

                                <View style={styles.feildBorder1}>
                                    {/* <TouchableOpacity onPress={() => this.setState({ showHours: !this.state.showHours, showCategory: false })} style={{ height: '100%' }}> */}
                                    <TouchableOpacity onPress={() => this.setState({
                                        //  showHours: !this.state.showHours, showCategory: false
                                        showOperationalHour: true

                                    })} style={{ height: '100%' }}>

                                        <Text style={{ color: APP_BLACK, fontFamily: FONT_SEMIBOLD, paddingTop: 13, paddingLeft: 20 }}>{this.state.operationalHour}</Text>
                                    </TouchableOpacity>
                                </View>

                                {this.showHours()}


                                {this.state.showOperationalHour == true ?

                                    <View style={{ backgroundColor: 'white', width: 340, height: 130, alignSelf: "center", borderRadius:10,marginTop:7}}>

                                        <FlatList
                                            data={selectedHour}
                                            renderItem={({ item }) =>
                                                <View>
                                                    <TouchableOpacity onPress={() => this.selectedHourFun(item)}>
                                                        <View style={{ flexDirection: 'row', marginLeft: 20, marginTop: 10 }}>
                                                            <Image style={{ height: 20, width: 20, alignSelf: 'center', tintColor: APP_BLACK, marginLeft: 0 }} source={this.state.selectedHourId == item.id ? RADIO_ON : RADIO_OFF}></Image>

                                                            <View style={{ marginLeft: 10 }}>
                                                                <Text style={{ fontSize: 15 }} >{item.title}</Text>
                                                                <Text style={{ fontSize: 9, color: 'grey' }} >{item.subTitle}</Text>

                                                            </View>
                                                        </View>
                                                    </TouchableOpacity>
                            
                                                </View>
                                            }
                                        />
                                    </View> : null}


                                {/* websitelink */}
                                {this.websiteLink()}
                                <View style={styles.TextContainer}>
                                    <Text style={styles.TextHeader}>User Location</Text>
                                    <View style={styles.MapsContainer}>
                                        {this.map()}
                                    </View>
                                </View>
                                {/* {this.address()} */}

                                {/* uploadDoc */}
                                {this.uploadDocuments()}
                                {/* uploadImage */}
                                {this.uploadImage()}
                                <View style={[styles.TextContainer,{marginTop:0}]}>
                                    <Text style={styles.TextHeader}>Select Subscription Plan</Text>
                                    <View style={styles.selectSubscription}>
                                        <TouchableOpacity onPress={() => this.selectSubRadioFree()} style={{ width: '55%', height: '100%', backgroundColor: 'transparent', flexDirection: 'row' }}>
                                            <Image style={{ height: 25, width: 25, alignSelf: 'center', tintColor: APP_BLACK }} source={this.state.subscriptionIconFree}></Image>
                                            <Text style={{ fontFamily: FONT_BOLD, fontSize: 16, color: APP_BLACK, marginLeft: 10, marginTop: 0, alignSelf: 'center' }}>Free
                                                {/* <Text onPress={()=>alert('kk')} style={{ color: APP_GREY,fontFamily:FONT_MEDIUM,fontSize: 14,color:'dodgerblue'}}>Detail</Text> */}
                                            </Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={() => this.selectSubRadioPaid()} style={{ width: '50%', height: '100%', backgroundColor: 'transparent', flexDirection: 'row' }}>
                                            <Image style={{ height: 25, width: 25, alignSelf: 'center', tintColor: APP_BLACK }} source={this.state.subscriptionIconPaid}></Image>
                                            <Text style={{ fontFamily: FONT_BOLD, fontSize: 16, color: APP_BLACK, marginLeft: 10, marginTop: 0, alignSelf: 'center' }}>Paid($20)
                                                {/* <Text onPress={()=>alert('kk')} style={{ color: APP_GREY,fontFamily:FONT_MEDIUM,fontSize: 14,color:'dodgerblue'}}>Detail</Text> */}
                                            </Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>


                                {this.state.serverError ? <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center', height: 27, backgroundColor: 'transparent', marginTop: 10 }}>
                                    <Text style={{ alignSelf: 'center', color: "#dd4b39", fontFamily: FONT_SEMIBOLD, fontSize: 16, marginTop: 10 }}>{this.state.serverErrorText}</Text>
                                </View> : null}

                                <View style={styles.button}>
                                    <TouchableOpacity onPress={() => this.newAccount()} style={{ height: '100%' }}>
                                        <Text style={styles.buttonText}>{this.state.loginText}</Text>
                                    </TouchableOpacity>
                                </View>

                                <View style={{ flexDirection: 'row', alignSelf: 'center', marginBottom: 5 }}>
                                    <View style={{ borderBottomWidth: 1, marginLeft: 3, borderBottomColor: APP_BLACK }}><Text onPress={() => this.props.navigation.goBack()} style={styles.registerTextUnderline}>Back to Signup</Text></View>
                                </View>
                            </View>
                            {/* </View> */}
                        </ScrollView>
                    </ImageBackground>
                    {/* </ScrollView> */}
                    {this.showDatePickerPopUp()}
                    {this.showDatePickerClosePopUp()}
                    {/* </View> */}
                </>
            </SafeAreaView>
        )
    }
};

const styles = StyleSheet.create({
    background: {
        height: height,
        width: width,
        alignItems: 'center'
    },
    OptionsView: {
        height: height,
        width: width,
        marginBottom: height < 896 ? 50 : 100
        //backgroundColor:'transparent'
    },
    otherText: {
        fontSize: 14,
        fontFamily: FONT_MEDIUM,
        color: APP_WHITE
    },
    view: {
        width: "90%",
        height: "100%",
        alignSelf: 'center',
        backgroundColor: 'transparent',
        marginTop: height < 896 ? 35 : 45,
    },
    feildBorder: {
        width: width - 40,
        height: height < 896 ? 45 : 50,
        borderRadius: 30,
        borderWidth: 1,
        borderColor: APP_BLACK,
        //backgroundColor:APP_BLACK,
        alignSelf: 'center',
        marginTop: 20
    },
    feildBorder1: {
        width: width - 40,
        height: height < 896 ? 45 : 50,
        borderRadius: 30,
        borderWidth: 1,
        marginTop: 20,
        borderColor: APP_BLACK,
        alignSelf: 'center'
    },
    feildBorder3: {
        width: width - 40,
        height: height < 896 ? 45 : 50,
        borderRadius: 30,
        borderWidth: 1,
        marginTop: 15,
        borderColor: APP_BLACK,
        //backgroundColor:APP_BLACK,
        alignSelf: 'center',

    },


    feildBorder2: {
        width: '90%',
        marginTop: 60,
        // padding:10,
        // height:30,
        // borderRadius:30,
        // borderWidth:1,
        // marginTop:15,
        borderColor: APP_BLACK,
        position: "absolute",
        flex: 1,
        borderWidth: 1,
        borderRadius: 40,
        backgroundColor: 'red',
        // backgroundColor:'red',

        // marginTop:20
        //backgroundColor:APP_BLACK,
        alignSelf: 'center'
    },

    textfeild: {
        width: width - 80,
        height: height < 896 ? 42 : 47,
        fontSize: 13,
        fontFamily: FONT_SEMIBOLD,
        borderRadius: 30,
        alignSelf: 'center',
        color: APP_BLACK,

    },
    button: {
        width: width - 45,
        height: height < 896 ? 50 : 50,
        borderRadius: 30,
        borderWidth: 1,
        marginTop: 10,
        borderColor: APP_BLACK,
        backgroundColor: APP_BLACK,
        alignSelf: 'center'
    },
    buttonText: {
        fontSize: 15,
        fontFamily: FONT_SEMIBOLD,
        color: APP_WHITE,
        alignSelf: 'center',
        marginTop: height < 896 ? 13 : 13
    },
    welcomeText: {
        alignSelf: 'center',
        fontSize: height < 896 ? 20 : 24,
        fontFamily: FONT_MEDIUM,
        backgroundColor: 'transparent',
        color: APP_BLACK,
        marginTop: height < 896 ? '20%' : '20%'
    },

    registerTextUnderline: {
        color: APP_BLACK,
        fontSize: 14,
        marginTop: 35,
        fontFamily: FONT_SEMIBOLD,
        alignSelf: 'center',
    },
    TextContainer: {
        paddingHorizontal: 5,
        paddingVertical: 15,
        backgroundColor: 'transparent',
        margin: 20
    },
    TextHeader: {
        color: APP_BLACK,
        fontSize: 15,
        fontFamily: FONT_SEMIBOLD
    },
    MapsContainer: {
        width: '100%',
        height: 150,
        backgroundColor: APP_GREY,
        alignSelf: 'center',
        overflow: 'hidden',
        borderRadius: 10,
        //position:'absolute',
        marginTop: 10,
    },
    ImageContainer: {
        width: '100%',
        height: 80,
        backgroundColor: 'transparent',
        alignSelf: 'center',
        overflow: 'hidden',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        marginTop: 15,
    },
    selectSubscription: {
        width: '100%',
        height: 70,
        backgroundColor: 'transparent',
        alignSelf: 'center',
        overflow: 'hidden',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        marginTop: 0,
    },
    availableHours: {
        width: '100%',
        alignSelf: 'center',
        height: 190,
        marginTop: 10,
        borderRadius: 10,
        backgroundColor: APP_WHITE
    },
    availableCategories: {
        width: '100%',
        alignSelf: 'center',
        height: 200,
        marginTop: 10,
        backgroundColor: APP_WHITE,
        borderRadius: 10
    },
    Maps: { flex: 1 },

});

export default BusinessRegister;






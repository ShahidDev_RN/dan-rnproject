import React, {Component} from 'react';
import {View,
    Dimensions ,
    Text,
    StyleSheet,
    TouchableOpacity,
    Image,
    TextInput,
    BackHandler,
    Alert,
    SafeAreaView,
    Platform,
    ScrollView,
    KeyboardAvoidingView,StatusBar,
    ImageBackground} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
import {FONT_BLACK,FONT_BOLD,FONT_EXTRABOLD,FONT_EXTRALIGHT,FONT_LIGHT,FONT_SEMIBOLD,
    FONT_THIN,APP_WHITE,APP_BLACK,APP_FACEBOOK,BACK_ICON, FONT_REGULAR,FONT_MEDIUM,NOTIFICATION_ICON, APP_GREY} from '../../Constants/App_Constants.js'
import * as API from '../../Requests/WebService.js'
    let { width, height } = Dimensions.get('window')
    const ASPECT_RATIO = width / height
class ForgotPassword extends Component {
    constructor(props){
        super(props);
        this.state={
            email:'',
            api_token:'',
            serverError:false, 
            serverErrorText:'',
            serverErrorTextColor:'#dd4b39',
            userButtonColor:'#E2E2E2',
            businessButtonColor:APP_WHITE,
            userType:'User',
            isloading:false
        }  
      } 


    handleEmail=(text)=>{
        this.setState({email:(text),serverError:false})
    }
    

      forgot_password(){
        if(this.state.email==''|| !this.state.email.includes('.com') || !this.state.email.includes('@')){
            if(this.state.email != ''){
                if(!this.state.email.includes('.com')){
                    this.setState({serverError : true , serverErrorText: 'Email Format Not Correct'})
                }
                if(!this.state.email.includes('@')){
                    this.setState({serverError : true , serverErrorText: 'Email Format Not Correct'})
                }
             }else{
                this.setState({serverError : true , serverErrorText: 'Enter Email'});
             } 
        }else{
            this.setState({isloading:true})
            if(this.state.userType=='User'){
                API.FORGOT_PASSWORD(this.state.email.trim()).then((response)=>{
                    if(response.status==1){
                        Alert.alert(
                                "Success",
                                "Check your email for the reset password link",
                                [
                                { text: "OK", onPress: () => this.setState({isloading:false},()=>{
                                    this.props.navigation.navigate('Login')
                                }) }
                                ],
                                { cancelable: false }
                            );
                    }else if(response.status==0){
                        this.setState({serverError:true,serverErrorText:response.message,isloading:false})
                    }
                })
            }else{
                API.FORGOT_PASSWORD_BUSINESS(this.state.email.trim()).then((response)=>{
                    if(response.status==1){
                        Alert.alert(
                            "Success",
                            "Check your email for the reset password link",
                            [
                              { text: "OK", onPress: () => this.setState({isloading:false},()=>{
                                  this.props.navigation.navigate('Login')
                              }) }
                            ],
                            { cancelable: false }
                          );
                    }else if(response.status==0){
                        this.setState({serverError:true,serverErrorText:response.message,isloading:false})
                    }
                })
            }
            
        }
       
    }

    selectUser(){
        this.setState({userButtonColor:'#E2E2E2',businessButtonColor:APP_WHITE,userType:'User'})
    }
    selectBusiness(){
        this.setState({userButtonColor:APP_WHITE,businessButtonColor:'#E2E2E2',userType:'Business'})
    }

    render(){
        return(
            <SafeAreaView forceInset={{ top: 'always' }} style={{flex:1,backgroundColor:APP_WHITE}}>
                <StatusBar barStyle={'dark-content'}></StatusBar>
                <OrientationLoadingOverlay
                visible={this.state.isloading}
                color="white"
                indicatorSize="large"
                />
                <ScrollView keyboardShouldPersistTaps={'always'} style={styles.OptionsView}>
               <View style={styles.OptionsView}>
                    <View style={styles.selectButton}>
                        <TouchableOpacity onPress={()=>this.selectUser()} style={{height:'100%',width:'50%',backgroundColor:this.state.userButtonColor,borderRadius:30}}>
                            <Text style={styles.userText}>User</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={()=>this.selectBusiness()} style={{height:'100%',width:'50%',backgroundColor:this.state.businessButtonColor,borderRadius:30}}>
                            <Text style={styles.userText}>Business</Text>
                        </TouchableOpacity>
                    </View>
                   <View style={styles.textView}>
                        <Text style={styles.text}>Forgot Password?</Text>
                        {/* <Text style={styles.descriptionText}>Please enter your email to get a new password, you can change your password further from settings.</Text> */}
                   </View>
                   <View style={styles.view}>
                        <View style={styles.feildBorder}>
                            <TextInput  ref={input => { this.textInput = input }}
                                style={styles.textfeild}  
                                placeholder="Enter Email"
                                keyboardType="email-address"
                                selectionColor={APP_BLACK}
                                placeholderTextColor={APP_BLACK}
                                onChangeText = {this.handleEmail}
                                /> 
                        </View>
                        
                        {this.state.serverError?<View style={{width:'100%', justifyContent:'center', alignItems:'center',height:20,backgroundColor:'transparent',marginTop:20}}>
                        <Text style={{alignSelf:'center',color:this.state.serverErrorTextColor,fontFamily:FONT_SEMIBOLD,fontSize:16}}>{this.state.serverErrorText}</Text>
                        </View>:null}
                        <View style={styles.button}>
                            <TouchableOpacity onPress={()=>this.forgot_password()}
                                activeOpacity={0.5}
                                style={{height:'100%',width:'100%'}}>
                                <Text style={styles.buttonText}>Submit</Text>
                            </TouchableOpacity>
                        </View>   
                    </View>
                    <TouchableOpacity onPress={()=>this.props.navigation.navigate('AppLogin')}>
                        <View style={{flexDirection:'row',alignSelf:'center',}}>
                                <View style={{ borderBottomWidth:1,marginLeft:3,borderBottomColor:APP_BLACK}}>
                                    <Text  style={styles.registerTextUnderline}>Back to Login</Text>
                                    </View>
                        </View>
                        </TouchableOpacity>
                    
               </View>
               </ScrollView>
            </SafeAreaView>
        )
    }
};

const styles = StyleSheet.create({
    OptionsView:{
        height:height,
        width:width,
        backgroundColor:APP_WHITE
    },
   view:{
        width:"90%",
        height:"30%",
        alignSelf:'center',
        backgroundColor:APP_WHITE,
        marginTop:20
   },
   feildBorder:{
    width:width-50,
    height:53,
    borderRadius:30,
    borderWidth:0.8,
    marginTop:35,
    borderColor:APP_BLACK,
    backgroundColor:'transparent',
    alignSelf:'center'
},
    textView:{
        width:"90%",
        height:50,
        alignSelf:'center',
        alignItems:'center',
        backgroundColor:'transparent',
        marginTop:50
    },
    text:{
        fontSize:22,
        fontFamily:FONT_MEDIUM,
        color:APP_BLACK,
        marginTop:10
    },
    descriptionText:{
        fontSize:13,
        textAlign:'center',
        fontFamily:FONT_MEDIUM,
        color:APP_WHITE,
        marginTop:10
    },
    textfeild:{
        width:width-90,
        height:51,
        fontSize:14,
        fontFamily:FONT_REGULAR,
        borderRadius:30,
        alignSelf:'center',
        color:APP_BLACK,
        //fontWeight:'300',
        backgroundColor:'transparent'
        },
        button:{
            width:width-45,
            height:50,
            borderRadius:30,
            //borderWidth:1,
            marginTop:30,
            //borderColor:,
            backgroundColor:'#e3e5e8',
            alignSelf:'center',
            shadowOffset: {
                width: 0,
                height: 1
              },
              shadowRadius:1,
              shadowOpacity: 0.5,borderTopWidth:0,
              elevation:Platform.OS=='android'?5:0
        },
    selectButton:{
        width:width-45,
        height:55,
        borderRadius:30,
        marginTop:40,
        backgroundColor:APP_WHITE,
        alignSelf:'center',
        flexDirection:'row',
        shadowOffset: {
            width: 0,
            height: 1
          },
          shadowRadius:1,
          shadowOpacity: 0.5,borderTopWidth:0,
          elevation:Platform.OS=='android'?5:0
    },
    buttonText:{
        fontSize:15,
        fontFamily:FONT_SEMIBOLD,
        color:APP_BLACK,
        alignSelf:'center',
        marginTop:13
    },
    userText:{
        fontSize:18,
        fontFamily:FONT_MEDIUM,
        color:APP_BLACK,
        alignSelf:'center',
        marginTop:15
    },
    registerTextUnderline:{
        color:APP_BLACK,
        fontSize:14,
        marginTop:35,
        fontFamily:FONT_SEMIBOLD,
        alignSelf:'center',
    },
    otherText:{
        fontSize:13,
        fontFamily:FONT_BOLD,
        color:APP_WHITE
      },
      OptionsView: {
        height: height,
        width: width,
        marginBottom: height < 896 ? 50 : 100
        //backgroundColor:'transparent'
        },

});

export default ForgotPassword;

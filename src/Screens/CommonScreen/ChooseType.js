import React, {Component} from 'react';
import {View,
    Dimensions ,
    Text,
    StyleSheet,
    TouchableOpacity,
    Image,
    TextInput,
    BackHandler,
    Alert,
    SafeAreaView,
    Platform,
    ScrollView,
    KeyboardAvoidingView,StatusBar,
    ImageBackground} from 'react-native';

import AsyncStorage from '@react-native-community/async-storage';
import {FONT_BLACK,FONT_BOLD,FONT_EXTRABOLD,FONT_EXTRALIGHT,FONT_LIGHT,USER_ICON,BUSINESS_ICON,
    FONT_SEMIBOLD,FONT_THIN,APP_WHITE,APP_BLACK,APP_FACEBOOK,CHECK_BOX_CHECK,CHECK_BOX_UNCHECK, 
    FONT_REGULAR, FONT_MEDIUM,LOGIN} from '../../Constants/App_Constants.js'
    let { width, height } = Dimensions.get('window')
    const ASPECT_RATIO = width / height
class Choosetype extends Component {
    constructor(props){
        super(props);
        global.type=''
        this.state={
            type:'',
        }  
      } 

componentDidMount(){
    if(Platform.OS=='ios'){
        this.setState({type:'ios'})
    }else{
        this.setState({type:'android'})
    }
    this.setState({deviceToken:'dMHNX87sSuqqEqhtXeT4K:APA91bFxsGOzSiTtGbasYHeeKIt6FvgKKrjd0j1CDOBXtSL6ryKAIKdJ1HtDQJYUMOHbVL5KLBTvbild5XtOTG1yVSnFSXGZZI1uHsvla2eRivH5gtwwwaEsV33654g_VIi0PVPn4TKl'})
}
user(){
    this.props.navigation.navigate('Login')
    global.type='User'
}
bussiness(){
    this.props.navigation.navigate('Login')
    global.type='Bussiness'
}
    render(){
        return(
            <SafeAreaView forceInset={{ top: 'always' }} style={{flex:1,backgroundColor:APP_BLACK}}>
                <StatusBar barStyle={'light-content'}></StatusBar>
                <View style={{flex:1, flexDirection:'column',backgroundColor:'transparent'}}>
                    <TouchableOpacity onPress={()=>this.user()} activeOpacity={1} style={{flex:0.5,backgroundColor:APP_BLACK}}>
                        <Image style={{height:75,width:65,alignSelf:'center',marginTop:120}} source={USER_ICON}></Image>
                        <Text style={{alignSelf:'center',fontFamily:FONT_MEDIUM,color:APP_WHITE,fontSize:25,marginTop:20}}>User</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>this.bussiness()} activeOpacity={1} style={{flex:0.5,backgroundColor:APP_WHITE}}>
                        <Image style={{height:75,width:85,alignSelf:'center',marginTop:120}} source={BUSINESS_ICON}></Image>
                        <Text style={{alignSelf:'center',fontFamily:FONT_MEDIUM,color:APP_BLACK,fontSize:25,marginTop:20}}>Business</Text>
                    </TouchableOpacity>
                </View>     
            </SafeAreaView>
        )
    }
};

const styles = StyleSheet.create({
  background:{
      height:height,
      width:width,
      alignItems:'center'
  },
  otherText:{
    fontSize:13,
    fontFamily:FONT_BOLD,
    color:APP_WHITE
  },
  welcomeText:{
      alignSelf:'center',
      fontSize:28,
      fontFamily:FONT_MEDIUM,
      color:APP_WHITE,
      //backgroundColor:APP_WHITE,
      marginTop:height<896?'18%':'38%'
  },
  feildView:{
      width:width,
      height:450,
      backgroundColor:"transparent",
      marginTop:100
  },
  feildBorder:{
      width:width-40,
      height:55,
      borderRadius:30,
      borderWidth:1.5,
      borderColor:APP_WHITE,
      backgroundColor:'transparent',
      alignSelf:'center'
  },
  feildBorderPassword:{
    width:width-45,
    height:55,
    borderRadius:30,
    borderWidth:1.5,
    marginTop:17,
    borderColor:APP_WHITE,
    backgroundColor:'transparent',
    alignSelf:'center'
},
textfeild:{
    width:width-80,
    height:50,
    fontSize:13,
    fontFamily:FONT_SEMIBOLD,
    borderRadius:30,
    alignSelf:'center',
    color:APP_WHITE,
    //fontWeight:'300',
    },
forgetPassword:{
    width:width-45,
    height:40,
    backgroundColor:"transparent",
    alignSelf:'center',
    marginTop:20,
    flexDirection:'row',
    justifyContent:'space-between'
},
button:{
    width:width-45,
    height:55,
    borderRadius:30,
    borderWidth:1,
    marginTop:20,
    borderColor:APP_BLACK,
    backgroundColor:APP_WHITE,
    alignSelf:'center'
},
buttonText:{
    fontSize:18,
    fontFamily:FONT_SEMIBOLD,
    color:APP_BLACK,
    alignSelf:'center',
    marginTop:14
},
ORView:{
    width:360,
    height:45,
    alignItems:'center',
    backgroundColor:'transparent',
    alignSelf:'center'
},
ORText:{
    fontSize:12,
    fontWeight:'600',
    marginTop:15,
    color:APP_WHITE
},
facebookButton:{
    width:width-45,
    height:55,
    borderRadius:30,
    borderColor:APP_BLACK,
    backgroundColor:APP_FACEBOOK,
    alignSelf:'center'
},
facebookbuttonText:{
    fontSize:17,
    color:APP_WHITE,
    fontFamily:FONT_SEMIBOLD,
    alignSelf:'center',
    marginTop:15
},
registerText:{
    color:APP_WHITE,
    fontSize:15,
    marginTop:50,
    fontFamily:FONT_MEDIUM,
    alignSelf:'center',
},
registerTextUnderline:{
    color:APP_WHITE,
    fontSize:15,
    marginTop:50,
    
    fontFamily:FONT_SEMIBOLD,
    alignSelf:'center',
}
});

export default Choosetype;

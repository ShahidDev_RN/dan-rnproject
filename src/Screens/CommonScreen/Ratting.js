import React, { Component } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  Image,
  TextInput,
  Dimensions,
  View, TouchableOpacity, Platform, KeyboardAvoidingView,Keyboard,ScrollView
} from 'react-native';
// import { ScrollView } from 'react-native-gesture-handler';
import { Rating } from 'react-native-ratings';
// import Star from 'react-native-star-view'
import {
  APP_BLACK, APP_WHITE, APP_GREY, Rating_HEADER, ADD, ADD_RATTING, BACK_ICON,
  RESTAURANT,
  FONT_REGULAR,
  FONT_MEDIUM,
  FONT_SEMIBOLD,
  APP_BORDER,
} from '../../Constants/App_Constants.js';
import { Add_Reviews } from '../../Requests/WebService.js';
import * as API from '../../Requests/WebService.js'
import AsyncStorage from '@react-native-community/async-storage';

let { width, height } = Dimensions.get('window')
// jimcarry

class AddRating extends Component {

  constructor(props) {
    super(props);
    this.state = {
      token: AsyncStorage.getItem("user_token"),
      addReview: '',
      rating: 0,
      startRating: 0,
      keyboardAvoid: false,
      // setTextInputValue:'',
      businessuserId: this.props.navigation.getParam('businessId')
    }
    console.log(this.state.token, 'parseeeeeeee')
    console.log(this.state.businessId, 'busineesID')
  }


  header() {
    if (Platform.OS == 'ios') {
      return (
        <View style={{ height: 65, width: '100%', backgroundColor: APP_BLACK, justifyContent: 'flex-end', borderBottomColor: '#2d2e2d', borderBottomWidth: 3 }}>
          <View style={{ height: '80%', width: '90%', backgroundColor: 'transparent', alignSelf: 'center' }}>
            <View style={{ flexDirection: 'row', height: '100%', width: '45%', backgroundColor: 'transparent', alignSelf: 'flex-start' }}>
              <TouchableOpacity onPress={() => this.props.navigation.navigate('SingleDetail')}>
                <Image source={BACK_ICON} style={{ marginTop: 13, height: 17, width: 24 }}></Image>
              </TouchableOpacity>
              <Text style={{ color: 'white', fontSize: 18, marginTop: 10, fontFamily: FONT_MEDIUM, marginLeft: 13 }}>Rating</Text>
            </View>
          </View>
        </View>
      )
    } else {
      return (
        <View style={{ height: 65, width: '100%', backgroundColor: APP_BLACK, justifyContent: 'flex-end', borderBottomColor: '#2d2e2d', borderBottomWidth: 3 }}>
          <View style={{ height: '80%', width: '90%', backgroundColor: 'transparent', alignSelf: 'center' }}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', height: '100%', width: '48%', backgroundColor: 'transparent', alignSelf: 'flex-start' }}>
              <TouchableOpacity onPress={() => this.props.navigation.navigate('SingleDetail')}>
                <Image source={BACK_ICON} style={{ marginTop: 13, height: 17, width: 24 }}></Image>
              </TouchableOpacity>
              <Text style={{ color: 'white', fontSize: 18, marginTop: 10, fontFamily: FONT_MEDIUM, width: 150, marginLeft: 10 }}>Rating</Text>
            </View>
          </View>
        </View>
      )
    }
  }

  // getUserDetail(){
  //   AsyncStorage.getItem('user_token')
  //   let parseToken = AsyncStorage.getItem('user_token')
  //   console.log('parseToken'.parseToken)
  //   this.setState({
  //     token:parseToken
  //   })

  // }
  // componentDidMount(){
  // }


  ratingCompleted = (rating) => {
    console.log("Rating is: " + rating)
    this.setState({ rating: rating })
  }

  validation = async () => {
    if (this.state.addReview === "") {
      alert('Please add review')
    } else {
      await this.Reviews()
    }
  }

  Reviews = async () => {
    await API.Add_Reviews(this.state.businessuserId, this.state.rating, this.state.addReview, this.state.token).then((response) => {
      console.log('reviewssss', JSON.stringify(response))
      if (response.status == 1) {
        console.log("statussss111")
        this.props.navigation.navigate("SingleDetail")
        // alert(response.message)
      } else if (response.status == 0) {
        alert(response.message)
      }
      else {
        console.log('errrroooorr')
      }
    })
  }

  AddRating() {
    return (
      <SafeAreaView style={{
        backgroundColor: APP_BLACK,
        // backgroundColor:'white',
        flex: 1
        // height: "100%",
        // width: "100%"
      }}>

        {this.header()}

        {/* <KeyboardAvoidingView
          style={{ flex:1,borderWidth:2,borderColor:'green',}}
          behavior={Platform.OS === "ios" ? 'height': ''}   
          keyboardVerticalOffset={Platform.OS === "ios" ? 50:0}
          enabled
        > */}

        {/* <KeyboardAvoidingView style={{
                        height: height, backgroundColor: APP_BLACK,
                        justifyContent: 'center',
                    }}
                    behavior="padding" enabled={this.state.keyboardAvoid}
                    keyboardVerticalOffset={1}> */}
        {/* <ScrollView automaticallyAdjustContentInsets={true} contentContainerStyle={{ flexGrow: 1 }}> */}
        <ScrollView automaticallyAdjustContentInsets={true} contentContainerStyle={{ flexGrow: 1 }}>
        <KeyboardAvoidingView
          style={{ flex: 1, }}
          behavior={Platform.OS === "ios" ? 'position' : ''}
          keyboardVerticalOffset={Platform.OS === 'ios' ? -30 : 0}
          enabled
        >
          <View style={{ flex: 1}}>
            <Image
              source={ADD_RATTING}
              style={Styles.ImageBG}
            />
            <Text style={[Styles.Header]}>{Rating_HEADER}</Text>
            <View style={{ marginTop: 15,height:Platform.OS === 'ios'?10:40}}>
              <Rating
                ratingCount={5}
                type={'star'}
                startingValue={0}
                fractions={1}
                jumpValue={0.5}
                //showRating
                imageSize={20}
                // tintColor={'black'}
                tintColor={'black'}
                // selectedColor={AppStyles.colorSet.mainTextColor}
                onFinishRating={this.ratingCompleted}
              />
            </View>

            <Text style={Styles.FeedbackHeader}>Your Valuable Feedback</Text>


            {/* <View style={{ flexDirection: "row", justifyContent: "space-between", padding: 20}}> */}
              <View style={{flexDirection: "row",padding:20,}}>
                
              <TextInput
                style={{ fontSize: 16, color: "white",
                  width: 280,
                 height:40,marginLeft:5}}
                placeholder="Enter your feedback here"
                placeholderTextColor="grey"
                onChangeText={(text) => this.setState({ addReview: text })}
                multiline={true}
                value={this.state.addReview}
                onEndEditing={(event) => {
                  console.log(event.nativeEvent.text);
                }}></TextInput>
              
              <TouchableOpacity onPress={()=>{Keyboard.dismiss()}}>
                <Text style={{ color: "white", height:20,marginRight:10}}>Done</Text>
              </TouchableOpacity>
            </View>
            <View style={{borderBottomWidth:1.5,borderBottomColor:"grey",width:330,alignSelf:"center",}}></View>
            {/* <View style={{
            , width: width - 45, alignSelf: "center",
            }}>
            </View> */}



            <View style={Styles.button}>
              <TouchableOpacity onPress={() => this.validation()} style={{ height: '100%' }}>
                <Text style={Styles.buttonText}>Add</Text>
              </TouchableOpacity>
            </View>

          </View>
        {/* </ScrollView> */}
        </KeyboardAvoidingView>
        </ScrollView>
      </SafeAreaView>
      // <SafeAreaView style={Styles.SafeArea}>
      //   {this.header()}

      //   <KeyboardAvoidingView
      //     behavior="height">
      //     {/* behavior={Platform.OS === "ios" ? "padding" : "height"}> */}
      //     <ScrollView keyboardShouldPersistTaps={'never'} contentContainerStyle={{ flexGrow: 1, }}>
      //       <View style={{ flex: 1, marginTop:50 }}>
      // <Image
      //   source={ADD_RATTING}
      //   style={Styles.ImageBG}
      // />
      //         <Text style={Styles.Header}>{Rating_HEADER}</Text>
      //         <View style={{marginTop:15}}>
      //           <Rating
      //             ratingCount={5}
      //             type={'star'}
      //             startingValue={0}
      //             fractions={1}
      //             jumpValue={0.5}
      //             //showRating
      //             imageSize={20}
      //             tintColor={'black'}
      //             // selectedColor={AppStyles.colorSet.mainTextColor}
      //             onFinishRating={this.ratingCompleted}
      //           />
      //         </View>

      //         <Text style={Styles.FeedbackHeader}>Your Valuable Feedback</Text>

      //         <View style={{
      //           flexDirection: "row", justifyContent: "space-between", borderBottomWidth: 1.5, borderBottomColor: '#2d2e2d',
      //           margin: 10, width: width - 45, alignSelf: "center",
      //         }}>
      //           <TextInput
      //             style={{ fontSize: 16, color: "white", width: 280, }}
      //             placeholder="Enter your feedback here"
      //             placeholderTextColor="grey"
      //             onChangeText={(text) => this.setState({ addReview: text })}
      //             multiline={true}
      //             value={this.state.addReview}
      //             onEndEditing={(event) => {
      //               console.log(event.nativeEvent.text);
      //             }}></TextInput>

      //           <TouchableOpacity>
      //             <Text style={{ color: "white", marginTop: 10 }}>Done</Text>
      //           </TouchableOpacity>
      //         </View>



      //         <View style={Styles.button}>
      //           <TouchableOpacity onPress={() => this.validation()} style={{ height: '100%' }}>
      //             <Text style={Styles.buttonText}>Add</Text>
      //           </TouchableOpacity>
      //         </View>
      //         <View style={{ marginTop: 20 }}></View>
      //       </View>
      //     </ScrollView>
      //   </KeyboardAvoidingView>
      // </SafeAreaView>
    );
  };

  render() {
    return (
      <View style={{ flex: 1 }}>
          {/* {this.header()} */}
        
        {/* <KeyboardAvoidingView
          style={{ flex: 1, }}
          behavior={Platform.OS === "ios" ? 'position' : ''}
          keyboardVerticalOffset={Platform.OS === 'ios' ? -30 : 0}
          enabled
        > */}
         
          {this.AddRating()}
        {/* </KeyboardAvoidingView> */}
      </View>
    )
  }
};

const Styles = StyleSheet.create({
  // SafeArea: {
  //   flex: 1,
  //   backgroundColor: APP_BLACK,
  // },
  SafeArea: {
    backgroundColor: APP_BLACK,
    height: "100%",
    width: "100%"

  },
  ScrollContainer: {
    flexGrow: 1,
  },
  ImageBG: {
    alignSelf: 'center',
    // marginTop: 10,
  },
  Header: {
    color: APP_WHITE,
    textAlign: 'center',
    fontSize:Platform.OS === 'ios'?18: 18,
    marginTop:Platform.OS === 'ios'?0: 20,
    height:Platform.OS === 'ios'?40:40
  },
  RatingBar: {
    alignSelf: 'center',
    marginTop: 10,
    height:Platform.OS === 'ios'?20:0,
    width:Platform.OS === 'ios'?100:0
    // height:30,width:150
  },
  FeedbackHeader: {
    color: APP_WHITE,
    fontSize: 14,
    marginTop: 25,
    fontFamily: FONT_MEDIUM,
    marginHorizontal: 20,
  },
  Input: {
    color: APP_GREY,
    fontSize: 16,
    minHeight: 0,
    width: '100%',
    backgroundColor: APP_BLACK
  },
  button: {
    width: width - 45,
    height: 50,
    borderRadius: 30,
    borderWidth: 1,
    marginTop: 60,
    borderColor: APP_BLACK,
    backgroundColor: APP_WHITE,
    alignSelf: 'center',
  },
  buttonText: {
    fontSize: 15,
    fontFamily: FONT_SEMIBOLD,
    color: APP_BLACK,
    alignSelf: 'center',
    marginTop: 13
  },
  textInputView: {
    width: width - 45,
    minHeight: 40,
    marginTop: 10,
    borderBottomWidth: 1.5,
    borderBottomColor: '#2d2e2d',
    backgroundColor: APP_BLACK,
    alignSelf: 'center'
  }
});

export default AddRating;


{/* <View style={{
              width: width - 45,
              minHeight:60,
              // minHeight: 40,
              marginTop: 10,
              borderBottomWidth: 1.5,
              borderBottomColor: '#2d2e2d',
              backgroundColor: APP_BLACK,
              alignSelf: 'center'
            }}>
              <View style={{flexDirection:""}}>
              <TextInput
                style={{
                  color: APP_GREY,
                  fontSize: 16,
                  minHeight: 0,
                  width: '100%',
                  backgroundColor: APP_BLACK
                }}
                value={this.state.addReview}
                selectionColor={APP_WHITE}
                placeholder="Text check"
                placeholderTextColor="white"
                onEndEditing={(event) => {
                  console.log(event.nativeEvent.text);
                }}
                onChangeText={(text) => this.setState({ addReview: text })}
                multiline={true}
              />
              <Text style={{ color: "white", textAlign: "right" }}>Done</Text>
              </View>
            </View> */}
import React, { Component } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Image,
  StatusBar,
  Dimensions,
  ImageBackground,
  FlatList,
  TouchableOpacity
} from 'react-native';
import MapView,{Marker} from 'react-native-maps';
import * as API from '../../Requests/WebService.js'
import { Rating, AirbnbRating } from 'react-native-ratings';
import Icon from 'react-native-vector-icons/MaterialIcons';
import  Share  from 'react-native-share';
import AsyncStorage from '@react-native-community/async-storage';
import {
  APP_BLACK,
  APP_GREY,
  APP_WHITE,
  BUSINESS_NAME,
  ADDRESS,
  DISTANCE,
  DIRECTIONS,
  AVAILABLE_HOURS,
  WEBSITE_LINK,
  Rating_HEADER,
  RESTAURANT,
  FONT_REGULAR,
  FONT_MEDIUM,
  FONT_SEMIBOLD,
  FONT_BOLD,
  SHARE,
  BACK_ICON,
  FAVORITE_INACTIVE,
  APP_BORDER,
  FAVORITE_DETAIL,
  ADD_PROFILE,
  FAVOURITE_ACTIVE, FAVOURITE_INACTIVE,
  RATTING,
  Map_Marker
} from '../../Constants/App_Constants';

import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
const { width, height } = Dimensions.get('window')
const ASPECT_RATIO = width / height
const LATITUDE_DELTA = 0.005
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO

class SingleDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      singleData: '',
      image: '',
      isloading: true,
      liked: true,
      businessuserId: '',
      businessId: this.props.navigation.getParam('businessId'),
      userId: '',
      isFavourite: false,
      color:false,
      businessReviews:'',
      userImage:'',
      coordinates:'',
    }

    this.props.navigation.addListener(
      'didFocus',
      payload => {
          this.Get_Data()
         
          //logEvent('MarketPlaceScreen')
          // console.log('payload', 'mjfygmgjmftggj')
      })

  }
  touchLike() {
    this.setState(() => ({ liked: !this.state.liked }));
  }
  componentDidMount() {
    this.Get_Data()
    
 }
Get_Data(){
  AsyncStorage.getItem("user_info").then((value) => {
    var data = JSON.parse(value)
    console.log(data._id, 'oooo')
  
    // businessId:''
    API.GET_SINGLE_BUSINESS(data._id,this.state.businessId).then((response) => {
     // alert(JSON.stringify(response.data.getBusiness[0].businessReviews.length))
       console.log('BUsiness detail', JSON.stringify(response))
      if (response.status == 1) {
        console.log('success---.>>')
        let coord = [response.data.getBusiness[0].businessLocation.coordinates[0], response.data.getBusiness[0].businessLocation.coordinates[1]]
        this.setState({ singleData: response.data.getBusiness[0], businessReviews:response.data.getBusiness[0].businessReviews,
          userImage:response.data.getBusiness[0].businessImages[0].image,
          isFavourite: response.data.getBusiness[0].is_favourite,
        coordinates:coord},()=>{
          this.setState({isloading:false})
        this.map_refs.animateToRegion({
           latitude:response.data.getBusiness[0].businessLocation.coordinates[0],
           longitude:response.data.getBusiness[0].businessLocation.coordinates[1],
           latitudeDelta: 0.0922,
           longitudeDelta: 0.0421,  
        })
      })
      }
      else {
        console.log('erororo+++')
        this.setState({
          isloading:false
        })
      }
      //   this.setState({ singleData: response.data.getBusiness[0], isFavourite: response.dat.getBusiness[0].isFavourite ,isloading: false })
      //   if (response.data.getBusiness[0].businessImages[0].image == undefined) {
      //     this.setState({ image: response.data.getBusiness[0].businessImages[0], businessuserId: response.data.getBusiness[0]._id })
      //   } else {
      //     this.setState({ image: response.data.getBusiness[0].businessImages[0].image, businessuserId: response.data.getBusiness[0]._id })
      //   }
    })
  }).done();
}

  Get_BUSINESS_REVIEWS() {
    API.GET_REVIEWS(businessuserId).then((response) => {
      console.log("getttttttt", JSON.stringify(response))
      if (response.status == 1) {

      } else {
        console.log("errorrrgettt")
      }
    })
  }

  Favourite_List(value) {
    if (value) {
      // API.ADD_FAVOURITE(this.state.businessuserId).then((response) => {
      API.ADD_FAVOURITE(this.state.businessId).then((response) => {
        console.log("Wishlisttttttttt", JSON.stringify(response))
        console.log("Wishlisttttttttt", response)

        if (response.status == 1) {
          this.setState({ isFavourite: value})
          // alert('fine')
          // AsyncStorage.setItem("Storeeee",JSON.stringify(response.status.data.user.favouriteBy[0]),() => {
          //   console.log("Business list")
          // })
        } else if (response.status == 0) {
          console.log("errorrrrrrr")
        }
      })
    }
    else {
      API.DELETE_WISHLIST(this.state.businessId).then((response) => {
        console.log("Wishlisttttttttt", JSON.stringify(response))
        console.log("DELELETEWishlisttttttttt", response)

        if (response.status == 1) {
          this.setState({ isFavourite: value})
          // alert('good')

          // alert(response.message)
          // AsyncStorage.setItem("Storeeee",JSON.stringify(response.status.data.user.favouriteBy[0]),() => {
          //   console.log("Business list")
          // })
        } else if (response.status == 0) {
          console.log("errorrrrrrr")
        }
        else {
          console.log('erororBIG')
        }
      })
    }
  }

  map = () => {
    if(this.state.coordinates.length>0){
    if (Platform.OS == 'ios') {
      return (
        <MapView
          style={Styles.Maps}
          mapType='standard'
          //provider='google'
          ref={(ref) => this.map_refs = ref}
          initialRegion={{
            // latitude: 37.78825,
            // longitude: -122.4324,
            latitude: this.state.coordinates[0],
            longitude: this.state.coordinates[1],
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
          }}
          scrollEnabled={false}>
          <Marker coordinate={{ latitude:this.state.coordinates[0],longitude: this.state.coordinates[1], }}>
          <Image resizeMode='contain' source={Map_Marker} style={{ height: 50, width: 50 }} />
        </Marker>
        </MapView>
      )
    } else {
      return (
        <MapView
          style={Styles.Maps}
          mapType='standard'
          provider='google'
          ref={(ref) => this.map_refs = ref}
          initialRegion={{
            // latitude: 37.78825,
            // longitude: -122.4324,
            latitude: this.state.coordinates[0],
            longitude: this.state.coordinates[1],
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
            
          }}
          scrollEnabled={false}>
          <Marker coordinate={{ latitude:this.state.coordinates[0],longitude: this.state.coordinates[1], }}>
          <Image resizeMode='contain' source={Map_Marker} style={{ height: 50, width: 50 }} />
        </Marker>
        </MapView>
        // <View style={{ width: '100%', height: '100%', backgroundColor: 'grey' }}>
        //   <ImageBackground style={{ width: '100%', height: '100%' }} source={require("../../Assets/map.png")}></ImageBackground>
        // </View>
      )
    }
  }
  }

  shareScreen() {
    let options={
      title: "Business Screen",
      message:"Message",
      subject:"Subject",
      url:"URl"
    };
    
    Share.open(options)
      .then((res) => {
        console.log(res);
      })
      .catch((err)=> {
        err && console.log(err);
      })
    };

    ratingItemView(item, index){
      let url = 'https://apps.appsmaventech.com/onyx/'+item.userId.userImage
      console.log('url', item.userId.userImage)
      return(
          <View style={{ flexDirection: 'row', flex:1, paddingHorizontal:15,
            marginTop:15,}}>
          <Image
              source={{uri:url}}
              style={{
                width: 50,
                height: 50,
                borderRadius: 10,
              }}
            />
            <View>
            <View style={{flexDirection:"row", justifyContent:'center',width:300,flex:1,}}>
                <View style={{flex:0.5}}>
                  <Text style={{marginLeft:20}}>{item.userId.userFirstName} {item.userId.userLastName}</Text>
                </View>
                <View style={{flex:0.5}}>
                  <Rating
                      ratingCount={5}
                      type={'star'}
                      startingValue={item.rating}
                      fractions={1}
                      readonly
                      jumpValue={0.5}
                      //showRating
                      imageSize={20}
                      tintColor={APP_WHITE}
                      // selectedColor={AppStyles.colorSet.mainTextColor}
                    // onFinishRating={this.ratingCompleted}
                    />
                </View>
            </View>
            <View style={{flex:1}}>
              <Text style={Styles.ReviewText}>
                {item.reviews}
              </Text>
            </View>
            </View>
          </View>
        
      )
    }
  render() {
    return (
      <SafeAreaView style={Styles.SafeArea}>
        <ScrollView>
          <OrientationLoadingOverlay
            visible={this.state.isloading}
            color="white"
            indicatorSize="large"
          />
          <ImageBackground
            source={{ uri: this.state.userImage }}
            style={Styles.Image}>
            <View style={Styles.IconContainer}>
              <View>
                <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                  <Image source={BACK_ICON} style={Styles.Icons} />
                </TouchableOpacity>
              </View>

              <View style={{ flexDirection: "row", padding: 10 }}>

                <TouchableOpacity onPress={() => this.Favourite_List(!this.state.isFavourite)}>
                  <Image
                    style={{ height: 26, width: 30,}}
                    source={this.state.isFavourite ? FAVOURITE_ACTIVE : FAVOURITE_INACTIVE}></Image>
                    {/* source={this.state.isFavourite ?  FAVOURITE_INACTIVE:FAVOURITE_ACTIVE}></Image> */}
                </TouchableOpacity>


                <TouchableOpacity onPress={() => this.shareScreen()}>
                  <Image
                    style={{ height: 27, width: 30, marginLeft: 15, tintColor:"black"}}
                    source={require("../../Assets/share.png")}></Image>
                </TouchableOpacity>
              </View>
              {/* <Image
                source={FAVORITE_DETAIL}
                style={[
                  Styles.Icons,
                  {
                    position: 'absolute',
                    right: 65,
                  },
                ]}
              />
         
              <Image
                source={SHARE}
                style={[
                  Styles.Icons,
                  {
                    position: 'absolute',
                    right: 20,
                  },
                ]}
              /> */}

            </View>
          </ImageBackground>
          <View style={Styles.Border} />
          <View style={Styles.TextContainer}>
            <Text style={Styles.TextHeader}>{BUSINESS_NAME}</Text>
            <Text style={Styles.TextNormal}>{this.state.singleData.businessName}</Text>
          </View>
          <View style={Styles.Border} />
          <View style={Styles.TextContainer}>
            <Text style={Styles.TextHeader}>{ADDRESS}</Text>
            <Text style={Styles.TextNormal}>
              {this.state.singleData.businessAddress}
            </Text>
          </View>
          {/* <View style={Styles.Border} />
          <View style={Styles.TextContainer}>
            <Text style={Styles.TextHeader}>{DISTANCE}</Text>
            <Text style={Styles.TextNormal}>2KM Far</Text>
          </View> */}
          <View style={Styles.Border} />
          <View style={Styles.TextContainer}>
            <Text style={Styles.TextHeader}>{AVAILABLE_HOURS}</Text>
            <Text style={Styles.TextNormal}>24 Hours</Text>
          </View>
          <View style={Styles.Border} />
          <View style={Styles.TextContainer}>
            <Text style={Styles.TextHeader}>{DIRECTIONS}</Text>
            <View style={Styles.MapsContainer}>
              {this.map()}
            </View>
          </View>
          <View style={Styles.Border} />
          <View style={Styles.TextContainer}>
            <Text style={Styles.TextHeader}>{WEBSITE_LINK}</Text>
            <Text style={[Styles.TextNormal, { color: 'dodgerblue' }]}>
              {this.state.singleData.businesswebsiteLink}
            </Text>
          </View>
          <View style={Styles.Border}/>
          <View style={{ flexDirection: 'row', padding: 10, justifyContent: "space-between" }}>
            <Text style={Styles.TextHeader}>{Rating_HEADER}</Text>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('AddRating', { businessId: this.state.businessId })}>
              <Image
                source={require("../../Assets/add_rating.png")}></Image>
            </TouchableOpacity>
          </View>
        
            <FlatList
              data={this.state.businessReviews}
              style={{width:'100%', padding:10}}
              //inverted
              renderItem={({ item, index }) => (this.ratingItemView(item,index))} />

        </ScrollView>
      </SafeAreaView>

    )
  }
};

const Styles = StyleSheet.create({
  SafeArea: {
    flex: 1,
    backgroundColor: APP_WHITE,
  },
  Image: {
    width: Dimensions.get('window').width,
    height: 200,
    resizeMode: 'cover',
  },
  IconContainer: {
    paddingHorizontal: 20,
    marginTop: 10,
    flexDirection: 'row',
    justifyContent: "space-between"
  },
  Icons: {
    width: 25,
    height: 25,
    marginTop: 10,
    resizeMode: 'contain',
    tintColor: "black",
    backgroundColor: 'transparent'
  },
  AddIcon: {
    width: 20,
    height: 20,
    marginTop: 0,
    right: 10,
    resizeMode: 'contain',
    tintColor: APP_WHITE,
  },
  Border: {
    backgroundColor: '#e3e5e8',
    width: Dimensions.get('window').width,
    height: 1,
  },
  TextContainer: {
    paddingHorizontal: 10,
    paddingVertical: 20,
  },
  TextHeader: {
    //fontWeight: 'normal',
    color: APP_BLACK,
    fontSize: 14,
    fontFamily: FONT_MEDIUM
  },
  TextNormal: {
    color: APP_GREY,
    fontFamily: FONT_MEDIUM,
    fontSize: 14,
  },
  MapsContainer: {
    width: '100%',
    height: 160,
    alignSelf: 'center',
    overflow: 'hidden',
    borderRadius: 10,
    marginTop: 10,
  },
  Maps: { flex: 1 },
  Icon: {
    alignSelf: 'center',
    position: 'absolute',
    right: 20,
    color: APP_WHITE,
    fontSize: 25,
  },
  RatingContainer: {
    flex: 1,
    paddingVertical: 15,
    paddingStart: 20,
  },
  ReviewContainer: {
    flex: 1,
    flexDirection: 'column',
    marginStart: 20,
    marginEnd: 20,
  },
  ReviewName: {
    color: APP_BLACK,
    fontSize: 16,
    fontFamily: FONT_SEMIBOLD,
    marginLeft:15,
    flex:1
  },
  ReviewText: {
    color: APP_GREY,
    marginLeft:20,
    fontSize: 12,
    fontFamily: FONT_REGULAR,
    marginTop:5,
  },
  RatingBar: {
    flex: 1,
    position: 'absolute',
    right: 0,
    alignSelf: 'center',
  },
});
export default SingleDetail;















